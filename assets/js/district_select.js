$(document).ready(function() {
    $('#district-modal-map').click(function() {
        $('#modal-map-dialog').css('width', '90%');
        $('#district-tab-content').css('max-height', '65vh');
        $('#district-tab-content').css('height', '65vh');
    });
    $('#district-modal-list').click(function() {
        $('#modal-map-dialog').css('width', '70%');
        $('#district-tab-content').css('max-height', '55vh');
        $('#district-tab-content').css('height', '');
    });
    if (!$('#modalMap').length)
        console.log('ERROR: не инициализировано модальное окно с картой - #modalMap');
    if (!$('#tab-district-checkboxes').length)
        console.log('ERROR: не инициализирована вкладка для чекбоксов с районами - #tab-district-checkboxes');
    // Как только будет загружен API и готов DOM, выполняем инициализацию
    ymaps.ready(mapInit);
});

//массив выбранных районов
var selectedDistricts = new Array();
var selectedMicroDistrict = new Array();

var jsonMkr = new Array();
var jsonDistricts = new Array();

var inexDistr = 1;
var myMap;

function mapInit() {
    if ($('#address').length) {
        var suggestView1 = new ymaps.SuggestView('address');
    }
    if ($('#adress').length) {
        var suggestView1 = new ymaps.SuggestView('adress');
    }
    if ($('#searchAdress').length) {
        var suggestView1 = new ymaps.SuggestView('searchAdress');
    }
    myMap = new ymaps.Map("map", {
        center: [54.989342, 73.368212],
        zoom: 11
    });

    //Отключил зум при двойном клике
    myMap.behaviors.disable('dblClickZoom');
    //
    myMap.behaviors.enable('scrollZoom');
    $.ajax({
        type: "GET",
        url: "/mls/search/GetDistricts",
        success: function(result) {
            try {
                var json = JSON.parse(result);
                jsonMkr = json['micro-districts'];
                jsonDistricts = json['districts'];
                loadDistrictPolygons();
                loadDistrictCheckBox();
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                alert('Ошибка при загрузке районов: ' + (result ? result : 'Сервер не отвечает, попробуйте позднее'));
            }
        },
        error: function(result) {
            console.log('ERROR: ' + result);
        },
        timeout: 15 * 60 * 1000
    });
}

function loadDistrictCheckBox() {
    var html = '';
    jsonDistricts.forEach(function(district) {
		// на проде эти районы пустые 
			if(district["id"] == 37
			|| district["id"] == 38
			|| district["id"] == 39
			|| district["id"] == 34
			|| district["id"] == 32
			|| district["id"] == 41)
				return;
        html += '<div class="row adt_block">';
        html += '<div class="col-md-12	col-lg-12">';
        html += '<div class="row checkbox-row">';
        html += '<div class="col-md-4	col-lg-4">';
        html += '<label class="checkbox-inline"><input type="checkbox" class="checkbox" name="DistrictsArr[]" value="' + district["id"] + '"><span class="pseudocheckbox">' + district["name"] + '</span></label>';
        html += '</div>';
        inexDistr = 1;
		for (var i = 0; i < jsonMkr.length; i++) {
			mkr = jsonMkr[i];
            console.log('inexDistr: '+inexDistr+' mkr:'+mkr["name"])
			if (!mkr["id-district"] || mkr["id-district"] != district["id"])
                continue ;
            if (inexDistr == 0) {
                html += '<div class="row checkbox-row">';
                html += '<div class="col-md-4	col-lg-4"><br></div>';
                inexDistr++;
				i--;
                continue ;
            } else {
                html += '<div class="col-md-4	col-lg-4">';
                html += '<label class="checkbox-inline"><input type="checkbox" class="checkbox" name="MicroDistrictsArr[]" value="' + mkr["id"] + '"><span class="pseudocheckbox">' + mkr["name"] + '</span></label>';
                html += '</div>';
            }
            if (inexDistr == 2) {
                inexDistr = 0;
                html += '</div>';
                continue ;
            }
            inexDistr++;
		}
		html += '</div></div></div>';
    });
    $('#tab-district-checkboxes').html(html);
}

function loadDistrictPolygons() {
    var distrArray = jsonDistricts;
    distrArray.forEach(function(district) {
        if (district["coords"] === undefined || district["coords"].length == 0)
            return;
        var coords = new Array();
        district["coords"].forEach(function(coord) {
            var point = new Array(coord["lat"], coord["lon"]);
            coords.push(point);
        });
        var polygon = new ymaps.Polygon([coords], {
            id: district["id"],
            hintContent: district["name"]
        }, {
            strokeWidth: 2.5,
            strokeColor: 'rgba(50, 150, 200, 1)',
            fillColor: 'rgba(27, 125, 190, 0.3)'
        });

        var timeoutId;

        var callbackDblClick = function(e) {
            clearTimeout(timeoutId);
            var objectId = polygon.properties.get("id");
            loadMkrPolygons(objectId);

            polygon.options.set({
                strokeColor: 'rgba(0, 0, 0, 0.5)',
                fillColor: 'rgba(40, 200, 40, 0)'
            });
            polygon.events.remove('dblclick', callbackClick);
            polygon.events.remove('click', callbackDblClick);
            polygon.events.remove(['mouseenter', 'mouseleave'], callbackMouseEnterLeave);

            if (selectedDistricts.includes(objectId)) {
                var position = selectedDistricts.indexOf(objectId);
                selectedDistricts.splice(position, 1);
            }
        };

        var callbackClick = function(e) {
            if (!timeoutId) {
                timeoutId = setTimeout(function() {
                    var objectId = polygon.properties.get("id");
                    var isSelectedDistrict = selectedDistricts.includes(objectId);
                    if (!isSelectedDistrict) {
                        polygon.options.set({
                            fillColor: 'rgba(40, 200, 40, 0.3)'
                        });
                        selectedDistricts.push(objectId);
                        myMap.geoObjects.remove(polygon);
                        myMap.geoObjects.add(polygon);
                    } else {
                        polygon.options.set({
                            fillColor: 'rgba(27, 125, 190, 0.3)'
                        });
                        position = selectedDistricts.indexOf(objectId);
                        selectedDistricts.splice(position, 1);
                        myMap.geoObjects.remove(polygon);
                        myMap.geoObjects.add(polygon);
                    }

                    timeoutId = undefined;
                }, 100);
            }
            e.preventDefault();
        };

        polygon.events.add('click', callbackClick);
        polygon.events.add('dblclick', callbackDblClick);

        //

        var callbackMouseEnterLeave = function(e) {
            var objectId = polygon.properties.get("id");
            //Мудреныч
            //добавил флаг isSelectedDistrict, используется в условиях ниже
            var isSelectedDistrict = selectedDistricts.includes(objectId);
            var isSelected = selectedMicroDistrict.includes(objectId);
            var type = e.get('type');
            if (type == 'mouseenter') {
                if (isSelected || isSelectedDistrict) {
                    polygon.options.set({
                        fillColor: 'rgba(40, 200, 40, 0.3)'
                    });
                } else {
                    polygon.options.set({
                        fillColor: 'rgba(10, 80, 120, 0.3)'
                    });
                }
            } else {
                if (isSelected || isSelectedDistrict) {
                    polygon.options.set({
                        strokeWidth: 3.5,
                        fillColor: 'rgba(56, 255, 59, 0.5)'
                    });
                } else {
                    polygon.options.set({
                        strokeWidth: 2.5,
                        fillColor: 'rgba(27, 125, 190, 0.3)'
                    });
                }
            }
        };
        polygon.events.add(['mouseenter', 'mouseleave'], callbackMouseEnterLeave);

        myMap.geoObjects.add(polygon);
    });
}

function loadMkrPolygons(idDistrict) {
    var mkrArray = jsonMkr;
    mkrArray.forEach(function(mkr) {
        if (!mkr["id-district"] || mkr["id-district"] != idDistrict)
            return;
        if (mkr["coords"] === undefined || mkr["coords"].length == 0)
            return;

        var coords = new Array();
        mkr["coords"].forEach(function(coord) {
            var point = new Array(coord["lat"], coord["lon"]);
            coords.push(point);
        });
        var polygon = new ymaps.Polygon([coords], {
            id: mkr["id"],
            hintContent: mkr["name"]
        }, {
            strokeWidth: 2.5,
            strokeColor: 'rgba(50, 150, 200, 1)',
            fillColor: 'rgba(27, 125, 190, 0.3)'
        });

        polygon.events.add('click', function(e) {
            var objectId = polygon.properties.get("id");
            if (selectedMicroDistrict.includes(objectId)) {
                selectedMicroDistrict.splice(selectedMicroDistrict.indexOf(objectId), 1);
                polygon.options.set({
                    strokeWidth: 3.5,
                    fillColor: 'rgba(27, 125, 190, 0.3)'
                });
            } else {
                selectedMicroDistrict.push(objectId);
                polygon.options.set({
                    strokeWidth: 2.5,
                    fillColor: 'rgba(56, 255, 59, 0.5)'
                });
            }
        });

        polygon.events.add(['mouseenter', 'mouseleave'], function(e) {
            var objectId = polygon.properties.get("id");
            var isSelected = selectedMicroDistrict.includes(objectId);
            var type = e.get('type');
            if (type == 'mouseenter') {
                if (isSelected) {
                    polygon.options.set({
                        fillColor: 'rgba(40, 200, 40, 0.3)'
                    });
                } else {
                    polygon.options.set({
                        fillColor: 'rgba(10, 80, 120, 0.3)'
                    });
                }
            } else {
                if (isSelected) {
                    polygon.options.set({
                        strokeWidth: 3.5,
                        fillColor: 'rgba(56, 255, 59, 0.5)'
                    });
                } else {
                    polygon.options.set({
                        strokeWidth: 2.5,
                        fillColor: 'rgba(27, 125, 190, 0.3)'
                    });
                }
            }
        });

        myMap.geoObjects.add(polygon);
    });
}