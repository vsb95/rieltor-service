
$(document).on("click", ".delete-img", function() {
  $(this).parent().remove();    
});
$(document).on("click", ".rotate-img", function() {
  var container = $(this).parent().children('.image-container');
  var inputAngle = $(this).parent().children('.input-angle');
  if(container.hasClass('rotate90')){
    container.removeClass('rotate90');
    container.addClass('rotate180');
    inputAngle.val(180);
  }else if(container.hasClass('rotate180')){
    container.removeClass('rotate180');
    container.addClass('rotate270');
    inputAngle.val(270);
  }else if(container.hasClass('rotate270')){
    container.removeClass('rotate270');
    inputAngle.val(0);
  } else {
    container.addClass('rotate90');
    inputAngle.val(90);
  }
});
// ************************ Drag and drop ***************** //
let dropArea;
let uploadProgress = []
let progressBar;
var imagesList = new Object();
var countUnLoadedImages=0;
var totalLoadedImages=0;

function preventDefaults (e) {
    e.preventDefault()
    e.stopPropagation()
}

function highlight(e) {
    dropArea.classList.add('highlight')
}

function unhighlight(e) {
    dropArea.classList.remove('active')
}

function handleDrop(e) {
    var dt = e.dataTransfer
    var files = dt.files

    handleFiles(files)
}


function initializeProgress(numFiles) {
    $('#progress-bar-percent').css('display','initial');
    $('#progress-bar').css('display','initial');
    progressBar.value = 0
    uploadProgress = []

    for(let i = numFiles; i > 0; i--) {
        uploadProgress.push(0)
    }
}

function updateProgress(fileNumber, percent) {
    uploadProgress[fileNumber] = percent
    let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
    console.debug('update', fileNumber, percent, total);
    total = Math.round(total*100)/100;
    progressBar.value = total
    $('#progress-bar-percent').html(total+'%');
}

function handleFiles(files) {
    files = Array.from(files);
    initializeProgress(files.length)
    files.forEach(uploadFile)
    files.forEach(previewFile)
}

function previewFile(file) {
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = function() {
        //$('#gallery').append('<div class="delete-img" file="'+file.name+'"><img src="'+reader.result+'"><p>Удалить изображение</p></div>');
    }
}

function uploadFile(file, i) {
    imagesList[file.name] = "";
    var url = '/mls/add/upload'
    var xhr = new XMLHttpRequest()
    var formData = new FormData()
    xhr.open('POST', url, true)
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

    // Update progress (can be used to show progress indicator)
    xhr.upload.addEventListener("progress", function(e) {
        updateProgress(i, (e.loaded * 100.0 / e.total) || 100)
    })
    countUnLoadedImages++;
    xhr.addEventListener('readystatechange', function(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            countUnLoadedImages--;
            totalLoadedImages++;
            updateProgress(i, 100) // <- Add this
            try{
                var json = JSON.parse(xhr.response);
                if(json['status'] != "OK")
                {
                    console.log(json);
                    alert('Произошла ошибка: \n'+json["msg"]);
                    return;
                }
                imagesList[json['origin_name']] = json['new_name'];
                path = json['new_name'];
                $('#gallery').append('<li class="image-item">'
                + '<input type="hidden" class="input-src" name="ImagesList['+totalLoadedImages+'][src]" value="'+path+'"/>'
                + '<input type="hidden" class="input-angle" name="ImagesList['+totalLoadedImages+'][angle]" value="0"/>'
                + '<input type="hidden" class="input-order" name="ImagesList['+totalLoadedImages+'][order]" value="'+totalLoadedImages+'"/>'
                +'<div class="image-container"><img src="/'+path+'"></div>'
                +'<label><input type="radio" name="MainPhoto" value="'+path+'"/>Главное фото</label>'
                +'<div class="rotate-img fa-btn" value="'+path+'"><i class="fas fa-redo-alt"></i></div>'
                +'<div class="delete-img fa-btn" value="'+path+'"><i class="fas fa-trash"></i></div>'
                +'</li>');
            }catch(ex){
                console.log('response = '+xhr.response);
                console.log(ex);
                alert("Произошла ошибка при загрузке фото. Обратитесь в техподдержку");
            }
        }
        else if (xhr.readyState == 4 && xhr.status != 200) {
            countUnLoadedImages--;
            console.log('response = '+xhr.response);
            alert('Произошла ошибка. Повторите заного');
        }
    })

    //formData.append('type', 'ujpu6gyk')
    formData.append('file', file)
    xhr.send(formData)
}