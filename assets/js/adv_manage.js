
var ClickedDeleteAdvBtn;
$(document).on("click", ".delete-adv", function() {
    ClickedDeleteAdvBtn = $(this);
  var value=$(this).attr('adv');  
  $("#modalContent").html('<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-sx-12"><p>Вы уверены что хотите удалить это объявление?<br>Его нельзя будет восстановить</p></div>'
  + '<div class="col-lg-2 col-md-2"></div>'
  +'<div class="col-lg-4 col-md-4 col-sm-6 col-sx-12"><a class="btn btn-primary btn-md btn-send delete-adv-apply" adv="'+value+'">Да, я хочу удалить</a></div>'
  +'<div class="col-lg-4 col-md-4 col-sm-6 col-sx-12"><a class="btn btn-primary btn-md edit-btn"  onclick="closeModal()">Нет, не буду</a></div>'
  + '<div class="col-lg-2 col-md-2"></div></div>');
  openModal();  
});
$(document).on("click", ".delete-adv-apply", function() {
  var value=$(this).attr('adv');
    $.ajax({
      type: "GET",
      url: '/mls/cabinet/action?action=delete&id='+value,
      success: function (result) {
          console.log(result);    
          try {
              if (result == null) {
                  $("#modalContent").html('нет ответа от сервера');
                  openModal();
                  return;
              }
              if(result == "OK"){
                  var row = ClickedDeleteAdvBtn.closest('.row.search_result');
                  row.css("display",'none');   
                  closeModal();
              }           
              else{
                  $("#modalContent").html(result);
                  openModal();
              }             
          } catch (error) {
              console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
              console.log('result =  ' + result);
              $("#modalContent").html(result);
              openModal();
          }
      },
      error: function (result) {
          console.log('result =  ' + result);            
          try {
              if(result != null && result.statusText == "timeout"){
                  $("#modalContent").html("Время ожидания операции истекло. Повторите еще раз");
                  openModal();
                  return;
              }
              var json = JSON.parse(result);
              alert('error: '+JSON.stringify(json));
              document.body.appendChild(document.createTextNode(JSON.stringify(json)));
          } catch (error) {
              console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
              console.log('result =  ' + result);
              $("#modalContent").html('Ошибка: '+result);
              openModal();
          }
      },
      timeout: 15 * 60 * 1000
  });  
});