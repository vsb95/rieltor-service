
$(document).ready(function() {
  $(".modal").each( function(){
      $(this).wrap('<div class="overlay"></div>')
  }); 
});

function openModal(){	  
  $("#modal1").parents(".overlay").addClass("open");
  setTimeout( function(){
	$("#modal1").addClass("open");
  }, 350);
  
  $(document).on('click', function(e){
      var target = $(e.target);
      
      if ($(target).hasClass("overlay")){
          $(target).find(".modal-page").each( function(){
              $(this).removeClass("open");
          });
          setTimeout( function(){
              $(target).removeClass("open");
          }, 350);
      }      
  });  
}

function closeModal(){  	  
$("#modal1").removeClass("open");
  setTimeout( function(){ 
	$("#modal1").parents(".overlay").removeClass("open");
  }, 350);
}