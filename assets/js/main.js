$(document).ready(function() {
  $(document).on('click', '.set-favorite', function (e) {
    var frm = $(this).parent("form");
    var child = $(this).children("i");
    $.ajax({
        type: "POST",
        url: "/favorites/set/",
        data: frm.serialize(),
        success: function(data) {
            if(data == "added"){
              child.removeClass("far");
              child.addClass("fa");
            }else if(data == "deleted"){
              child.removeClass("fa");
              child.addClass("far");
            }else{
              $("#modalContent").html(data);
              openModal();
            }
        },
        error: function(result) {
            console.log(result);
            $("#modalContent").html('Ошибка: '+result);
            openModal();
        },
        timeout: 15 * 60 * 1000
    });
  });
});
