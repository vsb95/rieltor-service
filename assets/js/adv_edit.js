function FillEditForm(json) {
    var adv = null;
    try {
        adv = JSON.parse(json);
        if (adv['property']['IsNewFlat'] == true && adv['offer-type'] == "Sell")
            adv['property']['category'] += '_new';
        if (adv['property']['category'] == "HouseWithLot")
            adv['property']['category'] = 'house';
        if (adv['category'].toLowerCase() == 'newflat')
            adv['category'] = 'living';
        $('input:radio[name=OfferType][value=' + adv['offer-type'].toLowerCase() + ']')[0].checked = true;
        $('input:radio[name=RealtyType][value=' + adv['category'].toLowerCase() + ']')[0].checked = true;
        $('#objCategory-select').val(adv['property']['category'].toLowerCase());

        $('input:radio[name=OfferType]').prop('disabled', 'disabled');
        $('input:radio[name=RealtyType]').prop('disabled', 'disabled');
        $('#objCategory-select').prop('disabled', 'disabled');

        $("#realty-type-row-1").css('display', 'block');
        ShowRows();
        GetForm();
        // Переделать на ожидание ответа от аякса, а не таймаут
        setTimeout(() => {
            FillOffer(adv);
        }, 2000);
    } catch (err) {
        console.log(err);
        alert('Произошла ошибка: ' + err);
    }
}

function FillOffer(adv) {
    $.each(adv, function(index, value) {
        switch (index) {
            case 'id':
            case 'offer-type':
            case 'offer-status':
            case 'category':
            case 'commission':
            case 'agent_fee':
            case 'creation-date':
            case 'last-update-date':
            case 'sales-agent':
                break;
            case 'title':
                $('input[name=Title]').val(value);
                break;
            case 'sell-type':
                $('input:radio[name=SellType][value=' + value.toLowerCase() + ']')[0].checked = true;
                break;
            case 'is-on-prepayment':
                $('input[name=IsOnPrepayment]')[0].checked = true;
                break;
            case 'is-can-mortgage':
                $('input[name=IsCanMorgage]')[0].checked = true;
                break;
            case 'property':
                FillProperty(value);
                break;
            case 'commission-type':
                $('select[name=CommissionType]').val(value.toLowerCase());
                switch (value) {
                    case 'Fixed':
                        $('input[name=Commission]').val(adv['commission']);
                        $('input[name=Commission]').css('display', 'block');
                        $('input[name=Commission]').inputmask({ "mask": "999 999 руб" });
                        break;
                    case 'Percent':
                        $('input[name=Commission]').val(adv['agent_fee']);
                        $('input[name=Commission]').css('display', 'block');
                        $('input[name=Commission]').inputmask({ "mask": "99%" });
                        break;
                }
                break;
            case 'is-haggle':
                $('input:checkbox[name=IsCanHaggle]')[0].checked = true;
                break;
            case 'contract-type':
                $('select[name=ContractType]').val(value.toLowerCase());
                break;
            case 'price':
                $('input[name=Cost]').val(value);
                break;
            case 'rent-pledge':
                $('input[name=PledgeCost]').val(value);
                break;
            case 'utilities-type':
                $('select[name=UtilitiesType]').val(value.toLowerCase());
                break;
            case 'utilities-price':
                $('input[name=UtilitiesCost]').val(value);
                break;
            case 'prepay':
                if (value != 0) {
                    $('select[name=PrepayPeriod]').val(value);
                }
                break;
            case 'price-period':
                $('input:radio[name=RentPeriod][value=' + value.toLowerCase() + ']')[0].checked = true;
                break;
            default:
                console.log(index + ': ' + value);
                break;
        }
    });
}

function FillProperty(property) {
    if (!property) {
        alert('Не передано поле "property"');
        return;
    }

    $.each(property, function(index, value) {
        if (index == 'building') {
            FillBuilding(value);
            return;
        }
        switch (index) {
            case 'area-unit':
            case 'Toilet':
            case 'renovation':
            case 'category':
            case 'CreateDt':
                break;
            case 'cadastral-number':
                $('input[name=KadastrNumber]').val(value);
                break;
            case 'description':
                $('textarea[name=Description]').val(value.replace(/<br>/g, "\n"));
                break;
            case 'youtube':
                $('input[name=YoutubeUrl]').val(value);
                break;
            case 'room-type':
                $('select[name=PlainingRooms]').val(value.toLowerCase());
                break;
            case 'is-studio':
                $('select[name=PlainingRooms]').val('studia');
                break;
            case 'area-total':
                $('input[name=AreaSizeTotal]').val(value);
                break;
            case 'area-living':
                $('input[name=AreaSizeLiving]').val(value);
                break;
            case 'area-kitchen':
                $('input[name=AreaSizeKitchen]').val(value);
                break;
            case 'area-room':
                $('input[name=AreaSizeRoom]').val(value);
                break;
            case 'renovation-id':
                $('select[name=RenovationType]').val(value);
                break;
            case 'floor':
                $('input[name=Floor]').val(value);
                break;
            case 'count-rooms':
                if (value == 5) value = "5+";
                $('select[name=CountRooms]').val(value);
                break;
            case 'offered-rooms':
                if (value == 5) value = "5+";
                $('select[name=CountRentRooms]').val(value);
                break;
            case 'balcon-count':
                $('input:radio[name=CountBalcon][value=' + value + ']')[0].checked = true;
                break;
            case 'loggia-count':
                $('input:radio[name=CountLoggia][value=' + value + ']')[0].checked = true;
                break;
            case 'CountToilet':
                if(value > 0)
                    $('input:radio[name=CountToilet][value=' + value + ']')[0].checked = true;
                break;
            case 'CountBathroom':
                if(value > 0)
                    $('input:radio[name=CountToiletWithBathroom][value=' + value + ']')[0].checked = true;
                break;

            case 'furniture-fill':
                switch (value) {
                    case "Full":
                        $('select[name=FurnitureFill]').val('full');
                        break;
                    case "Part":
                        $('select[name=FurnitureFill]').val('part');
                        break;
                    case "OnRequest":
                        $('select[name=FurnitureFill]').val('on-request');
                        break;
                }

            case 'is-studio':
                $('select[name=PlainingRooms]').val('studia');
                break;

            case 'AirConditioner':
                $('input:checkbox[value=conditioner]')[0].checked = true;
                break;
            case 'Shower':
                $('input:checkbox[value=shower]')[0].checked = true;
                break;
            case 'Internet':
                $('input:checkbox[value=internet]')[0].checked = true;
                break;
            case 'Television':
                $('input:checkbox[value=tv]')[0].checked = true;
                break;
            case 'WashingMachine':
                $('input:checkbox[value=washing-machine]')[0].checked = true;
                break;
            case 'Dishwasher':
                $('input:checkbox[value=dishwasher]')[0].checked = true;
                break;
            case 'Refrigerator':
                $('input:checkbox[value=fridge]')[0].checked = true;
                break;
            case 'Bath':
                $('input:checkbox[value=bath]')[0].checked = true;
                break;

            case 'WithChildren':
                if (!value) $('input:radio[value=without-child]')[0].checked = true;
                break;
            case 'WithPets':
                if (!value) $('input:radio[value=without-animal]')[0].checked = true;
                break;

            case 'WaterSupply':
                $('input:checkbox[value=water]')[0].checked = true;
                break;
            case 'SewerageSupply':
                $('input:checkbox[value=sewerage]')[0].checked = true;
                break;
            case 'ElectricitySupply':
                $('input:checkbox[value=electricity]')[0].checked = true;
                break;
            case 'GasSupply':
                $('input:checkbox[value=gas]')[0].checked = true;
                break;
            case 'Pool':
                $('input:checkbox[value=pool]')[0].checked = true;
                break;
            case 'Sauna':
                $('input:checkbox[value=sauna]')[0].checked = true;
                break;
            case 'Phone':
                $('input:checkbox[value=phone]')[0].checked = true;
                break;
            case 'Outbuilding':
                $('input:checkbox[value=outbuilding]')[0].checked = true;
                break;
            case 'Foundation':
                $('input:checkbox[value=foundation]')[0].checked = true;
                break;
            case 'Garage':
                $('input:checkbox[value=garage]')[0].checked = true;
                break;

            case 'HeatingSupply':
                $('select[name=HeatingType]').val(value ? 1 : 0);
                break;
            case 'lot-area':
                $('input[name=LotAreaSize]').val(value);
                break;
            case 'lot-area-unit':
                switch (value) {
                    case 'Hectare':
                        value = "hectare";
                        break;
                    case 'SqMeter':
                        value = "meter";
                        break;
                    case 'OneHundred':
                        value = "hundred";
                        break;
                }
                $('select[name=LotAreaType]').val(value.toLowerCase());
                break;
            case 'lot-area-type':
                switch (value) {
                    case 'Person':
                        value = "izs";
                        break;
                    case 'Garden':
                        value = "garden";
                        break;
                }
                $('select[name=LotAreaState]').val(value);
                break;
            case 'toilet-type':
                $('input:radio[name=ToiletType][value=' + value.toLowerCase() + ']')[0].checked = true;
                break;
            case 'part-size':
                $('input[name=PartSize]').val(value);
                break;

            case 'ceiling-height':
                $('input[name=BuildingHeightFloor]').val(value);
                break;
            case 'flat-place-type':
                switch (value) {
                    case 'NotAngle':
                        value = 'not_angle';
                        break;
                    case 'Angle':
                        value = 'angle';
                        break;
                    default:
                        console.log('\tprop.' + index + ': ' + value);
                        break;
                }
                $('select[name=PlaceTypeInHouse]').val(value);
                break;
            case 'window-view':
                switch (value) {
                    case 1:
                        $('#yardWindow')[0].checked = true;
                        $('#streetWindow')[0].checked = false;
                        break;
                    case 2:
                        $('#yardWindow')[0].checked = false;
                        $('#streetWindow')[0].checked = true;
                        break;
                    case 3:
                        $('#yardWindow')[0].checked = true;
                        $('#streetWindow')[0].checked = true;
                        break;
                    default:
                        console.log('\tprop.' + index + ': ' + value);
                        break;
                }
                $('select[name=PlaceTypeInHouse]').val(value);
                break;
            case 'IsNewFlat':
                var control = $('input:radio[name=IsNewBuilding][value=' + (value ? 1 : 0) + ']')[0];
                if (control) control.checked = true;
                break;
            case 'images':
                $.each(value, function(i, path) {
                    $('#gallery').append('<li class="image-item">' +
                        '<input type="hidden" class="input-src"   name="ImagesList[' + i + '][src]" value="' + path + '"/>' +
                        '<input type="hidden" class="input-angle" name="ImagesList[' + i + '][angle]" value="0"/>' +
                        '<input type="hidden" class="input-order" name="ImagesList[' + i + '][order]" value="' + i + '"/>' +
                        '<div class="image-container"><img src="' + path + '"></div>' +
                        '<label><input type="radio" name="MainPhoto" value="' + path + '"/>Главное фото</label>' +
                        '<div class="rotate-img fa-btn" value="' + path + '"><i class="fas fa-redo-alt"></i></div>' +
                        '<div class="delete-img fa-btn" value="' + path + '"><i class="fas fa-trash"></i></div>' +
                        '</li>');
                });
                totalLoadedImages = value.length;
                break;
            default:
                console.log('\tprop.' + index + ': ' + value);
                break;
        }

    });
}

function FillBuilding(building) {
    if (!building) {
        alert('Не передано поле "building"');
        return;
    }
    $.each(building, function(index, value) {
        switch (index) {
            case 'Parking':
            case 'material':
                break;
            case 'series':
                $('input[name=BuildingSeria]').val(value);
                break;
            case 'floors-total':
                $('input[name=TotalFloor]').val(value);
                break;
            case 'built-year':
                $('input[name=BuildingYear]').val(value);
                break;
            case 'parking-type':
                $('select[name=BuildingParkingType]').val(value.toLowerCase());
                break;
            case 'material-id':
                $('select[name=BuildingMaterialType]').val(value);
                break;
            case 'is-has-lift':
                var control = $('input:radio[name=IsHasLift][value=' + (value ? 1 : 0) + ']')[0];
                if (control)
                    control.checked = true;
                break;
            case 'is-has-trash-tube':
                if (value) $('input[name=BuildingIsHasTrash]')[0].checked = true;
                break;
            case 'location':
                FillLocation(value);
                break;
            case 'Security':
                $('input:checkbox[value=security]')[0].checked = true;
                break;
                
            case 'count-passenger-lift':
                if(value > 0)
                    $('input:radio[name=BuildingCountPassengerElevator][value=' + value + ']')[0].checked = true;
                break;
            case 'count-weight-lift':
                if(value > 0)
                    $('input:radio[name=BuildingCountWeightElevator][value=' + value + ']')[0].checked = true;
                break;
            case '':
                break;
            default:
                console.log('\t\tbuilding.' + index + ': ' + value);
                break;
        }
    });

}

function FillLocation(location) {
    if (!location) {
        alert('Не передано поле "location"');
        return;
    }
    $.each(location, function(index, value) {
        switch (index) {
            case 'locality-name':
            case 'id-district':
            case 'id-micro-district':
            case 'microdistrict-name':
            case 'district-name':
            case 'coord':
            case 'region':
                break;
            case 'address':
                $('#address').val(value);
                break;
            case 'subdistrict-name':
                $('input[name=BuildingMicrodistrictName]').val(value);
                break;
            case 'orientir':
                $('input[name=BuildingOrientir]').val(value);
                break;
            case 'distance':
                $('input[name=DistanceOutCity]').val(value);
                break;
            default:
                console.log('\t\t\tloc.' + index + ': ' + value);
                break;
        }
    });

}