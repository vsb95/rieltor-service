
function InitImageUploadBoxes(){
    $('imageuploadbox').each(function(){
        var box = $(this);
        if(box.attr('is-init')) return;
        var action = box.attr('action');
        if(!action){
            box.attr('action', '/upload/photo');
            action = box.attr('action');
        }
        var isEnabledSelectMainImage = box.attr('enable-select-main');
        if(!isEnabledSelectMainImage){
            box.attr('enable-select-main', 'false');
            isEnabledSelectMainImage = box.attr('enable-select-main');
        }
        var name = box.attr('name');
        if(!name){
            box.attr('name', 'ImagesList');
            name = box.attr('name');
        }
        var text = box.html();
        console.log('== InitImageUploadBoxes ==\nname = '+name+'\naction = '+action+'\nisEnabledSelectMainImage = '+isEnabledSelectMainImage);
        
        box.html('<span>'+text+'</span>'
        +'<div style="width:100%">'
        +'    <div style="display: inline-block; margin-left:5px">'
        +'        <label class="button"><input type="file" class="input-img" multiple accept="image/*">Выберите</label>'
        +'    </div>'
        +'    <div class="files_input">фото или перетащите сюда</div>' 
        +'</div>'
        +'<progress class="progressbar" max=100 value=0></progress>'
        +'<p class="progressbar-percent"></p>'
        +'<ul class="image-upload-box-gallery" /></ul>');

        box.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
          })
          .on('dragover dragenter', function() {
            box.addClass('highlight');
          })
          .on('dragleave dragend drop', function() {
            box.removeClass('highlight');
          })
          .on('drop', function(e) {
            currentImageUploadBox = box;
            handleFiles(e.originalEvent.dataTransfer.files);
          });
        box.attr('is-init', true);
    });
}

$(document).on("click", ".delete-img", function() {
  $(this).parent().remove();    
});
$(document).on("click", ".rotate-img", function() {
  //$(this).parent().children('img').css('transform','rotate(' + 90 + 'deg)');
  var container = $(this).parent().children('.image-container');
  var inputAngle = $(this).parent().children('.input-angle');
  if(container.hasClass('rotate90')){
    container.removeClass('rotate90');
    container.addClass('rotate180');
    inputAngle.val(180);
  }else if(container.hasClass('rotate180')){
    container.removeClass('rotate180');
    container.addClass('rotate270');
    inputAngle.val(270);
  }else if(container.hasClass('rotate270')){
    container.removeClass('rotate270');
    inputAngle.val(0);
  } else {
    container.addClass('rotate90');
    inputAngle.val(90);
  }
});

$(".image-upload-box-gallery").sortable({
    update: function(event, ui){
        var position = ui.item.prevAll().length + 1;
        ui.item.children('input[class=input-order]').val(position);
    }
});

$(document).on("change", ".input-img", function() {
    var input = $(this);
    var files = input.prop('files');
    currentImageUploadBox = input.parent().parent().parent().parent();
    handleFiles(files);
  });
// ************************ Drag and drop ***************** //
var currentImageUploadBox;
var uploadProgress = []
var currentImageUploadProgressBar;
var imagesList = new Object();
var countUnLoadedImages=0;
var totalLoadedImages=0;

function initializeProgress(numFiles) {
    currentImageUploadBox.children('.progressbar-percent').css('display','initial');
    currentImageUploadBox.children('.progressbar').css('display','initial');
    currentImageUploadBox.children('.progressbar').val(0);
    uploadProgress = []

    for(let i = numFiles; i > 0; i--) {
        uploadProgress.push(0)
    }
}

function updateProgress(fileNumber, percent) {
    uploadProgress[fileNumber] = percent
    let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
    console.debug('update', fileNumber, percent, total);
    total = Math.round(total*100)/100;
    currentImageUploadBox.children('.progressbar').val(total);
    currentImageUploadBox.children('.progressbar-percent').html(total+'%');
}

function handleFiles(files) {
    files = Array.from(files);
    initializeProgress(files.length);
    files.forEach(uploadFile);
}

function uploadFile(file, i) {
    imagesList[file.name] = "";
    var url = currentImageUploadBox.attr('action');//'/mls/add/upload'
    var xhr = new XMLHttpRequest()
    var formData = new FormData()
    xhr.open('POST', url, true)
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

    // Update progress (can be used to show progress indicator)
    xhr.upload.addEventListener("progress", function(e) {
        updateProgress(i, (e.loaded * 100.0 / e.total) || 100)
    })
    countUnLoadedImages++;
    xhr.addEventListener('readystatechange', function(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            countUnLoadedImages--;
            totalLoadedImages++;
            updateProgress(i, 100) // <- Add this
            try{
                var json = JSON.parse(xhr.response);
                if(json['status'] != "OK")
                {
                    console.log(json);
                    alert('Произошла ошибка: \n'+json["msg"]);
                    return;
                }
                imagesList[json['origin_name']] = json['new_name'];
                path = json['new_name'];

                var imageName = currentImageUploadBox.attr('name')+'['+totalLoadedImages+']';
                var imageItem = '<li class="image-item">'
                + '<input type="hidden" class="input-src" name="'+imageName+'[src]" value="'+path+'"/>'
                + '<input type="hidden" class="input-angle" name="'+imageName+'[angle]" value="0"/>'
                + '<input type="hidden" class="input-order" name="'+imageName+'[order]" value="'+totalLoadedImages+'"/>'
                +'<div class="image-container"><img src="/'+path+'"></div>';
                if(currentImageUploadBox.attr('enable-select-main') == "true")
                    imageItem += '<label><input type="radio" name="MainPhoto" value="'+path+'"/>Главное фото</label>'
                imageItem += '<div class="rotate-img fa-btn" value="'+path+'"><i class="fas fa-redo-alt"></i></div>'
                imageItem += '<div class="delete-img fa-btn" value="'+path+'"><i class="fas fa-trash"></i></div>'
                imageItem += '</li>';
                currentImageUploadBox.children('.image-upload-box-gallery').append(imageItem);
            }catch(ex){                
                $("#modalContent").html(xhr.response);
                openModal();
                console.log('response = '+xhr.response);
                console.log(ex);
                alert("Произошла ошибка при загрузке фото. Обратитесь в техподдержку");
            }
        }
        else if (xhr.readyState == 4 && xhr.status != 200) {
            countUnLoadedImages--;
            console.log('response = '+xhr.response);
            alert('Произошла ошибка. Повторите заного');
        }
    })

    //formData.append('type', 'ujpu6gyk')
    formData.append('file', file)
    xhr.send(formData)
}
