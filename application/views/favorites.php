<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
.delete-favorite{
    position: absolute;
    top:0;
    right: -25px;
    padding : 2px 6px;
}
</style>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        <div class="row">    
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-md-push-8 col-lg-push-8">
                <div class="search_job"> 
                    <button id="clearAll" class="btn btn-primary btn-md btn-main">Очистить все</button>
                    <h4>Фильтрация</h4>
                    <form action="/favorites/get" method="POST" id="frm">
                        <p style="margin:0; padding:0; margin-top:5px;">Тип сделки</p>
                        <div class="btn-group">
                            <select name="deal_type" id="DealTypeSelect" style="border: 1px solid #b8b8b8;">
                                <option value="" selected>Все</option>
                                <option value="rent">Аренда</option>
                                <option value="sell">Продажа</option>
                            </select>
                        </div>
                        <p style="margin:0; padding:0; margin-top:5px;display:none;">Источник</p>
                        <div class="btn-group"  style="display:none;">
                            <select name="is_mls">
                                <option value="" selected>Все</option>
                                <option value="1">Из мультилистинга</option>
                                <option value="0">Из площадок</option>
                            </select>
                        </div>
				        <input type="submit" class="btn btn-primary btn-md" value="Показать">
                    </form>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-md-pull-4 col-lg-pull-4" id="searchResult">
                <p> Сначала выберите фильтр </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var dealType="";
$(document).ready(function() {    
    getAdv();  

    $("#clearAll").click(function() {
            $("#modalContent").html('<p> Вы точно хотите удалить все объекты '
            +(dealType == ''?"":dealType == "sell"?' типа "продажи"':' типа "аренда"')
            +' вашего списка избранного?</p>'
            +'<button id="delete-all-subfrm" class="btn btn-primary btn-md btn-main" style="width:80px;margin-right:15px;">ДА</button><button class="btn btn-primary btn-md btn-main" style="width:80px;margin-left:15px;" onclick="closeModal();">НЕТ</button>');
            openModal();        
    });  
});

$(document).on('click', '.delete-favorite', function (e) {
    var id = $(this).attr('id');
    console.log(id);
    var row = $(this).parent().parent();
    $.ajax({
        type: "POST",
        url: "/favorites/delete/",
        data: "id="+id,
        success: function(data) {
            if(data != "deleted"){                
                $("#modalContent").html(data);
                openModal();
            }
            if(row)
                row.css("display","none");
        },
        error: function(result) {
            console.log(result);
            $("#modalContent").html('Ошибка: '+result);
            openModal();
        },
        timeout: 15 * 60 * 1000
    });
});
$(document).on('click', '#delete-all-subfrm', function(e){
    $.ajax({
        type: "POST",
        url: "/favorites/deleteall",
        data: "deal_type="+dealType,
        success: function(data) {
            location.reload();
        },
        error: function(result) {
            console.log(result);
            $("#modalContent").html('Ошибка: '+result);
            openModal();
        },
		timeout: 5*60*1000
    });
});
$("#frm").submit(function(e) {
    dealType = $("#DealTypeSelect").val();
    getAdv();
    e.preventDefault();
});
function getAdv() {
    var $preloader = $('#page-preloader'),
        $spinner = $preloader.find('#spinner');
    $spinner.text("Загружаем объявления...");
    $spinner.fadeIn();
    $preloader.delay(100).fadeIn('slow');
    var form = $("#frm");
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        success: function (result) {
            try {
                $("#searchResult").html('');
                if (result == null) {
                    $spinner.fadeOut(1);
                    $preloader.fadeOut(1);
                    $("#modalContent").html('нет ответа от сервера');
                    openModal();
                    return;
                }
                var json = JSON.parse(result);
                if (json == null || json["status-type"] == null) {
                    $spinner.fadeOut(1);
                    $preloader.fadeOut(1);
                    $("#modalContent").html('нет ответа от сервера');
                    openModal();
                    return;
                }
                if (json["status-type"] != "OK") {
                    $spinner.fadeOut(1);
                    $preloader.fadeOut(1);
                    $("#modalContent").html(json["status"]);
                    openModal();
                    return;
                }
                var myNode = document.getElementById("searchResult");
                                
                if (json["html-pages"] != null) {
                    json["html-pages"].forEach(function (page) {
                        var newRow = document.createElement("div");
                        newRow.setAttribute("class", "row list-item");
                        newRow.innerHTML = page;
                        myNode.appendChild(newRow);
                    });
                }           
                $('.fotorama').fotorama();
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                $("#modalContent").html(result);
                openModal();
            }
            $spinner.fadeOut();
            $preloader.delay(50).fadeOut('slow');
        },
        error: function (result) {
            $spinner.fadeOut();
            $preloader.delay(50).fadeOut('slow');  
            console.log(result);            
            try {
                if(result != null && result.statusText == "timeout"){
                    $("#modalContent").html("Время ожидания операции истекло. Повторите еще раз");
                    openModal();
                    return;
                }
                var json = JSON.parse(result);
                alert('error: '+JSON.stringify(json));
                document.body.appendChild(document.createTextNode(JSON.stringify(json)));
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                $("#modalContent").html('Ошибка: '+result);
                openModal();
            }
        },
        timeout: 15 * 60 * 1000
    });
};

</script>