<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .modal-form p{
        text-align: left;
        margin: 5px 0 5px 0;
    }
    .modal-form input, .modal-form .btn-group, .modal-form .checkbox-inline{
        max-width:400px;
        float: left;
        margin-bottom:10px;
    }
</style>
<div id="content-for-modal" style="display:none;">
    <form method="POST" action="/blacklist/search" class="modal-form">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
            <p>Выберите раздел</p>
            <div class="btn-group" required>
                <select name="section">
                    <option value="owners">Собственники</option>
                    <option value="clients">Клиенты</option>
                    <option value="agents">Риэлторы</option>
                    <?php 
                        if($this->user_model->IsLoginedAsAdmin()){
                            echo '<option value="staff">Сотрудники</option>';
                        }
                    ?>
                </select>  
            </div>    
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
            <p>Объект внесения в Ч.С.</p>
            <input type="text" name="bad_ass" class="form-control" placeholder="Иванов Иван Иванович" required/>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
            <p>Причина внесения в черный список</p>
            <textarea class="form-control" rows="4" placeholder="Укажите по какой причине вносится в черный список"  name="description" required ></textarea> 
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
            <p>Загрузить фото, скриншот</p>
            <imageuploadbox name="images" ></imageuploadbox>    
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
            <p>Тут будет текст условия, описания</p>
            <label class="checkbox-inline"><input type="checkbox" class="checkbox cb-accept" name="accept" value="1" required><span class="pseudocheckbox">Мною прочитаны правила, и я с ними согласен</span></label>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">   
            <input type="submit" class="btn btn-primary btn-md submit-add-message disabled" value="Добавить"/>
        </div>
    </div>
    </form>
</div>
<form id="frm">
<div class="row round-block">    
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">   
        <div class="btn-group">
            <select id="selectType">
                <option value="" selected>Все</option>
                <option value="owners">Собственники</option>
                <option value="clients">Клиенты</option>
                <option value="agents">Риэлторы</option>
                <?php 
                    if($this->user_model->IsLoginedAsAdmin()){
                        echo '<option value="staff">Сотрудники</option>';
                    }
                ?>
                <option value="my">Мои сообщения</option>
            </select>  
        </div>    
    </div>   
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">   
        <input type="text" placeholder="Найти" name="phrase" class="form-control" id="searchPhrase">    
    </div>   
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">     
        <a class="btn btn-primary btn-md btn-send" style="margin-top:0;" id="add-message">+ Добавить сообщение</a>
    </div> 
</div>
</form>
<div class="row round-block" id="row_table" style="position: relative;">   
    <div class="spinner-content" id="table-spinner" ><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
        <table class="table table-striped table-hover table-dark">
            <thead>
                <tr>
                    <th>Агентство / Риэлтор</th>
                    <th>Раздел</th>
                    <th>Объект внесения в Ч.С.</th>
                    <th>Причина внесения в черный список, примечание</th>
                    <th>Дата</th>
                </tr>
            </thead>
            <tbody id="table_body">

            </tbody>
        </table>
    </div> 
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#selectType').on('change', function() {
            load($(this).val());
        });
        $('#add-message').on('click', function() {
            $("#modalContent").html($('#content-for-modal').html());
            InitImageUploadBoxes();
            openModal();  
        });
        $('#searchPhrase').on('keyup', function() {
            filtering();
        });
        $('#frm').on('submit', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation;  
        });

        function filtering(){
            var text = $('#searchPhrase').val().toLowerCase();
            $("#table_body tr").each(function(i){
                var row = $(this);
                var textRow = row.text().toLowerCase();                
                if(!text || textRow.includes(text)){
                    row.css('display','table-row'); 
                }else{
                    row.css('display','none');
                }
                
            });
        }
        function load(type){
            $("#table-spinner").css("display", 'block');
            $("#table_body").html("");
            $.ajax({
                type: "GET",
                url: '/blacklist/search',
                data: 'type='+type,
                success: function(result) {  
                    if(!result){
                        $("#table-spinner").css("display", 'none');      
                        return;
                    }
                    try {
                        var json= JSON.parse(result);
                        var innerHtml = "";
                        console.log(json);
                        $.each( json, function(key, value ) {
                            var user = value["org_name"]+'<br>'+value["user_name"]+'<br>Т. '+value["user_phone"];
                            var row = '<tr><td>'+ user +"</td>";
                            row+= '<td>'+ value["section"]+"</td>";
                            row+= '<td>'+ value["bad_ass"]+"</td>";
                            row+= '<td>'+ value["description"]+"</td>";
                            row+= '<td>'+ value["date"]+"</td>";      
                            row+= '<td>';
                            if(value["images"] > 0){
                                row += '<a class="btn-table-row show-images" id="'+value["id"]+'"><i class="fa fa-paperclip" aria-hidden="true"></i></a>';
                            }
                            row+= "</td>";                      
                            if($('#selectType').val() == 'my'){
                                row += '<td><a class="btn-table-row delete-black-list" id="'+value["id"]+'"><i class="fa fa-times" aria-hidden="true"></i></a></td>';
                            }
                            row +="</tr>";
                            innerHtml+= row;
                        });
                        $("#table_body").html(innerHtml);
                        filtering();
                    } catch (error) {
                        console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                        console.log('result =  ' + result);
                        alert('Ошибка: '+result);
                    }
                    $("#table-spinner").css("display", 'none'); 
                },     
                error: function(result) {
                    console.log(result);
                    $("#modalContent").html(result);
                    openModal();
                },           
                timeout: 5*60*1000
            });
        }      
        load('');  
    });
    $(document).on('click', '.show-images', function (e) {
        var id = $(this).attr('id');
        $.ajax({
            type: "GET",
            url: "/blacklist/show?id="+id,
            success: function(result) {
                if(!result){   
                    return;
                }
                try {
                    var json= JSON.parse(result);
                    var innerHtml = '<div style="margin:auto"><div class="fotorama" data-allowfullscreen="true" data-maxheight="80%" >';
                    console.log(json);
                    $.each( json, function(key, value ) {
                        var row = '<img src="'+value['image_url']+'"/>';
                        innerHtml+= row;
                    });
                    innerHtml+= '</div></div>';
                    $("#modalContent").html(innerHtml);
                    $('.fotorama').fotorama();
                    openModal();
                } catch (error) {
                    console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                    console.log('result =  ' + result);
                    alert('Ошибка: '+result);
                }
            },
			error: function(result) {
				console.log(result);
                $("#modalContent").html(result);
                openModal();
			},        
            timeout: 5*60*1000
        });
    });
    $(document).on('click', '.delete-black-list', function (e) { 
        var row = $(this).closest('tr');
        var id = $(this).attr('id');
        console.log(id);
        $.ajax({
            type: "GET",
            url: "/blacklist/delete?id="+id,
            success: function(result) {
                console.log(result);
                if(result == 'OK'){
                    row.css('display', 'none');
                }else{
                    $("#modalContent").html(result);
                    openModal();
                }
            },
			error: function(result) {
				console.log(result);
                $("#modalContent").html(result);
                openModal();
			},        
            timeout: 5*60*1000
        });
    });
    $(document).on('change', '.cb-accept', function (e) { 
       if($(this)[0].checked){
        $(this).closest('.row').find('.submit-add-message').removeClass('disabled');
       }else{
        $(this).closest('.row').find('.submit-add-message').addClass('disabled');
       }
    });
    $(document).on('submit', '.modal-form', function (e) {
	    e.preventDefault();
		e.stopImmediatePropagation;  
        $.ajax({
            type: "POST",
            url: "/blacklist/create",
            data: $(this).serialize(),
            success: function(result) {
                console.log(result);
                $("#modalContent").html(result);
                //closeModal();  
            },        
			error: function(result) {
				console.log(result);
                $("#modalContent").html(result);
                openModal();
			},        
            timeout: 5*60*1000
        });
    }); 
</script>