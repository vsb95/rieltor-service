<div style="height: 80px;"></div>
</div>
</div>
  </body>
</html>

<!-- VK Widget 
<script type="text/javascript" src="https://vk.com/js/api/openapi.js?160"></script>
<div id="vk_community_messages"></div>
<script type="text/javascript">
VK.Widgets.CommunityMessages("vk_community_messages", 171557172, {disableExpandChatSound: "1",tooltipButtonText: "Техподдержка", disableButtonTooltip : 0});
</script>
-->
<script type="text/javaScript">
    $(document).ready(function() {

        $(":input").inputmask();
        
        
        // get current URL path and assign 'active' class
        var pathname = window.location.pathname;
        $('.nav > li > a[href="'+pathname+'"]').parent().addClass('active-nav');
        var img = $('.nav > li > a[href="'+pathname+'"]').children('img');
        if(img.attr('src')){
          var new_src = img.attr('src').replace('.svg', '_activ.svg');
          img.attr('src', new_src)
        }
    });
    
    //$(window).on('load', function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.fadeOut();

    $(document).on('click', '.help-spoiler-trigger', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('#help-row').slideToggle(300);			
    });

    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
  //});
</script>
<?php if(!$this->user_model->IsGranted(555)){ ?>
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(51957308, "init", {
          id:51957308,
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          webvisor:true
    });
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/51957308" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<?php } ?>
<?php 
	if($this->user_model->IsGranted(555) || $this->user_model->IsLoginedAsSuperAdmin()){ ?>
    <script type="text/javaScript">
      $(document).ready(function() {
        OneSignal.push(function() {
          OneSignal.sendTags({
            userId: '<?php echo $_SESSION['id_user']; ?>',
            user_type: '<?php echo $this->user_model->IsGranted(555)? 'support': $this->user_model->IsLoginedAsAdmin() ?'admin': 'user'; ?>'
          }).then(function(tagsSent) {
            // Callback called when tags have finished sending
            console.log(tagsSent);   
          });
        });
        OneSignal.push(function() {
          OneSignal.on('subscriptionChange', function(isSubscribed) {
            if (isSubscribed) {
              // The user is subscribed
              //   Either the user subscribed for the first time
              //   Or the user was subscribed -> unsubscribed -> subscribed
              OneSignal.getUserId( function(userId) {
                // Make a POST call to your server with the user ID
                $.ajax({
                  type: "POST",
                  url: '/notification/linkuser?onesignal_id='+userId,
                  success: function(data)
                  {
                    console.log(data);
                  },
                  error: function(result) {
                    console.log(result);
                  },
                  timeout: 15*60*1000
                });
              });
            }
          });
        });
      });
    </script>    
  <?php }	?>
<script src="/assets/js/modal.js"></script>
<script src="/assets/js/main.js"></script>
<script src="/assets/js/adv_manage.js"></script>
<script src="/assets/js/scroll.js"></script>
<script src="/assets/js/spoiler.js"></script>
<script src="/assets/js/jquery.inputmask.bundle.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- Bitrix -->
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b10943278/crm/site_button/loader_1_gef8fq.js');
</script>
</body>
</html>