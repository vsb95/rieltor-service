<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('1_header_new', array('title' =>'Помощь', 'styles' =>array("/assets/css/home.css")));
echo '<div class="row" style="margin-bottom:250px">';
echo '<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">';
echo '<div class="blog">
<div class="blog-post">Cвоих сотрудников в МЛС вы можете пригласить в личном кабинете администратора выслав приглашение на почту</div>
<ul>';
foreach($videos as $video){ ?>
    <a id="<?php echo $video['anchor'];?>"></a>
<li>
    <div class="blog-post">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <iframe width="100%"  height="350px" src="<?php echo $video['url'];?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="post-title"> <?php echo $video['title'];?></div>
                <div class="post-content"> <?php echo $video['description'];?></div>
            </div>
        </div>
    </div>
</li>
<?php }

echo '</ul></div>';
 $this->load->view('1_footer_new'); ?>