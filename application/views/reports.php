<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row round-block">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php
        if(isset($msg) && !empty($msg)){
            echo "<h4>$msg</h4>";
        }
    ?>
        <table class="table table-striped table-hover table-dark" id="grid">
            <thead>
                <tr>
                    <th scope="col">Отчет</th>
                    <th scope="col">Дата создания</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Имя клиента</th>
                    <th scope="col">Телефон клиента</th>
                    <th scope="col" colspan="2">Обновить</th>
                    <th scope="col">Поделиться</th>
                </tr>
            </thead>
            <tbody>
    <?php
        if(isset($reports) && !empty($reports)){
            foreach($reports as $report){
                //var_dump($report);
                echo '<tr>';
                    echo '<td scope="row"><a target="_blanc" href="/page/show/'.$report["id"].'?itsmy=1">'.$report["id"].'</a></td>';
                    echo '<td>'.$report["create_date"].'</td>';
                    echo '<td>'.($report["is_public"]==1?'Публичный': 'Приватный - '.($report["is_open"]==1?'Открыт':'Не открыт')).'</td>';
                    echo '<td>'.(!empty($report["client_name"]) ? $report["client_name"] : '').'</td>';
                    echo '<td>'.(!empty($report["client_phone"]) ? '<a href="tel:'.$report["client_phone"].'">'.$report["client_phone"].'</a>' : '').'</td>';
                    echo '<td><a target="_blanc" href='.($this->user_model->IsGranted(2)?'"/reports/refresh/'.$report["id"].'"':'"#" class="ungranted"').'>По дате</a></td>';
                    if(isset($report["filter_fields"]) && !empty($report["filter_fields"]))
                        echo '<td><a href='.($this->user_model->IsGranted(2)?'"/reports/recreate/'.$report["id"].'"':'"#" class="ungranted"').'>Новый по фильтру</a></td>';
                    else
                        echo '<td></td>';
                    echo '<td><div class="ya-share2" data-services="vkontakte,whatsapp,viber" data-title="Подборка для вас" data-url="'.
                    SERVER_DOMAIN.'/page/show/'.$report["id"].'" data-description="'.$report["description"].'"></div></td>';
                echo '</tr>';
            }
        }
    ?>
        </tbody>
    </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
  
    });
</script>

<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async defer></script>
<script src="//yastatic.net/share2/share.js" async defer></script>