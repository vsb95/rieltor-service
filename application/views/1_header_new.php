<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="referrer" content="no-referrer" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title><?php echo $title;?></title>
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="<?php echo $title;?>" />
    <meta property="og:description"        content="Пожалуй, самый лучший сервис для риэлторов" />
    <meta property="og:image"              content="/favicon.ico" />
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    
    <link href="/assets/css/style_main_new.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="/assets/css/modal.css" rel="stylesheet">
    <link href="/assets/css/preloader.css" rel="stylesheet">
    <link href="/assets/css/spinner.css" rel="stylesheet">
    <link href="/assets/fonts/TTNorms/stylesheet.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/assets/js/jquery_1.12.4.min.js"></script>
    <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery.min.js"><\/script>')</script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php    
    if(isset($scripts) && !empty($scripts)){
      foreach($scripts as $script){
        echo "<script src=\"".$script."\"></script>";
      }
    }
    if(isset($styles) && !empty($styles)){      
      foreach($styles as $style){
        echo "<link href=\"".$style."\" rel=\"stylesheet\">";
      }
    }      
	if($this->user_model->IsGranted(555) || $this->user_model->IsLoginedAsSuperAdmin()){
		echo '<link rel="manifest" href="/manifest.json" /><script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script><script>  var OneSignal = window.OneSignal || [];  OneSignal.push(function() {    OneSignal.init({      appId: "6c07ad52-55aa-493b-9083-03920ae81274",    });  });</script>';
	}
	?>
  </head>
  <body>  
  <div class='overlay overlay_ noprint'>
      <div class='modal-page' id='modal1'>
        <div class='content' id='modalContent'>	        
        </div>  
      </div> 
    </div> 
  <div id="page-preloader" style="display: none;"><span id="spinner">Загрузка...</span></div>

<div class="row container-fluid">
  <?php if($this->user_model->IsLogined()) {  $this->user_log_model->log( "open '".$title."'", 1);	?>
    <div class="col-md-4	col-lg-3 col-sm-4 col-xs-4" id="navbar">
      <ul class="nav nav-tabs nav-stacked">
        <li class="logo"><a href="/home" class="navbar-brand"><img  src="/assets/img/logo.png" alter="Главная"/></a></li>    
        <li><a href="/mls/search"><img src="/assets/img/menu/ic_1.svg">Мультилистинг</a></li>
        <li><a href=<?php echo ($this->user_model->IsGranted(1) || $this->user_model->IsGranted(2)?'"/board"':'"#" class="ungranted"'); ?>><img src="/assets/img/menu/ic_2.svg">Объявления</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" 
            data-toggle="dropdown" role="button" 
            aria-haspopup="true" aria-expanded="false"><img src="/assets/img/menu/ic_3.svg">Внешние источники<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href= <?php echo ($this->user_model->IsGranted(2)?'"/work"':'"#" class="ungranted"'); ?>>Сформировать отчет</a></li>
            <li><a href= <?php echo ($this->user_model->IsGranted(2)?'"/work/cutter"':'"#" class="ungranted"'); ?>>Скачать фото</a></li>     
          </ul>
        </li>
        <?php if($this->user_model->IsLoginedAsAdmin()){ ?>
          <li><a href="/mls/feed"><img src="/assets/img/menu/ic_4.svg">Автоматическая загрузка</a></li>
        <?php } ?>
        <?php if($this->user_model->IsGranted(555)){ ?>
          <li><a href="#"><img src="/assets/img/menu/ic_5.svg">Новостройки</a></li>
        <?php } ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="/assets/img/menu/ic_6.svg">Ипотека<span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="/credit/NewRequest">Создать заявку</a></li>
            <li><a href="/credit/ListRequest">Мои заявки</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/credit">Ставки</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/evaluation/promo">Оценка для ипотеки</a></li>
          </ul>
        </li>        
        <li><a href="/demand/search"><img src="/assets/img/menu/ic_7.svg">Спрос</a></li>
        <li><a href="/evaluation"><img src="/assets/img/menu/ic_9.svg">Оценка</a></li>
        <!--<li><a href="#"><img src="/assets/img/menu/ic_7.svg">Реклама</a></li>-->
        <li><a href="/events"><img src="/assets/img/menu/ic_8.svg">Мероприятия</a></li>
        <li><a href="https://yadi.sk/d/vfoYPFopkRjmeQ" target="_blanc"><img src="/assets/img/menu/ic_9.svg">Обучение</a></li>
        <?php if($this->user_model->IsGranted(555)){ ?>          
          <li><a href="/blacklist"><img src="/assets/img/menu/ic_10.svg">Черный список</a></li>
        <?php } ?>
        <?php
            if($this->user_model->IsLoginedAsAdmin()){
              echo '<li class="dropdown">';
              echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="/assets/img/menu/ic_10.svg">Администрирование<span class="caret"></span></a>';
              echo '<ul class="dropdown-menu dropdown-menu-right">';
              echo '<li><a href='.($this->user_model->IsGranted(1) || true || $this->user_model->IsGranted(2)?'"/adminpanel/reports"':'"#" class="ungranted"').'>Отчеты сотрудников</a></li>';
              echo '<li><a href='.($this->user_model->IsGranted(2) || true?'"/adminpanel/statistic"':'"#" class="ungranted"').'>Статистика по сотрудникам</a></li>';
              echo '</ul>';
              echo '</li>';
            }   
            
              //superadmin 
              if($this->user_model->IsLoginedAsSuperAdmin()){ ?>
                <li class="dropup">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="/assets/img/menu/ic_10.svg">СуперАдмин<span class="caret"></span></a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="/super/report/countOfferPerOrg">Отчет "Объекты по организациям"</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/super/panel">Панель</a></li>
                    <li><a href="/work/actualize">Актуализировать</a></li>
                    <li><a href="/super/log">Лог</a></li>
                    <li><a href="/creditpublic/log">Лог по банкам</a></li>
                    <li><a href="/super/credit/HashingBank">Редактор ставок по ипотеке</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/super/org/">Список организаций</a></li>
                    <li><a href="/super/org/add">Добавить организацию</a></li>
                    <li><a href="/super/grantlist">Список грантов</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/super/email">Отправить письмо</a></li>
                    <li><a href="/map/">Зоны</a></li>
                  </ul>
                </li>
            <?php }else if($this->user_model->IsGranted(555)){ ?>
              <li class="dropup">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="/assets/img/menu/ic_10.svg">Техподдержка<span class="caret"></span></a>
              <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="/super/report/countOfferPerOrg">Отчет "Объекты по организациям"</a></li>
                <li><a href="/super/credit/HashingBank">Редактор ставок по ипотеке</a></li>
                <li><a href="/creditpublic/log">Лог по банкам</a></li>
                <li><a href="/map/">Зоны</a></li>
              </ul>
            </li>
            <?php } ?>      
        <hr>
        <li class="nav-footer" style="margin-top: 5px !important"><a href="/home">Блог</a></li>
        <li class="nav-footer" style="margin-top: 5px !important"><a href="/home/help">Помощь</a></li>
        <li class="nav-footer" style="margin-top: 5px !important"><a href="/home/terms">Условия использования</a></li>
        <li class="nav-footer" style="margin-top: 5px !important"><a href="/home/about">О сервисе</a></li>
      </ul>
    </div>
    <div class="col-md-8 col-lg-9 col-sm-8 col-xs-8 adt" id="article">
      <div class="row">
        <div class="col-md-6	col-lg-6 col-sm-4 col-xs-4 ">
          <div class="left_headline"><p><?php echo $title;?></p></div>
        </div>
        <div class="col-md-3	col-lg-3  col-sm-4 col-xs-4">
          <a class="btn btn-primary btn-md btn-send" href="/mls/add" role="button">+ Подать объявление</a></p>
        </div>
        <div class="col-md-3	col-lg-3  col-sm-4 col-xs-4">
          <div class="right_headline"><div class="lk_img" style="background-image: url(<?php if(isset($_SESSION['user_photo'])) echo '/assets/img/user/'.$_SESSION['user_photo']; ?>)"><img src=""></div><button type="button" class="btn btn-default lk_img_btn dropdown-toggle" data-toggle="dropdown">Личный кабинет</button>
              <ul class="dropdown-menu menu-lk" role="menu">
              <li><a href="/mls/cabinet">Мои объявления</a></li>
              <li><a href=<?php echo ($this->user_model->IsGranted(1) || $this->user_model->IsGranted(2)?'"/reports"':'"#" class="ungranted"'); ?>>Мои отчеты</a></li>
              <li><a href=<?php echo ($this->user_model->IsGranted(1) || $this->user_model->IsGranted(2)?'"/favorites"':'"#" class="ungranted"'); ?>>Избранное</a></li>
              <li class="divider"></li>
              <li><a href="/home/tariffs">Тарифный план</a></li>
              <li class="divider"></li>
              <li><a href="/settings">Настройки</a></li>
              <li class="divider"></li>
              <li><a href="/auth/logout">Выход</a></li>
              </ul>
          </div>
      </div>
    </div>    
  <?php } else{ ?>
    <div style="height: 80px;"></div>
    </div>
  <?php } ?>
  
      
