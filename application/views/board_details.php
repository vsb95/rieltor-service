<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
#merged-pages{
	background-color: transparent;	
}
#btn-alarm p{
	display: inline-block;
}
.page-adress{
	font-size: 20px;
}
.round-block{
	padding:15px;
}
</style>
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 <?php echo isset($advertisement) && $advertisement['is_deleted']?' deleted':'';?>">
		<div class="round-block">
		<?php 
		if(isset($result) && !empty($result)){
			//var_dump($result);
			echo '<h1>'.$result["status"].'</h1>';
		}
		
		if(isset($advertisement) && !empty($advertisement)){
			?>

			<form id="alarm-form" action="/board/Sendalarm" method="post" style="padding-bottom: 10px;">
				<a id="btn-alarm" href="#" style="float:right">
					<input type="hidden" name="id" value="<?php echo $advertisement["id"]; ?>" />
					<input type="hidden" name="url" value="<?php echo $advertisement["url"]; ?>" />
					<img src="/assets/img/ic_flag.svg" style="padding: 10px 0; margin:0" />
					<p>Здесь ошибка</p>
				</a>
			</form>				

			<?php
			if(!isset($advertisement['adress']['street']) || empty($advertisement['adress']['street']))
				$advertisement['adress']['street'] = $advertisement["url"];
			if($this->user_model->IsLoginedAsSuperAdmin()){
				//var_dump($advertisement);
			}
			//echo '<a href="'.$advertisement["url"].'">Ссылка на оригинал</a>';
			echo '<div class="page-title"><p class="page-adress"><a id="original-url" href="'.$advertisement["url"].'">'.$advertisement['adress']['street'].(isset($page['adress']['house-number'])?' '.$page['adress']['house-number']:'').'</a></p></div>';
			
			echo '<div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-maxwidth="1000" data-maxheight="500" data-width="95%" data-ratio="800/600">';
			foreach($advertisement['images'] as $imagePath)
				echo '<img src="'.$imagePath.'">';
			echo '</div>';
			
			echo '					
			<form id="favorite-form" action="/board/favorite" method="post">
				<input type="hidden" name="id" value="'.$advertisement["id"].'" />
				<input type="hidden" name="is_mls" value="0"/>
				<input type="hidden" name="deal_type" value="'.($advertisement["rent-type"] == 2?'rent':'sell').'" />
				<input type="hidden" name="obj_type" value="'.$advertisement["obj-type"].'" />					
				<a class="btn btn-default set-favorite set-favorite-big" style="position: inherit;margin: 8px;"><i class="'.(isset($advertisement["is_liked"]) && $advertisement["is_liked"] == 1 ?'fa':'far').' fa-heart" aria-hidden="true"></i> Избранное</a>
			</form>
			';

			echo '<div class="table-responsive-sm table-responsive-xs"><table class="table table-hover"><tbody>';
			
			echo '<tr><td>'.($advertisement["rent-type"] == 2?'Аренда':'Продажа'.($advertisement["sell-type"] == 1?' новостройки':' вторички')).'</td><td>'.$advertisement["obj-type"].'</td></tr>';

			if(isset($advertisement["is-agency"]))
				echo '<tr><td>Агентство</td><td>'.($advertisement["is-agency"]?'Да':'Нет').'</td></tr>';
			if(isset($advertisement["cost"]) && !empty($advertisement["cost"])){
				echo '<tr><td>Стоимость</td><td>'.$advertisement["cost"].' &#8381;';
				if(isset($advertisement["size"]) && !empty($advertisement["size"]) 
					&& isset($advertisement["rent-type"]) && !empty($advertisement["rent-type"])
					&& $advertisement["rent-type"] == 1)
					echo ' ('.number_format ($advertisement["cost"]/$advertisement["size"],0,"."," ").' &#8381;/кв.м)';
				echo '</td></tr>';
			}
			if(isset($advertisement["prepay"]) && !empty($advertisement["prepay"]))
				echo '<tr><td>Залог</td><td>'.$advertisement["prepay"].'</td></tr>';
			if(isset($advertisement["cost-type"]) && !empty($advertisement["cost-type"]))
				echo '<tr><td>Коммунальные услуги</td><td>'.$advertisement["cost-type"].'</td></tr>';
			if(isset($advertisement["rent-type"]) && !empty($advertisement["rent-type"])){
					if($advertisement["rent-type"] == 3)
					echo '<tr><td>Посуточно</td><td>Да</td></tr>';
			}
			if(isset($advertisement["size"]) && !empty($advertisement["size"]))
				echo '<tr><td>Площадь</td><td>'.$advertisement["size"].'м2</td></tr>';
			if(isset($advertisement["floor"]) && !empty($advertisement["floor"]))
				echo '<tr><td>Этаж</td><td>'.$advertisement["floor"].(!empty($advertisement["adress"]["max-floor"])?"/".$advertisement["adress"]["max-floor"]:"").'</td></tr>';
			if(isset($advertisement["count-rooms"]) && !empty($advertisement["count-rooms"]))
				echo '<tr><td>Комнаты</td><td>'.$advertisement["count-rooms"].'</td></tr>';
			if(isset($advertisement["adress"]["house-type"]) && !empty($advertisement["adress"]["house-type"]))
				echo '<tr><td>Тип дома</td><td>'.$advertisement["adress"]["house-type"].'</td></tr>';
				
			$microDistrict = isset($advertisement["adress"]["micro-district"]) && !empty($advertisement["adress"]["micro-district"]) ?$advertisement["adress"]["micro-district"]:'Мкр не определен';
			$district = isset($advertisement["adress"]["district"]) && !empty($advertisement["adress"]["district"])?$advertisement["adress"]["district"]:'Район не определен';
			echo '<tr><td>Район</td><td>'.$microDistrict.' ('.$district.')</td></tr>';	
			if(isset($advertisement["date-publish"]))
				echo '<tr><td>Дата публикации</td><td>'.date('H:i:s d.m.Y',strtotime($advertisement["date-publish"])).'</td></tr>';								
			if($this->user_model->IsGranted(555) && isset($advertisement["date-last-refresh"]))
				echo '<tr><td>Дата последнего обновления</td><td>'.date('H:i:s d.m.Y',strtotime($advertisement["date-last-refresh"])).'</td></tr>';
			if(isset($advertisement["is_deleted"]) && $advertisement["is_deleted"])
				echo '<tr><td colspan="2">ОБЪЕКТ УДАЛЕН</td></tr>';
			
			if(isset($advertisement["id-master-page"])){
				echo '<tr><td>Дубликат</td><td><a href="/board/details/'.$advertisement["id-master-page"].'" target=\"_blank\">Rieltor-service.ru</a></td></tr>';
			}

			if($this->user_model->IsGranted(555) && isset($advertisement["debug-info"])){
				echo '<tr><td>Отладочная информация</td><td>'.$advertisement["debug-info"].'</td></tr>';				
			}
			echo '</tbody></table></div>';


			echo '<div class="description">'.$advertisement['description'].'</div>';
			if(isset($advertisement["adress"]["coord"]) && !empty($advertisement["adress"]["coord"])){
				echo '<div class="map"><img style="width:100%" src="http://static.maps.2gis.com/1.0?zoom=15&size=500,350&markers='.$advertisement["adress"]["coord"]["lon"].','.$advertisement["adress"]["coord"]["lat"].'"/></div>';
			}
		}
		?>	
		</div>	
	</div>	
	<div class ="col-lg-4 col-md-4 col-sm-12 col-xs-12" <?php if(!isset($advertisement)) echo 'style="display:none;"'; ?>>
		<div class="round-block">
			<div class="page-title">
				<p style="font-size: 18pt;">Комментарии</p>
			</div>
			<ul style="list-style: none; padding:0;">
				<?php 
				if(isset($comments) && !empty($comments)){
					//var_dump($comments);
					foreach($comments as $comment){
						$date = new DateTime($comment['date']);
						echo '<li><div class="round-block-item"><p class="round-block-title">'.$date->format('H:i d.m').'&emsp;'
							.(empty($comment['user_name']) ? $comment['user_email']:$comment['user_name'])
							.'</p><p class="round-block-text">'.$comment['comment'].'</p></div></li>';
					}
				}
				else{
					echo '<li style="color: #676671;">Еще никто не оставлял комментариев об этом объекте. Вы будете первым</li>';
				}						
				?>
			</ul>	
			<div class="comments-form">
				<form id="comments-form" action="/board/comment" method="POST">
					<input type="hidden" name="id_page" value="<?php echo $id_page; ?>">
					<textarea name="comment" id="comment-form-text" required rows="3" style="width:100%" placeholder="Ваш комментарий..."></textarea>
				</form>
				<button id="btn-comment" style="width:100%; margin:0;" class="btn btn-primary btn-md">Отправить</button>
			</div>
		</div>
	</div>
</div>
				<?php 				
				if(isset($mergedPages) && !empty($mergedPages)){
					echo '<div class="row" id="merged-pages">';
					echo '<h3>Дубликаты:</h3>';
					echo '<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">';
					foreach($mergedPages as $slavePage)
						echo '<div class="row list-item">'. $this->load->view('board_list_item', array("page"=>$slavePage), true).'</div>';
					echo '</div></div>';
				}
				?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#btn-alarm").click(function() {		
			$("#modalContent").html('<p>Опишите в чем заключается ошибка</p><textarea id="alarm_description" style="height: 150px; width: 100%;" required/><button onclick="sendAlarm()" class="btn-main">Отправить</button>');
			openModal();		
		});
		$("#btn-favorite").click(function() {
			var $form = $("#favorite-form");
			$.ajax({
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: $form.serialize()
			}).done(function(msg) {
				$("#modalContent").html(msg);
				openModal();
			}).fail(function(err) {
				$("#modalContent").html(err);
				openModal();
			});
			//отмена действия по умолчанию для кнопки submit
			//e.preventDefault(); 
		});
		$("#btn-comment").click(function() {
			if(!$("#comment-form-text").val().trim()){
				$("#modalContent").html("Комментарий не заполнен");
				openModal();
				return;
			}
			var $form = $("#comments-form");
			$.ajax({
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: $form.serialize()
			}).done(function(msg) {
				if(msg == "done")
					$("#modalContent").html("Ваш комментарий добавлен!");
				else
					$("#modalContent").html(msg);
				openModal();
			}).fail(function(err) {
				$("#modalContent").html(err);
				openModal();
			});
			//отмена действия по умолчанию для кнопки submit
		});
	});
	function sendAlarm() {	
		var description = 	$('#alarm_description').val();
		if(!description){
			alert("Укажите в чем ошибка");
			return;
		}
		var url = $("#original-url").attr("href");
		var $form = $("#alarm-form");
		$.ajax({
			type: $form.attr('method'),
			url: $form.attr('action'),
			data: $form.serialize()+"&description="+description,
		}).done(function(msg) {
			console.log('success: '+msg);
		}).fail(function(err) {
			console.log('fail: '+err);
		});
		$("#modalContent").html('Спасибо за вашу помощь');
		openModal();
		//отмена действия по умолчанию для кнопки submit
		//e.preventDefault(); 
	}
</script>