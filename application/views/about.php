<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('1_header_new', array('title' =>'О сервисе')); ?>
<style>
    .td-logo{
        max-width: 36px;
        margin-right:10px;
    }
    table{
        font-size: 14px;
    }
    #tbody tr td{
        vertical-align: middle;
        max-height:36px;
    }
</style>
<div class="row" >    
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="background-color:white">     
        <div class="feed-description">
            <p>РиэлторСервис – закрытая мультилистинговая площадка профессиональных агентств недвижимости и риэлторов г. Омска, в лице двух профессиональных риэлтороских сообществ: общественного объединения ассоциация “Омская палата недвижимости” и некоммерческого партнерства “Омский Союз Риэлторов”. </p>
            <p>Риэлтор сервис включает в себя различный функционал, призванный сократить временные затраты специалиста, и обеспечить его актуальной информацией о рынке недвижимости Омска и объектах недвижимости.</p>
        </div> 
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    </div> 
    
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">    
        <div class="round-block">
        <h4 style="text-align: center;">Cписок участников млс</h4> 
            <table class="table table-striped table-hover table-dark">
                <thead>
                    <tr>
                        <th scope="col">Организация</th>
                        <th scope="col">Количество объектов</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                <?php 
                if(isset($reportCountPerOrg) && !empty($reportCountPerOrg) && $reportCountPerOrg['status-type'] == "OK"){ 
                    foreach($reportCountPerOrg['rows'] as $row){
                        echo '<tr>';
                        echo '<td>';
                        if(isset($row['org']['logo_name']) && !empty($row['org']['logo_name']))
                            echo '<img class="td-logo" src="/assets/img/logo/'.$row['org']['logo_name'].'"/>';
                        echo $row['org']['name'].'</td>';
                        echo '<td>'.$row['offer-count'].'</td>';
                        echo '</tr>';
                    }
                }
                ?>
            </tbody>
        </table>
        </div>
    </div>
</div>

<?php $this->load->view('1_footer_new'); ?>