<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="referrer" content="no-referrer" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

	<title>Отчет</title>
	<meta property="og:title"              content="Отчет" />
	<meta property="og:description"        content="Список квартир по вашему запросу" />

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/assets/css/result_page.css?1" rel="stylesheet">
    <link href="/assets/css/modal.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->	
	<!-- fotorama.css & fotorama.js. -->
<style>
</style>
</head>
<table class="table table-striped table-hover">
		<tbody>
		<tr>
				<td colspan="9"><?php echo isset($reportDescription) ? $reportDescription: 'Отчет за '.$createDate; ?></td>
		</tr>
		<?php
			if(isset($client) && !empty($client))
				echo '<tr><td colspan="9">Клиент: '.$client['name'].' т. '.$client['phone'].'</td></tr>';
	
			if(!isset($advertisments) || empty($advertisments)){
				echo '404. Не сформировано';
			}
			else{
				for($i =0; $i<count($advertisments); $i++){
					$adv = $advertisments[$i];
					if(isset($adv['is_deleted']) && !empty($adv['is_deleted']) && $adv['is_deleted'] == 1){				
							continue;				
					}

					echo '<tr>';
					
					echo '<td>'.$adv['adress']['street'].(isset($page['adress']['house-number'])?' '.$page['adress']['house-number']:'').'</td>';
					//			
					$type = '';
					if(isset($adv['obj-type']) && !empty($adv['obj-type'])){
						$type = isset($adv['obj-type']['name']) && !empty($adv['obj-type']['name']) ? $adv['obj-type']['name'] : $adv['obj-type'];
					}else{
						$type = $adv['count-rooms'];
					}
					echo '<td>'.$type.'</td>';			
					//
					echo '<td>'.$adv["size"].'м2</td>';
					//	
					$floor = '';
					if(isset($adv["floor"]) && !empty($adv["floor"])){
						$floor = $adv["floor"];
					}
					if($floor!='' && isset($adv["adress"]["max-floor"]) && !empty($adv["adress"]["max-floor"])){
						$floor .= '/'.$adv["adress"]["max-floor"];
					}
					if($floor!='' ){
						echo '<td>'.$floor.' этаж</td>';
					}else{
						echo '<td></td>';
					}
					//
					echo '<td>'.number_format($adv['cost'], 0, ',', ' ').'р</td>';
					/*echo '<td>'.'Тип продажи'.'</td>';
					echo '<td>'.'Телефон'.'</td>';
					echo '<td>'.'Имя'.'</td>';
					echo '<td>'.'Доп параметры'.'</td>';*/
					echo '</tr>';
					} 
			}
	?>
	
	</tbody>
</table>
			<?php
			/*
	var_dump($org);
	var_dump($user);
	var_dump($reportDescription);
	var_dump($is_hidden_description);
	var_dump($createDate);
	var_dump($advertisments);
	*/	
	?>
	</div><!-- /.container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	window.print();
});
</script>
</body>
</html>