<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="referrer" content="no-referrer" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

	<title>Отчет</title>
	<meta property="og:title"              content="Отчет" />
	<meta property="og:description"        content="Список квартир по вашему запросу" />

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/assets/css/result_page.css?1" rel="stylesheet">
    <link href="/assets/css/modal.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>

	<link rel="stylesheet" media="print" type="text/css" href="/assets/css/print.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->	
<style>
.txt{
	display: block;
}
.image-logo{
	max-width: 300px;
	max-height: 300px;
	width:100%;
}
.table-img{
	width:175px;
	max-height: 175px;
	padding:0;
	vertical-align: middle !important;
}
.table-img img{
	max-width:175px;
	max-height: 175px;
	vertical-align: middle;
}
.container,.row, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
	padding:0;
}
.tbl,
.tbl tr,
.tbl tbody td,
.tbl thead th,
.tbl tfoot th {
	border: none !important;
	page-break-inside: avoid;
}
.obj{
	margin-bottom: 30px;
}
.bold{
	font-weight: bold;
}
</style>
  </head>
<?php	
	if (!empty($org['org_url']) && strpos($org['org_url'], 'http') !== true)
		$org['org_url'] = "http://".$org['org_url']; 
	?>
    <div class="container">
		<div class="row header-result">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					<a target="_blank" href="<?php if(isset($org['org_url']) && !empty( $org['org_url'])) echo $org['org_url']; else echo "#";?>">
							<img class="image-logo" src="/assets/img/logo/<?php echo $org['logo_name'];?>" alter="<?php echo $org['name'];?>"/>
					</a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 header-info" style="padding:15px;">
					<?php 
						$result = "";
						if(isset($org['name']) && !empty($org['name'])){
							$result .= '<span class="txt">'.$org['name'].'</span>';
						}
						if(isset($org['adress']) && !empty($org['adress'])){
							$result .= '<span class="txt">'.$org['adress'].'</span>';
						}
						if(isset($org['email']) && !empty($org['email'])){
							$result .= '<span class="txt">'.$org['email'].'</span>';
						}
						if(isset($org['org_url']) && !empty($org['org_url'])){
							$result .= '<span class="txt">'.$org['org_url'].'</span>';
						}
						echo $result;
						?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					<?php 
					if(isset($user['photo']) && !empty($user['photo'])){
						echo '<img class="image-logo" src="/assets/img/user/'.$user['photo'].'" alter="'.$user['full_name'].'"/>';
					}		
					?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 header-manager" style="padding:15px;">
					<?php 			
						if(isset($user['full_name']) && !empty($user['full_name'])){
							echo '<span class="txt">Ваш личный менеджер: </span>';
							echo '<span class="txt">'.$user['full_name'].'</span>';
							if(isset($user['phone']) && !empty($user['phone'])){
								echo '<span class="txt">'.$user['phone'].'</span>';
							}
							if(isset($user['email']) && !empty($user['email'])){
								echo '<span class="txt">'.$user['email'].'</span>';
							}
							if(isset($user['tagline']) && !empty($user['tagline']))
								echo '<hr><span class="txt">&laquo;'.$user['tagline'].'&raquo;</span>';
						}
					?>
				</div>
		</div>
		<div class="row page">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<?php 
	if(!isset($advertisments) || empty($advertisments)){
		echo '404. Не сформировано';
	}
	else{
		for($i =0; $i<count($advertisments); $i++){
			$page = $advertisments[$i];
			if(isset($page['is_deleted']) && !empty($page['is_deleted']) && $page['is_deleted'] == 1){				
					continue;				
			}
			echo '<div class="obj">';
			echo '<table class="table tbl" >';
			echo '<tbody>';
			echo '<tr>';
			if(isset($page['images']) && !empty($page['images'])){		
				if(count($page['images'])>1){
					echo '<td class="table-img"><img src="'.$page['images'][0].'"></td>';
					echo '<td class="table-img"><img src="'.$page['images'][1].'"></td>';
				}else{
					echo '<td class="table-img"><img src="'.$page['images'][0].'"></td>';
					echo '<td></td>';
				}				
			}
			else{
				echo '<td></td><td></td>';
			}
			echo '<td style="vertical-align: middle;">';
			echo '<span class="txt bold">'.$page['adress']['street'].(isset($page['adress']['house-number'])?' '.$page['adress']['house-number']:'').'</span>';
			//
			$type = '';
			if(isset($page['obj-type']) && !empty($page['obj-type'])){
				$type = isset($page['obj-type']['name']) && !empty($page['obj-type']['name']) ? $page['obj-type']['name'] : $page['obj-type'];
			}else{
				$type = $page['count-rooms'];
			}
			echo '<span class="txt bold">'.$type.'</span>';
			//
			$floor = '';
			if(isset($page["floor"]) && !empty($page["floor"])){
				$floor = $page["floor"];
			}
			if($floor!='' && isset($page["adress"]["max-floor"]) && !empty($page["adress"]["max-floor"])){
				$floor .= '/'.$page["adress"]["max-floor"];
			}
			if($floor!='' ){
				echo '<span class="txt">'.$floor.' этаж</span>';
			}
			//
			echo '<span class="txt">'.$page["size"].' м2</span>';
			echo '</td>';
			
			if(isset($page['cost']) && !empty($page['cost']))
				echo '<td class="bold" style="vertical-align: middle;">'.number_format($page['cost'], 0, ',', ' ').'р</td>';
			else
				echo '<td style="vertical-align: middle;">?? р</td>';
			//
			//echo '<span class="txt>'.$.'</span>';

			echo '</tr>';
			echo '<tr>';
			//
			if(isset($page['images']) && !empty($page['images'])){		
				if(count($page['images'])>3){
					echo '<td class="table-img"><img src="'.$page['images'][2].'"></td>';
					echo '<td class="table-img"><img src="'.$page['images'][3].'"></td>';
				}else if(count($page['images'])>2){
					echo '<td class="table-img"><img src="'.$page['images'][2].'"></td>';
					echo '<td></td>';
				}
			}
			else{
				echo '<td></td><td></td>';
			}
			//
			echo '<td colspan="2">';
			if($is_hidden_description != true)
				echo '<p style="font-size: 12px;">'.$page['description'].'</p>';
			echo '</td>';			
			echo '</tr>';			
			echo '</tbody>';			
			echo '</table>';			
			echo '</div>';			
		} 
	}
	?>
				
			
		</div>
	</div><!-- /.container -->
	<?php
	/*var_dump($org);
	var_dump($user);
	var_dump($reportDescription);
	var_dump($is_hidden_description);
	var_dump($createDate);
	var_dump($advertisments);*/
	?>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	window.print();
});
</script>
</body>
</html>