<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="referrer" content="no-referrer" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

	<title>Отчет</title>
	<!--
	<meta property="og:type" content="article" />
	<meta property="og:image:width" content="968">
	<meta property="og:image:height" content="504">
	-->
	<meta property="og:title"              content="Отчет" />
	<meta property="og:description"        content="Список квартир по вашему запросу" />

	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Rieltor-service">
	<meta property="og:url" content="<?php echo SERVER_DOMAIN.'page/show/'.$page_id; ?>">
	<meta property="og:locale" content="ru_RU">
	<meta property="og:image" content="<?php echo isset($logo_image) && !empty($logo_image) ? SERVER_DOMAIN.'assets/img/logo/'.$logo_image : SERVER_DOMAIN.'favicon.ico'; ?>">
	

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/assets/css/result_page.css?1" rel="stylesheet">
    <link href="/assets/css/modal.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>

	<link rel="stylesheet" media="print" type="text/css" href="/assets/css/print.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->	
	<!-- fotorama.css & fotorama.js. -->
<link  href="/assets/css/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
  </head>
<div class='overlay overlay_ noprint'>
	<div class='modal-page' id='modal1'>
		<div class='content'>			
		<?php
		if(isset($is_creator) && $is_creator === true){
				echo '<iframe width="100%" height="500" src="https://www.youtube.com/embed/7z-9YePyKgw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>					';
		}
		else{?>
			<form id="contact-form" method="post" action="#">
				<h4>Оставьте ваши контактные данные, по которым с вами свяжется риэлтор</h4><br/>
				<input name="name" placeholder="Ваше имя" required><br/>
				<br/>
				<input name="phone" placeholder="Ваш телефон" required>	<br/><br/>
				<input type="submit" class="btn" value="ОТПРАВИТЬ">		
			</form>		
		<?php }?>
		</div>  
	</div> 
</div> 
<?php
	$isUsingCheckboxes = isset($user_email) && !empty($user_email) && !(isset($is_edit) && $is_edit === true);
	
	if (strpos($org_url, 'http') !== true)
		$org_url = "http://".$org_url; 
	?>
    <div class="container-fluid noprint">
		<div class="row header-result">
			<div class="col-lg-1 col-md-0 col-xs-0"></div>
			<div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 flex-header">
				<div class="header-logo">
					<a target="_blank" href="<?php if(isset($org_url) && !empty( $org_url)) echo $org_url; else echo "#";?>">
							<img class="org-logo-big" src="/assets/img/logo/<?php echo $logo_image;?>" alter="<?php echo $org_name;?>"/>
					</a>
				</div>
				<div class="header-info">
				<?php 
					$result = "";
					if(isset($org_name) && !empty($org_name)){
						$result .= '<p class="org-name">'.$org_name.'</p>';
					}
					if(isset($org_adress) && !empty($org_adress)){
						$result .= '<p class="org-adress">'.$org_adress.'</p>';
					}
					if(isset($org_email) && !empty($org_email)){
						$result .= '<p class="org-email">Email: <a href="mailto:'.$org_email.'">'.$org_email.'</a></p>';
					}
					if(isset($org_url) && !empty($org_url)){
						$result .= '<p class="org-site">Сайт: <a target="_blank" href="'.$org_url.'">'.$org_url.'</a></p>';
					}
					echo $result;
						?>
				</div>
			</div>  
			<div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 flex-header">
				<div class="header-logo">
					<?php 
					if(isset($user_image) && !empty($user_image)){
						echo '<img class="img-responsive img-rounded center-block user-photo" src="/assets/img/user/'.$user_image.'" alter="'.$user_name.'"/>';
					}		
					?>
				</div>
				<div class="header-manager">
				<?php 			
					if(isset($user_name) && !empty($user_name)){
						echo '<h4>Ваш личный менеджер: </h4>';
						echo '<p class="user-name">'.$user_name.'</p>';
						if(isset($phone) && !empty($phone)){
							echo '<div class="call-me-btn">';
							echo '<a class="phone-number" href="tel:'.$phone.'">';
							echo '<span class="txt">'.$phone.'</span>';
							echo '<span class="round"><i class="fa fa-phone"></i></span>';
							echo '</a>';
							echo '</div>';
						}
						if(isset($user_email) && !empty($user_email)){
							echo '<div class="call-me-btn">';
							echo '<a class="phone-number" href="mailto:'.$user_email.'">';
							echo '<span class="txt">'.$user_email.'</span>';
							echo '<span class="round"><i class="fas fa-envelope"></i></span>';
							echo '</a>';
							echo '</div>';
						}
						if(isset($user_tagline) && !empty($user_tagline))
							echo '<p class="user-tagline">&laquo;'.$user_tagline.'&raquo;</p>';
					}
				?>
				</div>
			</div>			
			<div class="col-lg-1 col-md-0 col-xs-0"></div>
		</div>
	<?php 			
	if(isset($pages) && !empty($pages)){
		if($isUsingCheckboxes){
			echo '<form id="frm" method="post" action="/page/sendemail">';
			echo '<div class="row page">';
				echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
				echo '<p>'.$reportDescription.'<br/><strong>Для того чтобы посмотреть любую из квартир, просто <u>отметьте галочками понравившиеся варианты</u> и нажмите кнопку "Посмотреть выбранные варианты" в конце страницы, либо свяжитесь с нами по контактным данным, указанным в правом верхнем углу </strong></p>';
				echo '</div>';
			echo '</div>';
		}
		else
			echo '<form id="frm" method="post" action="/page/save">';
			echo '<input id="pageId" name="pageId" type="hidden" value="'.$page_id.'">';
			echo '<input id="client_id" name="client_id" type="hidden" value="'.(isset($client) && isset($client["id"])?$client["id"] :'').'">';
			if(isset($user_email) && !empty($user_email))
				echo '<input id="user_email" name="user_email" type="hidden" value="'.$user_email.'">';
			if(isset($user_name) && !empty($user_name))
				echo '<input id="user_name" name="user_name" type="hidden" value="'.$user_name.'">';
				if(isset($is_edit) && $is_edit === true){
					echo '<div class="row page">';
						echo '<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">';
							echo '<label><input id="isPublicRadio" name="isPublic" type="radio" value=1 '.(isset($isPublic) && $isPublic == 1?'checked':'').'/>Публичный</label><br/>';
							echo '<label><input id="IsPrivateRadio" name="isPublic" type="radio" value=0 '.(isset($isPublic) && $isPublic != 1?'checked':'').'/>Приватный</label>';
							echo '<div class="checkbox-select"><label class="checkbox-container">Скрыть описание<input type="checkbox" name="hidden_description" value="1" '.($is_hidden_description == true?' checked':'').'><span class="checkmark"></span></label></div>';
							echo '<label id="client_nameInput" '.(isset($isPublic) && $isPublic == 1?'style="display:none"':'').'>Имя клиента: <input id="client_nameInput_inner"  name="client_name" placeholder="Имя клиента"  value="'. (isset($client) && isset($client["name"]) ?$client["name"]:'') .'"></label><br/>';					
							echo '<label id="client_phoneInput" '.(isset($isPublic) && $isPublic == 1?'style="display:none"':'').'>Телефон клиента: <input id="client_phoneInput_inner" name="client_phone" placeholder="Телефон клиента" value="'.(isset($client) && isset($client["phone"])?$client["phone"] :'').'"></label><br/>';
						echo '</div>';
						echo '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">';
						echo '<p><strong>Публичный</strong> - отчет можно выложить в соцсети и каждому потенциальному клиенту, желающему посмотреть квартиры, будет необходимо оставить контактный номер телефона </p>';
						echo '<p><strong>Приватный</strong> - у клиента не будет запрашиваться номер телефона. Подразумевается, что вы знаете какой клиент выбрал квартиры в этом отчете (имя и телефон клиента обязательны к заполнению)</p>';
						echo '</div>';
						echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
						echo '<h3>Описание отчета</h3>';
						echo '<textarea name="report_description" style="width:100%; height:200px;">'.$reportDescription.'</textarea>';
						echo '</div>';
					echo '</div>';
				}
				
		for($i =0; $i<count($pages); $i++){
			$page = $pages[$i];
			//var_dump($page);
			
			if(isset($selected) && !empty($selected) && in_array($i, $selected)){
				echo '<div class="row page selected-page">';
			} else if(isset($page['is_deleted']) && !empty($page['is_deleted']) && $page['is_deleted'] == 1){
				if((isset($is_creator) && $is_creator === true) ){
					echo '<div class="row page selected-page deleted">';
				}else{
					continue;
				}
			}else{
				echo '<div class="row page">';
			}
			if(isset($page['images']) && !empty($page['images'])){
				$minDate = strtotime("2018-12-08 12:00:00.0");
				$createDate1 = strtotime($createDate);
				$isOldPage = $minDate > $createDate1;
				
				$minDate = strtotime("2018-12-17 02:00:00.0");
				$createDate1 = strtotime($createDate);
				$isApiLow68 = $minDate > $createDate1;
				
				echo '<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">';
				echo '<div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-maxwidth="1000" data-width="95%" data-ratio="800/600">';
				
				foreach($page['images'] as $imagePath){
					if($isOldPage)
						echo '<img src="/assets/page_images/'.$imagePath.'">';	
					else if($isApiLow68)			
						echo '<img src="/'.$imagePath.'">';
					else
						echo '<img src="'.$imagePath.'">';
				}
				echo '</div></div>';
			}
			else{
				echo '<div class="col-lg-0 col-md-0 col-sm-0 col-xs-0"></div>';
			}
				echo '<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">';
				echo '<div class="page-title">';
				if(isset($is_edit) && $is_edit === true){	
					echo '<input type="text" name="streets['.$i.']" style="width:100%;" value="'.$page['adress']['street'].(isset($page['adress']['house-number'])?' '.$page['adress']['house-number']:'').'"/>';
				}				
				else{
					echo '<p class="page-adress">'.$page['adress']['street'].(isset($page['adress']['house-number'])?' '.$page['adress']['house-number']:'').'</p>';
				}
					if((isset($is_creator) && $is_creator === true) && isset($page['url']) && !empty($page['url']))			
						echo '<a href="'.$page['url'].'" class="noprint" rel="noreferrer">Ссылка на оригинал</a>';

					$floor = '';
					if(isset($page["floor"]) && !empty($page["floor"])){
						$floor = $page["floor"];
					}
					if($floor!='' && isset($page["adress"]["max-floor"]) && !empty($page["adress"]["max-floor"])){
						$floor .= '/'.$page["adress"]["max-floor"];
					}
					if($floor!='' ){
						$floor.=' этаж, ';
					}
					$type = '';
					if(isset($page['obj-type']) && !empty($page['obj-type'])){
						$type = isset($page['obj-type']['name']) && !empty($page['obj-type']['name']) ? $page['obj-type']['name'] : $page['obj-type'];
					}else{
						$type = $page['count-rooms'];
					}
					echo '<p class="page-cost">';
					if(isset($is_edit) && $is_edit === true){	
						echo '<input type="text" name="costs['.$i.']" value="'.$page['cost'].'"/>';
						if(isset($page["rent-type"]) && !empty($page["rent-type"]) && $page["rent-type"] != 1)
							echo '<input type="text" name="cost-types['.$i.']" value="'.$page['cost-type'].'"/>';
					}				
					else {		
						if(isset($page['cost']) && !empty($page['cost']))
							echo $type.' '.(is_numeric($page['cost']) ?number_format($page['cost'], 0, ',', ' '):$page['cost']).'р'
								.(isset($page["rent-type"]) && !empty($page["rent-type"]) && $page["rent-type"] != 1? ' '.$page['cost-type']:'').', '.$floor.$page["size"].'м2';						
						else
							echo $type.' ?? р'
								.(isset($page["rent-type"]) && !empty($page["rent-type"]) && $page["rent-type"] != 1? ' '.$page['cost-type']:'').', '.$floor.$page["size"].'м2';
					}
					echo '</p></div>';
					echo '<div class="description" >';
					if(isset($is_edit) && $is_edit === true){	
						$description =  str_replace('<br />', "\r\n", $page['description']);	
						echo '<textarea name="descriptions['.$i.']" style="width:100%; height:200px;">'.$description.'</textarea>';
					if(isset($page['is_deleted']) && !empty($page['is_deleted']) && $page['is_deleted'] == 1)
						echo '<div class="checkbox-select noprint"><label class="checkbox-container">Удалить<input type="checkbox" name="deleted_pages[]" value="'.$i.'" checked><span class="checkmark"></span></label></div>';
					else
						echo '<div class="checkbox-select noprint"><label class="checkbox-container">Удалить<input type="checkbox" name="deleted_pages[]" value="'.$i.'"><span class="checkmark"></span></label></div>';
					
					}				
					else if($is_hidden_description != true){
						echo $page['description'];
					}
					echo '</div>';
				if($isUsingCheckboxes) //$page["adress"]['street']
					echo '<div class="checkbox-select noprint"><label class="checkbox-container">Хочу посмотреть<input type="checkbox" name="selected_pages[]" value="'.$i.'"><span class="checkmark"></span></label></div>';
				
				echo '</div>';//echo '<label><input type="checkbox" name="selected_pages[]" value="'.$page["adress"]['street'].'" />Посмотреть</label>';	
			echo '</div>';
		} 
	} else{
		echo '404. Не сформировано';	
	}
	?>
	
	<div class="footer-result row row-eq-height">
			<div class="col-lg-4 col-md-2 col-xs-0"></div>
			<div class="col-lg-4 col-md-8 col-xs-12 col-sm-12">
			<?php			
		if($isUsingCheckboxes){
			if(isset($is_creator) && $is_creator === true)
				echo '<button value="submit" disabled class="btn noprint"><span class="send-email-btn">Посмотреть выбранные варианты</span></button>';
			else	
				echo '<button id="send" disabled class="btn" noprint><span class="send-email-btn">Посмотреть выбранные варианты</span></button>';
		}
		else{
			if(isset($is_creator) && $is_creator === true)
			 echo '<button id="send" class="btn">Сохранить</button>';
			//echo '<button value="submit" class="btn">Сохранить</button>';
		}
		echo '</form>';
		 ?>
			</div>
			<div class="col-lg-4 col-md-2 col-xs-0"></div>
		</div>
		<?php 
		if(isset($org_ya_geo) && !empty($org_ya_geo)){
			//echo '<div class="row">';
			//echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
			echo '<iframe class="noprint" src="https://yandex.ru/map-widget/v1/-/'.$org_ya_geo.'" width="100%" height="500"></iframe>';
			//echo '</div>';
			//echo '</div>';
		}
		?>
			
				
			
<?php 
if(isset($is_creator) && $is_creator === true){
	if(isset($is_edit) && $is_edit === true)
		echo '<a href="/page/show/'.$page_id.'"><div class="fixedbut fixedbut-inner"><i class="fas fa-eye" aria-hidden="true"></i> Просмотр</div></a>';
	else{		
		echo '<div class="fixedbut noprint">';
		echo '<a href="#" onclick="openModal();"><div class="fixedbut-inner"><i class="fas fa-question" aria-hidden="true"></i> Помощь</div></a>';
		if($org_name == 'АН Галеон' || $this->user_model->IsGranted(555)){
			echo '<a target="_blank" href="/page/printing?type=self&id='.$page_id.'"><div class="fixedbut-inner"><i class="fas fa-print" aria-hidden="true"></i> Печать для себя</div></a>';
		}
		echo '<a target="_blank" href="/page/printing?type=client&id='.$page_id.'"><div class="fixedbut-inner"><i class="fas fa-print" aria-hidden="true"></i> Печать для клиента</div></a>';
		
		echo '<a href="/page/edit/'.$page_id.'"><div class="fixedbut-inner"><i class="fas fa-edit" aria-hidden="true"></i> Редактировать</div></a>';
		echo '<div class="ya-share2" style="padding: 0 45px;"  data-services="vkontakte,whatsapp,viber" data-title="Подборка для вас" data-url="'.SERVER_DOMAIN.'/page/show/'.$page_id.'" data-description="'.$reportDescription.'"></div>';
		echo '</div>';
		echo '<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async defer></script>';
		echo '<script src="//yastatic.net/share2/share.js" async defer></script>';
	}

} else{
	if(!isset($_GET["itsmy"]) || $_GET["itsmy"] != 1){
		$url = '/page/reportopen/'.$page_id;
		echo '<br><br><script type="text/javascript">';
		echo '$(document).ready(function() {';
			echo '$.get("'.$url.'", function(data, status){console.log("Data: " + data + "\nStatus: " + status);});';
			/*echo 'var request = new XMLHttpRequest();';
			echo 'request.open("GET", "'.$url.'");';
			echo 'request.onreadystatechange = function () { ';
				echo 'if (request.readyState === 4 && request.status === 200) { ';
					echo 'console.log(request.responseText);';
				echo '}';
			echo '}';*/
		echo '});';
		echo '</script><br><br>';
	}
	
	if(isset($phone) && !empty($phone))
		echo '<a href="tel:'.$phone.'" id="popup__toggle" ><div class="circlephone" style="transform-origin: center;"></div><div class="circle-fill" style="transform-origin: center;"></div><div class="img-circle" style="transform-origin: center;"><div class="img-circleblock" style="transform-origin: center;"></div></div></a>';
} 
?>
		
	</div><!-- /.container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="/assets/js/fotorama.js"></script> <!-- 16 KB -->
<?php 
if((!isset($_GET["itsmy"]) || $_GET["itsmy"] != 1) && !isset($_SESSION['id_user']))
	echo '<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter49830892 = new Ya.Metrika2({ id:49830892, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, ut:"noindex" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/49830892?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->';
	?>
<script type="text/javascript">
var isPublicPage = <?php echo isset($isPublic) && $isPublic == 1 ?  '1':'0'; ?>;
var isEdit = <?php echo isset($is_edit) && $is_edit === true ?  '1':'0'; ?>;
var clientname='';
var clientphone='';
var isEnabledBtn = false;
$(document).ready(function() {
	var boxes = $("input:checkbox");
	<?php
	if(!isset($is_creator) || $is_creator !== true){
		if(!isset($is_edit) || $is_edit !== true){
	?>
		$("input:checkbox").on("change", function(){
			if(isEdit == 1) return;
			var newIsEnabledBtn = false;
			for (var i=0;i<boxes.length;i++) {
				var box = boxes[i]; 
				if ($(box).prop('checked')) {
					newIsEnabledBtn = true;				
					break; 
				}
			}
			if(newIsEnabledBtn != isEnabledBtn){
				isEnabledBtn = newIsEnabledBtn;
			console.log(isEnabledBtn);
				if(isEnabledBtn){
					$('#send').stop(true) 
					.animate(
					{ 
						bottom: "15px",
						opacity: "show",
						opacity: 1,
						height: "show"
				});

				}else{
					$('#send').stop(true) 
					.animate(
					{ 
						bottom: "-30px",
						opacity: "hide",
						height: "hide"
				});
				}
			}
			$("#send")[0].disabled = !isEnabledBtn;	
		});
	<?php
		}
	}else if(isset($is_edit) && $is_edit === true){		
		echo '$("#send").stop(true).animate({ bottom: "15px",	opacity: "show",opacity: 1,	height: "show" });';
	}
	?>

	$("input:radio").on("change", function(e){
		var isPublic = e.target.value;

		$("#client_nameInput_inner")[0].required = isPublic != 1;
		$("#client_phoneInput_inner")[0].required = isPublic != 1;
		
		var clientNameLabel = document.getElementById("client_nameInput");
		var clientPhoneLabel = document.getElementById("client_phoneInput");
		if(isPublic == 1){
			clientNameLabel.setAttribute("style", "display:none");
			clientPhoneLabel.setAttribute("style", "display:none");

			clientNameLabel.children[0].value ='';
			clientPhoneLabel.children[0].value ='';
		}else{
			clientNameLabel.setAttribute("style", "display:inline-block");
			clientPhoneLabel.setAttribute("style", "display:inline-block");
		}
	});

	$("#frm").submit(function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopImmediatePropagation;  
		submitForm();
	});  
	$("#contact-form").submit(function(e) {
		e.preventDefault();
		e.stopImmediatePropagation;  
		var data = $("#contact-form").serializeArray(); 
		clientname = data[0].value;
		clientphone= data[1].value;
  		submitForm();
	});  
});
function submitForm(){
	if(isEdit == 0 && isPublicPage == 1 && (!clientname || clientname.length == 0 || !clientphone || clientphone.length == 0)) {
		openModal();
		return;
	}
	var form = $("#frm");
	var url = form.attr('action');
	$.ajax({
		type: "POST",
		url: url,
		data: 'client_name='+clientname+'&client_phone='+clientphone+'&'+form.serialize(), // serializes the form's elements.
		success: function(data)
		{
			alert(data); // show response from the php script.
			console.log(data);
		},
		error: function(result) {
			console.log(result);
			var json = JSON.parse(result);
			alert('error: '+JSON.stringify(json));
			document.body.appendChild(document.createTextNode(JSON.stringify(json)));
			console.log(JSON.stringify(json));
		},
		timeout: 15*60*1000
	});
	closeModal();
}

</script>
<script src="/assets/js/modal.js"></script>
</body>
</html>