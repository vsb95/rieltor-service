<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
	.round-block{
		padding: 15px;
	}
	.round-block div{
		padding:5px;
	}
	.round-block .btn{
		max-width: 200px;
	}
	.round-block p{
		margin:0;
		padding:0;
		margin-top:10px;
		margin-bottom:5px;
	}
</style>
<div class="row upload-image-form ">
	<?php 
	if(isset($_GET["result"]) && !empty($_GET["result"])){
		if($_GET["result"] == "1"){
			echo '<h2 style="text-align:center">Настройки успешно сохранены!</h2>';
		}
		else{			
			echo '<h3 class="error" style="text-align:center">Произошла ошибка во время сохранения настроек </h3>';
		}
	}
		?>
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">	
		<?php if(isset($is_admin) && $is_admin == 1){?>
		<div class="row round-block">
			<h3>Организация</h3>	
			<form method="post" action="/settings/saveOrg" enctype="multipart/form-data" class="settings-form"> 
				<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
					<p>Название</p>
					<input class="form-control" type="text" required name="org_name" placeholder="ООО &laquo;Моя компания&raquo;" value="<?php echo $org["name"] ?>" />
					<p>Адрес</p>
					<input class="form-control" type="text" name="org_adress" placeholder="Москва, ул. Поперечная 24"  value="<?php echo $org["adress"] ?>" /> 
					<p>Ссылка на сайт:</p>
					<input class="form-control" type="text" name="org_url" placeholder="www.example.ru"  value="<?php echo $org["url"] ?>" /> 
					<p>Email</p>
					<input class="form-control" type="email" name="org_email" placeholder="example@mail.com"  value="<?php echo $org["email"] ?>" /> 
					<p>Ссылка на вашу организацию на Яндекс Карте <a href="#" id="yageohelp">[?]</a>:</p>
					<input class="form-control" type="text" name="org_ya_geo" placeholder="CBubi6bI1D"  value="<?php echo $org["ya_geo"] ?>" />          
				
					<br/><input type="submit" name="submit" id="submit1" class="btn btn-primary btn-md" value="Сохранить" />		
				</div> 	
				<div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
					<div> 							
						<p>Логотип</p>
						<?php if(isset( $org["logo_name"]) && !empty( $org["logo_name"])) echo '<img class="org-logo" src="/assets/img/logo/'.$org["logo_name"].'"/>'; ?>
						<input class="form-control" type="file" name="org_logo" accept="image/gif, image/png, image/jpeg" />					
					</div> 		
					<div style="margin-top: 15px"> 	
						<p>Водяной знак</p>
						<?php if(isset( $org["logo_stamp"]) && !empty( $org["logo_stamp"])) 
							echo '<img class="org-logo" src="/assets/img/logo/'.$org["logo_stamp"].'"/>';
						echo '<p> * Новый водяной знак будет применен только к будущим объявлениям</p>
						<p> * Водяной знак должен быть в формате <strong>.PNG</strong> и иметь прозрачный фон. Максимальный размер <strong>500 Кб</strong></p>'; ?>
						<input class="form-control" type="file" name="org_stamp" accept="image/png" />													
					</div> 									
				</div>      
			</form>
		</div>
		<div class="row round-block">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px 0; padding:0;"> 
				<h3>Список сотрудников</h3>
				<p>Подключенные пакеты:</p>
				<table class="table table-striped table-hover table-dark">				
					<thead>
						<tr>
							<th scope="col">Пакет</th>
							<th scope="col">Количество</th>
						</tr>
					</thead>
					<tbody>
					<tr><td>Поиск</td><td><?php echo $count_grant_search_enable.'/'.$org['count_grant_search']; ?></td></tr>
					<tr><td>Поиск+</td><td><?php echo $count_grant_full_search_enable.'/'.$org['count_grant_full_search']; ?></td></tr>
					</tbody>
				</table>
				<form action="/settings/changegrant" method="POST" id="frmChangeGrant">
				<table class="table table-striped table-hover table-dark">
					<thead>
						<tr>
							<th scope="col">Логин</th>
							<th scope="col">Имя</th>
							<th scope="col">Доступ</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					if(isset($users)){
						foreach($users as $org_user){						
							echo $org_user["is_activate"] == 1 ? '<tr>':'<tr class="not-activated">';
							echo '<td>'.$org_user["login"].'</a></td>';
                            echo '<td>'.$org_user["full_name"];
							if($org_user["is_admin"] == 1)
                                echo ' (Администратор)';
							echo '</td>';
							echo '<td><select class="grant-select" name="grants['.$org_user["id"].']">';
							if($org_user["id_grant"] == 555 ){
								if($this->user_model->IsGranted(555))
									echo '<option value="555" selected>Техподдержка</option>';
								else
									echo '<option value="2" selected>Поиск+</option>';
							}
							else{
								echo '<option value="0"'.($org_user["id_grant"] == 0?' selected':'').'>Отключен</option>';
								echo '<option value="1"'.($org_user["id_grant"] == 1?' selected':'').'>Поиск</option>';
								echo '<option value="2"'.($org_user["id_grant"] == 2?' selected':'').'>Поиск+</option>';
							}
                            echo '</select></td>';
							echo '<td><a class="btn btn-default delete-user" login="'.$org_user["login"].'"><i class="fa fa-times" aria-hidden="true"></i></a></td>';
							echo '</tr>';
						}
					}
					?>
					</tbody>
				</table>
				<br/><input type="submit" class="btn btn-primary btn-md" value="Обновить">
				</form>
			</div>
		</div>
		<?php } ?>
		<div class="row round-block">
			<h3>Личные</h3>
			<form method="post" action="/settings/save" enctype="multipart/form-data" class="settings-form"> 	
				<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">		
					<p>Ваше имя</p>
					<input class="form-control" type="text" name="firstname" id="firstname" placeholder="Иван" value="<?php echo $user["full_name"] ?>" required>
					<p>Ваш телефон</p>
					<input class="form-control" type="text" name="phone" id="phone"  placeholder="+7-999-9999-5849" value="<?php echo $user["phone"] ?>" required>
					<p>Ваш дополнительный телефон</p>
					<input class="form-control" type="text" name="phone2" id="phone2"  placeholder="+7-999-9999-5849" value="<?php if(isset($user["phone_alternative"])) echo $user["phone_alternative"] ?>">
					<p>Email</p>
					<input class="form-control" type="email" name="email" placeholder="exaple@mail.com" value="<?php echo $user["email"] ?>"/> 
					<p>Ваш слоган</p>
					<input class="form-control" type="text" name="tagline" placeholder="Найти квартиру - моя задача" value="<?php echo $user["tagline"] ?>" /> 
				
					<br/><input type="submit" name="submit" id="submit2"  class="btn btn-primary btn-md" value="Сохранить" />
					
				</div>
				<div class="col-lg-4 col-md-5 col-sm-12 col-xs-12"> 							
					<p>Фотография</p>
					<?php if(isset($user["photo"]) && !empty($user["photo"])) echo '<img class="org-logo" src="/assets/img/user/'.$user["photo"].'"/>'; ?>
					<input class="form-control" type="file" name="user_photo" id="user_photo" value="<?php echo $user["photo"] ?>" accept="image/gif, image/png, image/jpeg"/>
				</div> 		
			</form>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class='onesignal-customlink-container'></div>
			</div>
		</div>
		<div class="row round-block">
			<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">		
				<h3>Смена пароля</h3>
				<form method="post" action="/settings/changepassword" class="settings-form" id="frmChangePsw"> 
					<p>Текущий пароль</p>
					<input class="form-control" type="password" required name="password_old" id="password_old">
					<p>Новый пароль</p>
					<input class="form-control" type="password" required name="password1" id="password1">
					<p>Повторите новый пароль</p>
					<input class="form-control" type="password" required name="password2" id="password2">
					
					<br/><input type="submit" name="bChangePsw" class="btn btn-primary btn-md" value="Сменить" />
				</form>
			</div>
		</div>			
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div class="round-block"> 
			<h3>Помощь</h3>
			<p>Настройка личных параметров открывает новые возможности в отчете, например:</p>
				<p> - Указанный телефон включает кнопку быстрого звонка в отчете для клиента</p>
				<p> - Указанная "ссылка на вашу организацию на Яндекс Карте" включает карту в конце отчета</p>
			
		</div>
	<?php if(isset($is_admin) && $is_admin == 1){ ?>		
		<div class="round-block"> 
			<h3>Пригласить сотрудника</h3>
			<form method="post" class="settings-form" action="/settings/adduser" id="reg_frm"> 				
				<p>Email</p>
				<input class="form-control" type="email" required name="new_user_email" placeholder="example@ex.ru"/> 			
				<p style="font-size: 13px;"><i>Мы отправим письмо вашему сотруднику с дальнейшими указаниями</i></p>
				<br/><input type="submit" name="submit" id="submit_reg" class="btn btn-primary btn-md" value="Пригласить" style="margin:10px 0;"/>	
			</form>
		</div>

	<?php } ?>
	<div>
<div>
<script type="text/javascript">	
var badLogin;
$(document).ready(function() {
    $(".delete-user").click(function() {
		badLogin = $(this).attr('login');
		console.log(badLogin);
		$.ajax({
            type: "POST",
            url: "/settings/trydeleteuser/",
			data: "user_login="+badLogin,
            success: function(data) {
                console.log(data);
				var json = JSON.parse(data);
                console.log(json);
				if(json['offers'] && json['offers']>0){
					var html = '<div>';
					html+= '<h4>У данного пользователя '+json['offers']+' объявлений.<br>На кого их передать?</h4>';
					html+= '<select id="cbNewOwner">';
					$.each(json['users'], function (i, user) {
						console.log(user);	
						if(user['email'] == badLogin) return;
						html+= '<option value="'+user['id']+'">'+user['name']+' ('+user['phone']+')</option>';
					});
					html+= '</select>';
					html+= '<br><button onclick="deleteUser()">Передать</button>';
					html+= '</div>';
					$("#modalContent").html(html);
					openModal();
				}else{
					deleteUser();
					// сделать прямой вызов на удаление
				}
            },
            error: function(result) {
                console.log(result);
				$("#modalContent").html('Ошибка: '+result);
				openModal();
            },
            timeout: 15 * 60 * 1000
        });
	});
    $("#frmChangePsw").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data) {
                console.log(data);
				$("#modalContent").html(data);
				openModal();
            },
            error: function(result) {
                console.log(result);
				$("#modalContent").html('Ошибка: '+result);
				openModal();
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault();
    });
    $("#frmChangeGrant").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data) {
                console.log(data);
				$("#modalContent").html(data);
				openModal();
            },
            error: function(result) {
                console.log(result);
				$("#modalContent").html('Ошибка: '+result);
				openModal();
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault();
    });
});
function deleteUser(){
	var $preloader = $('#page-preloader'),
		$spinner = $preloader.find('#spinner');
	$spinner.text("Увольняем сотрудника...");
	$spinner.fadeIn();
	$preloader.delay(200).fadeIn('slow');
	var ownerId = $('#cbNewOwner').val();
	console.log(badLogin+' -> '+ownerId);
	$.ajax({
		type: "POST",
		url: "/settings/deleteuser/",
		data: "user_login="+badLogin+"&new_owner_id="+ownerId,
		success: function(data) {
			$spinner.fadeOut();
			$preloader.delay(50).fadeOut('slow');  
			console.log(data);
			$("#modalContent").html(data);
			openModal();
		},
		error: function(result) {
			$spinner.fadeOut();
			$preloader.delay(50).fadeOut('slow');  
			console.log(result);
			$("#modalContent").html('Ошибка: '+result);
			openModal();
		},
		timeout: 15 * 60 * 1000
	});
}
$(document).ready(function() {   
    $('#yageohelp').click(function() {		
		$("#modalContent").html("1. Откройте яндекс Карты<br>2. Найдите вашу организцаию и нажмите кнопку поделиться<br>3. Скопируйте последнюю часть ссылки (после последнего знака \'/\')<br>4. Вставьте это значение в это поле");
		openModal();
    });

    $("#reg_frm").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data) {
                console.log(data);
				$("#modalContent").html(data);
				openModal();
            },
            error: function(result) {
                console.log(result);
				$("#modalContent").html('Ошибка: '+result);
				openModal();
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault();
    });
});

</script>