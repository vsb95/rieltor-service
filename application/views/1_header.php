<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="referrer" content="no-referrer" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title><?php echo $title;?></title>
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="<?php echo $title;?>" />
	<meta property="og:description"        content="Пожалуй, самый лучший сервис для риэлторов" />
	<meta property="og:image"              content="/favicon.ico" />

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="/assets/css/modal.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery.min.js"><\/script>')</script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Custom styles for this template -->
    <link href="/assets/css/main.css?2" rel="stylesheet">

<!-- Bootstrap core JavaScript
================================================== -->
<script src="/assets/js/bootstrap.min.js"></script>

    <?php    
    if(isset($scripts) && !empty($scripts)){
      foreach($scripts as $script){
        echo "<script src=\"".$script."\"></script>";
      }
    }
    if(isset($styles) && !empty($styles)){      
      foreach($styles as $style){
        echo "<link href=\"".$style."\" rel=\"stylesheet\">";
      }
    }
      
    ?>
  </head>

  <body>
    <div class='overlay overlay_ noprint'>
      <div class='modal-page' id='modal1'>
        <div class='content' id='modalContent'>	        
        </div>  
      </div> 
    </div> 
    <div>
    <div class="container-fluid" style="margin:0; background:rgba(255, 255, 0, 0.235)">
      <div class="row" style="background: transparent;padding:0; margin:0;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p style="text-align: center;">Это устаревший дизайн, в скором времени эта страница будет переведена на <a href="/">новый дизайн</a>.</p>
        </div>
    </div>
  </div>

    <div class="container-fluid" id="navbar-container">
    <nav class="navbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="/home" class="navbar-brand"><img  src="/assets/img/logo.png" alter="Главная"/></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" 
                  data-toggle="dropdown" role="button" 
                  aria-haspopup="true" aria-expanded="false">ПЛОЩАДКИ<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href= <?php echo ($this->user_model->IsGranted(2)?'"/work"':'"#" class="ungranted"'); ?>>Сформировать отчет</a></li>
                  <li><a href= <?php echo ($this->user_model->IsGranted(2)?'"/work/cutter"':'"#" class="ungranted"'); ?>>Скачать фото</a></li>     
                  <li><a href= <?php echo ($this->user_model->IsGranted(1) || $this->user_model->IsGranted(2)?'"/board"':'"#" class="ungranted"'); ?>>Поиск</a></li>
                </ul>
              </li>
              <li><a href="/mls/search">МЛС</a></li>
              <li><a href="https://yadi.sk/d/vfoYPFopkRjmeQ">ОБУЧЕНИЕ</a></li>
              
              <li><a href="/events">МЕРОПРИЯТИЯ</a></li>
              <?php
                $isAdmin = $this->session->userdata('is_admin');
                if(isset($isAdmin) && !empty($isAdmin) && $isAdmin == "1"){
                  echo '<li class="dropdown">';
                  echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">АДМИНИСТРИРОВАНИЕ<span class="caret"></span></a>';
                  echo '<ul class="dropdown-menu">';
                  echo '<li><a href='.($this->user_model->IsGranted(1) || true || $this->user_model->IsGranted(2)?'"/adminpanel/reports"':'"#" class="ungranted"').'>Отчеты сотрудников</a></li>';
                  echo '<li><a href='.($this->user_model->IsGranted(2) || true?'"/adminpanel/statistic"':'"#" class="ungranted"').'>Статистика по сотрудникам</a></li>';
                  //superadmin 
                  if($this->user_model->IsLoginedAsSuperAdmin()){
                      echo '<li role="separator" class="divider"></li>';
                      echo '<li><a href="/credit/edit">Редактор ставок по ипотеке</a></li>';
                      echo '<li><a href="/super/panel">Панель</a></li>';
                      echo '<li><a href="/work/actualize">Актуализировать</a></li>';
                      echo '<li><a href="/super/log">Лог</a></li>';
                      echo '<li role="separator" class="divider"></li>';
                      echo '<li><a href="/super/org/">Список организаций</a></li>';
                      echo '<li><a href="/super/org/add">Добавить организацию</a></li>';
                      echo '<li><a href="/super/grantlist">Список грантов</a></li>';
                      echo '<li role="separator" class="divider"></li>';
                      echo '<li><a href="/super/email">Отправить письмо</a></li>';
                      echo '<li><a href="/map/">Зоны</a></li>';
                  }else if($this->user_model->IsGranted(555)){
                    echo '<li><a href="/credit/edit">Редактор ставок по ипотеке</a></li>';
                    echo '<li><a href="/map/">Зоны</a></li>';
                  }
                  echo '</ul>';
                  echo '</li>';
                }
              ?>              
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" 
                  data-toggle="dropdown" role="button" 
                  aria-haspopup="true" aria-expanded="false">ЛИЧНЫЙ КАБИНЕТ <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href=<?php echo ($this->user_model->IsGranted(1) || $this->user_model->IsGranted(2)?'"/reports"':'"#" class="ungranted"'); ?>>Мои отчеты</a></li>
                  <li><a href=<?php echo ($this->user_model->IsGranted(1) || $this->user_model->IsGranted(2)?'"/favorites"':'"#" class="ungranted"'); ?>>Избранное</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="/settings">Настройки</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="/auth/logout">Выход</a></li>
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
 </div>
<div id="page-preloader" style="display: none;"><span id="spinner">Загрузка...</span></div>

    <div class="container">
    <?php if(isset($help_video)){?>
    
      <div class="row" id="help-slider"> 
			<a href="#" class="help-spoiler-trigger"><span>Помощь</span></a>
      <div class="row" id="help-row" style="display:none">    
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding:0;">
          <iframe width="100%" src="<?php echo $help_video; ?>" height="200" frameborder="3" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <p><?php echo $help_description; ?></p>
          </div>
      </div></div>
          <?php }?>
      
