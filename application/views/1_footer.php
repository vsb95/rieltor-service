<div style="height: 80px;"></div>
</div><!-- /.container -->

<script type="text/javascript" src="https://vk.com/js/api/openapi.js?160"></script>

<!-- VK Widget -->
<div id="vk_community_messages"></div>
<script type="text/javascript">
VK.Widgets.CommunityMessages("vk_community_messages", 171557172, {disableExpandChatSound: "1",tooltipButtonText: "Техподдержка", disableButtonTooltip : 0});
</script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
<script type="text/javaScript">
    $(document).ready(function() {
        // get current URL path and assign 'active' class
        var pathname = window.location.pathname;
        $('.nav > li > a[href="'+pathname+'"]').parent().addClass('active-nav');
    });
    
    //$(window).on('load', function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.fadeOut();

    $(document).on('click', '.help-spoiler-trigger', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('#help-row').slideToggle(300);			
    });
  //});
</script>
<?php if(!$this->user_model->IsGranted(555)){ ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(51957308, "init", {
        id:51957308,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/51957308" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<?php } ?>
<!-- /Yandex.Metrika counter -->
<!--<script type="text/javascript" src="//effect.com/app/app.php?widget2&hash=ed81519b054234b994a835b66fac465c"></script>-->
<script src="/assets/js/modal.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>