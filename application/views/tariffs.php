<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .content {
        background-color: white;
        padding: 25px;
        box-shadow: 0px 0 5px rgba(200, 200, 200, 0.2);
        border-radius: 7px;
        border: 1px solid rgb(228, 226, 226);
        overflow: hidden;
    }
    .content ul{
        padding: 0;
        margin:5px;
    }
    .cell-cost ul li{
        list-style: none;
    }
    .cell-name{
        font-weight: bold;
    }
    th{
        text-align: center;
    }
</style>
<div class="row content">
    <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-hover table-dark" id="grid">
            <thead>
                <tr>
                    <th scope="col" class="col-md-2">Наименование</th>
                    <th scope="col" class="col-md-6">Что входит</th>
                    <th scope="col" class="col-md-5">Цена <br>
                        (для членов проф. объединений)<br>
                        (Бесплатный период до 1 июля)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="cell-name">Пакет МЛС</td>
                    <td class="cell-list">
                        <ul>
                            <li>Мультилистинг</li>
                            <li>Автовыгрузка объектов более чем на 25 площадок недвижимости (в стоимость уже включены сайты МЛСН, Авито, Домофонд, Циан и другие)  + ваш личный сайт недвижимости</li>
                            <li>Раздел Новостройки</li>
                            <li>Анализ цен</li>
                            <li>Лидогенерация</li>
                            <li>Отзывы по домам </li>
                            <li>Раздел обучение</li>
                            <li>Календарь мероприятий</li>
                            <li>Черный список</li>
                            <li>Чат профессионалов по недвижимости</li>
                            <li>Сервис “спрос”</li>
                            <li>Режим просмотра “с клиентом”</li>
                            <li>Система межрегиональных сделок</li>
                        </ul>
                    </td>
                    <td class="cell-cost">                        
                        <ul>
                            <li>- 980р. до 10 пользователей</li>
                            <li>- 1380р. свыше 10 пользователей</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Поисковая система по всем площадкам недвижимости</td>
                    <td class="cell-list">
                        <ul>
                            <li>Автоматический поиск объектов недвижимости одновременно более чем по 10 площадкам недвижимости</li>
                        </ul>
                    </td>
                    <td class="cell-cost">                        
                        <ul>
                            <li>- 950р. до 10 пользователей</li>
                            <li>- 1350р. свыше 10 пользователей</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Автоматизация формирования отчетов по объектам клиенту</td>
                    <td class="cell-list">
                        <ul>
                            <li>Автоматическое формирование отчета: <a href="/home/help#search_report">из поиска</a> и <a href="/home/help#url_report">из ссылок</a></li>
                            <li><a href="/home/help#report_refresh">Актуализация отчета в 1 клик</a></li>
                            <li>Формирование нового отчета в 1 клик</li>
                            <li>Аналитика для руководителя</li>
                            <li>Возможность отправки отчета в соц. сети/ мессенджеры</li>
                        </ul>
                    </td>
                    <td class="cell-cost">                        
                        <ul>
                            <li>- 1500р. за пользователя</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Сервис Ипотека</td>
                    <td class="cell-list">
                        <ul>
                            <li>Фильтр по всем банкам</li>
                            <li>Онлайн подача заявки</li>
                        </ul>
                    </td>
                    <td class="cell-cost">                        
                        <ul>
                            <li>- 600р. до 10 пользователей</li>
                            <li>- 1100р. свыше 10 пользователей</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Скриптор</td>
                    <td class="cell-list">
                        <ul>
                            <li>Программа под ключ</li>
                        </ul>
                    </td>
                    <td class="cell-cost">                        
                        <ul>
                            <li>- 600р. до 10 пользователей</li>
                            <li>- 1100р. свыше 10 пользователей</li>
                        </ul>
                    </td>
                </tr>                
            </tbody>
        </table>
        <p>* Цена для компаний, которые не входят ни в 1 из 2 проф. объединений <strong>+30%</strong></p>
        <div>
            <p>Скидка при оплате: </p>
            <ul style="margin-left: 20px;">
                <li>За 3 месяца:  <strong>-5%</strong></li>
                <li>За 6 месяцев: <strong>-10%</strong></li>
                <li>За год: <strong>-15%</strong></li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>