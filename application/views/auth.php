<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<title>Добро пожаловать</title>
	<meta property="og:url"                content="http://rieltor-service.ru" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="Добро пожаловать" />
	<meta property="og:description"        content="Пожалуй, самый лучший сервис для риэлторов" />
	<meta property="og:image"              content="/favicon.ico" />
		<!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="/assets/css/auth.css" rel="stylesheet">
    <script src="/assets/js/jquery_1.12.4.min.js"></script>
    <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery.min.js"><\/script>')</script>
</head>
<body>	
	<table>
		<tr>
			<td class="img">
				<!--<img src="/assets/img/auth.png"/>				-->
			</td>
			<td>
				<div class="panel">
					<p> <span class="sp1">риэлтор</span><span class="sp2">сервис</span></p>
					<p class="description">Закрытая риэлторская база данных</p>
					<div class="login-page form">
						<form action="/auth/forgive" method="POST" id="forgive_frm" style="display: none;" >
							<input type="email" style="visibility: collapse;"/>
							<input type="email" required placeholder="Email" name="email"/>
							<button class="btn-main" style="width:48%; padding:18px 3px;">Восстановить</button>
							<p class="message"  style="width:48%; display: inline-block; padding:0;"><a href="#">Вспомнил</a></p>
						</form>
						<form action="/auth/login" method="POST" id="login_frm">
							<input required name="username" type="text" placeholder="Логин"/>
							<input required name="password" type="password" placeholder="Пароль"/>
							<button class="btn-main" style="width:48%;">Вход</button>
							<div class="message" style="width:48%; display: inline-block; padding:0;"><a href="#">Забыли пароль?</a></div>
						</form>
					</div>
				</div>
			</td>
</tr>
</table>
</body>
	<script src="/assets/js/bootstrap.min.js"></script>
<script type="text/javascript">	
		$('.message a').click(function(){
			$('form').animate({height: "toggle", opacity: "toggle"}, "slow");
		});
$(document).ready(function() {			
	var requestUrl = "<?php echo isset($request) ? $request : '/mls/search'; ?>";
	$("#login_frm").submit(function(e) {
		var form = $(this);
		var url = form.attr('action');
		$.ajax({
			type: "POST",
			url: url,
			data: form.serialize(),
			success: function(data)
			{
				if(data =="OK"){
					window.location.href = requestUrl;
				}
				else
					alert(data);
			},
			error: function(result) {
				console.log(result);
				var json = JSON.parse(result);
				alert('error: '+JSON.stringify(json));
				document.body.appendChild(document.createTextNode(JSON.stringify(json)));
				console.log(JSON.stringify(json));
			},
			timeout: 15*60*1000
		});
		e.preventDefault();
	});
		
	$("#forgive_frm").submit(function(e) {
		var form = $(this);
		var url = form.attr('action');
		$.ajax({
			type: "POST",
			url: url,
			data: form.serialize(),
			success: function(data)
			{
				alert(data);
			},
			error: function(result) {
				console.log(result);
				var json = JSON.parse(result);
				alert('Ошибочка: '+JSON.stringify(json));
				document.body.appendChild(document.createTextNode(JSON.stringify(json)));
				console.log(JSON.stringify(json));
			},
			timeout: 15*60*1000
		});
		e.preventDefault();
	});
});
	</script>
</html>