<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="margin:auto; background-color: white; max-width:500px; padding:35px">
    <?php 
        if(isset($head))
            echo '<h3>'.$head.'</h3>'; 
        if(isset($description)){
            echo '<br>';
            echo '<h4>'.$description.'</h4>'; 
        }
    ?>
</div>