<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if(!isset($page) || empty($page))
	return;
?>	

<div class="row search_result">			
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<?php	
		if(isset($page['images']) && !empty($page['images'])){
			echo '<div class="fotorama" data-nav="false" data-maxwidth="100%" data-height="300" data-fit="cover">';
			foreach($page['images'] as $imagePath)
				echo '<img src="'.$imagePath.'">';
			echo '</div>';
		}
		?>
	</div>
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 announce">
	<?php if(isset($isForFavorite) && !empty($isForFavorite) && $isForFavorite) echo '<a class="btn btn-default delete-favorite" id="'.$page["id"].'"><i class="fa fa-times" aria-hidden="true"></i></a>';	?>
	
		<a target="_blank" href="/board/details/<?php echo $page['id']; ?>">
			<div class="row announce_headline">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 announce_name">
					<p><?php echo $page['obj-type']; ?></p>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 announce_price">
					<p><?php							
						if($page['cost'] == -1) {
							echo 'Договорная';
						}
						else{
							echo number_format($page['cost'], 0, ',', ' ').'&#8381;'; 
						}
					?></p>
				</div>
			</div>
			<div class="row announce_adress">	
				<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">					
					<?php
					$district = '';					
					if(isset($page["adress"]["micro-district"]) && !empty($page["adress"]["micro-district"]))
					{
						$district = '<span>  ('. $page["adress"]["micro-district"].')</span>';
					} 
					else if(isset($page["adress"]["district"]) && !empty($page["adress"]["district"]))
						$district = '<span>  ('. $page["adress"]["district"].')</span>';
					echo '<p>'.$page['adress']['street'].'</p>'.$district; 
					?>			
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 announce_price">
					<?php					
						if($page['cost'] == -1 && isset($page["size"]) && !empty($page["size"]) 
							&& isset($page["rent-type"]) && !empty($page["rent-type"]) && $page["rent-type"] == 1)
								echo '<span class="sub-price"> ('.number_format ($page['cost']/$page['size'],0,"."," ").' &#8381;/кв.м)&nbsp;&nbsp;</span>';
					?>
				</div>
			</div>
		</a>
		<div class="row announce_param">
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p>Площадь</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p>Этаж</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p>Источник</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p>Обновлено</p>
			</div>
		</div>
		<div class="row announce_numbers">
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p><?php if(isset($page['size'])) echo $page['size'].'м²'; ?></p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p><?php echo $page['floor'].'/'.(isset($page["adress"]['max-floor']) && !empty($page["adress"]['max-floor'])?$page["adress"]['max-floor']:''); ?></p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p><a target="_blank" href="<?php echo $page['url']; ?>"><?php echo $page['page-source']; ?></a></p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<p><?php if (($timestamp = strtotime($page['date-last-refresh'])) !== false) {echo date('d.m   H:i', $timestamp);}?></p>
			</div>
		</div>
		<div class="row announce_discription">
			<p><?php echo isset($page['description']) ? mb_substr(strip_tags($page['description']),0,400, "utf-8").'........':'Описания нет'; ?></p>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 announce_check">
				<label class="checkbox-inline">
					<input type="checkbox" class="checkbox select-adv" name="selected_adv[]" value="<?php echo $page['id']; ?>">
					<span class="pseudocheckbox">В отчет</span>
				</label>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<?php 
			if(!isset($isForFavorite) || empty($isForFavorite) || !$isForFavorite){
				echo '<form>';
				echo '					
				<input type="hidden" name="id" value="'.$page["id"].'" />
				<input type="hidden" name="is_mls" value="0"/>
				<input type="hidden" name="deal_type" value="'.($page["rent-type"] == 2?'rent':'sell').'" />
				<input type="hidden" name="obj_type" value="'. $page["obj-type"].'" />	
				';
				echo '<a class="btn btn-default set-favorite set-favorite-small"><i class="'.(isset($page["is_liked"]) && $page["is_liked"] == 1 ?'fa':'far').' fa-heart" aria-hidden="true"></i> Избранное</a>';
				echo '</form>';
			}
			?>
			</div>
		</div>
	</div>
</div>