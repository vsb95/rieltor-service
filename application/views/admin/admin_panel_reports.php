<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form action="/adminpanel/reports/">
            Дата от:
            <input type="date" name="dtStart" value="2018.06.25">
            Дата до:
            <input type="date" name="dtEnd" value="2918.06.25">
            <?php
            if(isset($users) && !empty($users)){
                //var_dump($users);
                echo ' Сотрудник: <select style="margin:0;" name="user">';
                    echo '<option value="-1" selected></option>';
                    foreach($users as $key => $value){   
                        $f = $value['id'];
                        echo '<option value="'.$value['id'].'" '.(isset($selectedUserId) && $selectedUserId == $value['id']?'selected':'').'>'
                        .(empty($value['user_name'])?$value['login']:$value['user_name']).(isset($value['org_name'])?' ('.$value['org_name'].')':'')                        
                        .'</option>';		
                    }
                    
                echo '</select>';
            }
            ?>
            <input type="submit">
        </form>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table class="table table-striped table-hover table-dark" id="grid">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Дата создания</th>
                    <th scope="col">Пользователь</th>
                    <th scope="col">Организация</th>
                    <th scope="col">Город</th>
                    <th scope="col">Создан</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                foreach($pages as $page){
                    //var_dump($page);
                    $city =$page["id_city"];
                    foreach($cities as $_city){
                        if($_city["id"] == $page["id_city"]){
                            $city =$_city["name"];
                            break;
                        }                        
                    }
                    echo '<tr>';
                    echo '<td scope="row"><a target="_blanc" href="/page/show/'.$page["id"].'?itsmy=1">'.$page["id"].'</a></td>';
                    echo '<td>'.$page["create_date"].'</td>';
                    echo '<td>'.(empty($page["user_name"]) ? $page["login"] : $page["user_name"]).(isset($page["user_id"])?' ('.$page["user_id"].')':'').'</td>';
                    echo '<td>'.$page["org_name"].'</td>';
                    echo '<td>'.$city.'</td>';
                    echo '<td>'.(isset($page["filter_fields"]) && !empty($page["filter_fields"])?'Через поиск':'Через ссылки').'</td>';
                    echo '</tr>';
                }
            ?>
            </tbody>
        </table>
    </div>
</div>
<script>
   
  </script>