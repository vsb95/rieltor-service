<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row" style="background: white; padding:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="frm">
            Дата от:
            <input type="date" name="dtStart">
            Дата до:
            <input type="date" name="dtEnd">
            <input type="hidden" name="type" value="count-offer-per-org">
            <input type="submit">
        </form>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-hover table-dark" id="grid" style="font-size: 12px;">
            <thead>
                <tr>
                    <th scope="col">Организация</th>
                    <th scope="col">Количество объектов</th>
                </tr>
            </thead>
            <tbody id="tbody">
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#frm').submit(function(e){
    $.ajax({
        type: "POST",    
        url: "/super/report/get",
        data: $(this).serialize(),
        success: function(data) {
            try {
                if (data == null) {
                    $("#modalContent").html('нет ответа от сервера');
                    openModal();
                    return;
                }
                var json = JSON.parse(data);
                console.log(json);
                var rows = '';
                if(json['status-type'] != "OK"){
                    $("#modalContent").html(json['status']);
                    openModal();
                    return;
                }
                json['rows'].forEach(function(item) {    
                    console.log(item);                
                    rows+='<tr>';
                    rows+='<td>'+item['org']['name']+'</td>';
                    rows+='<td>'+item['offer-count']+'</td>';
                    rows+='</tr>';
                });
                $('#tbody').html(rows);
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + data);
                $("#modalContent").html(data);
                openModal();
            }
        },
        error: function(result) {
            console.log(result);
            $("#modalContent").html('Ошибка: '+result);
            openModal();
        },
        timeout: 15 * 60 * 1000
    });
    e.preventDefault();
    e.stopPropagation();
  });
});
</script>