<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row" style="background: white; padding:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form action="/super/org/add_org" method="POST">
            <label for="org_name">Название организации: </label> <br/>
            <input type="text" id="org_name" name="org_name"/>
            <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="isTest" value="1"><span class="pseudocheckbox">Тестовая организация?</span></label>
            
            <hr/>

            <label for="admin_user_login">Логин администратора: </label> <br/>
            <input type="text" id="admin_user_login" name="admin_user_login"/><br/>
            <label for="admin_user_password">Пароль администратора: </label> <br/>
            <input type="text" id="admin_user_password" name="admin_user_password"/><br/>
                <hr>
            <p class="label">Регион</p><br>
            <div class="btn-group" style="width:250px;">
                <select id="cbRegion" onchange="RefreshCity(this.value)"></select>
            </div>
            <p class="label">Город</p><br>
            <div class="btn-group" style="width:250px;">
                <select name="id_city" id="cbCity"></select>
            </div>
            <hr>
            <button id="send" class="btn create-page-btn">Создать</button>
        </form>
    </div>
</div>
<script type="text/javascript">
var cities = Array();
var selectedIdCity = 643;
$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/Super/Panel/GetCities",
        success: function(data) {
            console.log(data);
            cities = JSON.parse(data);
            if (cities["status-type"] && cities["status-type"] != "OK") {
                console.log(cities);

                var opt = document.createElement("option");
                opt.innerHTML = "Настройка временно недоступна";
                var cbRegion = document.getElementById('cbRegion');
                cbRegion.appendChild(opt);

                var opt = document.createElement("option");
                opt.innerHTML = "Настройка временно недоступна";
                var cbCity = document.getElementById('cbCity');
                opt.setAttribute("value", selectedIdCity);
                opt.setAttribute("selected", "selected");
                cbCity.appendChild(opt);

                cbCity.setAttribute("disabled", "disabled");
                cbRegion.setAttribute("disabled", "disabled");
                return;
            }
            var regions = Array();
            var selectedRegion = "";
            for (var i = 0; i < cities.length; i++) {
                if (cities[i]["id"] == selectedIdCity) {
                    selectedRegion = cities[i]["region-name"];
                }

                if (!regions.includes(cities[i]["region-name"])) {
                    regions.push(cities[i]["region-name"]);
                }
            }
            regions.sort();
            var cbRegion = document.getElementById('cbRegion');
            for (var i = 0; i < regions.length; i++) {
                var opt = document.createElement("option");
                opt.innerHTML = regions[i];
                if (regions[i] == selectedRegion)
                    opt.setAttribute("selected", "selected");
                cbRegion.appendChild(opt);
            }
            RefreshCity(selectedRegion);
        },
        error: function(result) {
            console.log(result);
            var json = JSON.parse(result);
            alert('error: ' + JSON.stringify(json));
            document.body.appendChild(document.createTextNode(JSON.stringify(json)));
            console.log(JSON.stringify(json));
        },
        timeout: 15 * 60 * 1000
    });

});

function RefreshCity(value) {
    var cbCity = document.getElementById('cbCity');
    while (cbCity.childNodes.length > 0) {
        cbCity.removeChild(cbCity.childNodes[cbCity.childNodes.length - 1]);
    }
    var filteredCities = Array();
    for (var i = 0; i < cities.length; i++) {
        if (cities[i]["region-name"] == value) {
            filteredCities.push(cities[i]);
        }
    }
    filteredCities.sort(function compare(a, b) {
        if (a["name"] < b["name"]) {
            return -1;
        }
        if (a["name"] > b["name"]) {
            return 1;
        }
        return 0;
    });
    for (var i = 0; i < filteredCities.length; i++) {
        var opt = document.createElement("option");
        opt.setAttribute("value", filteredCities[i]["id"]);
        opt.innerHTML = filteredCities[i]["name"];
        if (filteredCities[i]["id"] == selectedIdCity)
            opt.setAttribute("selected", "selected");
        cbCity.appendChild(opt);
    }
};
</script>