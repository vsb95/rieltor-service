<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
select{
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    border: 1px solid #bbb;
    color: #333;
    padding: 3px;
    background: white;
    margin: 6px;
    font: 400 13.3333px Arial;
    /*text-rendering: auto;
    color: initial;
    letter-spacing: normal;
    word-spacing: normal;
    text-transform: none;
    text-indent: 0px;
    text-shadow: none;
    display: inline-block;
    text-align: start;*/
    height: auto;
}
</style>
<div class="row round-block">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form action="grantlist">
            Организация:
            <div class="btn-group" style="max-width:300px; margin-right:15px">
                <select name="id_org">
                    <option selected></option>
                <?php
                if(isset($organizations) && !empty($organizations)){
                    foreach($organizations as $key => $value){
                        echo '<option value="'.$value['id'].'">'.$value['name'].' ('.$value['id'].')</option>';		
                    }      
                }
                ?>
                </select>
            </div>
            <input type="submit" >
        </form>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-hover table-dark">
            <thead>
                <tr>
                    <th scope="col">Организация</th>
                    <th scope="col">Имя</th>
                    <th scope="col">email</th>
                    <th scope="col">Активирован</th>
                    <th scope="col">Удален</th>
                    <th scope="col">Админ</th>
                    <th scope="col">Права</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            //var_dump($organizations);
            //var_dump($users);
                foreach($users as $user){
                    echo '<tr><form class="update_grant_frm" action="/super/grantlist/updateGrant" method="post">';
                    echo '<input type="hidden" name="id_user" value="'.$user["id"].'"/>';
                    echo '<td scope="row">'.$user["org_name"].'</td>';
                    echo '<td>'.$user["full_name"].' ('.$user["id"].')</td>';
                    echo '<td>'.$user["email"].'</td>';
                    echo '<td>'.$user["is_activate"].'</td>';
                    echo '<td>'.$user["is_deleted"].'</td>';
                    echo '<td>'.$user["is_admin"].'</td>';
                    echo '<td><div class="btn-group"><select name="id_grant">';
                    foreach($grants as $grant){
                        echo '<option value="'.$grant["id"].'" '.($grant["id"] == $user["id_grant"]? ' selected':'').'>'.$grant["name"].'</option>';
                    }
                    echo '</select></div></td><td><input type="submit"></td>';
                    echo '</form></tr>';
                }
            ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row" style="height: 300px">
</div>
<script type="text/javascript">
    $(".update_grant_frm").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data) {
                console.log(data);
                alert(data);
            },
            error: function(result) {
                console.log(result);
                alert('error: ' + result);
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault();
    });
$(document).ready(function() {
});
</script>