<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row" style="background: white; padding:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a href="/super/org/add" class="btn btn-primary btn-md btn-send" style="width:150px">Добавить</a>
    <form action="/super/org/updateGrantCount" method="POST" id="frmCountGrant">
        <select name="id_org" style="margin:0; width:300px;height:35px;padding:5px;">
            <?php foreach($organizations as $org) echo '<option value="'.$org["id"].'">'.$org["name"].'</option>'; ?>
        </select>
        <input type="text" placeholder="Поиск" name="count_search" style="margin:0; width:70px;height:35px;padding:5px;">
        <input type="text" placeholder="Поиск+" name="count_full_search" style="margin:0; width:70px;height:35px;padding:5px;">
        <input type="submit">
    </form>
        <table class="table table-striped table-hover table-dark">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Колво сотрудников</th>
                    <th scope="col">Поиск</th>
                    <th scope="col">Поиск+</th>
                    <th scope="col">Город</th>
                    <th scope="col">Дата регистрации</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                foreach($organizations as $org){
                    $city =$org["id_city"];
                    foreach($cities as $_city){
                        if($_city["id"] == $org["id_city"]){
                            $city =$_city["name"];
                            break;
                        }                        
                    }
                    echo '<tr>';
                    echo '<td scope="row">'.$org["id"].'</td>';
                    echo '<td>'.$org["name"].'</td>';
                    echo '<td>'.(!isset($org["user_count"]) || empty($org["user_count"])?'0':$org["user_count"]).'</td>';
                    echo '<td>'.$org["count_grant_search"].'</td>';
                    echo '<td>'.$org["count_grant_full_search"].'</td>';
                    echo '<td>'.$city.'</td>';
                    echo '<td>'.$org["reg_date"].'</td>';
                    echo '</tr>';
                }
            ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">

$("#frmCountGrant").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data) {
                console.log(data);
                alert(data);
            },
            error: function(result) {
                console.log(result);
                alert('error: ' + result);
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault();
    });
$(document).ready(function() {
  
});
</script>