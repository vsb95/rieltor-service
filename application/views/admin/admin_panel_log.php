<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row" style="background: white; padding:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="frm">
            Дата от:
            <input type="datetime-local" name="dtStart">
            Дата до:
            <input type="datetime-local" name="dtEnd">
            <input type="text" name="id_city" style="height:auto; padding:3px; width:100px;" placeholder ="id Города">
            <div class="btn-group" style="height:auto; padding:3px; width:100px;">
                <select name="id_type" >
                    <option value="" selected>Уровень</option>
                    <option value=1>Debug</option>
                    <option value=2>Info</option>
                    <option value=3>Access</option>
                    <option value=4>Error</option>
                    <option value=5>Warning</option>
                    <option value=6>Alarm</option>
                </select>
            </div>
            <div class="btn-group" style="max-width:300px; margin-right:15px">
                <select name="id_org">
                    <option value="" selected>Организация</option>
                <?php
                if(isset($organizations) && !empty($organizations)){
                    foreach($organizations as $key => $value){
                        echo '<option value="'.$value['id'].'">'.$value['name'].' ('.$value['id'].')</option>';		
                    }      
                }
                ?>
                </select>
            </div>
            <input type="submit">
            <a class="down">Статистика</a>
        </form>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-hover table-dark" id="grid" style="font-size: 12px;">
            <thead>
                <tr>
                    <th scope="col">Дата</th>
                    <th scope="col">Уровень</th>
                    <th scope="col">Организация</th>
                    <th scope="col">Пользователь</th>
                    <th scope="col">Событие</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody id="tbody">
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.down').click(function(){
        $.ajax({
            type: "GET",    
            url: "/super/log/Statistic",
            data: $('#frm').serialize(),
            success: function(data) {
                $("#modalContent").html(data);
                openModal();
            },
            error: function(result) {
                $("#modalContent").html(data);
                openModal();
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault();
        e.stopPropagation();
    });

  $('#frm').submit(function(e){
    $.ajax({
        type: "GET",    
        url: "/super/log/get",
        data: $('#frm').serialize(),
        success: function(data) {
            try {
                if (data == null) {
                    $("#modalContent").html('нет ответа от сервера');
                    openModal();
                    return;
                }
                var json = JSON.parse(data);
                var rows = '';
                json.forEach(function(item) {
                    
                    rows+='<tr>';
                    rows+='<td>'+item['date']+'</td>';
                    rows+='<td>'+item['type_name']+'</td>';
                    rows+='<td>'+item['org_name']+'</td>';
                    rows+='<td>'+(item['user_name']?item['user_name']:item['login'])+' ('+item['user_id']+')</td>';
                    rows+='<td colspan="3">'+item['action']+'</td>';
                    rows+='</tr>';
                });
                $('#tbody').html(rows);
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + data);
                $("#modalContent").html(data);
                openModal();
            }
        },
        error: function(result) {
            console.log(result);
            $("#modalContent").html('Ошибка: '+result);
            openModal();
        },
        timeout: 15 * 60 * 1000
    });
    e.preventDefault();
    e.stopPropagation();
  });
});
</script>