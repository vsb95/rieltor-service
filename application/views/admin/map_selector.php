<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// http://webmap-blog.ru/yandex-maps/risuem-mnogougolnik-na-karte-s-pomoshhyu-api-yandeks-kart-2-x
?>

</div>
<div class="container-fluid" style="margin-bottom: 100px">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div id="map" style="height:600px"></div>
            <div id="geometry"></div>
        </div>
 
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label>
                    <input type="checkbox" id="isEnabledEditing"/> Включена отрисовка зоны
                </label>
            <input type="text" placeholder="Название зоны" id="zonename" />
            <div class="btn-main" id="add">Добавить</div>
            <form action="/map/sendNewZone" id="frm">
                <table class="table" id="coord-table">
                    <tbody>
                    </tbody>
                </table>
                <input type="submit" class="btn-main" value="Сохранить" />
            </form>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">        
            <form action="/map/getbadgeocoord" id="frm-coord" style="background-color: white;">
                <h4>Отобразить координаты, которые неудалось привязать к районам</h4>    
                <label>
                    <input type="checkbox" name="isOnlyWithoutRegion" value="1" checked/> без района
                </label>
                <label>
                    <input type="checkbox" name="isOnlyWithoutMicroRegion" value="1" checked/> без мкр
                </label><br/>
                <div class="btn-group colors" data-toggle="buttons">
                   <?php
                    for($i=0; $i< count($advTypes);$i++){
                        $advType = $advTypes[$i];
                        echo '<label class="btn btn-primary">';
                        echo '<input type="checkbox" name="objtypes[]" value='.$advType["id"].' autocomplete="off">'.$advType["name"];
                        echo '</label>';
                    }
                   ?>
                </div>
                <input type="submit" id="btnbadgeo" class="btn-main" value="Показать" />
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var jsonMkr = '<?php  if(isset($microdistricts)) echo $microdistricts;  ?>';
        var jsonDistricts = '<?php  if(isset($districts)) echo $districts;  ?>';
        // Как только будет загружен API и готов DOM, выполняем инициализацию
        ymaps.ready(init);

        var polygon;
	    var myMap;

        function init() {
            myMap = new ymaps.Map("map", {
                center: [54.967885, 73.347871],
                zoom: 12
            });
            myMap.behaviors.enable('scrollZoom');
            //Добавляем элементы управления	
            var searchControl = new ymaps.control.SearchControl({
                options: {
                    float: 'right',
                    floatIndex: 100,
                    noPlacemark: false,
                    fitMaxWidth: true,
                }
            });
            myMap.controls.add(searchControl);

            polygon = new ymaps.Polygon([[]], {}, {});
            myMap.geoObjects.add(polygon);

            // Обработка нажатия на кнопку Завершить редактирование	
            $('#add').click(
                function () {
                    $('#geometry').html('');
                    polygon.editor.stopEditing();
                    printGeometry(polygon.geometry.getCoordinates());
                    //myMap.geoObjects.remove(polygon);
                    //
                    polygon = new ymaps.Polygon([[]], {}, {});
                    myMap.geoObjects.add(polygon);
                    polygon.editor.startDrawing();
                //
                });
                loadDistrictPolygons(myMap);
        }

	function loadDistrictPolygons() {
		var objectManager = new ymaps.ObjectManager();
		var distrArray = JSON.parse(jsonDistricts);
		distrArray.forEach(function (district) {
			if (district["coords"] === undefined || district["coords"].length == 0) 
				return;
			var coords = new Array();
			district["coords"].forEach(function (coord) {
				var point = new Array(coord["lat"], coord["lon"]);
				coords.push(point);
			});
			var polygon = new ymaps.Polygon([coords],
				{
					id: district["id"],
					hintContent: district["name"]
				},
				{
					strokeWidth: 1.5,
					strokeColor: 'rgba(50, 150, 200, 0.5)',
					fillColor: 'rgba(27, 125, 190, 0.2)'
				});

			polygon.events.add('click', function (e) {
				var objectId = polygon.properties.get("id");
				loadMkrPolygons(objectId);
				myMap.geoObjects.remove(polygon);
			});
			myMap.geoObjects.add(polygon);
            loadMkrPolygons(district["id"]);
		});
	}
	function loadMkrPolygons(idDistrict) {
		var objectManager = new ymaps.ObjectManager();
		var mkrArray = JSON.parse(jsonMkr);
		mkrArray.forEach(function (mkr) {
			if (!mkr["id-district"] || mkr["id-district"] != idDistrict)
				return;				             
			if (mkr["coords"] === undefined || mkr["coords"].length == 0) 
				return;

			var coords = new Array();
			mkr["coords"].forEach(function (coord) {
				var point = new Array(coord["lat"], coord["lon"]);
				coords.push(point);
			});
			var polygon = new ymaps.Polygon([coords],
				{
					id: mkr["id"],
					hintContent: mkr["name"]
				},
				{
					strokeWidth: 1.5,
					strokeColor: 'rgba(50, 150, 200, 1)',
					fillColor: 'rgba(56, 255, 59, 0.5)'
				});

			myMap.geoObjects.add(polygon);
		});
	}

    function printGeometry(coords) {
        var name = $('#zonename').prop('value');
        $('#zonename').prop('value', '');
        var frmtStr = stringify(coords);
        $('#coords').value = frmtStr;

        var rowCount = $('#coord-table > tbody > tr').length;
        $('#coord-table > tbody:last-child').append('<tr><td><input type="text" required name="zone[' + rowCount + '][name]" value="' + name + '"/></td><td><input type="text" name="zone[' + rowCount + '][coords]" value="' + frmtStr + '"/></td></tr>');

        function stringify(coords) {
            var res = '';
            if ($.isArray(coords)) {
                res = '[';
                for (var i = 0, l = coords.length; i < l; i++) {
                    if (i > 0) {
                        res += ',';
                    }
                    res += stringify(coords[i]);
                }
                res += ']';
            } else if (typeof coords == 'number') {
                res = coords.toPrecision(6);
            } else if (coords.toString) {
                res = coords.toString();
            }
            return res;
        }
    }

    $("#frm").submit(function (e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                alert(data); // show response from the php script.
            },
            error: function (result) {
                console.log(result);
                var json = JSON.parse(result);
                alert('error: ' + JSON.stringify(json));
                document.body.appendChild(document.createTextNode(JSON.stringify(json)));
                console.log(JSON.stringify(json));
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $("#frm-coord").submit(function (e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                console.log(data);
                var coordList = JSON.parse(data)["coordinates"];
                coordList.forEach(function (keyValue) {
                    var coord = keyValue["Key"];
                    var description = keyValue["Value"];
                    console.log(coord);
				    var point = new Array(coord["lat"], coord["lon"]);
                    var placemark = new ymaps.Placemark(point,{
                        balloonContent: description
                    });
                    myMap.geoObjects.add(placemark);
                });
                //$("#frm-coord").hide();
                //$("#btnbadgeo")[0].disabled = true;
            },
            error: function (result) {
                console.log(result);
                var json = JSON.parse(result);
                alert('error: ' + JSON.stringify(json));
                document.body.appendChild(document.createTextNode(JSON.stringify(json)));
                console.log(JSON.stringify(json));
            },
            timeout: 15 * 60 * 1000
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    $("#isEnabledEditing").change(function () {
        if(this.checked) {
            polygon.editor.startDrawing();
        }else{
            polygon.editor.stopEditing();
        }
    });
</script>