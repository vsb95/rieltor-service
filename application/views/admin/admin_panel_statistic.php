<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form action="/adminpanel/Statistic/">
            Дата от:
            <input type="date" name="dtStart" value="2018.06.25">
            Дата до:
            <input type="date" name="dtEnd" value="2918.06.25">
            <input type="submit">
        </form>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table class="table table-striped table-hover table-dark" id="grid">
            <thead>
                <tr>
                    <th scope="col">Пользователь</th>
                    <th scope="col">Создано отчетов</th>
                    <th scope="col">Открыто клиентами</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                foreach($rows as $row){
                    //var_dump($row);
                    echo '<tr>';
                    echo '<td>'.(empty($row["user_name"]) ? $row["login"] : $row["user_name"]).'</td>';
                    echo '<td>'.$row['created_count'].'</td>';
                    echo '<td>'.$row['opened_count'].'</td>';
                    echo '</tr>';
                }
            ?>
            </tbody>
        </table>
    </div>
</div>
<script>
   
  </script>