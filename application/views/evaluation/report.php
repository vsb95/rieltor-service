<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
      @media print
      {    
          .print
          {
              display: block !important;
          }
          .noprint, .noprint *
          {
              display: none !important;
          }
          *{
            font-size: 12px;
          }
          
          @page { margin: 0; }
          body { margin: 1.3cm; }
          h2{
            font-size: 18px !important;
          }
          h2 span{
              display: inline;
          }
      }
      body p{
        text-align: left;
      }
      .table>tbody>tr>td{
        text-align: left;
      }
      #user_table{
          width:100%;
      }
      #user_table td{
        padding: 0px 5px;
      }
    </style>
</head>
<body>
    <div class="print" style="display:none">
        <table id="user_table">
            <tbody>
                <tr>
                    <td>
                        <?php if(isset($user['org']['logo_name']) && !empty($user['org']['logo_name']))  echo' <img src="/assets/img/logo/'.$user['org']['logo_name'].'" style="max-height:100px"/>'; ?>
                    </td>
                    <td>
                        <table >
                            <tbody>
                                <?php
                                    if(isset($user['org']['name']) && !empty($user['org']['name']))
                                        echo '<tr><td>'.$user['org']['name'].'</td></tr>';
                                        
                                    if(isset($user['org']['url']) && !empty($user['org']['url']))
                                        echo '<tr><td>'.$user['org']['url'].'</td></tr>';
                                    
                                    if(isset($user['org']['adress']) && !empty($user['org']['adress']))
                                        echo '<tr><td>'.$user['org']['adress'].'</td></tr>';
                                ?>                                
                            </tbody>
                        </table>
                    </td>

                    <td>
                        <?php if(isset($user['photo']) && !empty($user['photo']))  echo' <img src="/assets/img/user/'.$user['photo'].'" style="max-height:100px"/>'; ?>
                    </td>
                    <td>
                        <table >
                            <tbody>
                                <?php
                                    if(isset($user['full_name']) && !empty($user['full_name']))
                                        echo '<tr><td>'.$user['full_name'].'</td></tr>';                                        
                                    if(isset($user['phone']) && !empty($user['phone']))
                                        echo '<tr><td>'.$user['phone'].'</td></tr>';                                    
                                    if(isset($user['phone_alternative']) && !empty($user['phone_alternative']))
                                        echo '<tr><td>'.$user['phone_alternative'].'</td></tr>';
                                    if(isset($user['email']) && !empty($user['email']))
                                        echo '<tr><td>'.$user['email'].'</td></tr>';
                                ?>                                
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <img src="/assets/img/logo.png" style="float: right;"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <h2>Результат оценки <span class="print" style="display:none">за <?php echo date('d.m.y'); ?></span></h2>
  <div>
      <?php 
        if(!isset($pages) || empty($pages))            
            die('Не загружены или не выбраны страницы для оценки');
        if(!isset($my_obj) || empty($my_obj))            
            die('Не указаны данные по оцениваемому объекту');
         
        if(!isset($my_obj['renovation']) || empty($my_obj['renovation']))            
            die('Не указан ремонт оцениваемого объекта');
        if(!isset($my_obj['floor']) || empty($my_obj['floor']))            
            die('Не указан этаж оцениваемого объекта');
        if(!isset($my_obj['furniture']) || empty($my_obj['furniture']))            
            die('Не указана мебель оцениваемого объекта');
        if(!isset($my_obj['floor_max']) || empty($my_obj['floor_max']))            
            die('Не указана этажность дома оцениваемого объекта');
        if(!isset($my_obj['size']) || empty($my_obj['size']))            
            die('Не указана площадь оцениваемого объекта');
      
        $avgCost = 0;
        $avgCostByMeter = 0;
        $materials = array();
        $districts = array();

        foreach($pages as $page){
            // договорную стоимость пропускаем
            if($page['cost'] == -1) continue;
            $avgCost += $page['cost'];
            if(isset($page["size"]) && !empty($page["size"]) 
                && isset($page["rent-type"]) && !empty($page["rent-type"]) && $page["rent-type"] == 1)
                    $avgCostByMeter += $page['cost'] / $page["size"];
            if(isset($page["adress"]) && isset($page["adress"]['house-material']) && !empty($page["adress"]['house-material']) 
                && !in_array($page["adress"]['house-material']['name'], $materials))
                $materials[] = $page["adress"]['house-material']['name'];
            if(isset($page["adress"]["district"]) && !empty($page["adress"]["district"]) && !in_array($page["adress"]["district"],$districts))            
                $districts[] = $page["adress"]["district"];
        }
        $avgCost = $avgCost / count($pages);
        $avgCostByMeter = $avgCostByMeter / count($pages);
        $adress = str_replace('Россия, Омск ','', $my_obj['adress']);
        $adress= str_replace('Россия, ','', $adress);
        
        echo '<p style="font-size: 18px;"><strong>Оцениваемый объект: '.$adress.'</strong></p>'; 
        echo '<p>Средняя стоимость по объектам сравнения: '.number_format ($avgCost,0,"."," ").' руб</p>';
        echo '<p>Средняя стоимость за кв.м: '.number_format ($avgCostByMeter,0,"."," ").' руб</p>';
        echo '<p>Районы: '.implode(", ", $districts).'</p>'; 
        echo '<p>Материалы дома по сравнительным объектам: '.implode(", ", $materials).'</p>'; 

        $percent = $avgCostByMeter / 100;
        $recomendedCostByMeter = $avgCostByMeter;

        switch($my_obj['renovation']){
            case 1: $recomendedCostByMeter += $percent*6; break;
            case 2: break;
            case 3: $recomendedCostByMeter -= $percent*5; break;
            case 4: $recomendedCostByMeter -= $percent*5; break;
            case 5: $recomendedCostByMeter -= $percent*10; break;
            case 6: $recomendedCostByMeter += $percent*10; break;
            default: die('Неизвестный ремонт');
        }
        switch($my_obj['furniture']){
            case 'full': $recomendedCostByMeter += $percent*10; break;
            case 'half': $recomendedCostByMeter += $percent*4; break;
            case 'none': $recomendedCostByMeter -= $percent*2; break;
            default: die('Неизвестная мебель');
        }
        if(isset($my_obj['isHasBalcon']) && $my_obj['isHasBalcon'] == 1)
            $recomendedCostByMeter += $percent*3;
        if(isset($my_obj['isHasLoggia']) && $my_obj['isHasLoggia'] == 1)
            $recomendedCostByMeter += $percent*4;
        if($my_obj['floor'] < 2)
            $recomendedCostByMeter -= $percent*10;
        else if($my_obj['floor'] == $my_obj['floor_max'])
            $recomendedCostByMeter -= $percent*5;

        
        $recomendedCost = $recomendedCostByMeter * $my_obj['size'];        
        echo '<p><strong>Рекомендуемая цена оцениваемого объекта: '.number_format ($recomendedCost,0,"."," ").' руб ('.number_format($recomendedCostByMeter,0,"."," ").' руб/м²)</strong></p>';
      ?>
    <p>Оценка выполнена сравнительным методом путем расчета средней арифметической с учетом использования коэффициентов</p>
    <p>Сравнительной базой послужили:</p>
      <table class="table table-striped table-hover table-dark">
        <thead>
            <tr>
                <th>Адрес</th>
                <th>Площадь</th>
                <th>Этаж</th>
                <th>Материал дома</th>
                <th>Стоимость</th>
                <th>Стоимость за м²</th>
            </tr>
        </thead>
        <tbody id="table_body">
            <?php 
                foreach($pages as $page){                    
                    $district = '';					
                    if(isset($page["adress"]["micro-district"]) && !empty($page["adress"]["micro-district"]))
                    {
                        $district = '<span>  ('. $page["adress"]["micro-district"].')</span>';
                    } 
                    else if(isset($page["adress"]["district"]) && !empty($page["adress"]["district"]))
                        $district = '<span>  ('. $page["adress"]["district"].')</span>';
                        
                    echo '<tr>';
                    echo '<td>'.$page['adress']['street'].$district.'</td>';
                    echo '<td>'.$page['size'].' м²</td>';
                    echo '<td>'.$page['floor'].'/'.(isset($page["adress"]['max-floor']) && !empty($page["adress"]['max-floor'])?$page["adress"]['max-floor']:'').'</td>';
                    echo '<td>';
                    if(isset($page["adress"]) && isset($page["adress"]['house-material']) && !empty($page["adress"]['house-material']))
                        echo $page["adress"]['house-material']['name'];
                    echo '</td>';
                    echo '<td>'.number_format ($page['cost'],0,"."," ").'</td>';
                    echo '<td>'.number_format ($page['cost']/$page['size'],0,"."," ").'</td>';
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>

  </div>     
  <div style="margin:auto;">
        <?php if(!isset($isForPrint) || !$isForPrint) echo '<a id="print-report-btn" style="max-width: 150px;" class="noprint btn btn-primary btn-md">Напечатать</a>'; ?>        
        <?php if(isset($pdfFile) && !empty($pdfFile)) echo '<a style="max-width: 150px;" class="noprint btn btn-primary btn-md" href="'.$pdfFile.'" download>Скачать</a>'; ?>
        
  </div>
</body>
</html>