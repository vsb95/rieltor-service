<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if(!isset($page) || empty($page))
	return;
?>	

<div class="row search_result"  style="min-height:260px">
	<div class="col-md-five-half 	col-lg-five-half">
		<?php	
		if(isset($page['images']) && !empty($page['images'])){
			echo '<div class="fotorama" data-nav="false" data-width="260" data-maxwidth="100%" data-height="260" data-fit="cover">';
			foreach($page['images'] as $imagePath)
				echo '<img src="'.$imagePath.'">';
			echo '</div>';
		}
		?>
	</div>
	<div class="col-md-five-three 	col-lg-five-three announce">
		<a target="_blank" href="/board/details/<?php echo $page['id']; ?>">
			<div class="row announce_headline">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 announce_name">
					<p><?php echo $page['obj-type']; ?></p>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 announce_price">
					<p><?php							
						if($page['cost'] == -1) {
							echo 'Договорная';
						}
						else{
							echo number_format($page['cost'], 0, ',', ' ').'&#8381;'; 
						}
					?></p>
				</div>
			</div>
			<div class="row announce_adress">	
				<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">					
					<?php
					$district = '';					
					if(isset($page["adress"]["micro-district"]) && !empty($page["adress"]["micro-district"]))
					{
						$district = '<span>  ('. $page["adress"]["micro-district"].')</span>';
					} 
					else if(isset($page["adress"]["district"]) && !empty($page["adress"]["district"]))
						$district = '<span>  ('. $page["adress"]["district"].')</span>';
					echo '<p>'.$page['adress']['street'].'</p>'.$district; 
					?>			
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 announce_price">
					<?php					
						if($page['cost'] == -1 && isset($page["size"]) && !empty($page["size"]) 
							&& isset($page["rent-type"]) && !empty($page["rent-type"]) && $page["rent-type"] == 1)
								echo '<span class="sub-price"> ('.number_format ($page['cost']/$page['size'],0,"."," ").' &#8381;/кв.м)&nbsp;&nbsp;</span>';
					?>
				</div>
			</div>
		</a>
		<div class="row announce_param">
			<div class="col-md-3	col-lg-3">
				<p>Площадь</p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p>Этаж</p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p>Материал дома</p>
			</div>
		</div>
		<div class="row announce_numbers">
			<div class="col-md-3	col-lg-3">
				<p><?php if(isset($page['size'])) echo $page['size'].'м²'; ?></p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p><?php echo $page['floor'].'/'.(isset($page["adress"]['max-floor']) && !empty($page["adress"]['max-floor'])?$page["adress"]['max-floor']:''); ?></p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p><?php if(isset($page["adress"]) && isset($page["adress"]['house-material']) && !empty($page["adress"]['house-material'])) echo $page["adress"]['house-material']['name']; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6	col-lg-6 announce_check">
				<label class="checkbox-inline">
					<input type="checkbox" class="checkbox select-adv" name="selected_adv[]" value="<?php echo $page['id']; ?>">
					<span class="pseudocheckbox">Добавить к оценке</span>
				</label>
			</div>
		</div>
	</div>
</div>