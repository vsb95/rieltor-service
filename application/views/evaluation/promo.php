<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .main_banks{
        border-bottom: 1px solid #999999;
        margin-bottom: 20px;
        padding-bottom: 20px;
    }
    .oth_banks img {
        margin: 4px 11px;
    }
</style>
<div class="row round-block">
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3" style="text-align: center;">
		<a href="http://omsk-etalon.ru" target="_blank"><img src="/assets/img/etalon_cropped_logo.png" style="height:100px" /></a>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
		<h3 style="margin-top:40px;">Городской юридический центр недвижимости</h3>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">    
		<h3 style="margin-top:40px;"><img src="/assets/img/ico_phone.png" style="height:35px;margin-right:10px;margin-top:-5px;" />211-335</h3>
	</div>
</div>
<div class="row round-block">
	<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
		<h3 style="margin-top:10px;text-align:right">Заказать оценку для ипотеки</h3>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
		<a href="/evaluation/etalonrequest" class="btn btn-primary btn-md" style="max-width:200px; display:block; margin:auto">Заказать</a>
	</div>
</div>
<div class="row round-block">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h3 >Отчет об оценке подготовим за 1 день </h3>
		<h4 >Единственная оценочная компания, которая аккредитована во всех банках Омска!  </h4>
		<h4 >Делаем оценку: </h4>
        <ul>
            <li><h4 >Квартир</h4></li>
            <li><h4 >Дач</h4></li>
            <li><h4 >Домов / коттеджей</h4> </li>
            <li><h4 >Коммерческой недвижимости</h4></li>
        </ul>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1 style="text-align: center;">Мы аккредитованы:</h1>
        <div class="row">
            <div class="main_banks text-center col-md-6 col-md-offset-3">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/1.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/2.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/3.jpg" alt="" title="">
            </div>
        </div>
        <div class="row">
            <div class="oth_banks col-md-12 text-center">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/4.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/5.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/6.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/7.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/8.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/9.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/10.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/11.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/12.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/13.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/14.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/15.jpg" alt="" title="">
                <img src="http://omsk-etalon.ru/wp-content/themes/etalon/images/banks_logo/16.jpg" alt="" title="">
            </div>
        </div>
    </div>
</div>