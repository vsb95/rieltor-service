<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
	.error {
    	border: 3px solid red !important;
    	border-radius: 5px 0 0 5px!important;
	}
	.adt_block>div:nth-child(1) {
		line-height: unset !important;
	}

	.adt_block p {
		margin: 10px 0 5px 0;
	}

	.checkbox-inline {
		display: block;
	}

	.sub-price {
		font-family: TTNorms-Light;
		font-size: 14px;
		color: #676671;
		letter-spacing: -0.33px;
		float: right !important;
	}

	.modal_data {
		margin-bottom: 5px;
	}

	.modal_data p,
	.modal_data input {
		display: inline !important;
		max-width: 80% !important;
		margin: 3px;
	}

	.modal_data div {
		display: inline-block !important;
		max-width: 80% !important;
		margin: 3px;
	}
</style>
<?php 	
	if(!isset($initData) || empty($initData)){
		echo '<div class="row param">';
		echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:20px;">';
		echo '<h3>Приносим свои извинения<br/><br/>Сервис временно не доступен :(</h3>';
		echo '</div>';
		echo '</div>';
		return;
	}
?>
    <div class="row round-block">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3" style="text-align: center;">
            <a href="http://omsk-etalon.ru" target="_blank"><img src="/assets/img/etalon_cropped_logo.png" style="height:100px" /></a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
            <h3 style="margin-top:40px;">Сервис создан профессиональными оценщиками</h3>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
            <a href="/evaluation/promo" class="btn btn-primary btn-md" style="margin-top:30px;">Оценка для банка</a>
        </div>
    </div>

    <div class="row param">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form id="frm-1">
				<h3 style="color:black">Шаг 1. Укажите характеристики оцениваемого объекта</h3>
				<p>Чем более полно вы заполните характеристику объекта недвижимости, который оцениваете, тем точнее будут результаты оценки!</p>
				<div class="modal_data">
					<p>Адрес:</p>
					<input type="text" name="my_obj[adress]" id="modal_adress" placeholder="Адрес..."
						class="form-control" style="display:inline;margin-left:15px; width: 80%;"
						<?php if(isset($_SESSION['name_city']) && !empty($_SESSION['name_city'])) echo 'value="'.$_SESSION['name_city'].', "'; ?> />
				</div>
				<div>
					<div class="modal_data" style="width:auto;display: inline-block;">
						<p>S = </p>
						<input type="text" name="my_obj[size]" required class="form-control" id="modal_size"
							style="width: 100px;display:inline;" /><span style="display:inline;">м²</span>
					</div>
					<div class="modal_data" style="width:auto;display: inline-block;">
						<p>Этаж</p>
						<input type="text" name="my_obj[floor]" required class="form-control" id="modal_floor"
							style="width: 50px;display:inline;" />
						<p>из</p>
						<input type="text" name="my_obj[floor_max]" required class="form-control"
							id="modal_floor_max" style="width: 50px;display:inline;" />
					</div>
				</div>

				<div>
					<div class="modal_data" style="width:auto;display: inline-block;">
						<p>Мебель</p>
						<div class="btn-group">
							<select name="my_obj[furniture]" required>
								<option value="full">Остается вся</option>
								<option value="half">Только встроенная</option>
								<option value="none">Без мебели</option>
							</select>
						</div>
					</div>
					<div class="modal_data" style="width:auto;display: inline-block;">
						<p>Ремонт</p>
						<div class="btn-group">
							<select name="my_obj[renovation]" required>
								<option value="5">Черновая отделка</option>
								<option value="4">Чистовая отделка</option>
								<option value="3">Плохой ремонт</option>
								<option value="2">Хороший ремонт</option>
								<option value="1">Евроремонт</option>
								<option value="6">Дизайнерский</option>
							</select>
						</div>
					</div>
				</div>
				<label class="checkbox-inline"><input type="checkbox" class="checkbox" name="my_obj[isHasBalcon]"
						value="1"><span class="pseudocheckbox">Есть балкон</span></label>
				<label class="checkbox-inline"><input type="checkbox" class="checkbox" name="my_obj[isHasLoggia]"
						value="1"><span class="pseudocheckbox">Есть лоджия</span></label>
				<div id="hideFrm-1" class="btn btn-primary btn-md" style="margin-top:20px;max-width:150px">Далее</div>
			</form>
   		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form id="frm" action="/evaluation/search" method="post" style="display:none">
				<h3 style="color:black">Шаг 2. Выберите квартиры, с которыми будет производиться сравнение для оценки</h3>
				<p>Залог правильной оценки - верно подобранные объекты для сравнения!</p>
				<p>Советы:</p>
				<ul>
                    <li>Укажите все параметры квартир с которыми будет производиться сравнение, они должны быть относительно близки оцениваемому объекту</li>
					<li>Выберите от 5 до 10 квартир на сравнение</li>
					<li>Выбирайте квартиры аналогичные вашей и в том же микрорайоне</li>
				</ul>
				<input type="hidden" id="lastIndex" name="IndexPage" />
				<input type="hidden" name="renttype" value="1" />
				<input type="hidden" name="IsOnlyMerged" value="1" />
				<input type="hidden" name="objTypeSelect" value="0" />
				<?php echo '<input type="hidden" name="IdCity" id="city" value="'.$initData["id_city"].'">'; ?>				
				<div class="row adt_block">
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<p>Тип продажи</p>
						<div class="radio_button"><input type="checkbox" class="radio" id="sell_type_1"
								name="selltypes[]" value="1"><label for="sell_type_1"
								class="radio-inline">Новостройка</label></div>
						<div class="radio_button"><input type="checkbox" class="radio" id="sell_type_2"
								name="selltypes[]" value="2"><label for="sell_type_2"
								class="radio-inline">Вторичка</label></div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<p>Стоимость</p>
						<input type="text" name="costmin" class="form-control" placeholder="От"style="display: inline-block; max-width:49.1%;">
						<input type="text" name="costmax" class="form-control" placeholder="До" style="display: inline-block; max-width:49.1%;">
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <p>Микрорайоны</p>
                        <div class="btn-group btn-group-map">
                            <div class="button-map" data-toggle="modal" data-target="#modalMap">Выбрать</div>
                        </div>
					</div>
				</div>
				<div class="row adt_block">
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" id="size_label">
						<p>Общая площадь</p>
						<input type="text" name="sizemin" class="form-control" placeholder="От" id="size_min"
							style="display: inline-block; max-width:49.1%;">
						<input type="text" name="sizemax" class="form-control" placeholder="До" id="size_max"
							style="display: inline-block; max-width:49.1%;">
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" id="floor_label">
						<p>Этаж</p>
						<input type="text" name="minFloor" placeholder="От" class="form-control" id="floor_min"
							style="display: inline-block; max-width:49.1%;">
						<input type="text" name="maxFloor" placeholder="До" class="form-control" id="floor_max"
							style="display: inline-block; max-width:49.1%;">
						<label class="checkbox-inline" id="IsNotLastFloorCb"><input type="checkbox" class="checkbox"
								name="IsNotLastFloor" value="1"><span class="pseudocheckbox">Не последний</span></label>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="countRoomsDiv">
						<p>Количество комнат</p>
						<div class="radio_button"><input type="radio" class="radio" id="count_room_1"
								name="CountRooms[]" value="1" checked><label for="count_room_1"
								class="radio-inline">1</label></div>
						<div class="radio_button"><input type="radio" class="radio" id="count_room_2"
								name="CountRooms[]" value="2"><label for="count_room_2" class="radio-inline">2</label>
						</div>
						<div class="radio_button"><input type="radio" class="radio" id="count_room_3"
								name="CountRooms[]" value="3"><label for="count_room_3" class="radio-inline">3</label>
						</div>
						<div class="radio_button"><input type="radio" class="radio" id="count_room_4"
								name="CountRooms[]" value="4"><label for="count_room_4" class="radio-inline">4</label>
						</div>
						<div class="radio_button"><input type="radio" class="radio" id="count_room_5"
								name="CountRooms[]" value="5"><label for="count_room_5" class="radio-inline">5+</label>
						</div>
					</div>
				</div>
				<div class="row adt_block">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<?php
									if(isset($initData["house-types"]) && !empty($initData["house-types"])){
										echo '<p>Материал дома</p>';
										for($i=0; $i< count($initData["house-types"]);$i++){
											$type = $initData["house-types"][$i];			
											echo '<label class="checkbox-inline"><input type="checkbox" class="checkbox" name="housetypes[]" value="'.$type["id"].'"><span class="pseudocheckbox">'.$type["name"].'</span></label>';		
										}
									}	
								?>
							</div>
						</div>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">						
						<input type="hidden" id="mkrs" name="microdistricts" />
						<input type="hidden" id="selDistricts" name="districts" />
						<!--
                            <p style="text-align:center"><strong>Микрорайоны</strong></p>
                            <div id="map" style="height:500px"></div>
                         -->
                        
					</div>
				</div>
				<div class="row adt_block">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<input type="submit" value="Показать" class="btn btn-primary btn-md" role="button" id="send">
					</div>
				</div>
<div class="modal fade" id="modalMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="modal-map-dialog" style="width:90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Районы и микрорайоны</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-district-map" data-toggle="tab" id="district-modal-map">На карте</a></li>
                    <li ><a href="#tab-district-checkboxes" data-toggle="tab" id="district-modal-list">Список</a></li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content" id="district-tab-content" style="height:65vh;max-height:65vh;">
                    <div class="tab-pane " id="tab-district-checkboxes">                        
                        
                    </div>
                    <div class="tab-pane active" id="tab-district-map">
                        <div style="margin:10px;">
                            <p style="margin:0;">* Однократное нажатие - выбор района\микрорайона</p>
                            <p style="margin:0;">* Двукратное нажатие - открыть микрорайоны в районе</p> 
                        </div>
                        <div id="map" style="width:100%; height:65vh"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div style="width: 50%;margin: auto;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Выбрать</button>
                    <button type="button" class="btn btn-primary btn-main">Сбросить</button>
                </div>
            </div>
        </div>
    </div>
</div>
			</form>
    	</div>
    </div>
<div class="search" id="result-row" style="display:none">
    <div class="row">
        <div class="col-md-8	col-lg-8">
            <div class="row">
                <div class="col-md-6	col-lg-6 search_stat">
                    <p id="total-by-filter"></p>
                </div>
                <div class="col-md-6	col-lg-6 search_sort">
                    <p>Сначала новые</p><img>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <form id="frmReport" action="/evaluation/report" method="post">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-md-push-8 col-lg-push-9 ">
				<div id="side_block" class="search_job"  style="z-index:999; width:100%; position: absolute">
                    <div class="row search_job_head">
                        <p>Работа с объявлениями</p>
                    </div>
                    <div class="row search_discr">
                        <a class="btn btn-primary btn-md btn-main" role="button" id="isSelectAll">Выбрать все</a>
                    </div>
                    <div class="row search_discr">
                        <input type="submit" class="btn btn-primary btn-md disabled" id="create-btn" role="button" value="Оценить" />
                    </div>
                </div>
            </div>
            <input type="hidden" name="my_obj" id="my_obj" />
            <div style="display:none" id="hidden-fields">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 col-md-pull-4 col-lg-pull-3" id="search-result">
            </div>
    	</form>
    </div>
    <div id="showMore" class="row" style="visibility:hidden;">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div id="showMoreBtn" class="btn btn-primary btn-md" style="margin-top:20px;">Показать еще</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> </div>
    </div>
</div>

<script type="text/javascript">
    var isCanSlide = false;
    var isSelectAll = false;
    var selltype = 0;

    $(document).on('submit', '#frmReport', function(e) {
        e.preventDefault();

        var $preloader = $('#page-preloader'),
            $spinner = $preloader.find('#spinner');
        $spinner.text("Оцениваем...");
        $spinner.fadeIn();
        $preloader.delay(350).fadeIn('slow');
        var url = $("#frmReport").attr('action');
        var data = $("#frmReport ").serialize() + '&' + $("#frm-1").serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(result) {
                $("#modalContent").html(result);
                openModal();
                $spinner.fadeOut();
                $preloader.delay(50).fadeOut('slow');
            },
            error: function(result) {
                $spinner.fadeOut();
                $preloader.delay(50).fadeOut('slow');
                console.log(result);
                var json = JSON.parse(result);
                alert('error: ' + JSON.stringify(json));
                document.body.appendChild(document.createTextNode(JSON.stringify(json)));
            },
            timeout: 15 * 60 * 1000
        });
    });
    $(document).on('click', '.select-adv', function(e) {
        var checkedCount = $("#search-result input:checkbox:checked").length;
        if (checkedCount < 1) {
            $('#create-btn').addClass('disabled');
        } else {
            $('#create-btn').removeClass('disabled');
        }
    });
    $(document).on('click', '#print-report-btn', function(e) {

        var doc = $("#modalContent").html();

        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write(doc);
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

    });

    $(document).ready(function() {
        $('#hideFrm-1').on("click", function() {   
            var floor = parseInt($('#modal_floor').val()); 
            var floorMax = parseInt($('#modal_floor_max').val()); 
            if(isNaN(floor)){
                alert('Не указан этаж');
                return;			
            }
            if(isNaN(floorMax)){
                alert('Не указана этажность');
                return;			
            }
            if(isNaN($('#modal_size').val())){
                alert('Не указана площадь');
                return;			
            }
            console.log(floor+'/'+floorMax);
            if (floor > floorMax) {
                alert('Указанный этаж больше этажности здания');
                return;
            }
            var isHasError = false;
            $('#frm-1 input').each(function(){
                if(!$(this)[0].validity.valid){
                    console.log($(this));
                    isHasError = true;
                    $(this).addClass('error');
                }else{
                    $(this).removeClass('error');
                }
            });
            if(isHasError)
                return;

            $('#frm-1').slideToggle(300);
            $('#frm').slideToggle(300);
        });
        $("#objTypeSelect").on("change", function(e) {
            AddFilterCheck();
        });
        $("#isSelectAll").on("click", function() {
            isSelectAll = !isSelectAll;
            $("#search-result input:checkbox").prop('checked', isSelectAll);
            var checkedCount = $("#search-result input:checkbox:checked").length;
            if (checkedCount < 1) {
                $('#create-btn').addClass('disabled');
            } else {
                $('#create-btn').removeClass('disabled');
            }
        });
        $("#isSelectAllMobile").on("change", function() {
            CheckBoxes();
        });
        var i = 1;
        $("#showMoreBtn").click(function(e) {
            i++;
            document.getElementById("lastIndex").value = i;
            getAdv();
        });
        $("#frm").submit(function(e) {
            i = 0;
            $('#search-result').html("");
            $("#lastIndex").val(i);
            e.preventDefault(); // avoid to execute the actual submit of the form.
            getAdv();
        });

        function getAdv() {
            $("#total-by-filter").html("");
            var $preloader = $('#page-preloader'),
                $spinner = $preloader.find('#spinner');
            $spinner.text("Подбираем варианты...");
            $spinner.fadeIn();
            $preloader.delay(200).fadeIn('slow');
            $("#mkrs").val(selectedMicroDistrict);
            $("#selDistricts").val(selectedDistricts);
            var form = $("#frm");
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function(result) {
                    try {
                        if (result == null) {
                            $spinner.fadeOut(1);
                            $preloader.fadeOut(1);
                            $("#modalContent").html('нет ответа от сервера');
                            openModal();
                            return;
                        }
                        var json = JSON.parse(result);
                        if (json == null || json["status-type"] == null) {
                            $spinner.fadeOut(1);
                            $preloader.fadeOut(1);
                            $("#modalContent").html('нет ответа от сервера');
                            openModal();
                            return;
                        }
                        if (json["status-type"] != "OK") {
                            $spinner.fadeOut(1);
                            $preloader.fadeOut(1);
                            $("#modalContent").html(json["status"]);
                            openModal();
                            return;
                        }
                        $("#total-by-filter").html("Найдено " + json["total-count-by-filter"] +
                            " результата");
                        if (json["html-pages"] != null) {
                            json["html-pages"].forEach(function(page) {
                                $('#search-result').append(page);
                            });
                        }
                        $('.fotorama').fotorama();

                        var showMoreDiv = document.getElementById("showMore");
                        showMoreDiv.style.visibility = 'visible';

                    } catch (error) {
                        console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                        console.log('result =  ' + result);
                        $("#modalContent").html('Ошибка ' + error.message);
                        openModal();
                    }
                    $('#result-row').css('display', 'block');
                    isCanSlide = true;
                    $spinner.fadeOut();
                    $preloader.delay(50).fadeOut('slow');
                    if($("#lastIndex").val() == 0)
                        $('html,body').animate({scrollTop: $('#result-row').offset().top},'slow');
                },
                error: function(result) {
                    $spinner.fadeOut();
                    $preloader.delay(50).fadeOut('slow');
                    console.log(result);

                    try {
                        if (result != null && result.statusText == "timeout") {
                            $("#modalContent").html(
                                "Время ожидания операции истекло. Повторите еще раз");
                            openModal();
                            return;
                        }
                        var json = JSON.parse(result);
                        alert('error: ' + JSON.stringify(json));
                        document.body.appendChild(document.createTextNode(JSON.stringify(json)));
                    } catch (error) {
                        console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                        console.log('result =  ' + result);
                        $("#modalContent").html('Ошибка: ' + result);
                        openModal();
                    }
                },
                timeout: 15 * 60 * 1000
            });
        };

        $(".cost").on("change", function() {
            var val = XFormatPrice($(this).val());
            $(this).val(val);
        });

        function XFormatPrice(_number) {
            var decimal = 0;
            var separator = ' ';
            var decpoint = '.';
            var format_string = '#';

            var r = parseFloat(_number)

            var exp10 = Math.pow(10, decimal); // приводим к правильному множителю
            r = Math.round(r * exp10) / exp10; // округляем до необходимого числа знаков после запятой

            rr = Number(r).toFixed(decimal).toString().split('.');

            b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1" + separator);

            r = (rr[1] ? b + decpoint + rr[1] : b);
            return format_string.replace('#', r);
        }
    });

    function AddFilterCheck() {
        var objTypeId = $("#objTypeSelect").val();
        if (objTypeId != 4) {
            $('#IsNotLastFloorCb').css('display', 'inline-flex');
        } else {
            $('#IsNotLastFloorCb').css('display', 'none');
        }

        if (objTypeId == 0) {
            $('#countRoomsDiv').css('display', 'inherit');
        } else {
            $('#countRoomsDiv').css('display', 'none');
        }
        if (objTypeId == 4) {
            $('#floor_label').css('display', 'none');
            $('#floor_min').css('visibility', 'hidden');
            $('#floor_min').attr("disabled", "disabled");
            $('#floor_max').css('visibility', 'hidden');
            $('#floor_max').attr("disabled", "disabled");

            $('#size_label').css('display', 'none');
            $('#size_min').css('visibility', 'hidden');
            $('#size_min').attr("disabled", "disabled");
            $('#size_max').css('visibility', 'hidden');
            $('#size_max').attr("disabled", "disabled");

        } else {
            $('#floor_label').css('display', 'block');
            $('#floor_min').css('visibility', 'inherit');
            $('#floor_min').removeAttr("disabled");
            $('#floor_max').css('visibility', 'inherit');
            $('#floor_max').removeAttr("disabled");

            $('#size_label').css('display', 'block');
            $('#size_min').css('visibility', 'inherit');
            $('#size_min').removeAttr("disabled");
            $('#size_max').css('visibility', 'inherit');
            $('#size_max').removeAttr("disabled");
        }
    }

    function CheckBoxes() {
        var isDisabledBoxes = $("#isSelectAllMobile").prop('checked') || $("#isSelectAll").prop('checked');
        $("#search-result input:checkbox").prop('checked', isDisabledBoxes);
        $("#isSelectAll").prop('disabled', false);
        $("#isSelectAllMobile").prop('disabled', false);
    }


	//добавляем таймер чтобы блок не дергался при быстрой прокрутке
	var side_block_timer = 0;
	//функция для анимации блока
	function side_block_animate() {
		var wnd = $(document).scrollTop();
		var frm = $('#frmReport').offset().top - 75;//$('div#side_block').height() / 4;
		var t = $(document).scrollTop();

		var targetOffset = 0;//$(window).scrollTop() - $(window).height() / 4;
		if (Math.abs(t) > Math.abs(frm))
			targetOffset = t;
		else
			targetOffset = frm;
		$('#side_block').stop(true) //если анимация была - останавливаем
			.animate({ top: targetOffset - frm }); //смещаем блок к верху скрола
	}
	//при скроле
	$(window).scroll(function () {
		if (!isCanSlide) return;
		//если таймер есть, сбрасываем
		if (side_block_timer) clearTimeout(side_block_timer);
		//устанавливаем таймер для анимации
		side_block_timer = setTimeout(side_block_animate, 100);
	});
</script>