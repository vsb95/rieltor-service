<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    #frm p{
        margin: 15px 0 0 0;
    }
</style>
<div class="row  round-block"  style="background-color:white; padding: 30px 20px; margin-bottom:30px">    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">     
        <form id="frm" action="/demand/create/new">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Тип недвижимости</p>
                    <div class="btn-group">
                        <select name="objtype" required>
                            <option>1-к квартира</option>
                            <option>2-к квартира</option>
                            <option>3-к квартира</option>
                            <option>4-к квартира</option>
                            <option>5-к и более квартира</option>
                            <option>Комната</option>
                            <option>Дом\коттедж\дача</option>
                        </select>       
                    </div>    
                </div>                
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Районы</p>
                    <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Выбрать</button>
                    <ul class="dropdown-menu" role="menu">
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="districts[]" value="1"><span class="pseudocheckbox">Центральный</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="districts[]" value="2"><span class="pseudocheckbox">Кировский</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="districts[]" value="3"><span class="pseudocheckbox">Ленинский</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="districts[]" value="4"><span class="pseudocheckbox">Советский</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="districts[]" value="5"><span class="pseudocheckbox">Октябрьский</span></label></li>
                    </ul>  
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Этаж</p>
                    <div class="btn-group">
                        <select name="floortype" required>
                            <option selected>не важно</option>
                            <option>не первый</option>
                            <option>не последний</option>
                            <option>не первый и не последний</option>
                        </select>       
                    </div>    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Форма расчета</p>
                    <div class="btn-group">
                        <select name="payform" required>
                            <option selected>Наличная</option>
                            <option>Ипотека</option>
                            <option>Мат.капитал</option>
                            <option>Жилищные сертификаты</option>
                        </select>       
                    </div>    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Цена</p>
                    <input type="text" name="costMin" placeholder="От" style="display: inline-block; max-width:49.1%;" class="form-control" required id="minCost">
                    <input type="text" name="costMax" placeholder="До" style="display: inline-block; max-width:49.1%;" class="form-control" required id="maxCost">
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Площадь</p>
                    <input type="text" name="sizeMin" placeholder="От" style="display: inline-block; max-width:49.1%;" class="form-control" required>
                    <input type="text" name="sizeMax" placeholder="До" style="display: inline-block; max-width:49.1%;" class="form-control" required>
                </div>     
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <p>Заметка</p>
                    <textarea name="comment" class="form-control" rows="4" placeholder="Напишите заметку для себя, например, имя и телефон клиента. Эта информация будет видна только вам"></textarea>
                </div>           
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <input type="submit" value="Создать" class="btn btn-primary btn-md" style="margin-top:25px;" >
                </div>            
            </div>  
        </form>
    </div> 
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#frm").submit(function(e) {
	    e.preventDefault();
		e.stopImmediatePropagation; 
        var costMin = $('#minCost').val();
        var costMax = $('#maxCost').val();
        if(costMin > costMax){
            $("#modalContent").html('Не верно указана цена');
            openModal(); 
            return;
        }
        var diffPercent = (costMax/costMin-1)*100;
        if(diffPercent > 40){
            $("#modalContent").html('Разница минимальной и максимальной ценой не должно превышать 40%');
            openModal(); 
            return;
        }

        var form = $("#frm");
        var url = form.attr('action');
        var data = form.serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(result) {    
                $("#modalContent").html(result);
                openModal();   
                if(result == 'Успешно создано!'){
                    setTimeout(() => {                        
					    window.location.href = '/demand/mylist';
                    }, 3000);
                }
            },        
            timeout: 5*60*1000
        });
	});
});
</script>
