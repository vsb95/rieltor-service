<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    #frm p{
        margin: 15px 0 0 0;
    }
</style>
<div class="row round-block" style="background-color:white; padding: 30px 20px; margin-bottom:30px">    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">     
        <div class="row">  
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <a href="/demand/mylist" class="btn btn-primary btn-md btn-send" style="margin:0; margin-bottom:15px"  >Мои клиенты</a>
            </div>   
        </div>
        <form id="frm_get" action="/demand/search/find">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Тип недвижимости</p>           
                    <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Выбрать</button>
                    <ul class="dropdown-menu" role="menu">
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="obj_types[]" value="1-к квартира"><span class="pseudocheckbox">1-к квартира</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="obj_types[]" value="2-к квартира"><span class="pseudocheckbox">2-к квартира</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="obj_types[]" value="3-к квартира"><span class="pseudocheckbox">3-к квартира</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="obj_types[]" value="4-к квартира"><span class="pseudocheckbox">4-к квартира</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="obj_types[]" value="5-к и более квартира"><span class="pseudocheckbox">5-к и более квартира</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="obj_types[]" value="Комната"><span class="pseudocheckbox">Комната</span></label></li>
                        <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="obj_types[]" value="Дом\коттедж\дача"><span class="pseudocheckbox">Дом\коттедж\дача</span></label></li>
                    </ul>
                </div>                
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Район</p>
                    <div class="btn-group">
                        <select name="district" required>
                            <option value="1">Центральный</option>
                            <option value="2">Кировский</option>
                            <option value="3">Ленинский</option>
                            <option value="4">Советский</option>
                            <option value="5">Октябрьский</option>
                        </select>       
                    </div>    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Этаж</p>
                    <div class="btn-group">
                        <select name="floor_type" required>
                            <option value="не важно" selected>Все этажи</option>
                            <option value="не первый">Все кроме первого</option>
                            <option value="не последний">Все кроме последнего</option>
                            <option value="не первый и не последний">Все кроме первого и последнего</option>
                        </select>       
                    </div>    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Цена</p>
                    <input type="text" name="cost_min" placeholder="От" style="display: inline-block; max-width:49.1%;" class="form-control">
                    <input type="text" name="cost_max" placeholder="До" style="display: inline-block; max-width:49.1%;" class="form-control">
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Площадь</p>
                    <input type="text" name="size_min" placeholder="От" style="display: inline-block; max-width:49.1%;" class="form-control">
                    <input type="text" name="size_max" placeholder="До" style="display: inline-block; max-width:49.1%;" class="form-control">
                </div>     
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <input type="submit" value="Показать" class="btn btn-primary btn-md" style="margin-top:25px;" >
                </div>             
            </div>
        </form>
    </div> 
</div>
<div class="row round-block" id="row_table" style="display:none">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-hover table-dark">
            <thead>
                <tr>
					<th>Тип недвижимости</th>
					<th>Этаж&nbsp;</th>
					<th>Форма расчета</th>
					<th>Площадь</th>
					<th>Цена</th>
					<th>Агент</th>
                </tr>
            </thead>
            <tbody id="table_body">

            </tbody>
        </table>
    </div>
 </div>
<div class="row" style="height: 150px;"> </div>

<script type="text/javascript">
$(document).ready(function() {
    $("#frm_get").submit(function(e) {
	    e.preventDefault();
		e.stopImmediatePropagation;  
        get();
	});  	
    function get(){
        $("#table_body").html("");
        var form = $("#frm_get");
        var url = form.attr('action');
        var data = $("#frm_get").serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(result) {    
                if(result == "null"){           
                    $("#modalContent").html('Нет ставок для выбранных банков с такими программами');
                    openModal();  
                    return;
                }
                
                var json= JSON.parse(result);
                var innerHtml = "";
                $.each( json, function(key, value ) {
                    var row = '<tr><td>'+ value["obj_type"] +"</td><td>"+ value["floor_type"] +"</td><td>"+ value["form_pay"] +"</td>";
                    row+="<td>"+ value["size_min"] + " - "+ value["size_max"] +"</td>";
                    row+="<td>"+ value["cost_min"] + " - "+ value["cost_max"] +"</td>";
                    row+="<td> ";
                    if(value["user_name"])
                        row+="<strong>"+value["user_name"]+"</strong>";                        
                    if(value["user_email"])
                        row+="<br>"+value["user_email"];
                    row+="<br>";
                    if(value["user_phone"])
                        row+=value["user_phone"];
                    if(value["user_phone_alt"])
                        row+=" / "+value["user_phone_alt"];        
                    row +="</td></tr>";
                    innerHtml+= row;
                });
                $("#table_body").html(innerHtml);
                $('#row_table').css('display', 'block');
                $('html,body').animate({scrollTop: $('#row_table').offset().top},'slow');
            },        
            timeout: 5*60*1000
        });
    }
    get();
});
</script>
