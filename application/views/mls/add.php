<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .note {
        width: 500px;
        margin: 50px auto;
        font-size: 1.1em;
        color: #333;
        text-align: justify;
    }
    #drop-area {
        border: 2px dashed #ccc;
        border-radius: 20px;
        padding: 20px;
    }
    #drop-area.highlight {
        border-color: purple;
    }
    #gallery {
        margin-top: 10px;
    }
    .button {
        display: inline-block;
        padding: 10px;
        background: #ccc;
        cursor: pointer;
        border-radius: 5px;
        border: 1px solid #ccc;
    }
    .button:hover {
        background: #ddd;
    }
    .input-group-addon{
        display: unset  !important;
    }
    #fileElem {
        display: none;
    }
    .image-item{
        display: inline-block;
        margin: 10px;
    }
    .image-item label{
        display: inline-block;
        margin: 0;
    }
    .image-item .image-container{
        display: block;
        max-width:150px;
        overflow: hidden;
    }
    .image-item .image-container img{
        max-width:150px;
        transform-origin: top left;             /* IE 10+, Firefox, etc. */
        -webkit-transform-origin: top left;     /* Chrome */       
        -ms-transform-origin: top left;         /* IE 9 */
    }
    .image-item .fa-btn{
        display: inline-block;
        cursor:pointer;
        margin-left: 5px;
    }
    .image-container:hover{
        cursor:move;
    }
    .image-container.rotate90,
    .image-container.rotate270 {
        height: 150px;
    }
    .image-container.rotate90 img {
        transform: rotate(90deg) translateY(-100%);
        -webkit-transform: rotate(90deg) translateY(-100%);
        -ms-transform: rotate(90deg) translateY(-100%);
        max-height: 150px;
    }
    .image-container.rotate180 img {
        transform: rotate(180deg) translate(-100%, -100%);
        -webkit-transform: rotate(180deg) translate(-100%, -100%);
        -ms-transform: rotate(180deg) translateX(-100%, -100%);
        max-width: 150px;
    }
    .image-container.rotate270 img {
        transform: rotate(270deg) translateX(-100%);
        -webkit-transform: rotate(270deg) translateX(-100%);
        -ms-transform: rotate(270deg) translateX(-100%);
        max-height: 150px;
    }
    #objCategory-select .select-commercial{
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-8 col-lg-8">
        <div class="adt_main">
        <form id="frm" action="/mls/add/addobject" method="POST">
        <?php if(isset($isEdit) && $isEdit)
            echo '<input type="hidden" name="Id" value="'.$advertisment['id'].'">';
        ?>
        <div class="adt_desk">
            <p class="adt_headline">Тип объявления</p>
            <div class="row adt_block" id="offer-type-row">
                <div class="col-md-4	col-lg-4">
                    <p>Тип сделки</p>
                </div>
                <div class="col-md-8	col-lg-8">
                    <div class="radio_button"><input type="radio" class="radio" id="radio_1" name="OfferType" value="rent"><label for="radio_1" class="radio-inline">Аренда</label></div>
                    <div class="radio_button"><input type="radio" class="radio" id="radio_2" name="OfferType" value="sell" ><label for="radio_2" class="radio-inline">Продажа</label></div>
                </div>
            </div>
            <div class="row adt_block" id="rent-type-row" style="display:none">
                <div class="col-md-4	col-lg-4">
                    <p>Срок аренды</p>
                </div>
                <div class="col-md-8	col-lg-8">
                    <!--<div class="radio_button"><input type="radio" class="radio" id="radio_11aag" name="RentPeriod" value="day"><label for="radio_11aag" class="radio-inline">Посуточно</label></div>-->
                    <div class="radio_button"><input type="radio" class="radio" id="radio_12dds" name="RentPeriod" value="month" checked><label for="radio_12dds" class="radio-inline">Длительная</label></div>
                </div>
            </div>
            <div class="row adt_block" id="realty-type-row-1" style="display:none">
                <div class="col-md-4	col-lg-4">
                    <p>Тип недвижимости</p>
                </div>
                <div class="col-md-8	col-lg-8">
                    <div class="radio_button"><input type="radio" class="radio" id="radio_11" name="RealtyType" value="living"><label for="radio_11" class="radio-inline">Жилая</label></div>
                    <div class="radio_button"><input type="radio" disabled class="radio" id="radio_12" name="RealtyType" value=""><label for="radio_12" class="radio-inline">Коммерческая</label></div>
                </div>
            </div>
            
            <div class="row adt_block" id="obj-type-row" style="display:none">
                <div class="col-md-4	col-lg-4">
                    <p>Объект</p>
                </div>
                <div class="col-md-8	col-lg-8">
                    <div class="btn-group btn-floor" style="width:100%">
                        <select name="ObjectCategory" id="objCategory-select">
                            <option class="select-living" value="" selected disabled>Выберите объект</option>
                            <option class="select-living" value="apartment">Квартира</option>
                            <option class="select-living select-sale"  value="apartment_new">Новостройка</option>
                            <option class="select-living" value="room">Комната</option>
                            <option class="select-living" value="house">Дом\коттедж\дача</option>
                            <option class="select-living select-sale" value="apartment_part">Доля в квартире</option>
                            <option class="select-living select-sale" value="house_part">Доля в доме\коттедже</option>
                            <option class="select-living select-sale" value="lot">Участок</option>
                            <option class="select-commercial" value="office">Офис</option>
                            <option class="select-commercial" value="building">Здание</option>
                            <option class="select-commercial" value="warehouse">Склад</option>
                            <option class="select-commercial" value="market-place">Торговая площадь</option>
                            <option class="select-commercial" value="free-space">Помещение свободного назначения</option>
                        </select>    
                    </div>                
                </div>
            </div>            
        </div>
        <div class="adt_desk" id="adress-row" style="display:none" disabled>            
            <p class="adt_headline">Адрес</p>
            <div class="row adt_block">
                <div class="input-group" style="width: 100%;">
                    <input type="text" id="address" name="Address" required placeholder="Введите адрес">
                </div>
                <!-- 
                    <iframe  src="https://www.google.com/maps/embed?pb=" width="100%" height="300" frameborder="0" style="border:0" margin="auto" allowfullscreen></iframe>
                -->
            </div>
        </div>
        <content id="obj-type-fields">
        <div class="row" >
            <!-- Сюда вставляется тело формы -->
        </div>
        </content>
        <content id="images-fields">
        </content>
        <div class="adt_desk" style="display:none" id="contact-row">
        <?php if(!isset($isEditAdmin) || !$isEditAdmin || !isset($users) || count($users) < 2){ ?>
            <p class="adt_headline">Контакты</p>
            <div class="row">
                <div class="col-md-6	col-lg-6">
                    <div class="input-group">
                        <input type="tel" id="phone" class="form-control" placeholder="Основной телефон" name="Phone" required <?php if(isset($currentUser['phone']) && !empty($currentUser['phone'])) echo 'value="'.$currentUser['phone'].'" readonly'; ?> onfocusout="phoneFormat(this)">
                    </div>
                </div>
                <div class="col-md-6	col-lg-6">
                    <div class="input-group">
                        <input type="tel" id="phone2" class="form-control" placeholder="Доп. телефон" name="PhoneAlternative" <?php if(isset($currentUser['phone_alternative']) && !empty($currentUser['phone_alternative'])) echo 'value="'.$currentUser['phone_alternative'].'" readonly'; ?> onfocusout="phoneFormat(this)">
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <p class="adt_headline">Ответсвенный пользователь</p>
            <div class="row">
                <div class="col-md-6	col-lg-6">
                    <div  class="btn-group">
                        <select name="OwnerId">
                        <?php 
                            foreach($users as $user){
                                echo '<option value="'.$user['id'].'"'.(isset($advertisment) && $advertisment['sales-agent']['id'] == $user['id'] ?' selected ':'').'>';
                                if(isset($user['name']))
                                    echo $user['name'];
                                if(isset($user['phone']))
                                    echo ' ('.$user['phone'].')';
                                echo '</option>';
                            }
                        ?>
                        </select>
                    </div>
                </div>
            </div>
            
        <?php }  ?>
        </div>
        <div class="row" id="submit-row" style="display:none">    
            <div class="col-md-4	col-lg-4">
                <input type="submit" value="<?php echo isset($isEdit) && $isEdit?'Сохранить':'Добавить' ?>" class="btn btn-primary btn-md" role="button" id="send" >
            </div>
            <div class="col-md-4	col-lg-4">
            </div>
        </div>
        </form>
    </div>
</div>
<div class="col-md-4	col-lg-4 right-menu">
    <ul class="listing">
        <li>Данные организации</li>
        <li>Адрес</li>
        <li>Об объекте</li>
        <li>О здании</li>
        <li>Фотографии и описание</li>
        <li>Цена и условия сделки</li>
        <li>Контакты</li>
    </ul>
    
    <?php if(isset($advertisement) && isset($advertisement['id']) && !empty($advertisement['id'])) var_dump($advertisement); ?>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#frm').submit(function(e){
            e.preventDefault();
            e.stopPropagation();
            if(countUnLoadedImages > 0){
                alert("Пожалуйста подождите, еще не все фото загружены.\nОсталось: "+countUnLoadedImages);
                return;
            }
            var $preloader = $('#page-preloader'),
                $spinner   = $preloader.find('#spinner');   
                $spinner.html("Добавляем объявление...");
            $spinner.fadeIn();
            $preloader.delay(350).fadeIn('slow');
            var selOfferTypeValue = $('input[name=OfferType]:checked').val();
            var objCategory = $('#objCategory-select').val();
            var frm = $("#frm");
            $("#modalContent").html('');
            $.ajax({
                type: "POST",    
                url: "/mls/add/addobject",
                data: frm.serialize(),
                success: function(data) {
                    try {
                        $('.input-angle').val(0); // сбарсываем все повороты, тк изображения уже повернуты
                        if (data == null) {
                            $spinner.fadeOut(1);
                            $preloader.fadeOut(1);
                            $("#modalContent").html('нет ответа от сервера');
                            openModal();
                            $spinner.fadeOut();
                            $preloader.fadeOut();
                            return;
                        }
                        var json = JSON.parse(data);
                        if(json['status-type'] != "OK"){
                            // заменяем у перевернутых изображений урлы
                            if(json['new-images']){
                                console.log( json['new-images'] );
                                $( ".input-src" ).each(function() {
                                    var oldUrl = $( this ).val();
                                    if(oldUrl in json['new-images'])
                                    {  
                                        $( this ).val(json['new-images'][oldUrl]);
                                        console.log('value found: ' + oldUrl +' \n=> '+json['new-images'][oldUrl]);
                                    }else{
                                        //console.log('value not found: ' + oldUrl );
                                    }
                                });
                            }
                            $("#modalContent").html(json['status']);
                            openModal();
                            $spinner.fadeOut();
                            $preloader.fadeOut();
                            return;
                        }          
                        $("#modalContent").html(json['status']);
                        openModal();              
                        setTimeout(
                            function() 
                            {
                                window.location.href = "/mls/cabinet?offerType="+selOfferTypeValue+"&objType="+objCategory;
                            }, 3000);
                    } catch (error) {
                        console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                        console.log('result =  ' + data);
                        $("#modalContent").html(data);
                        openModal();
                    }
                    $spinner.fadeOut();
                    $preloader.delay(100).fadeOut('slow');
                },
                error: function(result) {
                    console.log(result);
                    $("#modalContent").html('Ошибка: '+result);
                    openModal();
                    $spinner.fadeOut();
                    $preloader.fadeOut();
                },
                timeout: 15 * 60 * 1000
            });
        });
        $('input:radio[name=OfferType]').click(function() {
            $("#realty-type-row-1").css('display', 'block');	

            ShowRows();
            GetForm();
        });
        $('input:radio[name=RealtyType]').click(function() {
            ShowRows();
            GetForm();
        });
        $('#objCategory-select').change(function() {
            GetForm();
        });

        // Форматируем номера
        $("#phone").val(phoneFormat($("#phone").val()));
        $("#phone2").val(phoneFormat($("#phone2").val()));

        <?php 
            if(isset($isEdit) && $isEdit){
                $serialized = json_encode(json_encode($advertisment));
                echo 'FillEditForm('.$serialized.');';
            }
        ?>
    });
    
    $(document).on("change", "#cbUtilities", function() {
        var value=$('#cbUtilities :selected').attr('value');
        
        switch(value){
            case '': 
                $('#tbUtilitiesCost').css('display', 'block');
                $('#tbUtilitiesCostDescr').css('display', 'block');  
                $('#tbUtilitiesCostDescr').html('Стоимость К/У и счетчиков');
                $('#tbUtilitiesCost').removeAttr('disabled');
            break;
            case 'counter': 
                $('#tbUtilitiesCost').css('display', 'block');
                $('#tbUtilitiesCostDescr').css('display', 'block');  
                $('#tbUtilitiesCostDescr').html('Стоимость К/У');
                $('#tbUtilitiesCost').removeAttr('disabled');
            break;
            case 'utilities': 
                $('#tbUtilitiesCost').css('display', 'block');
                $('#tbUtilitiesCostDescr').css('display', 'block');  
                $('#tbUtilitiesCostDescr').html('Стоимость счетчиков');
                $('#tbUtilitiesCost').removeAttr('disabled');
            break;
            case 'full': 
            $('#tbUtilitiesCostDescr').css('display', 'none');  
            $('#tbUtilitiesCost').css('display', 'none'); 
            $('#tbUtilitiesCost').attr('disabled','disabled');
            break;
        }
    });
    $(document).on("change", "#commsission-type", function() {
        var value=$('#commsission-type :selected').attr('value');
        
        switch(value){
            case 'null':
            case 'none': 
                $('#tbCommission').css('display', 'none'); break;
            case 'percent': $('#tbCommission').css('display', 'block'); $('#tbCommission').inputmask({"mask": "99%"}); break;
            case 'fixed': $('#tbCommission').css('display', 'block'); $('#tbCommission').inputmask({"mask": "999 999 руб"}); break;
        }
    });
    function ShowRows(){
        var selOfferTypeValue = $('input[name=OfferType]:checked').val();

        if(selOfferTypeValue=="rent"){
            $("#rent-type-row").css('display', 'block');	
            if($("#objCategory-select option:selected").hasClass( "select-sale" )){
                $("#objCategory-select option:selected").attr('selected', false);
            }
            $(".select-sale").each(function(item) {
                $(this).css('display', 'none');
            });
        }else if(selOfferTypeValue=="sell"){
            $("#rent-type-row").css('display', 'none');	
            $(".select-sale").each(function(item) {
                $(this).css('display', 'block');
            });
        }else{
            console.log('bad selOfferTypeValue '+selOfferTypeValue);
        }
        
        $("#obj-type-row").css('display', 'block');

        var selRealtyType = $('input[name=RealtyType]:checked').val();
        if(selRealtyType=="living"){
            $(".select-commercial").each(function(item) {
                $(this).css('display', 'none');
            });                     
            $(".select-living").each(function(item) {
                $(this).css('display', 'block');
            });                     
        } else if(selRealtyType=="commercial") {
            $(".select-living").each(function(item) {
                $(this).css('display', 'none');
            });            
            $(".select-commercial").each(function(item) {
                $(this).css('display', 'block');
            });                     
        }  else{
            if(selRealtyType) console.log('bad selRealtyType '+selRealtyType);
        }
    }

    function GetForm(){
        var selOfferTypeValue = $('input[name=OfferType]:checked').val();
        var objCategory = $('#objCategory-select').val();
        if(!selOfferTypeValue || !objCategory){
            return;
        }
        var url = "/mls/add/GetObjectFields/?offerType="+selOfferTypeValue+"&objType="+objCategory;
        $.ajax({
            url: url,
            success: function(result) {
                $("#adress-row").css('display', 'block');	
                $('#obj-type-fields').html(result);
                $('html,body').animate({scrollTop: $('#adress-row').offset().top},'slow');
                $("#submit-row").css('display', 'block');	
                $("#contact-row").css('display', 'block');	

                dropArea = document.getElementById("drop-area");
                // Prevent default drag behaviors                    
                dropArea.addEventListener('dragenter', preventDefaults, false);
                document.body.addEventListener('dragenter', preventDefaults, false);
                //
                dropArea.addEventListener('dragover', preventDefaults, false);
                document.body.addEventListener('dragover', preventDefaults, false);
                //
                dropArea.addEventListener('dragleave', preventDefaults, false);
                document.body.addEventListener('dragleave', preventDefaults, false);
                //
                dropArea.addEventListener('drop', preventDefaults, false);
                document.body.addEventListener('drop', preventDefaults, false);

                // Highlight drop area when item is dragged over it
                dropArea.addEventListener('dragenter', highlight, false);
                dropArea.addEventListener('dragover', highlight, false);
                
                dropArea.addEventListener('dragleave', unhighlight, false);
                dropArea.addEventListener('drop', unhighlight, false);

                // Handle dropped files
                dropArea.addEventListener('drop', handleDrop, false);
                progressBar = document.getElementById('progress-bar');

                // инициализируем маски
                $(":input").inputmask();

                <?php if(isset($_SESSION['name_city']) && !empty($_SESSION['name_city'])){ ?>
                    $( "#address" ).focusin(function() {                    
                        if(!$(this).val())
                            $(this).val('<?php echo $_SESSION['name_city']; ?>, ');
                    });
                <?php  } ?>
            },
            error: function(result) {
                alert("Произошла ошибка");
                console.log("ERROR: "+result);            
            },
            timeout: 5*60*1000
        });

    }  


    function phoneFormat( elem ) {
        var re = /(?:([\d]{1,}?))??(?:([\d]{1,3}?))??(?:([\d]{1,3}?))??(?:([\d]{2}))??([\d]{2})$/;
        var val= "";
        if(!elem) return elem;
        if(typeof(elem) == "string"){
        return  elem.replace( /[^0-9]/g, '' ).replace( re, function( all, a, b, c, d, e ){
            return ( a  ?  a == 7 ? "+" + a + " " : a+" " : "" ) + ( b ? b + " " : "" ) + ( c ? c + "-" : "" ) + ( d ? d + "-" : "" ) + e;
        });
        }else
        elem.value = elem.value.replace( /[^0-9]/g, '' ).replace( re, function( all, a, b, c, d, e ){
            return ( a  ?  a == 7 ? "+" + a + " " : a+" " : "" ) + ( b ? b + " " : "" ) + ( c ? c + "-" : "" ) + ( d ? d + "-" : "" ) + e;
        })
    }
    
    function init(){
        var suggestView1 = new ymaps.SuggestView('address');
    }
</script>
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async defer></script>
<script src="//yastatic.net/share2/share.js" async defer></script>