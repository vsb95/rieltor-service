<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">Об объекте</p>
    
    <?php 
        $this->load->view('mls/add/fields/cadastrial_number');  
        $this->load->view('mls/add/fields/count_sell_rooms');  
        $this->load->view('mls/add/fields/area_room');  
        $this->load->view('mls/add/fields/count_rooms');  
        $this->load->view('mls/add/fields/floor');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/area_total');  
        $this->load->view('mls/add/fields/area_kitchen');  
        $this->load->view('mls/add/fields/balcon_loggia');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/count_toilet');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/renovation_type');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/place_type_in_building');  
    ?>
</div>
