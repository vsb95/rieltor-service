<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <?php $this->load->view('mls/add/fields/rent_furniture_fill');  ?>
</div>
<div class="adt_desk">
    <p class="adt_headline">Об объекте</p>
    <?php 
    
    $this->load->view('mls/add/fields/building_microdistrict_name'); 
    $this->load->view('mls/add/fields/area_total'); 
    $this->load->view('mls/add/fields/area_living'); 
    $this->load->view('mls/add/fields/house_total_floor'); 
    echo '<div class="row adt_block"></div>';
    $this->load->view('mls/add/fields/house_toilet_type'); 
    $this->load->view('mls/add/fields/house_heating_type'); 
    $this->load->view('mls/add/fields/renovation_type'); 
    echo '<div class="row adt_block"></div>';
    $this->load->view('mls/add/fields/rent_child_animal'); 
    echo '<div class="row adt_block"></div>';
    
    ?>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>В доме</p>
        </div>
        <div class="col-md-8	col-lg-8">
            
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Техника</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[2]" value="fridge"><span class="pseudocheckbox">Холодильник</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[3]" value="tv"><span class="pseudocheckbox">Телевизор</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[4]" value="dishwasher"><span class="pseudocheckbox">Посудомоечная машина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[5]" value="phone"><span class="pseudocheckbox">Телефон</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[6]" value="washing-machine"><span class="pseudocheckbox">Стиральная машина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                </div>
            </div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Комфорт</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[7]" value="bath"><span class="pseudocheckbox">Ванна</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[8]" value="pool"><span class="pseudocheckbox">Бассейн</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[9]" value="shower"><span class="pseudocheckbox">Душевая кабина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[10]" value="conditioner"><span class="pseudocheckbox">Кондиционер</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[11]" value="sauna"><span class="pseudocheckbox">Баня</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[12]" value="internet"><span class="pseudocheckbox">Интернет</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[13]" value="garage"><span class="pseudocheckbox">Гараж</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                </div>
            </div>
        </div>
    </div>
</div>
