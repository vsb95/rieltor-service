<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">О здании</p>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Название</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Введите название" name="BuildingMicrodistrictName">
            </div>
        </div>
    </div>
<?php if(isset($offerType) && $offerType == "sell") { 
        $this->load->view('mls/add/fields/build_year');  
    ?>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Материал и тип дома *</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="btn-group" style="display:inline;">
                <select name="BuildingMaterialType" style="width:48%;"  required>              
                    <option disabled selected>Материал</option>  
                    <option value="brick">Кирпичный</option>
                    <option value="panel">Панельный</option>
                    <option value="monolite">Монолитный</option>
                    <option value="wood">Деревянный</option>
                    <option value="brick-monolite">Монолитно-кирпичный</option>
                    <option value="concrete">Железобетонный</option>
                    <option value="metall">Металлический</option>
                    <option value="block">Блочный</option>
                </select>
            </div>
            <div class="btn-group" style="width:48%;display:inline-flex;">
                <select name="BuildingSeria" required>              
                    <option disabled selected>Тип</option>  
                    <option value="Хрущевка">Хрущевка</option>
                    <option value="Ленинградская серия">Ленинградская серия</option>
                    <option value="Омская серия">Омская серия</option>
                    <option value="Элитный">Элитный</option>
                    <option value="Полнометражный">Полнометражный</option>
                    <option value="Индивидуальный проект">Индивидуальный проект</option>
                    <option value="Улучшенный">Улучшенный</option>
                    <option value="Старый фонд">Старый фонд</option>
                    <option value="Малосемейка">Малосемейка</option>
                </select>
            </div>
        </div>
    </div>
<?php } else{ ?>    
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Новостройка</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="radio_button"><input type="radio" class="radio" id="radio_111" name="IsNewBuilding" value="1"><label for="radio_111" class="radio-inline">Да</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_112" name="IsNewBuilding" value="0" checked><label for="radio_112" class="radio-inline">Нет</label></div>
        </div>
    </div>
<?php } ?>

    <?php
        $this->load->view('mls/add/fields/building_height_floor');  
    ?>    
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Пассажирских лифтов</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="radio_button"><input type="radio" class="radio" id="radio_71_345" value=""  name="BuildingCountPassengerElevator" checked><label for="radio_71_345" class="radio-inline">Нет</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_72_345" value="1" name="BuildingCountPassengerElevator"><label for="radio_72_345" class="radio-inline">1</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_73_345" value="2" name="BuildingCountPassengerElevator"><label for="radio_73_345" class="radio-inline">2</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_74_345" value="3" name="BuildingCountPassengerElevator"><label for="radio_74_345" class="radio-inline">3</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_75_345" value="4" name="BuildingCountPassengerElevator"><label for="radio_75_345" class="radio-inline">4</label></div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Грузовых лифтов</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="radio_button"><input type="radio" class="radio" id="radio_71_34" name="BuildingCountWeightElevator" value="" checked><label for="radio_71_34" class="radio-inline">Нет</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_72_34" name="BuildingCountWeightElevator" value="1"><label for="radio_72_34" class="radio-inline">1</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_73_34" name="BuildingCountWeightElevator" value="2"><label for="radio_73_34" class="radio-inline">2</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_74_34" name="BuildingCountWeightElevator" value="3"><label for="radio_74_34" class="radio-inline">3</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_75_34" name="BuildingCountWeightElevator" value="4"><label for="radio_75_34" class="radio-inline">4</label></div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>В доме</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="BuildingIsHasTrash" value="1"><span class="pseudocheckbox">Мусоропровод</span></label>
        </div>
    </div>
    
    <?php
        $this->load->view('mls/add/fields/building_parking');  
    ?>                  
</div>