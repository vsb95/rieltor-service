<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">Об объекте</p>
<?php 
    $this->load->view('mls/add/fields/cadastrial_number'); 
    $this->load->view('mls/add/fields/building_microdistrict_name'); 
    $this->load->view('mls/add/fields/building_orientir'); 
    $this->load->view('mls/add/fields/distance_out_city'); 
if(isset($type) && $type != "lot"){
    $this->load->view('mls/add/fields/build_year');         
} 
if(isset($type) && $type == "house_part"){
    $this->load->view('mls/add/fields/part_size');     
} 
 if(isset($type) && $type != "lot"){ 
    $this->load->view('mls/add/fields/house_material');    
    $this->load->view('mls/add/fields/area_total'); 
    $this->load->view('mls/add/fields/house_total_floor');
    echo '<div class="row adt_block"></div>';
    $this->load->view('mls/add/fields/house_toilet_type'); 
    $this->load->view('mls/add/fields/house_heating_type'); 
 } ?>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Дополнительно</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="electricity"><span class="pseudocheckbox">Электричество</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <?php if(isset($type) && $type != "lot"){ ?>
                        <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="phone"><span class="pseudocheckbox">Телефон</span></label>
                    <?php } else{ ?>
                        <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="foundation"><span class="pseudocheckbox">Фундамент</span></label>
                    <?php } ?>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="sewerage"><span class="pseudocheckbox">Канализация</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="sauna"><span class="pseudocheckbox">Баня</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="water"><span class="pseudocheckbox">Водоснабжение</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="garage"><span class="pseudocheckbox">Гараж</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="gas"><span class="pseudocheckbox">Газ</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="pool"><span class="pseudocheckbox">Бассейн</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="security"><span class="pseudocheckbox">Охрана</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[]" value="outbuilding"><span class="pseudocheckbox">Хоз. постройки</span></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row checkbox-row"></div>
    <?php 
        $this->load->view('mls/add/fields/lot_area');  
        $this->load->view('mls/add/fields/lot_state');  
    ?>
</div>
