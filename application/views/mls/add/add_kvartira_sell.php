<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">Об объекте</p>
    
    <?php 
    if(!isset($type) || $type !='kvartira_new'){
        $this->load->view('mls/add/fields/cadastrial_number');  
    }    
    $this->load->view('mls/add/fields/count_rooms');  
    $this->load->view('mls/add/fields/room_plaining');      
    
    if(isset($type) && $type =='kvartira_new'){ ?>
        <div class="row">    
            <div class="col-lg-12">
                <p>Планировка картинка</p>    
                <input type="text" name="PlainingImage" class="form-control">
            </div>
        </div>
    <?php 
    } 
    $this->load->view('mls/add/fields/floor');  
    $this->load->view('mls/add/fields/area_total');  
    $this->load->view('mls/add/fields/area_living');  
    $this->load->view('mls/add/fields/area_kitchen');  
    $this->load->view('mls/add/fields/balcon_loggia');  
    $this->load->view('mls/add/fields/window_type');  
    $this->load->view('mls/add/fields/count_toilet');  
    $this->load->view('mls/add/fields/renovation_type');  
    $this->load->view('mls/add/fields/place_type_in_building');  
    ?>
</div>