<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">Об объекте</p>
    <?php 
        $this->load->view('mls/add/fields/rent_furniture_fill');  
        $this->load->view('mls/add/fields/count_rent_rooms');  
        $this->load->view('mls/add/fields/area_room');  
        $this->load->view('mls/add/fields/count_rooms');  
        $this->load->view('mls/add/fields/floor');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/balcon_loggia');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/count_toilet');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/renovation_type');  
        echo '<div class="row adt_block"></div>';
        $this->load->view('mls/add/fields/rent_child_animal');  
        echo '<div class="row adt_block"></div>';
    ?>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Дополнительно</p>
        </div>
        <div class="col-md-8	col-lg-8">
            
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Общие</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[2]" value="in-room"><span class="pseudocheckbox">Мебель в комнатах</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[3]" value="balcon"><span class="pseudocheckbox">Балкон</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" id="yardWindow" name="Windows[]" value="yard"><span class="pseudocheckbox">Окна во двор</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[4]" value="loggia"><span class="pseudocheckbox">Лоджия</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" id="streetWindow" name="Windows[]" value="street"><span class="pseudocheckbox">Окна на улицу</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                </div>
            </div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Техника</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[5]" value="fridge"><span class="pseudocheckbox">Холодильник</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[6]" value="tv"><span class="pseudocheckbox">Телевизор</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[7]" value="dishwasher"><span class="pseudocheckbox">Посудомоечная машина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[8]" value="phone"><span class="pseudocheckbox">Телефон</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[9]" value="washing-machine"><span class="pseudocheckbox">Стиральная машина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                </div>
            </div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Комфорт</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[10]" value="bath"><span class="pseudocheckbox">Ванна</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline" style="display:none;"><input type="checkbox" class="checkbox" name="Furnitures[11]" value=""><span class="pseudocheckbox">Своя кухня</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[12]" value="shower"><span class="pseudocheckbox">Душевая кабина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline" style="display:none;"><input type="checkbox" class="checkbox" name="Furnitures[13]" value=""><span class="pseudocheckbox">Раздельный санузел</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[14]" value="conditioner"><span class="pseudocheckbox">Кондиционер</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline" style="display:none;"><input type="checkbox" class="checkbox" name="Furnitures[15]" value=""><span class="pseudocheckbox">Совмещенный санузел</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[16]" value="internet"><span class="pseudocheckbox">Интернет</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline" style="display:none;"><input type="checkbox" class="checkbox" name="Furnitures[17]" value=""><span class="pseudocheckbox">Свой санузел</span></label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="adt_desk">
    <p class="adt_headline">О здании</p>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Лифт</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="radio_button"><input type="radio" class="radio" id="radio_221" name="IsHasLift" value="1" checked><label for="radio_221" class="radio-inline">Есть</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_222" name="IsHasLift" value="0"><label for="radio_222" class="radio-inline">Нет</label></div>
        </div>
    </div>
    
    <?php
        $this->load->view('mls/add/fields/building_height_floor');  
        $this->load->view('mls/add/fields/building_parking');  
    ?>
</div>