<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="adt_desk">
    <p class="adt_headline">О здании</p>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Название ЖК</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="input-group">
                <input type="text" name="BuildingMicrodistrictName" class="form-control">    
            </div>
        </div>
    </div>
    <!--
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Корпус</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="input-group">
                <input type="text" name="BuildingCorpus" class="form-control">    
            </div>
        </div>
    </div>
-->
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Застройщик</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="input-group">
                <input type="text" class="form-control" name="BuilderName">
            </div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Срок сдачи *</p>
        </div>
        <div class="col-md-8	col-lg-8">       
            <div class="btn-group" style="width:100px;">                             
                <select name="BuildEndYear" style="width:100px;display:inline;" required>
                    <option value="" selected disabled>Год</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                    <option value="2029">2029</option>
                </select>
            </div>
            <div class="btn-group"  style="width:100px;">
                <select name="BuildEndQuartal" style="width:100px;display:inline;" required>
                    <option value="" selected disabled>Квартал</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
        </div> 
        <div class="col-md-4	col-lg-4">
            <label class="checkbox-inline" style="padding-left: 0;"><input type="checkbox" name="IsBuildingDone" value="1" class="checkbox"><span class="pseudocheckbox">Сдан</span></label>
        </div>            
    </div>                         
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Тип дома *</p>
        </div>
        <div class="col-md-8	col-lg-8"> 
            <div class="btn-group">                               
                <select name="BuildingMaterialType" required>              
                    <option value="" selected></option>  
                    <option value="brick">Кирпичный</option>
                    <option value="panel">Панельный</option>
                    <option value="monolite">Монолитный</option>
                    <option value="wood">Деревянный</option>
                    <option value="brick-monolite">Монолитно-кирпичный</option>
                    <option value="concrete">Железобетонный</option>
                    <option value="metall">Металлический</option>
                    <option value="block">Блочный</option> 
                </select>
            </div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Пассажирских лифтов</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="radio_button"><input type="radio" class="radio" id="radio_5571" value=""  name="BuildingCountPassengerElevator" checked><label for="radio_5571" class="radio-inline">Нет</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_5572" value="1" name="BuildingCountPassengerElevator"><label for="radio_5572" class="radio-inline">1</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_5573" value="2" name="BuildingCountPassengerElevator"><label for="radio_5573" class="radio-inline">2</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_5574" value="3" name="BuildingCountPassengerElevator"><label for="radio_5574" class="radio-inline">3</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_5575" value="4" name="BuildingCountPassengerElevator"><label for="radio_5575" class="radio-inline">4</label></div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Грузовых лифтов</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="radio_button"><input type="radio" class="radio" id="radio_6671" name="BuildingCountWeightElevator" value=""  checked><label for="radio_6671" class="radio-inline">Нет</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_6672" name="BuildingCountWeightElevator" value="1"><label for="radio_6672" class="radio-inline">1</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_6673" name="BuildingCountWeightElevator" value="2"><label for="radio_6673" class="radio-inline">2</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_6674" name="BuildingCountWeightElevator" value="3"><label for="radio_6674" class="radio-inline">3</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_6675" name="BuildingCountWeightElevator" value="4"><label for="radio_6675" class="radio-inline">4</label></div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Подземная парковка</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="btn-group">
                <select name="IsHasSubgroundParking">
                    <option value="" selected disabled></option>
                    <option value="1">Есть</option>
                    <option value="0">Нет</option>
                </select>        
            </div>                        
        </div>
    </div>
</div>