<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>       

<div class="adt_desk">
    <p class="adt_headline">Цена и условия сделки</p>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Стоимость *</p>
        </div>
        <div class="col-md-7	col-lg-7">
            <div class="input-group">
                <input type="text" class="form-control" name="Cost"  required"> 
            </div>
        </div>
        <div class="col-md-1	col-lg-1">Р</div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
        </div> 
        <div class="col-md-4	col-lg-4">
            <label class="checkbox-inline" style="padding-left: 0;"><input type="checkbox" class="checkbox" name="IsCanMorgage" value="1"><span class="pseudocheckbox">Возможна ипотека</span></label>
        </div>            
    </div> 
<?php if($type != 'kvartira_new') { ?>           
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
        </div> 
        <div class="col-md-4	col-lg-4">
            <label class="checkbox-inline" style="padding-left: 0;"><input type="checkbox" class="checkbox" name="IsOnPrepayment" value="1"><span class="pseudocheckbox">Под авансом/задатком</span></label>
        </div>            
    </div> 
<?php } ?>  
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Тип продажи</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="radio_button"><input type="radio" class="radio" id="radio_331" name="SellType" value="free" checked><label for="radio_331" class="radio-inline">Свободная продажа</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_332" name="SellType" value="alternative"><label for="radio_332" class="radio-inline">Альтернатива</label></div>   
        </div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Вид договора *</p>
        </div>
        <div class="col-md-8	col-lg-8"> 
            <div class="btn-group">   
                <select name="ContractType" required> 
                    <option value="" selected disabled></option>  
                    <option value="none">Нет</option>
                    <option value="exclusive">Эксклюзив</option>
                    <option value="soft">Мягкий</option>
                </select>           
            </div> 
        </div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Комиссия *</p>
        </div>
        <div class="col-md-4	col-lg-4">
            <div class="btn-group">
                <select name="CommissionType" id="commsission-type" required>
                    <option value="null" selected>Не указано</option>
                    <option value="none">Нет</option>
                    <option value="percent">Процент</option>
                    <option value="fixed">Фиксированно</option>
                </select>
            </div>            
        </div>
        <div class="col-md-4  col-lg-4">
            <div class="input-group">
                <input type="text" id="tbCommission" name="Commission" placeholder="Комиссия..." value="" class="form-control" style="display:none;"/>     
            </div>
        </div>
        </div>
    </div> 
</div>