<div class="row adt_block">
    <div class="col-md-4	col-lg-4">
        <p>Комнат в продажу *</p>
    </div>
    <div class="col-md-4	col-lg-4">
        <div class="btn-group">
            <select role="menu" name="SellCountRooms" required>
                <option selected disabled>Не выбрано</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5+">5+</option>
            </select>
        </div>
    </div>
</div>