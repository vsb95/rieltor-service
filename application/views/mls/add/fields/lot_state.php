<div class="row adt_block">
    <div class="col-md-4	col-lg-4">
        <p>Статус участка</p>
    </div>
    <div class="col-md-4	col-lg-4">
        <div class="btn-group">
            <select role="menu" name="LotAreaState">
                <option selected disabled>Не выбрано</option>
                <option value="garden">Садоводство</option>
                <option value="izs">ИЖС</option>
                <!--
                <option value="farm">Фермерское хозяйство</option>
                <option value="percon">Личное подсобное хоз-во</option>
                <option value="propmzone">Земля промназначения</option>
                <option value="dnp">ДНП</option>
                -->
            </select>
        </div>
    </div>
</div>