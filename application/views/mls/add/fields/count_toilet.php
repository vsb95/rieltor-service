
<div class="row adt_block">
    <div class="col-md-4	col-lg-4">
        <p>Раздельные санузлы</p>
    </div>
    <div class="col-md-8	col-lg-8">
        <div class="radio_button"><input type="radio" class="radio" id="radio_61" name="CountToilet" value="" checked><label for="radio_61" class="radio-inline">Нет</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_62" name="CountToilet" value="1"><label for="radio_62" class="radio-inline">1</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_63" name="CountToilet" value="2"><label for="radio_63" class="radio-inline">2</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_64" name="CountToilet" value="3"><label for="radio_64" class="radio-inline">3</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_65" name="CountToilet" value="4"><label for="radio_65" class="radio-inline">4</label></div>
    </div>
</div>
<div class="row adt_block">
    <div class="col-md-4	col-lg-4">
        <p>Совмещенные санузлы</p>
    </div>
    <div class="col-md-8	col-lg-8">
        <div class="radio_button"><input type="radio" class="radio" id="radio_71" name="CountToiletWithBathroom" value="" checked><label for="radio_71" class="radio-inline">Нет</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_72" name="CountToiletWithBathroom" value="1"><label for="radio_72" class="radio-inline">1</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_73" name="CountToiletWithBathroom" value="2"><label for="radio_73" class="radio-inline">2</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_74" name="CountToiletWithBathroom" value="3"><label for="radio_74" class="radio-inline">3</label></div>
        <div class="radio_button"><input type="radio" class="radio" id="radio_75" name="CountToiletWithBathroom" value="4"><label for="radio_75" class="radio-inline">4</label></div>
    </div>
</div>