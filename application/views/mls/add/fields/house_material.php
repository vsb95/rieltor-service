<div class="row adt_block">
    <div class="col-md-4	col-lg-4">
        <p>Тип дома * </p>
    </div>
    <div class="col-md-4	col-lg-4">
        <div class="btn-group">
            <select name="BuildingMaterialType" style="width:200px;display:inline;" required>              
                <option value="" selected></option>  
                <option value="brick">Кирпичный</option>
                <option value="panel">Панельный</option>
                <option value="monolite">Монолитный</option>
                <option value="wood">Деревянный</option>
                <option value="brick-monolite">Монолитно-кирпичный</option>
                <option value="concrete">Железобетонный</option>
                <option value="metall">Металлический</option>
                <option value="block">Блочный</option>
                <option value="skeleton">Каркасно-насыпной</option>                
            </select>
        </div>
    </div>
</div>