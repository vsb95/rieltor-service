
<div class="row adt_block">
    <div class="col-md-4	col-lg-4">
        <p>Площадь участка</p>
    </div>
    <div class="col-md-4	col-lg-4">
        <input type="text" class="form-control" name="LotAreaSize">            
    </div>
    <div class="col-md-4	col-lg-4">
        <div class="btn-group btn-floor">
            <select role="menu" name="LotAreaType">
                <option value="hectare">Га</option>
                <option value="meter">м2</option>
                <option value="hundred">Соток</option>
            </select>
        </div>  
    </div>
</div>