<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">Фотографии</p>
    <div class="row adt_block" id="drop-area">    
        <span >Недопускаются к размещению фотографии с водяными знаками, чужих объектов и рекламные баннеры. Максимальный размер файла - 10 Мб. Расширение - Jpeg</span>
        <form class="my-form">
        <div style="width:100%; padding: 25px;">
            <div class="files_input" style="display: inline-block;  text-align: center">Перетащите сюда изображения или</div> 
            <div style="display: inline-block; margin-left:5px">
                <input type="file" id="fileElem" multiple accept="image/*"  onchange="handleFiles(this.files)"> 
                <label class="button" for="fileElem">Выберите изображения</label>
            </div>
        </div>
        </form>        
        <progress id="progress-bar" max=100 value=0 style="display:none; width:80%"></progress>
        <p id="progress-bar-percent" style="display:none"></p>
        <ul id="gallery" /></ul>
    </div>

    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
        <p>Видео</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8	col-lg-8">
        <div class="input-group input-name">
        <input type="text" class="form-control" placeholder="Ссылка на Youtube" name="YoutubeUrl">
        </div>
        </div>
        <div class="col-md-4	col-lg-4">
            <!--
            <div class="input-group input-name">
                <p><a class="btn btn-primary btn-md btn-youtube" role="button">+ Добавить видео</a></p>
            </div> 
            -->
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Заголовок</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8	col-lg-8">
            <div class="input-group input-name">
                <input type="text" class="form-control" name="Title" >
            </div>
        </div>
        <div class="col-md-4	col-lg-4">
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Описание</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12	col-lg-12">
            <textarea cols="40" rows="3" name="Description" maxlength="10000" placeholder="Пару слов об объекте..."></textarea>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#gallery").sortable({
   update: function(event, ui){
     var position = ui.item.prevAll().length + 1;
     ui.item.children('input[class=input-order]').val(position);
   }
});
</script>