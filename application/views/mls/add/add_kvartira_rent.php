<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <?php $this->load->view('mls/add/fields/rent_furniture_fill');  ?>
</div>
<div class="adt_desk">
    <p class="adt_headline">Об объекте</p>
<?php 
    $this->load->view('mls/add/fields/count_rooms'); 
    $this->load->view('mls/add/fields/area_total'); 
    $this->load->view('mls/add/fields/area_living'); 
    $this->load->view('mls/add/fields/floor'); 
    $this->load->view('mls/add/fields/balcon_loggia'); 
    $this->load->view('mls/add/fields/window_type'); 
    $this->load->view('mls/add/fields/count_toilet'); 
    $this->load->view('mls/add/fields/renovation_type'); 
    $this->load->view('mls/add/fields/rent_child_animal'); 
    ?>            
    <div class="row adt_block">
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>В квартире</p>
        </div>
        <div class="col-md-8	col-lg-8">
            
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Техника</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[2]" value="fridge"><span class="pseudocheckbox">Холодильник</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[3]" value="tv"><span class="pseudocheckbox">Телевизор</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[4]" value="dishwasher"><span class="pseudocheckbox">Посудомоечная машина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[5]" value="phone"><span class="pseudocheckbox">Телефон</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[6]" value="washing-machine"><span class="pseudocheckbox">Стиральная машина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                </div>
            </div>
        </div>
    </div>
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
            <p>Комфорт</p>
        </div>
        <div class="col-md-8	col-lg-8">
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[7]" value="bath"><span class="pseudocheckbox">Ванна</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[8]" value="conditioner"><span class="pseudocheckbox">Кондиционер</span></label>
                </div>
            </div>
            <div class="row checkbox-row">
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[9]" value="shower"><span class="pseudocheckbox">Душевая кабина</span></label>
                </div>
                <div class="col-md-6	col-lg-6">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="Furnitures[10]" value="internet"><span class="pseudocheckbox">Интернет</span></label>
                </div>
            </div>
        </div>
    </div>    
</div>
