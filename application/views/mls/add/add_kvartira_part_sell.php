<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">Об объекте</p>
    <?php 
    
    $this->load->view('mls/add/fields/cadastrial_number'); 
    $this->load->view('mls/add/fields/part_size'); 
    $this->load->view('mls/add/fields/count_rooms'); 
    $this->load->view('mls/add/fields/room_plaining'); 
    echo '<div class="row adt_block"></div>';
    $this->load->view('mls/add/fields/floor'); 
    $this->load->view('mls/add/fields/area_total'); 
    $this->load->view('mls/add/fields/area_living'); 
    $this->load->view('mls/add/fields/area_kitchen'); 
    echo '<div class="row adt_block"></div>';
    $this->load->view('mls/add/fields/balcon_loggia'); 
    $this->load->view('mls/add/fields/window_type'); 
    echo '<div class="row adt_block"></div>';
    $this->load->view('mls/add/fields/count_toilet'); 
    echo '<div class="row adt_block"></div>';
    $this->load->view('mls/add/fields/renovation_type'); 
    $this->load->view('mls/add/fields/place_type_in_building');    
     
    ?>    
</div>
