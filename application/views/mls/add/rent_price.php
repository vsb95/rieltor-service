<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="adt_desk">
    <p class="adt_headline">Цена и условия сделки</p>
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Стоимость *</p>
        </div>
        <div class="col-md-7	col-lg-7">
            <div class="input-group">
                <input type="text" class="form-control" name="Cost"  required"> 
            </div>
        </div>
        <div class="col-md-1	col-lg-1">Р</div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4	col-lg-4">
        </div> 
        <div class="col-md-4	col-lg-4">
            <label class="checkbox-inline" style="padding-left: 0;"><input type="checkbox" class="checkbox" name="IsCanHaggle" value="1"><span class="pseudocheckbox">Возможен торг</span></label>
        </div>            
    </div> 
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Включенные коммунальные платежи</p>
        </div>
        <div class="col-md-4	col-lg-4">
            <div class="btn-group" >
                <select name="UtilitiesType" style="max-width:320px" id="cbUtilities">              
                    <option value="" selected>Не включены</option> 
                    <option value="utilities">Коммуналка</option>
                    <option value="counter">Счетчики</option>
                    <option value="full">Коммуналка и Счетчики</option>
                </select>
            </div>
        </div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p id="tbUtilitiesCostDescr">Стоимость К/У (Если не включены)</p>
        </div>
        <div class="col-md-4	col-lg-4">
            <input type="text" id="tbUtilitiesCost" name="UtilitiesCost" placeholder="стоимость К\У" value="" class="form-control"  data-inputmask="'mask': '99 999 Р'" />   
        </div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Предоплата</p>
        </div>
        <div class="col-md-4	col-lg-4">
            <div class="btn-group">
                <select name="PrepayPeriod" style="max-width:320px">              
                    <option value="" ></option>  
                    <option value="1">За месяц</option>
                    <option value="2">За 2 месяца</option>
                    <option value="3">За 3 месяца</option>
                </select>
            </div>
        </div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Залог собственнику</p>
        </div>
        <div class="col-md-6	col-lg-6">
            <div class="input-group input-price">
                <input type="text" class="form-control" name="PledgeCost">
            </div>
        </div>
        <div class="col-md-2	col-lg-2">
            <div class="input-group input-price"> 
            </div>    
        </div>
    </div> 
    <div class="row adt_block">
        <div class="col-md-4  col-lg-4">
            <p>Комиссия *</p>
        </div>
        <div class="col-md-4	col-lg-4">
            <div class="btn-group">
                <select name="CommissionType" id="commsission-type" required>
                    <option value="null" selected>Не указано</option>
                    <option value="none">Нет</option>
                    <option value="percent">Процент</option>
                    <option value="fixed">Фиксированно</option>
                </select>
            </div>            
        </div>
        <div class="col-md-4  col-lg-4">
            <div class="input-group">
                <input type="text" id="tbCommission" name="Commission" placeholder="Комиссия..." value="" class="form-control"  style="display:none;"/>     
            </div>
        </div>
    </div> 
</div>