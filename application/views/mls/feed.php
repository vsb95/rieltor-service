<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .upload-error{
        font-weight: bold;
    }
    .virtual-user-img{
        max-width:100px;
        max-height:100px;
    }
    tr td{
        max-width: 150px;
    }
    tr td:first-child{
        width: 100px;
    }
    tr td:last-child{
        max-width: 300px;
    }

    #add-sub-feed-frm p{
        margin-top:15px;
    }
    .adt_main{
        padding: 10px 20px 20px 20px;
        margin-bottom: 20px;
    }

    .prg-description{
        line-height: normal;
        font-size: 14px;
        margin-top:5px;
    }
</style>

<div class="row adt_main">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Яндекс фид</h3>
            <?php    
            if($status){
                if(isset($yrl_date))
                    echo '<p>Дата создания: '.$yrl_date.'</p>'; 
                if(isset($yrl))
                    echo'<input type="text" class="form-control" style="width:70%;display: inline-block;" value="'.$yrl.'" placeholder="Здесь будет ссылка на ваш фид">';     
                else
                    echo '<h4>У вас еще не создан фид. Сначала <a href="/mls/add">добавьте хоть одно объявление</a></h4>';

                if(isset($yrl_download))
                    echo '<a class="btn btn-primary btn-md" style="width:25%;padding:10px; max-width:150px;" role="button" download href="'.$yrl.'" target="_blank">Скачать</a>';
            }
            else{
                echo '<h4>Сервер не доступен. Попробуйте позднее</h4>';
            }
            ?> 
    </div>
</div>
<div class="row adt_main">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Расширенные настройки</h3>
        <form id="add-sub-feed-frm">
            <p>Укажите номер телефона который будет использоваться в одном из фидов, чтобы отслеживать входящие звонки с разных площадок</p>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <p>Название</p>
                    <input type="text" name="name" placeholder="Фид для ЦИАНа" required class="form-control"/>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Телефон</p>
                    <input type="tel" name="phone" required class="form-control"/>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Формат фида *</p>              
                    <div class="btn-group">
                        <select name="format" required>  
                            <option value="yandex" selected>Яндекс</option>
                            <option disabled>Новые форматы будут добавлены позже</option>
                        </select>
                    </div>   
                    <p class="prg-description">* Укажите формат генерируемого фида для нужной площадки.<br>Чтобы узнать какие форматы поддерживает нужная вам площадка обратитесь к менеджеру или справке на нужной площадке</p>
                </div>                    
                <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                    <input type="submit" value="+ Добавить" class="btn btn-primary btn-md btn-send" role="button" style="margin-top: 45px;">
                </div>
            </div>   
        </form>
    </div>
</div>
<div class="row adt_main">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Импорт Яндекс фида</h3>
                    <?php
                    if(isset($auto_import_feeds) && !empty($auto_import_feeds)) {
                        echo '<h4>Ваши подключенные фиды:</h4>';
                        echo '<ul>';
                           foreach($auto_import_feeds as $feed){
                               echo '<li>'.$feed.'</li>';
                           }                           
                        echo '</ul>';
                    } ?>
            <?php    
            if($status){?>
                <div id="upload-result">
                <form action="/mls/feed/upload" method="POST" id="upload-form">
                    <p>Выберите файл для импорта объявлений в нашу базу</p>
                    <input id="file-upload" type="text" name="file" class="form-control" style="width:70%;display: inline-block;" placeholder = "Ссылка на ваш YRL-файл" required>
                    <a id="btn-upload" class="btn btn-primary btn-md" style="width:25%;padding:10px; max-width:150px;" role="button" href="#">Загрузить</a>
                    <br>
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="is_auto" value="1"><span class="pseudocheckbox">Подключить на автоматической основе *</span></label>
                    <p class="prg-description">* Фид будет обновляться в системе каждые 12 часов, загружаясь с вашего сайта </p>

                    <?php if($this->user_model->IsLoginedAsSuperAdmin()){ ?>
                        <br>
                    <p>Супер: укажи для кого загружаем фид</p>
                        <input type="email" name="email" placeholder="Email для оповещения"  required class="form-control"/>
                        <select name="org">
                        <?php
                        $res = $this->db->query("SELECT organization.id, organization.*, users.user_count 
                        FROM organization
                        LEFT JOIN  (
                            SELECT  count(user.id) as user_count, user.id_org 
                            FROM user            
                            Where user.is_activate = 1 AND user.is_deleted = 0
                            GROUP BY user.id_org
                        )  users 
                        ON users.id_org = organization.id 
                        order BY organization.reg_date ",array());
                        $organizations = $res->result_array();
                            foreach($organizations as $org){
                                echo '<option value="'.$org['id'].'">'.$org['name'].'</options>';
                            }
                        ?>
                        </select>
                    <?php } ?>
                </form>
            </div>
                   
            <?php if(isset($yrl_association) && count($yrl_association)>0){ ?>
            <div style="margin-top:15px;">
                <a href="#" class="spoiler-trigger"><span>Связь сотрудников из фида с вашими сотрудниками в системе</span></a>
                <div class="spoiler-block">
                    <form id="frm-association" method="POST" action ="/mls/feed/association">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Агент из фида</th>
                                    <th scope="col"></th>
                                    <th scope="col">Пользователь в системе</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(isset($yrl_association) && count($yrl_association)>0){
                                foreach($yrl_association as $virtualUserJson => $realUserId){
                                    $virtualUser = json_decode($virtualUserJson,true);
                                    echo '<tr>';
                                    echo '<td>';
                                    echo isset($virtualUser['photo']) ?'<img class="virtual-user-img" src="'.$virtualUser['photo'].'"/>':'Нет фото';
                                    echo '</td>';
                                    echo '<td><strong>'.$virtualUser['name'].'</strong>';
                                    echo '<br>'.$virtualUser['phone'];
                                    echo '<br>'.$virtualUser['email'].'</td>';

                                    echo '<td><div  class="btn-group">';
                                    if(isset($users)){     
                                        
                                        echo '<select onchange="userAssociation($(this))" virtual-id="'.$virtualUser['id'].'" name="association['.$virtualUser['id'].']"/>';
                                        echo '<option value="-1">Администратор</option>';
                                        foreach($users as $user){  
                                            echo '<option value="'.$user['id'].'"'.(isset($realUserId) && $realUserId == $user['id'] ?' selected ':'').'>';
                                            if(isset($user['name']))
                                                echo $user['name'];
                                            if(isset($user['phone']))
                                                echo ' ('.$user['phone'].')';
                                            echo '</option>';
                                        }                  
                                        echo '</select>';          
                                    }
                                    echo '</div></td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </form>
                    <a id="btn-frm-save" class="btn btn-primary btn-md" style="width:25%;padding:10px; max-width:150px;" role="button" href="#">Сохранить</a>
                </div>
            </div>
            <?php 
                }
            }
            else{
                echo '<h4>Сервер не доступен. Попробуйте позднее</h4>';
            }
            ?> 
    </div>
</div>
<div class="row adt_main">        
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
        <h3>Справка</h3>        
        
        <div class="feed-description">        
            <a href="#" class="spoiler-trigger"><span>Что такое фид?</span></a>
            <div class="spoiler-block">   
                <p>XML-feed (или XML-фид) - это ваша база недвижимости, выгруженная в формате XML. Автоматическая выгрузка данных экономит время специалистов по недвижимости на рутинную работу по добавлению и редактированию информации в объявлениях.</p>
                <p>Преимущества использования автовыгрузки:
                    <ul>
                        <li>Бесплатно</li>
                        <li>Добавляйте объявления, редактируйте их, удаляйте ТОЛЬКО на 1 сайте! На всех остальных сайтах изменения внесутся полностью в автоматическом режиме.</li>
                    </ul>
                </p>
            </div>
        </div>
        <div class="feed-description">        
            <a href="#" class="spoiler-trigger"><span>Как создать фид?</span></a>
            <div class="spoiler-block">   
                <p>Для формирования XML-фида, со всеми вашими объявлениями, Вам необходимо добавить объект недвижимость на МЛС. Наша система сразу же внесет его в Ваш XML-фид в автоматическом режиме. Все, что вам необходимо сделать после, это скопировать ссылку и отправить на те сайты, куда вы хотите подавать объявления в автоматическом режиме.</p>
            </div>
        </div>
        <div class="feed-description">        
            <a href="#" class="spoiler-trigger"><span>Где я могу использовать фид?</span></a>
            <div class="spoiler-block">   
                <p>Мы работаем над сервисом и в самом ближайшем времени вы сможете выборочно (а не все разом) добавлять объявления в автовыгрузку, а также будут подключены все известные сайты по недвижимости Омска.</p>
                <p>А пока что, Вы можете производить выгрузку в автоматическом режиме уже на следующие площадки:
                    <ul>
                        <li><a href="https://omsk.n1.ru/">https://omsk.n1.ru/</a></li>
                        <li><a href="https://likado.ru/">https://likado.ru/</a></li>
                        <li><a href="https://omsk.cian.ru/">https://omsk.cian.ru/</a></li>
                        <li><a href="https://realty.mail.ru/">https://realty.mail.ru/</a></li>
                        <li><a href="https://realty.yandex.ru">https://realty.yandex.ru</a></li>
                        <li><a href="https://www.mirkvartir.ru/">https://www.mirkvartir.ru/</a></li>
                        <li><a href="https://youla.ru/">https://youla.ru/</a></li>
                        <li><a href="https://www.russianrealty.ru">https://www.russianrealty.ru</a></li>
                        <li><a href="https://reinport.com/">https://reinport.com/</a></li>
                        <li><a href="https://help.kvadroom.ru/">https://help.kvadroom.ru/</a></li>
                        <li><a href="http://realtyport.su/">http://realtyport.su/</a></li>
                        <li><a href="https://www.gdeetotdom.ru/">https://www.gdeetotdom.ru/</a></li>
                        <li><a href="https://kvartiri-domiki.ru/">https://kvartiri-domiki.ru/</a></li>
                        <li><a href="https://regionalrealty.ru/">https://regionalrealty.ru/</a></li>
                        <li><a href="https://move.ru/">https://move.ru/</a></li>
                        <li><a href="https://www.realtymag.ru/">https://www.realtymag.ru/</a></li>
                        <li><a href="https://www.restate.ru/">https://www.restate.ru/</a></li>
                        <li><a href="https://ammis.ru/">https://ammis.ru/</a></li>
                        <li><a href="http://irr.ru/">http://irr.ru/</a></li>
                        <li><a href="https://multilisting.su/">https://multilisting.su/</a></li>
                        <li><a href="https://rosrealt.ru/">https://rosrealt.ru/</a></li>
                        <li><a href="https://ebn.ru/">https://ebn.ru/</a></li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row" style="height: 50px;">
</div>
<script type="text/javascript">
$(document).ready(function() {      
    $('#btn-upload').click(function(){
        var file = $('#file-upload').val();
        if(!file){
            alert("Не указан файл для загрузки");
            return;
        }
        
        var $preloader = $('#page-preloader'),
            $spinner = $preloader.find('#spinner');
        $spinner.text("Загружаем объявления...");
        $spinner.fadeIn();
        $preloader.delay(100).fadeIn('slow');

		var $form = $("#upload-form");
		$.ajax({
			type: $form.attr('method'),
			url: $form.attr('action'),
			data: $form.serialize(),
            timeout: 15 * 60 * 1000
		}).done(function(result) {
            if (!result) {
                $spinner.fadeOut(1);
                $preloader.fadeOut(1);
                $("#modalContent").html('нет ответа от сервера');
                openModal();
                return;
            }
            console.log(result);
            try{
                var json = JSON.parse(result);
                console.log(json);
                if(json['status-type'] != 'OK'){
                    $spinner.fadeOut(1);
                    $preloader.fadeOut(1);
                    $("#modalContent").html(json['status']);
                    openModal();
                    return;
                }
                var html = '<h4>'+json['status']+'</h4>';
                /*
                $.each(json['messages'], function(key, value ) {
                    console.log(key+": "+value);
                });
                $.each(json['errors'], function(key, value ) {
                    console.log(key+": "+value);
                    html+='<p class="upload-error">'+value+'</p>';
                });
                */
                $('#upload-result').html(html);
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                $("#modalContent").html(result);
                openModal();
            }
            $spinner.fadeOut();
            $preloader.delay(50).fadeOut('slow');
		}).fail(function(err) {
            $spinner.fadeOut(1);
            $preloader.fadeOut(1);
            console.log(err);
			$("#modalContent").html(err);
			openModal();
		});
    });
    $('#btn-frm-save').click(function(){
        var $preloader = $('#page-preloader'),
            $spinner = $preloader.find('#spinner');
        $spinner.text("Связываем пользователей...");
        $spinner.fadeIn();
        $preloader.delay(100).fadeIn('slow');

		var $form = $("#frm-association");
		$.ajax({
			type: $form.attr('method'),
			url: $form.attr('action'),
			data: $form.serialize(),
            timeout: 15 * 60 * 1000
		}).done(function(result) {
            if (!result) {
                $spinner.fadeOut(1);
                $preloader.fadeOut(1);
                $("#modalContent").html('нет ответа от сервера');
                openModal();
                return;
            }
            console.log(result);
            $("#modalContent").html(result);
            openModal();
            $spinner.fadeOut();
            $preloader.delay(50).fadeOut('slow');
		}).fail(function(err) {
            $spinner.fadeOut(1);
            $preloader.fadeOut(1);
            console.log(err);
			$("#modalContent").html(err);
			openModal();
		});
    });

});
</script>