<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row headline">
    <div class="col-md-five	col-lg-five">
        <p>Площадь участка, сот.</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Этажей в доме</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Тип дома</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Отопление</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Санузел</p>
    </div>
</div>
<div class="row">
    <div class="col-md-five	col-lg-five">       
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="LotAreaTotalMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="LotAreaTotalMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="FloorTotalMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="FloorTotalMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group">
            <select name="RenovationType">
                <option selected value="">Не важно</option>
                <option value="1">дизайнерский</option>
                <option value="2">евро</option>
                <option value="3">с отделкой</option>
                <option value="4">требует ремонта</option>
                <option value="5">хороший</option>
                <option value="6">частичный ремонт</option>
                <option value="7">чистовая отделка</option>
                <option value="8">под ключ</option>
                <option value="9">черновая отделка</option>
            </select>
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group">
            <select role="menu" name="HeatingType">
                <option selected value="">Не важно</option>
                <option value="1">Есть</option>
                <option value="0">Нет</option>
            </select>
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group">
            <select role="menu" name="ToiletType">
                <option selected value="">Не важно</option>
                <option value="inner">В доме</option>
                <option value="outer">На улице</option>
            </select>
        </div>
    </div>
</div>
<div class="row headline">
    <div class="col-md-five	col-lg-five">
        <p>Стоимость</p>
    </div>
</div>
<div class="row">
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="CostMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="CostMax" class="form-control form-floor">
        </div>
    </div>    
</div>
<div class="spoiler-row">
    <a href="#" class="spoiler-trigger"><span>Расширенный фильтр</span></a>
    <div class="spoiler-block" style="display:none">
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Наличие на участке</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Статус участка</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Год постройки дома</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Удаленность, км</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Дополнительно</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group btn-group-map">
                    <div class="button-map" data-toggle="modal" data-target="#modalFurniture">Не выбрано</div>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu" name="LotAreaState">
                        <option selected disabled>Не выбрано</option>
                        <option value="garden">Садоводство</option>
                        <option value="izs">ИЖС</option>
                        <!--
                        <option value="farm">Фермерское хозяйство</option>
                        <option value="percon">Личное подсобное хоз-во</option>
                        <option value="propmzone">Земля промназначения</option>
                        <option value="dnp">ДНП</option>
                        -->
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group-floor">
                    <input type="text" placeholder="От" name="BuildYearMin" class="form-control form-floor">
                    <input type="text" placeholder="До" name="BuildYearMax" class="form-control form-floor">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group-floor">
                    <input type="text" placeholder="От" name="DistanceOutCityMin" class="form-control form-floor">
                    <input type="text" placeholder="До" name="DistanceOutCityMax" class="form-control form-floor">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">
                            <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Районы и микрорайоны</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Контактный телефон</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Номер объявления</p>
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group btn-group-map">
                    <div class="button-map" data-toggle="modal" data-target="#modalMap">Не выбрано</div>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="input-group input-tel">
                    <input type="text" class="form-control" placeholder="Телефон" name="Phone">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Не выбрано" name="Id">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
        </div>
    </div>
</div>
