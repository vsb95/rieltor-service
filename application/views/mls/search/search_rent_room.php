<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row headline">
    <div class="col-md-five	col-lg-five">
        <p>Срок аренды</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Комнат в аренду</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Общая площадь, м<sup><small>2</small></sup></p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Площадь комнаты, м<sup><small>2</small></sup></p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Площадь кухни, м<sup><small>2</small></sup></p>
    </div>
</div>
<div class="row">
    <div class="col-md-five	col-lg-five">
        <div class="btn-group">                      
            <select name="RentPeriod">
                <option value="month" selected>Длительная</option>
                <option value="day" disabled>Посуточная</option>
            </select>
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group">
            <select name="CountRoomsOffered">   
                <option selected value="">Не важно</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5+">5 и более</option>
            </select>   
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="AreaTotalMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="AreaTotalMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="AreaRoomMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="AreaRoomMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="AreaKitchenMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="AreaKitchenMax" class="form-control form-floor">
        </div>
    </div>
</div>
<div class="row headline">
    <div class="col-md-five	col-lg-five">
        <p>Стоимость</p>
    </div>
</div>
<div class="row">
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="CostMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="CostMax" class="form-control form-floor">
        </div>
    </div>    
</div>
<div class="spoiler-row">
    <a href="#" class="spoiler-trigger"><span>Расширенный фильтр</span></a>
    <div class="spoiler-block" style="display:none">
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Этаж</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Уточнение по этажу</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Этажей в доме</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Ремонт</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Балкон/лоджия</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group-floor">
                    <input type="text" placeholder="От" name="FloorMin" class="form-control form-floor">
                    <input type="text" placeholder="До" name="FloorMax" class="form-control form-floor">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="FloorDetail">
                        <option selected value="">Не важно</option>
                        <option value="1+">Не первый</option>
                        <option value="-N">Не последний</option>
                        <option value="1+-N" >Не первый и не последний</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group-floor">
                    <input type="text" placeholder="От" name="FloorTotalMin" class="form-control form-floor">
                    <input type="text" placeholder="До" name="FloorTotalMax" class="form-control form-floor">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="RenovationType">
                        <option selected value="">Не важно</option>
                        <option value="1">дизайнерский</option>
                        <option value="2">евро</option>
                        <option value="3">с отделкой</option>
                        <option value="4">требует ремонта</option>
                        <option value="5">хороший</option>
                        <option value="6">частичный ремонт</option>
                        <option value="7">чистовая отделка</option>
                        <option value="8">под ключ</option>
                        <option value="9">черновая отделка</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">  
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Лифтов минимум</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Что нужно в квартире</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Районы и микрорайоны</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Санузел</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Количество санузлов</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">  
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group btn-group-map">
                    <div class="button-map" data-toggle="modal" data-target="#modalFurniture">Не выбрано</div>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group btn-group-map">
                    <div class="button-map" data-toggle="modal" data-target="#modalMap">Не выбрано</div>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">  
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">  
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Контактный телефон</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Номер объявления</p>
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="input-group input-tel">
                    <input type="tel" class="form-control" placeholder="Телефон" name="Phone">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Не выбрано" name="Id">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="IsWithoutCommission" value="1"><span class="pseudocheckbox">Без комиссии</span></label>
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
        </div>
    </div>
</div>
