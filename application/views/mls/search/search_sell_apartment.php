<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row headline">
    <div class="col-md-five	col-lg-five">
        <p>Общая площадь, м<sup><small>2</small></sup></p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Жилая площадь, м<sup><small>2</small></sup></p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Площадь кухни, м<sup><small>2</small></sup></p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Этаж</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Уточнение по этажу</p>
    </div>
</div>
<div class="row">
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="AreaTotalMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="AreaTotalMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="AreaLivingMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="AreaLivingMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="AreaKitchenMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="AreaKitchenMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="FloorMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="FloorMax" class="form-control form-floor">
        </div>
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group">
            <select name="FloorDetail">
                <option selected value="">Не важно</option>
                <option value="1+">Не первый</option>
                <option value="-N">Не последний</option>
                <option value="1+-N" >Не первый и не последний</option>
            </select>
        </div>
    </div>
</div>
<div class="row headline">
    <div class="col-md-five	col-lg-five">
        <p>Количество комнат</p>
    </div>
    <div class="col-md-five	col-lg-five">
        <p>Стоимость</p>
    </div>
</div>
<div class="row">
    <div class="col-md-five	col-lg-five">
        <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Выбрать</button>
        <ul class="dropdown-menu" role="menu">
            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="-1" checked><span class="pseudocheckbox">Все</span></label></li>
            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="1" ><span class="pseudocheckbox">1</span></label></li>
            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="2" ><span class="pseudocheckbox">2</span></label></li>
            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="3" ><span class="pseudocheckbox">3</span></label></li>
            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="4" ><span class="pseudocheckbox">4</span></label></li>
            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="999" ><span class="pseudocheckbox">5+</span></label></li>
        </ul>  
    </div>
    <div class="col-md-five	col-lg-five">
        <div class="btn-group-floor">
            <input type="text" placeholder="От" name="CostMin" class="form-control form-floor">
            <input type="text" placeholder="До" name="CostMax" class="form-control form-floor">
        </div>
    </div>    
</div>
<div class="spoiler-row">
    <a href="#" class="spoiler-trigger"><span>Расширенный фильтр</span></a>
    <div class="spoiler-block" style="display:none">
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Этажей в доме</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Тип дома</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Год постройки</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Высота потолка, м</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Лифтов минимум</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group-floor">
                    <input type="text" placeholder="От" name="FloorTotalMin" class="form-control form-floor">
                    <input type="text" placeholder="До" name="FloorTotalMax" class="form-control form-floor">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="HouseMaterials[]">        
                        <option selected value="">Не важно</option>
                        <option value="brick">Кирпичный</option>
                        <option value="panel">Панельный</option>
                        <option value="monolite">Монолитный</option>
                        <option value="wood">Деревянный</option>
                        <option value="brick-monolite">Монолитно-кирпичный</option>
                        <option value="concrete">Железобетонный</option>
                        <option value="metall">Металлический</option>
                        <option value="block">Блочный</option>
                    </select> 
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group-floor">
                    <input type="text" placeholder="От" name="BuildYearMin" class="form-control form-floor">
                    <input type="text" placeholder="До" name="BuildYearMax" class="form-control form-floor">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group-floor">
                    <input type="text" placeholder="От" name="CellHeightMin" class="form-control form-floor">
                    <input type="text" placeholder="До" name="CellHeightMax" class="form-control form-floor">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Балкон/лоджия</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Окна</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Ремонт</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Планировка</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Расположение в доме</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="WindowType">
                        <option selected value="">Не важно</option>
                        <option value="street">На улицу</option>
                        <option value="yard">Во двор</option>
                        <option value="streetANDyard" >На улицу и во двор</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="RenovationType">
                        <option selected value="">Не важно</option>
                        <option value="1">дизайнерский</option>
                        <option value="2">евро</option>
                        <option value="3">с отделкой</option>
                        <option value="4">требует ремонта</option>
                        <option value="5">хороший</option>
                        <option value="6">частичный ремонт</option>
                        <option value="7">чистовая отделка</option>
                        <option value="8">под ключ</option>
                        <option value="9">черновая отделка</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="PlainingType">
                        <option selected value="">Не важно</option>
                        <option value="studia">Студия</option>
                        <option value="isolated">Изолированные</option>
                        <option value="neighbor">Смежные</option>
                        <option value="free">Свбодная</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="FlatPlaceType">
                        <option selected value="">Не важно</option>
                        <option value="not_angle">Не угловая</option>
                        <option value="angle">Угловая</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Районы и микрорайоны</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Санузел</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Количество санузлов</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Тип продажи</p>
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group btn-group-map">
                    <div class="button-map" data-toggle="modal" data-target="#modalMap">Не выбрано</div>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select role="menu">
                        <option selected value="">Не важно</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="SellType">
                        <option selected value="">Не важно</option>
                        <option value="free" >Свободная</option>
                        <option value="alternative" >Альтернативная</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                    <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="IsCanMortgage" value="1"><span class="pseudocheckbox">Возможна ипотека</span></label>
            </div>
        </div>
        <div class="row headline">
            <div class="col-md-five	col-lg-five">
                <p>Вид договора</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Контактный телефон</p>
            </div>
            <div class="col-md-five	col-lg-five">
                <p>Номер объявления</p>
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
            <div class="col-md-five	col-lg-five">
            </div>
        </div>
        <div class="row">
            <div class="col-md-five	col-lg-five">
                <div class="btn-group">
                    <select name="ContractType">     
                        <option selected value="">Не важно</option>
                        <option value="exclusive">Эксклюзив</option>
                        <option value="soft">Мягкий</option>
                        <option value="none">Нет</option>
                    </select>
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <div class="input-group input-tel">
                    <input type="tel" class="form-control" placeholder="Телефон" name="Phone">
                </div>
            </div>
            <div class="col-md-five	col-lg-five">
                <input type="text" class="form-control" placeholder="Не выбрано" name="Id">
            </div>
            <div class="col-md-five	col-lg-five">

            </div>
            <div class="col-md-five	col-lg-five">
                
            </div>
        </div>
    </div>
</div>