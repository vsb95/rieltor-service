<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form id="frm">
    <input type="hidden" id="IndexPage" name="IndexPage" />
    <input type="hidden" id="mkrs" name="microdistricts" />
    <input type="hidden" id="selDistricts" name="districts" />
<div class="param">
    <div class="row headline">
        <div class="col-md-five	col-lg-five">
            <p>Поиск</p>
        </div>
        <div class="col-md-five	col-lg-five">
            
        </div>
        <div class="col-md-five	col-lg-five">
            <p>Тип сделки</p>
        </div>
        <div class="col-md-five	col-lg-five">
            <p>Тип недвижимости</p>
        </div>
        <div class="col-md-five	col-lg-five">
            <p>Объект</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-five-half	col-lg-five-half">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Район, микрорайон или улица" name="address" id="address"                
			    <?php if(isset($_SESSION['name_city']) && !empty($_SESSION['name_city'])) echo 'value="'.$_SESSION['name_city'].', "';?> 
                >
            </div>
        </div>
        <div class="col-md-five	col-lg-five">
            <div class="btn-group">
                <select name="OfferType" id="OfferTypeSelect">
                    <option disabled >Не выбрано</option>
                    <option value="rent">Аренда</option>
                    <option value="sell"selected>Продажа</option>
                </select>
            </div>
        </div>
        <div class="col-md-five	col-lg-five">
            <div class="btn-group">
                <select name="OfferCategory" id="OfferCategorySelect">
                    <option disabled >Не выбрано</option>
                    <option value="living" selected>Жилая</option>
                    <option value="commercial" disabled>Коммерческая</option>
                </select>
            </div>
        </div>
        <div class="col-md-five	col-lg-five">
            <div class="btn-group">
                <select name="ObjectTypeSelect" id="ObjectTypeSelect">
                    <option disabled>Не выбрано</option>
                    <option class="select-living" value="apartment" selected>Квартира</option>
                    <option class="select-sale select-living" value="apartment_new">Новостройка</option>
                    <option class="select-living select-rent" value="room">Комната</option>
                    <option class="select-living" value="house">Дом\коттедж\дача</option>
                    <option class="select-sale select-living" value="apartment_part">Доля в квартире</option>
                    <!--<option class="select-sale select-living" value="house_part">Доля в доме\коттедже</option>-->
                    <option class="select-sale select-living" value="lot">Участок</option>
                </select>
            </div>
        </div>
    </div>
    <content id="filter-fields">
    </content>    
    <div class="row search-param" id="btn-row" style="display:none">
        <div class="col-md-3	col-lg-3">
            <a class="btn btn-primary btn-md" role="button" id="search-btn">Поиск объявлений</a></p>
        </div>
        <div class="col-md-3	col-lg-3">
            <a class="btn btn-primary btn-md btn-chern" role="button" id="clear-btn">Очистить фильтр</a></p>
        </div>
    </div>
</div>    
<div class="modal fade" id="modalMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="modal-map-dialog" style="width:90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Районы и микрорайоны</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-district-map" data-toggle="tab" id="district-modal-map">На карте</a></li>
                    <li ><a href="#tab-district-checkboxes" data-toggle="tab" id="district-modal-list">Список</a></li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content" id="district-tab-content" style="height:65vh;max-height:65vh;">
                    <div class="tab-pane " id="tab-district-checkboxes">                        
                        
                    </div>
                    <div class="tab-pane active" id="tab-district-map">
                        <div style="margin:10px;">
                            <p style="margin:0;">* Однократное нажатие - выбор района\микрорайона</p>
                            <p style="margin:0;">* Двукратное нажатие - открыть микрорайоны в районе</p> 
                        </div>
                        <div id="map" style="width:100%; height:65vh"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div style="width: 50%;margin: auto;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Выбрать</button>
                    <button type="button" class="btn btn-primary btn-main">Сбросить</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFurniture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Что нужно в квартире</h4>
            </div>
            <div class="modal-body">
                <div class="row adt_block">
                    <div class="col-md-12	col-lg-12">
                        <div class="row checkbox-row">
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Мебель</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Без мебели</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Холодильник</span></label>
                            </div>
                        </div>
                        <div class="row checkbox-row">
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Стиральная машина</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Телевизор</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Кондиционер</span></label>
                            </div>
                        </div>
                        <div class="row checkbox-row">
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Ванна</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Душевая</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row adt_block">
                    <div class="col-md-12	col-lg-12">
                        <div class="row checkbox-row">
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Можно с детьми</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="checkbox1"><span class="pseudocheckbox">Можно с животными</span></label>
                            </div>
                            <div class="col-md-4	col-lg-4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div style="width: 50%;margin: auto;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Выбрать</button>
                    <button type="button" class="btn btn-primary btn-main">Сбросить</button>
                </div>
            </div>
        </div>
    </div>
</div>
          
</form>
<div class="search" style="display:none" id="result-row">
    <div class="row">
        <div class="col-md-8	col-lg-8">
            <div class="row">
                <div class="col-md-6	col-lg-6 search_stat"><p  id="total-by-filter"></p></div>
                <div class="col-md-6	col-lg-6 search_sort"><p>Сначала новые</p><img></div>
            </div>
        </div>
        <div class="col-md-4	col-lg-4">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="row search_elem">
        <div class="col-md-12	col-lg-12" id="search-result">
        </div>
    </div>
</div>
    
<div class="row">
    <div class="col-md-8	col-lg-8">
        <ul class="pagination" id="pagination">
        </ul>
    </div>
    <div class="col-md-4	col-lg-4">
        <div class="row">
        </div>
    </div>
</div>
<script type="text/javascript">
    var indexPage = 0;
    $(document).on("click", ".cbox-count-rooms", function(e) {
        var val = $(this).val();
        var isChecked = $(this)[0].checked;
        var parent = $(this).parents('ul');
        
        if(val == -1){         
            if(isChecked){   
                parent.find('input:checkbox').each(function(i, cb){
                    if(i == 0) return;
                    cb.checked = isChecked;
                });                    
            }
        }else{
            parent.find('input:checkbox')[0].checked = false;
            return;
        }
    });
    $(document).on("click", "#pagination>li>a", function(e) {
        var index = $(this).attr('index');
        indexPage = index;
        $('#pagination>li').each(function(){
            $(this).removeClass('active');
        });
        $(this).parent().addClass('active');      
        Search();
    });

    function Search(){
        var $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('#spinner');   
        $spinner.fadeIn();
        $preloader.fadeIn('slow');
        var frm = $("#frm");
        $("#search-result").html("");
        $("#total-by-filter").html("");
        
        $("#mkrs").val(selectedMicroDistrict);
        $("#selDistricts").val(selectedDistricts);
        $("#IndexPage").val(indexPage);
        $.ajax({
            type: "GET",
            url: "/mls/search/searching",
            data: frm.serialize(),
            success: function(result) {
                setLocation("/mls/search?"+frm.serialize());
                try {
                    if(result != null && result.statusText == "timeout"){
                        $("#modalContent").html("Время ожидания операции истекло. Повторите еще раз");
                        openModal();
                        return;
                    }
                    var json = JSON.parse(result);
                    $("#search-result").html(json["html"]);
                    $("#total-by-filter").html("Найдено "+json["total-count-by-filter"]+" результата");
                    if(indexPage == 0)
                        InitPagination(json["total-count-by-filter"]);
                    $('.fotorama').fotorama();
                    $("#result-row").css('display', 'block');	
                } catch (error) {
                    console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                    console.log('result =  ' + result);
                    alert('Ошибка: '+result);
                }
                $spinner.fadeOut();
                $preloader.delay(100).fadeOut();
            },
            error: function(result) {
                console.log(result);
                $("#modalContent").html('Ошибка: '+result);
                openModal();
                $spinner.fadeOut();
                $preloader.delay(100).fadeOut();
            },
            timeout: 15 * 60 * 1000
        });
    }        
    function InitPagination(total){
        var html = '';
        html+='<li class="active"><a href="#" index="0">1</a></li>';
        for(var i=1; i<total/50; i++){
            html+='<li><a index="'+i+'">'+(i+1)+'</a></li>';
        }
        $('#pagination').html(html);            
    }   
    $(document).ready(function() {
        $('#clear-btn').click(function(){
            $('#filter-fields').find('input:text').val('');
            $('#filter-fields').find('select').val('');
            $('#filter-fields').find('checkbox').checked = false;
        });

        $('#search-btn').click(function(){
            indexPage = 0;
            Search();
        });

        $('#OfferTypeSelect').change(function() {
            var value = $(this).val();
            if(value=="rent"){
                if($("#ObjectTypeSelect option:selected").hasClass( "select-sale" )){
                    $("#ObjectTypeSelect option:selected").attr('selected', false);
                }
            }else{
                if($("#ObjectTypeSelect option:selected").hasClass( "select-rent" )){
                    $("#ObjectTypeSelect option:selected").attr('selected', false);
                }
            } 
            HideObjTypes();
            GetFields();
        });
        $('#OfferCategorySelect').change(function() {
            var value = $(this).val();
            if(value=="living"){
                if($("#ObjectTypeSelect option:selected").hasClass( "select-commercial" )){
                    $("#ObjectTypeSelect option:selected").attr('selected', false);
                }
            }else{
                if($("#ObjectTypeSelect option:selected").hasClass( "select-living" )){
                    $("#ObjectTypeSelect option:selected").attr('selected', false);
                }
            } 
            HideObjTypes();
            GetFields();
        });
        $('#ObjectTypeSelect').change(function() {
            GetFields();
        });

        function GetFields(){
            var offerTypeValue = $('#OfferTypeSelect').val();
            var offerCategoryValue = $('#OfferCategorySelect').val();
            var objTypeValue = $('#ObjectTypeSelect').val();
            if(!offerTypeValue || !offerCategoryValue || !objTypeValue)
                return;
            var url = "/mls/search/GetSearchFields/?offerType="+offerTypeValue+"&offerCategory="+offerCategoryValue+"&objType="+objTypeValue;
            $.ajax({
                url: url,
                success: function(result) {
                    $("#btn-row").css('display', 'block');	
                    $('#filter-fields').html(result);                    
                },
                error: function(result) {
                    alert("Произошла ошибка");
                    console.log("ERROR: "+result);   
                    $("#modalContent").html('Ошибка: '+result);
                    openModal();         
                },
                timeout: 5*60*1000
            });
        }

        function HideObjTypes(){
            var offerType = $('#OfferTypeSelect').val();
            var offerCategoryValue = $('#OfferCategorySelect').val();
            $(".select-commercial").each(function(item) {
                $(this).css('display', offerCategoryValue == "living" ?'none':'block');
            });
            $(".select-living").each(function(item) {
                $(this).css('display', offerCategoryValue == "living" ?'block':'none');
            });
            $(".select-sale").each(function(item) {
                $(this).css('display', offerType == "rent" ?'none':'block');
            });
            $(".select-rent").each(function(item) {
                $(this).css('display', offerType == "rent" ?'block':'none');
            });
        }

		GetFields();
        Search();
    });
    
    function setLocation(curLoc){
        try {
            curLoc = curLoc.replace(/[^&]+=\.?(?:&|$)/g, '');
            history.pushState(null, null, curLoc);
            return;
        } catch(e) {}
        location.hash = '#' + curLoc;
    }
</script>
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async defer></script>
<script src="//yastatic.net/share2/share.js" async defer></script>