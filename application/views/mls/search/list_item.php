<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if(!isset($adv) || empty($adv))
	return;
	//var_dump($adv);
	//var_dump($adv['property']['building']['location']);
?>	

<div class="row search_result">
	
<?php if(!$isCabinet){ ?>
	<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
<?php }else{ ?>
	<div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
		<?php	
}
		if(isset($adv['property']['images']) && !empty($adv['property']['images'])){
			echo '<div class="fotorama" data-nav="false" data-maxwidth="100%" data-height="300" data-fit="cover">';
			foreach($adv['property']['images'] as $imagePath)
				echo '<img src="'.$imagePath.'">';
			echo '</div>';
		}
		?>
	</div>
<?php if(!$isCabinet){ ?>
	<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 announce">
<?php }else{ ?>
	<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 announce">
<?php } ?>
		<a target="_blank" href="/mls/details/show/<?php echo $adv['id']; ?>">
		<div class="row announce_headline">
			<div class="col-md-8	col-lg-8 announce_name">
				<p><?php
				switch($adv['offer-type']){
					case "Rent": echo 'Аренда '; break;
					case "Sell": echo 'Продажа '; break;
				}
				switch($adv['property']['category']){
					case "Apartment":
					echo $adv['property']['count-rooms'].' к. квартиры';
						break;
					case "Room":
						echo 'комнаты';
						break;
					case "House":
					case "HouseWithLot":
						echo 'дома';
						break;
					case "Lot":
						echo 'участка';
						break;
					default: echo $adv['property']['category']; break;
				} ?></p>
			</div>
			<div class="col-md-4	col-lg-4 announce_price">
				<p><?php echo number_format($adv['price'], 0, ',', ' ').'&#8381;';  
				if(isset($adv['property']["area-total"]) && !empty($adv['property']["area-total"]) 
				&& isset($adv['offer-type']) && !empty($adv['offer-type'])
				&& $adv['offer-type'] == "Sell")
					echo '<br><span class="sub-price" style="font-family: TTNorms-Light;font-size: 14px;color: #676671;letter-spacing: -0.33px;line-height: 35px;"> ('.number_format ($adv['price']/$adv['property']["area-total"],0,"."," ").' &#8381;/кв.м)&nbsp;&nbsp;</span>';
				?></p>
			</div>
		</div>
		<div class="row announce_adress">			
			<?php
			$district = '';					
			if(isset($adv['property']['building']['location']['microdistrict-name']) && !empty($adv['property']['building']['location']['microdistrict-name']))
			{
				$district = '<span>  ('. $adv['property']['building']['location']['microdistrict-name'].')</span>';
			} 
			else if(isset($adv['property']['building']['location']['district-name']) && !empty($adv['property']['building']['location']['district-name']))
				$district = '<span>  ('. $adv['property']['building']['location']['district-name'].')</span>';
			 echo '<p>'.$adv['property']['building']['location']['address'].'</p>'.$district; 
			 ?>			
		</div>
		</a>
		<div class="row announce_param">
			<div class="col-md-3	col-lg-3">
				<p>Площадь</p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p>Этаж</p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p>Обновлено</p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p>ЖК</p>
			</div>
		</div>
		<div class="row announce_numbers">
			<div class="col-md-3	col-lg-3">
				<p><?php 
				if(isset($adv['property']['area-total']))
					echo $adv['property']['area-total'].'м²'; 
				else if(isset($adv['property']['area-room']))
					echo $adv['property']['area-room'].'м²'; 
				else if(isset($adv['property']['lot-area'])){
					echo $adv['property']['lot-area']; 
					if(isset($adv['property']['lot-area-unit'])){
						switch($adv['property']['lot-area-unit']){
							case "SqMeter": echo 'м²'; break;
							case "OneHundred": echo ' соток'; break;
							case "Hectare": echo ' Га'; break;
							default: echo $adv['property']['lot-area-unit']; break;
						}
					}
				}
				else
					var_dump($adv['property']);
				?></p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p><?php echo (isset($adv['property']['floor']) ? $adv['property']['floor']:'').'/'.(isset($adv['property']['building']['floors-total']) && !empty($adv['property']['building']['floors-total'])?$adv['property']['building']['floors-total']:''); ?></p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p><?php if (($timestamp = strtotime($adv['last-update-date'])) !== false) echo date('d.m   H:i', $timestamp);?></p>
			</div>
			<div class="col-md-3	col-lg-3">
				<p><?php if(isset($adv['property']['building']['location']['subdistrict-name'])) echo $adv['property']['building']['location']['subdistrict-name']; ?></p>
			</div>
		</div>
		<div class="row announce_discription">
			<p><?php echo isset($adv['property']['description']) ? mb_substr(strip_tags($adv['property']['description']),0,230, "utf-8").'........':'Описания нет'; ?></p>
		</div>
		<div class="row" style="margin-top:0;margin-bottom:5px;">
			<?php if(!$isCabinet){ ?>
			<div class="col-md-6	col-lg-6 announce_check">
				<!--<label class="checkbox-inline"  ><input type="checkbox" class="checkbox" name="selected_adv[]" value="<?php echo $adv['id']; ?>"><span class="pseudocheckbox">Добавить в отчет</span></label>
			--></div>
			<div class="col-md-6	col-lg-6">	
			</div>
			<?php } else{ 
				echo '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">';
                echo '<a class="btn btn-primary btn-md edit-btn" role="button" target="_blank" href="/mls/add/edit/'.$adv['id'].'">Редактировать</a>';
				echo '</div>';	
			 
				echo '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">';
				echo '<a class="btn btn-primary btn-md btn-send delete-btn" role="button" href_="/mls/cabinet/action?id='.$adv['id'].'&action='.(isset($adv['archive-date']) && !empty($adv['archive-date']) ? 'out-archive' :'in-archive').'">'.(isset($adv['archive-date']) && !empty($adv['archive-date']) ? 'Активировать' :'В архив').'</a>';
				echo '</div>';	

				echo '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">';
				echo '<a class="btn btn-primary btn-md btn-send delete-adv" role="button" adv="'.$adv['id'].'">Удалить</a>';
				echo '</div>';			

				echo '<div class="col-lg-3">';		
				echo '</div>';		
			 } ?>
			</div>
		</div>
	</div>
</div>
