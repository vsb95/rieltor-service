<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .description, .tbl, .map{
        padding:15px;
    }
</style>
<?php         
    if(isset($advertisement) && !empty($advertisement) && $this->user_model->IsLoginedAsSuperAdmin()) {   
        echo '<div class="row round-block">';
        echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >';
        echo '<a href="#" class="spoiler-trigger"><span>DEBUG</span></a>';
        echo '<div class="spoiler-block">';
        var_dump($advertisement);
        var_dump($advertisement['property']['building']['location']);
        var_dump($advertisement['property']['building']);
        var_dump($advertisement['property']);
        var_dump($advertisement['sales-agent']);
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
?>
<div class="row" style="background-color:white;padding:15px;">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" >
        <?php 
        
        if(!isset($advertisement) || empty($advertisement)) {            
            if(isset($result) && !empty($result))
                echo '<h1>'.$result["status"].'</h1>';
            else
                echo '<h1>Объявление не найдено</h1>';
            return;
        }
        if(!isset($advertisement['property']['floor']))
            $advertisement['property']['floor'] = '';
        //var_dump($advertisement);
        //var_dump($advertisement['property']['building']['location']);
        $address = $advertisement['property']['building']['location']['address'];
        if(isset($advertisement['property']['building']['location']['microdistrict-name']) 
            && !empty($advertisement['property']['building']['location']['microdistrict-name']))
            $address.=' ('.$advertisement['property']['building']['location']['microdistrict-name'].'; '
                .$advertisement['property']['building']['location']['district-name'].')';
            else if(isset($advertisement['property']['building']['location']['district-name']) 
                    && !empty($advertisement['property']['building']['location']['district-name']))
                    $address.=' ('.$advertisement['property']['building']['location']['district-name'].')';
        
        $subtitle = '';
        switch($advertisement['offer-type']){
            case "Rent": $subtitle= 'Аренда '; break;
            case "Sell": $subtitle= 'Продажа '; break;
        }
        switch($advertisement['property']['category']){
            case "Apartment":$subtitle.= (isset($advertisement['property']['IsNewFlat']) && $advertisement['property']['IsNewFlat'] ? ' новостройки ' :''). $advertisement['property']['count-rooms'].' к. квартиры';break;
            case "Room":$subtitle.= 'комнаты';break;
            case "House":
            case "HouseWithLot":
                $subtitle.= 'дома';
                break;
            case "Lot":$subtitle.= 'участка'; break;
            default: $subtitle.= $advertisement['property']['category']; break;
        }
        $subtitle .= ' за '.number_format($advertisement['price'], 0,'.', ' ').'&#8381;';
        if($advertisement['offer-type'] == 'Rent' && isset($advertisement['price-period'])){
            $subtitle .=$advertisement['price-period'] == 'Month'?'/мес.':'/сут.';
        }
        else if($advertisement['offer-type'] == 'Sell' && isset($advertisement['property']["area-total"])){
            $subtitle .='<span class="sub-price" style="margin:5px;font-family: TTNorms-Light;font-size: 14px;color: #676671;letter-spacing: -0.33px;line-height: 35px;">('.number_format ($advertisement['price']/$advertisement['property']["area-total"],0,"."," ").' &#8381;/кв.м)</span>';
        }
        $unit = ' м²'; 
        $unitLot = ' м²'; 
        if(isset($advertisement['property']['lot-area-unit'])){
            switch($advertisement['property']['lot-area-unit']){
                case 'OneHundred': $unitLot = ' сот.';  break;
                case 'Hectare': $unitLot = ' Га.';  break;
                case 'SqMeter': $unitLot = ' м²';  break;
            }
        }
        ?>
        <div class="page-title">
            <h3 style="margin-top: 10px;"><?php echo isset($advertisement['title'])?$advertisement['title']:''; ?></h3>
            <h4 style="margin-top: 10px;"><?php echo isset($subtitle) && !empty($subtitle)?$subtitle:''; ?></h4>
            <p class="page-adress"><?php echo $address; ?></p>
        </div>
        <?php 
        if(isset($advertisement['property']['images']) && !empty($advertisement['property']['images'])){
            echo '<div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-maxwidth="1000" data-maxheight="500" data-width="95%" data-ratio="800/600">';
            foreach($advertisement['property']['images'] as $imagePath)
                echo '<img src="'.$imagePath.'">';
            echo '</div>';
        }
        ?>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" >
                <div class="table-responsive-sm table-responsive-xs tbl">
                    <table class="table table-hover">
                        <tbody>
                        <?php 

                            foreach($advertisement['property'] as $name => $value){
                                if(is_bool($value)) continue;
                                    $isContinue = false;
                                switch($name){                             
                                    case "images":                           
                                    case "building":                           
                                    case "category":                           
                                    case "offered-rooms":                         
                                    case "description":                           
                                    case "area-unit":                           
                                    case "CreateDt":                             
                                    case "youtube":                             
                                    case "renovation-id":                                
                                    case "count-rooms":                   
                                    case "lot-area-unit":
                                        $isContinue = true;
                                        break;  
                                    case "cadastral-number":$name = "Кадастровый номер";break;        
                                    case "CountBathroom":
                                        if($advertisement['property']['category'] == 'House' || $advertisement['property']['category'] == 'HouseWithLot') 
                                            $isContinue = true;
                                        $name = "Совмещенных санузлов";
                                        break;      
                                    case "CountToilet": 
                                        if($advertisement['property']['category'] == 'House' || $advertisement['property']['category'] == 'HouseWithLot') 
                                            $isContinue = true;
                                        $name = "Раздельных санузлов";
                                        break;          
                                    case "ceiling-height":$name = "Высота потолков";break;                                        
                                    case "room-type":
                                        $name="Планировка комнат";
                                        switch($value){
                                            case "Isolated": $value = "Изолированные"; break;
                                            case "Neighbor": $value = "Смежные"; break;  
                                            case 'NeighborIsolated': $value = "Смежно-Изолированные"; break;            
                                            default: break;
                                        }
                                        break;                         
                                    case "balcon-count":$name = "Кол-во балконов";break;      
                                    case "loggia-count":$name = "Кол-во лоджий";break;          
                                    case "Toilet": 
                                        if($advertisement['property']['category'] != "House" && $advertisement['property']['category'] != "HouseWithLot") $isContinue = true;
                                        $name = "Туалет"; 
                                        $value = $value?'Есть':'Нет'; 
                                        break;              
                                    case "part-size":$name = "Размер доли"; break;              
                                    case "renovation":$name = "Ремонт"; break;              
                                    case "flat-place-type":
                                        $name = "Расположение в доме";     
                                        $value =$value== 'Angle'?'Угловая':'Не угловая';
                                        break;                                
                                    case "window-view":
                                        $name = "Окна";
                                        switch($value){
                                            case '1': $value="Во двор"; break;
                                            case '2': $value="На улицу"; break;
                                            case '3': $value="Во двор и на улицу"; break;
                                        }
                                        break;                                       
                                    case "toilet-type":
                                        $name = "Санузел";
                                        switch($value){
                                            case 'inner': $value="В доме"; break;
                                            case 'outer': $value="На улице"; break;
                                        }
                                        break;                     
                                    case "floor":
                                        if($advertisement['property']['category'] == "Lot") 
                                            break;
                                        $name = "Этаж"; $value=$value.'/'.(isset($advertisement['property']['building']['floors-total'])?$advertisement['property']['building']['floors-total']:'?'); break;  
                                    case "area-living":$name = "Жилая площадь"; $value.=$unit; break;  
                                    case "area-total":$name = "Площадь общая";  $value.=$unit; break; 
                                    case "area-room":$name = "Площадь комнаты";  $value.=$unit; break;   
                                    case "area-kitchen":$name = "Площадь кухни";  $value.=$unit; break;   
                                    case "lot-area":$name = "Площадь участка";  $value.=$unitLot; break;     
                                    case "lot-area-type":
                                        $name = "Статус участка";  
                                        switch($value){
                                            case 'Garden': $value = 'Садоводчество'; break;
                                            case 'Person': $value = 'ИЖС'; break;
                                        }
                                        break; 
                                    case "furniture-fill":
                                        $name = "Мебель";  
                                        switch($value){
                                            case 'Full': $value = 'Полностью'; break;
                                            case 'Part': $value = 'Частично'; break;
                                            case 'OnRequest': $value = 'По договоренности'; break;
                                        }
                                        break;     
                                    default: $name="prop.".$name; break;
                                }
                                if(!$isContinue)
                                    echo '<tr><td>'.$name.'</td><td>'.(is_array($value)? var_dump($value) :$value).'</td></tr>';
                            }
                            foreach($advertisement as $name => $value){
                                if($name == 'property' || $name == 'sales-agent'|| $name == 'category' 
                                    || $name == 'offer-status' ||$name == 'offer-type' || $name == 'title' 
                                    || $name == 'agent_fee' || $name == 'commission' || $name == 'id' || $name == 'price-period' || is_bool($value)
                                    || $name == 'utilities-type' || $name == 'utilities-needle'|| $name == 'utilities-price') 
                                    continue;
                                switch($name){                                                                        
                                    case "commission-type": 
                                        $name = "Комиссия"; 
                                        switch($value){
                                            case 'Null': $value="Не указано"; break;
                                            case 'None': $value="Нет"; break;
                                            case 'Fixed': $value=number_format($advertisement['commission'], 0,'.', ' ').'&#8381;';  break;
                                            case 'Percent': $value=$advertisement['agent_fee'].'%;';  break;
                                        }                                    
                                        break;
                                    case "prepay": $name = "Предоплата"; $value.=' мес'; break;
                                    case "rent-pledge": $name = "Залог"; $value.=' р'; break;      
                                    case "price": 
                                        $name = "Стоимость"; 
                                        $value=number_format($value, 0,'.', ' ').'&#8381;'; 
                                        if(isset($advertisement['utilities-needle']) && !empty($advertisement['utilities-needle'])){
                                            switch($advertisement['utilities-needle']){
                                                case 'full': $value.=' + к/у и счетчики'; break;
                                                case 'utilities': $value.=' + к/у'; break;
                                                case 'counter': $value.=' + счетчики'; break;
                                                case 'none': break;
                                            }
                                        }
                                        if(isset($advertisement['utilities-price']) && !empty($advertisement['utilities-price'])){
                                            $value.=' ('.$advertisement['utilities-price'].' &#8381;)';
                                        }
                                        break;                                    
                                    case "creation-date": $name = "Дата создания"; $value = date('H:i d.m.Y',strtotime($value)); break;                          
                                    case "last-update-date": $name = "Дата обновления"; $value = date('H:i d.m.Y',strtotime($value)); break;                                  
                                    case "archive-date": $name = "Дата архивации"; $value = date('H:i d.m.Y',strtotime($value)); break;                                    
                                    case "sell-type": 
                                        $name = "Тип продажи";
                                        $value = $value == 'Alternative'?'Альтернатива':'Свободная продажа';
                                        break;                                                              
                                    case "contract-type": 
                                        $name = "Договор"; 
                                        switch($value){
                                            case 'Exclusive': $value='Эксклюзив'; break;
                                            case 'Soft': $value='Мягкий'; break;
                                            case 'None': $value='Нет'; break;
                                            default: break;
                                        }
                                        break;            
                                    default: $name="offer.".$name; break;
                                }
                                echo '<tr><td>'.$name.'</td><td>'.(is_array($value)? var_dump($value) :$value).'</td></tr>';
                            }
                            
                            // ToDo Вывести "о здании"
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 15px;" >
                <?php 
                foreach($advertisement['property'] as $name => $value){
                    if(!is_bool($value)
                    || $name =="Toilet" && $advertisement['property']['category'] != "House" && $advertisement['property']['category'] != "HouseWithLot") continue;
                    if($name == 'IsNewFlat' && !$value) continue;
                    switch($name){                           
                        case "is-studio": $name ="Студия"; break;     
                        case "Toilet": $name ="Туалет"; break;                   
                        case "Phone": $name ="Телефон"; break;                    
                        case "RoomFurniture": $name ="Мебель в комнатах"; break;                    
                        case "Television": $name ="Телевизор"; break;                    
                        case "WashingMachine": $name ="Стиральная машина"; break;                    
                        case "Dishwasher": $name ="Посудомоечная машина"; break;                    
                        case "Refrigerator": $name ="Холодильник"; break;                    
                        case "WithChildren": $name ="С детьми"; break;                    
                        case "WithPets": $name ="С животными"; break;                      
                        case "AirConditioner": $name ="Кондиционер"; break;                        
                        case "Shower": $name ="Душевая кабина"; break;                        
                        case "Internet": $name ="Интернет"; break;                             
                        case "Garage": $name ="Гараж"; break;                         
                        case "ElectricitySupply": $name ="Электричество"; break;                          
                        case "HeatingSupply": $name ="Отопление"; break;                         
                        case "Bath": $name ="Ванна"; break;                                   
                        case "Sauna": $name ="Баня"; break;                           
                        case "Outbuilding": $name ="Хоз. постройки"; break;                        
                        case "WaterSupply": $name ="Водоснабжение"; break;                                       
                        case "SewerageSupply": $name ="Канализация"; break;                                       
                        case "GasSupply": $name ="Газ"; break;                                       
                        case "Pool": $name ="Бассейн"; break;                                      
                        case "IsNewFlat": $name ="Новостройка"; break;                    
                        default: 
                            $name ='prop.'.$name;
                            break;
                    }
                    echo '<tr><td><label class="checkbox-inline" style="display:block;"><input type="checkbox" class="checkbox" disabled '
                    .($value?'checked':'').'><span class="pseudocheckbox">'.$name.'</span></label></td></tr>';
                }
                foreach($advertisement as $name => $value){
                    if(!is_bool($value)) continue;
                    switch($name){                                                                        
                        case "is-haggle": $name = "Торг уместен"; break;                                       
                        case "is-utilities-included": $name = "К\\У вкл"; break;                                      
                        case "is-electricity-included": $name = "Счетчики вкл"; break;                                      
                        case "is-on-prepayment": $name = "Под авансом/задатком"; break;                                                            
                        case "is-can-mortgage": $name = "Возможна ипотека"; break;                     
                        case "CreateDt": $name = "Дата создания"; break;             
                        default: 
                            $name ='offer.'.$name;
                            break;
                    }
                    echo '<tr><td><label class="checkbox-inline" style="display:block;"><input type="checkbox" class="checkbox" disabled '
                    .($value?'checked':'').'><span class="pseudocheckbox">'.$name.'</span></label></td></tr>';
                }
                ?>
            </div>
        </div>
        
        <div class="description"><?php echo isset($advertisement['property']['description']) ? $advertisement['property']['description']:'Описания нет'; ?></div>
        <?php 
        if(isset($advertisement['property']['building']['location']['coord']) && !empty($advertisement['property']['building']['location']['coord'])){
            echo '<div class="map"><img style="width:100%" src="http://static.maps.2gis.com/1.0?zoom=15&size=500,350&markers='.$advertisement['property']['building']['location']['coord']["lon"].','.$advertisement['property']['building']['location']['coord']["lat"].'"/></div>';
        }
        ?>
    </div>
    <div class ="col-lg-4 col-md-4 col-sm-12 col-xs-12" <?php if(!isset($advertisement)) echo 'style="display:none;"'; ?>>
        <div class="round-block">
            
            <?php
            if(isset($IsCanEdit) && $IsCanEdit == true){
                echo '<a class="btn btn-primary btn-md" style="margin-bottom:15px;" role="button" href="/mls/add/edit/'.$advertisement['id'].'">Редактировать</a>';
                
                echo '<a class="btn btn-primary btn-md" style="margin-bottom:15px;" role="button" href="/mls/cabinet/action?id='.$advertisement['id'].'&action='.(isset($advertisement['archive-date']) && !empty($advertisement['archive-date']) ? 'out-archive' :'in-archive').'">'.(isset($advertisement['archive-date']) && !empty($advertisement['archive-date']) ? 'Активировать' :'В архив').'</a>';
                
                echo '<a class="btn btn-primary btn-md delete-adv" style="margin-bottom:15px;" role="button" adv="'.$advertisement['id'].'">Удалить</a>';            
            }
            if(isset($advertisement['sales-agent']['photo']))
                echo '<div class="right_headline" style="position: inherit; display:inline-block;"><div class="lk_img" style=" width: 50px; height: 50px;position: inherit;  background-image: url('.$advertisement['sales-agent']['photo'].')"><img src=""></div></div>';
                
            if(isset($advertisement['sales-agent']['name']))
                echo '<p style="font-size: 18pt; ">'.$advertisement['sales-agent']['name'].'</p>';
            if(isset($advertisement['sales-agent']['org']['name']))
                echo '<p style="font-size: 12pt;">'.$advertisement['sales-agent']['org']['name'].'</p>';
            if(isset($advertisement['sales-agent']['phone']))
                echo '<a class="btn btn-primary btn-md btn-main" style="width:49%; float: left;" href="tel:'.$advertisement['sales-agent']['phone'].'" role="button">'.$advertisement['sales-agent']['phone'].'</a>';
            if(isset($advertisement['sales-agent']['phone-alternative']))
                echo '<a class="btn btn-primary btn-md btn-main" style="width:49%; float: right;" href="tel:'.$advertisement['sales-agent']['phone-alternative'].'" role="button">'.$advertisement['sales-agent']['phone-alternative'].'</a>';
            
            echo '<br><br><br>';
            ?>
        </div>
    <?php
    if(isset($advertisement['property']['youtube']) && !empty($advertisement['property']['youtube']))
        echo '<div class="round-block"><iframe width="100%"  height="350px" src="'.str_replace('watch?v=','/embed/', $advertisement['property']['youtube']).'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
    if($IsLogined){
    ?>
        <div class="round-block">
            <div class="page-title">
                <p style="font-size: 18pt;">Комментарии</p>
            </div>
            <ul style="list-style: none; padding:0;">
                <?php 
                if(isset($comments) && !empty($comments)){
                    //var_dump($comments);
                    foreach($comments as $comment){
                        $date = new DateTime($comment['date']);
                        echo '<li><div class="round-block-item"><p class="round-block-title">'.$date->format('H:i d.m').'&emsp;'
                            .(empty($comment['user_name']) ? $comment['user_email']:$comment['user_name'])
                            .'</p><p class="round-block-text">'.$comment['comment'].'</p></div></li>';
                    }
                }
                else{
                    echo '<li style="color: #676671;">Еще никто не оставлял комментариев об этом объекте. Вы будете первым</li>';
                }						
                ?>
            </ul>	
            <div class="comments-form">
                <form id="comments-form" action="/board/comment" method="POST">
                    <input type="hidden" name="is_mls" value="1">
                    <input type="hidden" name="id_page" value="<?php echo $id_page; ?>">
                    <textarea name="comment" id="comment-form-text" required rows="3" style="width:100%" placeholder="Ваш комментарий..."></textarea>
                </form>
                <button id="btn-comment" style="width:100%; margin:0;" class="btn btn-primary btn-md">Отправить</button>
            </div>
        </div>
    <?php } ?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#btn-comment").click(function() {
		if(!$("#comment-form-text").val().trim()){
			$("#modalContent").html("Комментарий не заполнен");
			openModal();
			return;
		}
		var $form = $("#comments-form");
		$.ajax({
			type: $form.attr('method'),
			url: $form.attr('action'),
			data: $form.serialize()
		}).done(function(msg) {
			if(msg == "done")
				$("#modalContent").html("Ваш комментарий добавлен!");
			else
				$("#modalContent").html(msg);
			openModal();
		}).fail(function(err) {
			$("#modalContent").html(err);
			openModal();
		});
		//отмена действия по умолчанию для кнопки submit
	});
});
</script>