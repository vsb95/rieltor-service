<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row search">
    <div class="row" style="margin-top: 0">
        <div class="col-md-8	col-lg-8">
            <div class="row">
                <div class="col-md-6	col-lg-6 search_stat"><p  id="total-by-filter"></p></div>
                <div class="col-md-6	col-lg-6 search_sort"><p>Сначала новые</p><img></div>
            </div>
        </div>
        <div class="col-md-4	col-lg-4">
            <div class="row" style="padding-right:20px">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php 
                    if(!isset($_GET['advType']) || $_GET['advType'] != 'archive')
                        echo '<a class="btn btn-primary btn-md btn-send" style="margin:auto;margin-top:0;" href="/mls/cabinet?advType=archive" role="button">Архив</a>';
                    ?>                    
                </div>                
            </div>
        </div>
    </div>
    <div class="row search_elem">    
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-md-push-8 col-lg-push-8">
            <div style="background-color:white;padding: 25px;box-shadow: 0px 0 5px rgba(200,200,200,0.2);border-radius: 7px;color: rgb(190, 190, 190);border: 1px solid rgb(228, 226, 226);overflow: hidden;"> 
                <p class="help-title">Фильтрация</p>
                <form id="frm">
                    <input type="hidden" id="IndexPage" name="IndexPage" />
                    <?php 
                    if(isset($advType))
                        echo '<input type="hidden" name="advType" value="'.$advType.'">';
                    ?>
                    <p style="margin:0; padding:0; margin-top:5px;">Тип сделки</p>
                    <div class="btn-group"style="margin:10px 0;">
                        <select name="OfferType" id="OfferTypeSelect">
                            <option value="" selected>Все</option>
                            <option value="rent">Аренда</option>
                            <option value="sell">Продажа</option>
                        </select>
                    </div>
                    <p style="margin:0; padding:0; margin-top:5px;">Категория</p>
                    <div class="btn-group"style="margin:10px 0;">
                        <select name="OfferCategory" id="OfferCategorySelect">
                            <option value="" selected>Все</option>
                            <option value="living">Жилая</option>
                            <option value="commercial" disabled>Коммерческая</option>
                        </select>
                    </div>
                    <p style="margin:0; padding:0; margin-top:5px;">Объект</p>
                    <div class="btn-group"style="margin:10px 0;">
                        <select name="ObjectTypeSelect" id="ObjectTypeSelect"  >
                            <option value=""  selected>Все</option>
                            <option class="select-living" value="apartment">Квартира</option>
                            <option class="select-sale select-living" value="apartment_new">Новостройка</option>
                            <option class="select-living select-rent" value="room">Комната</option>
                            <option class="select-living" value="house">Дом\коттедж\дача</option>
                            <option class="select-sale select-living" value="apartment_part">Доля в квартире</option>
                            <!--<option class="select-sale select-living" value="house_part">Доля в доме\коттедже</option>-->
                            <option class="select-sale select-living" value="lot">Участок</option>
                        </select>
                    </div>
                    <div id="count-room" style="display:none">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Количество комнат</button>
                            <ul class="dropdown-menu" role="menu">
                                <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="1" checked><span class="pseudocheckbox">1</span></label></li>
                                <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="2" checked><span class="pseudocheckbox">2</span></label></li>
                                <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="3" checked><span class="pseudocheckbox">3</span></label></li>
                                <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="4" checked><span class="pseudocheckbox">4</span></label></li>
                                <li><label class="checkbox-inline"><input type="checkbox" class="checkbox cbox-count-rooms" name="CountRooms[]" value="999" checked><span class="pseudocheckbox">5+</span></label></li>
                            </ul>  
                        </div>
                    </div>
                    <p>Районы</p>       
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Выбрать</button>
                        <ul class="dropdown-menu" role="menu">
                            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="DistrictsArr[]" value="35"><span class="pseudocheckbox">Центральный</span></label></li>
                            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="DistrictsArr[]" value="36"><span class="pseudocheckbox">Октябрьский</span></label></li>
                            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="DistrictsArr[]" value="33"><span class="pseudocheckbox">Советский</span></label></li>
                            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="DistrictsArr[]" value="40"><span class="pseudocheckbox">Ленинский</span></label></li>
                            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="DistrictsArr[]" value="31"><span class="pseudocheckbox">Кировский</span></label></li>
                        </ul>  
                    </div>
                    <p>Стоимость</p>            
                    <input type="text" placeholder="От" name="CostMin" class="form-control" style="display: inline-block; max-width:48%;">
                    <input type="text" placeholder="До" name="CostMax" class="form-control" style="display: inline-block; max-width:48%;">
                    
                    <?php 
                        if($this->user_model->IsLoginedAsAdmin()){
                            echo '<div style="margin-top:15px;">  <div class="radio_button" style="width:60%"><input type="radio" class="radio" id="radio_31" name="FilterBy" value="org" checked><label for="radio_31" class="radio-inline">По организации</label></div>
                                    <div class="radio_button"style="width:40%"><input type="radio" class="radio" id="radio_32" name="FilterBy" value="user"><label for="radio_32" class="radio-inline">Только мои</label></div>
                                </div>';
                            echo '<input type="hidden" name="OrganizationId" value="'.$_SESSION['id_org'].'">';
                        }
                        echo '<input type="hidden" name="UserId" value="'.$_SESSION['id_user'].'">';
                    ?>
                    <a class="btn btn-primary btn-md btn-main" role="button" id="search-btn" style="margin:10px 0;">Показать</a>
                </form>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-md-pull-4 col-lg-pull-4" id="searchResult">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8	col-lg-8">
        <ul class="pagination" id="pagination">
        </ul>
    </div>
    <div class="col-md-4	col-lg-4">
        <div class="row">
        </div>
    </div>
</div>
<div class="row" style="height: 50px;">
</div>
<script type="text/javascript">
var indexPage = 0;
$(document).ready(function() {      
    $('#OfferTypeSelect').change(function() {
        var value = $(this).val();
        if(value=="rent"){
            if($("#ObjectTypeSelect option:selected").hasClass( "select-sale" )){
                $("#ObjectTypeSelect option:selected").attr('selected', false);
            }
        }else{
            if($("#ObjectTypeSelect option:selected").hasClass( "select-rent" )){
                $("#ObjectTypeSelect option:selected").attr('selected', false);
            }
        } 
        HideObjTypes();
    });
    $('#OfferCategorySelect').change(function() {
        var value = $(this).val();
        if(value=="living"){
            if($("#ObjectTypeSelect option:selected").hasClass( "select-commercial" )){
                $("#ObjectTypeSelect option:selected").attr('selected', false);
            }
        }else{
            if($("#ObjectTypeSelect option:selected").hasClass( "select-living" )){
                $("#ObjectTypeSelect option:selected").attr('selected', false);
            }
        } 
        HideObjTypes();
    });
    $('#ObjectTypeSelect').change(function() {
        var value = $(this).val();
        if(value=="apartment" || value=="apartment_new"){
            $('#count-room').css('display', 'block');
        }else{
            $('#count-room').css('display', 'none');
        }
    });
    function HideObjTypes(){
        var offerType = $('#OfferTypeSelect').val();
        var offerCategoryValue = $('#OfferCategorySelect').val();
        $(".select-commercial").each(function(item) {
            $(this).css('display', offerCategoryValue == "living" ?'none':'block');
        });
        $(".select-living").each(function(item) {
            $(this).css('display', !offerCategoryValue || offerCategoryValue == "living" ?'block':'none');
        });
        $(".select-sale").each(function(item) {
            $(this).css('display', offerType == "rent" ?'none':'block');
        });
        $(".select-rent").each(function(item) {
            $(this).css('display', !offerType || offerType == "rent" ?'block':'none');
        });
    }
    $("#search-btn").click(function(e) {
        indexPage = 0;
        getAdv();
    });

    <?php
        if(isset($offerType))
            echo '$("#OfferTypeSelect").val("'.$offerType.'");';
        if(isset($objType)){
            $objType = str_replace('kvartira','apartment',$objType);
            echo '$("#ObjectTypeSelect").val("'.$objType.'");';
        }
        
        echo 'getAdv();';
    ?>

    
});
$(document).on("click", "#pagination>li>a", function(e) {
    var index = $(this).attr('index');
    indexPage = index;
    $('#pagination>li').each(function(){
        $(this).removeClass('active');
    });
    $(this).parent().addClass('active');      
    getAdv();
});

function InitPagination(total){
    var html = '';
    html+='<li class="active"><a href="#" index="0">1</a></li>';
    for(var i=1; i<total/50; i++){
        html+='<li><a index="'+i+'">'+(i+1)+'</a></li>';
    }
    $('#pagination').html(html);            
}   
function getAdv() {
    var $preloader = $('#page-preloader'),
        $spinner = $preloader.find('#spinner');
    $spinner.text("Загружаем объявления...");
    $spinner.fadeIn();
    $preloader.delay(100).fadeIn('slow');
    var form = $("#frm");
    $("#searchResult").html("");
    $("#total-by-filter").html("");
    $("#IndexPage").val(indexPage);
    $.ajax({
        type: "GET",
        url: "/mls/search/searching",
        data: form.serialize()+"&cabinet=1", // serializes the form's elements.
        success: function (result) {
            try {
                if (result == null) {
                    $spinner.fadeOut(1);
                    $preloader.fadeOut(1);
                    $("#modalContent").html('нет ответа от сервера');
                    openModal();
                    return;
                }
                var json = JSON.parse(result);
                if(!json["html"] && json["total-count-by-filter"] == 0)
                    json["html"] = "<p>У вас нет объявлений по заданному фильтру</p>"
                $("#total-by-filter").html("Найдено "+json["total-count-by-filter"]+" результата");
                if(indexPage == 0)
                    InitPagination(json["total-count-by-filter"]);
                $("#searchResult").html(json["html"]);
                $('.fotorama').fotorama();
                        
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                $("#modalContent").html(result);
                openModal();
            }
            $spinner.fadeOut();
            $preloader.delay(50).fadeOut('slow');
        },
        error: function (result) {
            $spinner.fadeOut();
            $preloader.delay(50).fadeOut('slow');  
            console.log(result);            
            try {
                if(result != null && result.statusText == "timeout"){
                    $("#modalContent").html("Время ожидания операции истекло. Повторите еще раз");
                    openModal();
                    return;
                }
                var json = JSON.parse(result);
                alert('error: '+JSON.stringify(json));
                document.body.appendChild(document.createTextNode(JSON.stringify(json)));
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                $("#modalContent").html('Ошибка: '+result);
                openModal();
            }
        },
        timeout: 15 * 60 * 1000
    });
};

$(document).on("click", ".delete-btn", function(e) {
    e.preventDefault();
    e.stopPropagation();
    var href=$(this).attr('href_');
    if(!href){
        alert('Не удалось выбрать объявление. Попробуйте перезагрузить страницу');
        return;
    }
    var btn = $(this);
    $.ajax({
        type: "GET",
        url: href,
        success: function (result) {
            console.log(result);    
            try {
                if (result == null) {
                    $("#modalContent").html('нет ответа от сервера');
                    openModal();
                    return;
                }
                if(result == "OK"){
                    var row = btn.closest('.row.search_result');
                    row.css("display",'none');   
                }           
                else{
                    $("#modalContent").html(result);
                    openModal();
                }             
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                $("#modalContent").html(result);
                openModal();
            }
        },
        error: function (result) {
            console.log(result);            
            try {
                if(result != null && result.statusText == "timeout"){
                    $("#modalContent").html("Время ожидания операции истекло. Повторите еще раз");
                    openModal();
                    return;
                }
                var json = JSON.parse(result);
                alert('error: '+JSON.stringify(json));
                document.body.appendChild(document.createTextNode(JSON.stringify(json)));
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                $("#modalContent").html('Ошибка: '+result);
                openModal();
            }
        },
        timeout: 15 * 60 * 1000
    });  
    
});
</script>
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async defer></script>
<script src="//yastatic.net/share2/share.js" async defer></script>