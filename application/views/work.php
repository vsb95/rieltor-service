<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 	
	if(!isset($isServerOnline) || empty($isServerOnline) || !$isServerOnline){
		echo '<div class="row inner-div">';
		echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:20px;">';
		echo '<h3>Приносим свои извинения<br/><br/>Сервис временно не доступен :(</h3>';
		echo '</div>';
		echo '</div>';
		return;
	}
?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">    
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <form id="frm" method="POST" action="<?php echo $action; ?>">
                    <textarea required id="linksTextArea" name="links" rows="15" placeholder="Вставьте сюда ссылки на подходящие объявления" style="height:400px"></textarea>
                    <br/>             
                    <label class="checkbox-inline">
                        <input type="checkbox" class="checkbox select-adv" id="isCutLogo" name="isCutLogo" value="1">
                        <span class="pseudocheckbox">Очистить фото</span>
                    </label>
                    <br>
                    <button id="send" class="btn btn-primary btn-md create-btn" style="width:250px; margin-top:25px;"><?php echo $title; ?></button>
                </form>	
                <div id="result" class="hidden">
                    <p id="result-title">Все получилось!</p>
                    <div id="result-time"></div>
                    <p id="result-content"></p>
                    <div class="ya-share2" id="shareBlock" style="display:none;" data-services="vkontakte,whatsapp,viber" 
                    data-title="Подборка для вас"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="search_job"> 
                    <p class="search_job_head">Помощь</p>
                    <ul>
                        <li>Скопируйте ссылки и вставьте в поле ввода подходящие объявления</li>
                        <li>Вы можете вставлять ссылки из Avito, MLSN, N1 и Yandex</li>
                        <li>Нажмите кнопку «<?php echo $title; ?>»</li>                 
                        <li>Выберите «Очистить фото» если необходимо убрать водяные знаки с фотографий объектов</li>        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $("#frm").submit(function(e) {
        e.preventDefault();
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('#spinner');   
    $spinner.text("<?php echo $spinner_title; ?>");   
    $spinner.fadeIn();
    $preloader.delay(350).fadeIn('slow');
    FormatLinks();
    
    var form = $("#frm");
    var url = form.attr('action');
    var data = $("#frm").serialize();
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(result) {
            try {
                $spinner.fadeOut();
                $preloader.delay(200).fadeOut('slow');
                //console.log(result);
                $('#result-title').html('');
                var resultContentDiv = document.getElementById("result-content");
                var resultTimeDiv = document.getElementById("result-time");
                if (result == null) {
                    alert('нет ответа от сервера');
                    return;
                }
                var json = JSON.parse(result);
                if (json == null || json["status-type"] == null) {
                    console.log(json);
                    alert('нет ответа от сервера');
                    return;
                }
                if(json["status-type"] == "ERROR"){
                    alert(json["status"]);
                    resultContentDiv.innerText = json["status"];
                    return;
                }
                if(json["status-type"] == "EMPTY"){
                    alert('Поле с ссылками не заполнено');
                    $('#result-title').html('Поле с ссылками не заполнено');
                    return;
                }
                $('#result-title').html('Все получилось!');
                
                //
                if(json["link"]!= null){

                    if(json["link-download"]!= null){
                        newA = document.createElement("a");
                        newA.setAttribute("target", "_blank");
                        newA.setAttribute("rel", "noopener noreferrer");
                        newA.setAttribute("href", json["link-download"]);
                        newA.setAttribute("class", "result-link");
                        newA.appendChild(document.createTextNode("Скачать архив с фотографиями"));
                        resultContentDiv.appendChild(newA);              
                    }
                    else{                    
                        resultContentDiv.appendChild(document.createTextNode("Ваша ссылка:"));
                        resultContentDiv.appendChild(document.createElement("br"));       
                        var newA = document.createElement("a");
                        newA.setAttribute("target", "_blank");
                        newA.setAttribute("rel", "noopener noreferrer");
                        newA.setAttribute("href", json["link"]);
                        newA.setAttribute("class", "result-link");
                        newA.appendChild(document.createTextNode(json["link"]));
                        resultContentDiv.appendChild(newA);
                        
			            $('#shareBlock').css('display', 'block');
			            $('#shareBlock').attr('data-url', json["link"]);
                        var share = Ya.share2('#shareBlock');
                        share.updateContent({
                            url: json["link"]
                        });
                    }
                    resultContentDiv.appendChild(document.createElement("br"));                
                }
                //
                if(json["unparsed-urls"].length >0){
                    var newDiv = document.createElement("div");
                    newDiv.setAttribute("class", "unparsed-url-list");
                    newDiv.appendChild(document.createTextNode("Не удалось распознать ссылки:"));
                    var newul = document.createElement("ul");
                    json["unparsed-urls"].forEach(function(element) {
                        var newli = document.createElement("li");

                        var newLiA = document.createElement("a");
                        newLiA.setAttribute("href", element);
                        newLiA.setAttribute("class", "unparsed-link");
                        newLiA.setAttribute("rel", "noreferrer");
                        newLiA.appendChild(document.createTextNode(element));
                        newli.appendChild(newLiA);
                        newul.appendChild(newli);
                    });
                    newDiv.appendChild(newul);
                    resultContentDiv.appendChild(newDiv);
                }
                
                if(json["debug"] != null ){
                    var newDiv = document.createElement("div");
                    newDiv.innerHTML=json["debug"];
                    resultContentDiv.appendChild(newDiv);
                }

                resultTimeDiv.appendChild(document.createTextNode("Запрос занял: "+json["total-sec"]+" сек"));
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                alert('Ошибка '+error.message);
            }
            finally{            
                document.getElementById("result").classList.remove("hidden");
            }

        },
        error: function(result) {
            $spinner.fadeOut();
            $preloader.delay(50).fadeOut('slow');  
            console.log(result);
            
            try {
                if(result != null && result.statusText == "timeout"){
                    alert("Время ожидания операции истекло. Повторите еще раз");
                    return;
                }
                var json = JSON.parse(result);
                alert('error: '+JSON.stringify(json));
                document.body.appendChild(document.createTextNode(JSON.stringify(json)));
            } catch (error) {
                console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
                console.log('result =  ' + result);
                alert('Ошибка: '+result);
            }
        },
		timeout: 5*60*1000
    });
  });

  
});
    var ctrlDown = false,
        ctrlKey = 17,
        cmdKey = 91,
        vKey = 86,
        cKey = 67;

        $(linksTextArea).keydown(function(e) {
            if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
        }).keyup(function(e) {
            if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
    });
    function FormatLinks(){
            var linksTextArea = document.getElementById("linksTextArea");
            var newValue = "";

            var lines = linksTextArea.value.split("\n");
            for(j=0;j<lines.length;j++){
                if(lines[j] && 0 !== lines[j].length){
                    var values = lines[j].split("http");
                    for(i=0;i<values.length;i++){
                        if(values[i] && 0 !== values[i].length){
                            newValue= newValue+ "http"+values[i]+"\n";
                        }
                    }
                }
            }
            linksTextArea.value =newValue;
            linksTextArea.setSelectionRange(linksTextArea.value.length,linksTextArea.value.length);
        } 

    $(linksTextArea).keyup(function(e) {        
        if (ctrlDown && e.keyCode == vKey){
            FormatLinks();
        }
    });
</script>
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async defer></script>
<script src="//yastatic.net/share2/share.js" async defer></script>