<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>

.checkbox-inline {
		display: block;
	}
</style>
<?php 	
	if(!isset($initData) || empty($initData)){
		echo '<div class="row inner-div">';
		echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:20px;">';
		echo '<h3>Приносим свои извинения<br/><br/>Сервис временно не доступен :(</h3>';
		echo '</div>';
		echo '</div>';
		return;
	}
?>
<div class="row param round-block">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<form id="frm" action="/board/search" method="post">		
			<input type="hidden" id="mkrs" name="microdistricts" />
			<input type="hidden" id="selDistricts" name="districts" />
			<input type="hidden" id="lastIndex" name="IndexPage" />
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<p>Поиск по адресу</p>
					<input type="text" name="searchAdress" id="address" placeholder="Адрес..." 
					<?php if(isset($_SESSION['name_city']) && !empty($_SESSION['name_city'])) echo 'value="'.$_SESSION['name_city'].', "';?> 
					class="form-control"/>
				</div>
			</div>
			<!-- -->
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<p>Тип сделки</p>
					<div class="radio_button"><input type="radio" class="radio" id="option1" checked onchange="RentTypeCheck(this)" name="renttype" value="2" checked><label for="option1" class="radio-inline">Аренда</label></div>
					<div class="radio_button"><input type="radio" class="radio" id="option2" onchange="RentTypeCheck(this)" name="renttype" value="1"><label for="option2" class="radio-inline">Продажа</label> </div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">	
					<p>Тип недвижимости</p>
					<div class="btn-group">
						<select id="objTypeSelect" name="objTypeSelect">
							<option value="0" selected>Квартиры</option>
							<option value="1">Комната</option>
							<option value="2">Дом\Коттедж</option>
							<option value="3">Коммерческая</option>
							<option value="4" id="sellTypeOption" hidden>Земля\Участок</option>
						</select>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<p>Право собственности</p>
					<div class="radio_button"><input type="checkbox" class="radio" id="IsAgencyNeeded"
							name="IsAgencyNeeded" value="1"><label for="IsAgencyNeeded"
							class="radio-inline">Агентства</label></div>
					<div class="radio_button"><input type="checkbox" class="radio" id="IsSelfmanNeeded"
							name="IsSelfmanNeeded" value="1"><label for="IsSelfmanNeeded"
							class="radio-inline">Без комиссии</label></div>
				</div>
			</div>
			<!-- -->
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<p>Стоимость</p>
					<input type="text" name="costmin" class="form-control" placeholder="От"style="display: inline-block; max-width:49.1%;">
					<input type="text" name="costmax" class="form-control" placeholder="До" style="display: inline-block; max-width:49.1%;">
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" id="floor_label">
					<p>Этаж</p>
					<input type="text" name="minFloor" placeholder="От" class="form-control" id="floor_min"
						style="display: inline-block; max-width:49.1%;">
					<input type="text" name="maxFloor" placeholder="До" class="form-control" id="floor_max"
						style="display: inline-block; max-width:49.1%;">
					<label class="checkbox-inline" id="IsNotLastFloorCb"><input type="checkbox" class="checkbox"
							name="IsNotLastFloor" value="1"><span class="pseudocheckbox">Не последний</span></label>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" id="size_label">
					<p>Общая площадь</p>
					<input type="text" name="sizemin" class="form-control" placeholder="От" id="size_min"
						style="display: inline-block; max-width:49.1%;">
					<input type="text" name="sizemax" class="form-control" placeholder="До" id="size_max"
						style="display: inline-block; max-width:49.1%;">
				</div>
			</div>
			<!-- -->
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" id="countRoomsDiv">
					<p>Количество комнат</p>
					<div class="radio_button"><input type="checkbox" class="radio" id="count_room_1"
							name="CountRooms[]" value="1" checked><label for="count_room_1"
							class="radio-inline">1</label></div>
					<div class="radio_button"><input type="checkbox" class="radio" id="count_room_2"
							name="CountRooms[]" value="2"><label for="count_room_2" class="radio-inline">2</label>
					</div>
					<div class="radio_button"><input type="checkbox" class="radio" id="count_room_3"
							name="CountRooms[]" value="3"><label for="count_room_3" class="radio-inline">3</label>
					</div>
					<div class="radio_button"><input type="checkbox" class="radio" id="count_room_4"
							name="CountRooms[]" value="4"><label for="count_room_4" class="radio-inline">4</label>
					</div>
					<div class="radio_button"><input type="checkbox" class="radio" id="count_room_5"
							name="CountRooms[]" value="5"><label for="count_room_5" class="radio-inline">5+</label>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">		
					<p>Источники</p>
					<?php 		
						echo '<input type="hidden" name="IdCity" id="city" value="'.$initData["id_city"].'">';

						if(isset($initData["sources"]) && !empty($initData["sources"])){
							for($i=0; $i< count($initData["sources"]);$i++){
								$source = $initData["sources"][$i];
								echo '<div class="radio_button"><input type="checkbox" class="radio" name="sources[]" value='.$source["id"].' id="source_'.$source["id"].'">';
								echo '<label for="source_'.$source["id"].'" class="radio-inline">'.$source["name"].'</label></div>';
								if($source["name"] == "Mlsn")
								echo '<div class="radio_button" id="isMls"><input type="checkbox" class="radio" id="isMls_1" name="isMls" value="1"><label for="isMls_1" class="radio-inline">MLSN млс</label></div>';
							}
						}				
					?>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<p>Микрорайоны</p>
					<div class="btn-group btn-group-map">
						<div class="button-map" data-toggle="modal" data-target="#modalMap">Выбрать</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">	
					<p>Сортировка</p>
					<div class="btn-group">
						<select name="OrderType" >
							<option value="0" selected>Сначала новые</option>
							<option value="1">Сначала старые</option>
							<option value="2">Сначала дешевые</option>
							<option value="3">Сначала дорогие</option>
						</select>	
					</div>
				</div>
			</div>
			<div class="row" style="margin: 20px 0 ;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
					<a href="#" class="spoiler-trigger"><span>Расширенный фильтр</span></a>
				</div>
			</div>
			<div class="row" id="extended-search" style="display: none;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<input type="text" name="searchPhrase" placeholder="Поиск в тексте..." id="searchPhrase" class="form-control">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p>Доп. параметры</p>					
					<div class="radio_button"><input type="checkbox" class="radio" id="IsOnlyWithPhoto"
							name="IsOnlyWithPhoto" value="1" checked><label for="IsOnlyWithPhoto"
							class="radio-inline">Только с фото</label></div>
					<div class="radio_button"><input type="checkbox" class="radio" id="IsOnlyMerged"
							name="IsOnlyMerged" value="1" checked><label for="IsOnlyMerged"
							class="radio-inline">Убрать дубликаты</label></div>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" id="IsWithFurniture">	
					<p>Мебель</p>
					<div class="radio_button"><input type="checkbox" class="radio" id="IsWithFurnitures_1"
							name="IsWithFurnitures[]" value="1" ><label for="IsWithFurnitures_1"
							class="radio-inline">Укомплектована</label></div>
					<div class="radio_button"><input type="checkbox" class="radio" id="IsWithFurnitures_0"
							name="IsWithFurnitures[]" value="0" ><label for="IsWithFurnitures_0"
							class="radio-inline">Без мебели</label></div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="selltypes" style="display:none">	
					<p>Тип продажи</p>
					<div class="radio_button"><input type="checkbox" class="radio" id="sell_type_1"
							name="selltypes[]" value="1"><label for="sell_type_1"
							class="radio-inline">Новостройка</label></div>
					<div class="radio_button"><input type="checkbox" class="radio" id="sell_type_2"
							name="selltypes[]" value="2"><label for="sell_type_2"
							class="radio-inline">Вторичка</label></div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="housetypes" style="display:none;">
					<?php
						if(isset($initData["house-types"]) && !empty($initData["house-types"])){
							echo '<p>Материал дома</p>';
							for($i=0; $i< count($initData["house-types"]);$i++){
								$type = $initData["house-types"][$i];			
								echo '<label class="checkbox-inline"><input type="checkbox" class="checkbox" name="housetypes[]" value="'.$type["id"].'"><span class="pseudocheckbox">'.$type["name"].'</span></label>';		
							}
						}	
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<input type="submit" value="Показать" class="btn btn-primary btn-md" role="button" id="send">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div id="container" style="display:none;">
						<div class="inner-container">
							<div class="toggle">
								<p><i class="fa fa-list" aria-hidden="true"></i></p>
							</div>
							<div class="toggle">
								<p><i class="fa fa-th-large" aria-hidden="true"></i></p>
							</div>
						</div>
						<div class="inner-container" id='toggle-container'>
							<div class="toggle">
								<p><i class="fa fa-list" aria-hidden="true"></i></p>
							</div>
							<div class="toggle">
								<p><i class="fa fa-th-large" aria-hidden="true"></i></p>
							</div>
						</div>
					</div>
					<br />
				</div>
			</div>
	<div class="modal fade" id="modalMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" id="modal-map-dialog" style="width:90%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Районы и микрорайоны</h4>
				</div>
				<div class="modal-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-district-map" data-toggle="tab" id="district-modal-map">На карте</a></li>
						<li ><a href="#tab-district-checkboxes" data-toggle="tab" id="district-modal-list">Список</a></li>
					</ul>
					
					<!-- Tab panes -->
					<div class="tab-content" id="district-tab-content" style="height:65vh;max-height:65vh;">
						<div class="tab-pane " id="tab-district-checkboxes">                        
							
						</div>
						<div class="tab-pane active" id="tab-district-map">
							<div style="margin:10px;">
								<p style="margin:0;">* Однократное нажатие - выбор района\микрорайона</p>
								<p style="margin:0;">* Двукратное нажатие - открыть микрорайоны в районе</p> 
							</div>
							<div id="map" style="width:100%; height:65vh"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div style="width: 50%;margin: auto;">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Выбрать</button>
						<button type="button" class="btn btn-primary btn-main">Сбросить</button>
					</div>
				</div>
			</div>
		</div>
	</div>
		</form>
	</div>
</div>

<div class="search" id="result-row" style="display:none">
    <div class="row">
        <div class="col-md-8	col-lg-8">
            <div class="row">
                <div class="col-md-6	col-lg-6 search_stat">
                    <p id="total-by-filter"></p>
                </div>
                <div class="col-md-6	col-lg-6 search_sort">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <form id="frmReport" action="/board/report" method="post">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-md-push-8 col-lg-push-9">
				<div id="side_block" class="search_job" style="z-index:999; width:100%">
					<div class="row search_job_head">
						<p>Работа с объявлениями</p>
					</div>
					<div class="row search_discr">
						<a class="btn btn-primary btn-md btn-main" role="button" id="isSelectAll">Выбрать все</a>
					</div>
					<div class="row search_discr">
						<p>Выберите "Очистить фото" если необходимо убрать водяные знаки с фотографий объектов</p>
						<label class="checkbox-inline">
							<input type="checkbox" class="checkbox select-adv" id="isCutLogo" name="isCutLogo" value="1">
							<span class="pseudocheckbox">Очистить фото</span>
						</label>
						<input type="submit" class="btn btn-primary btn-md disabled create-btn" role="button" value="Сформировать" />
					</div>
					<div id="reportResult"></div>
					<div class="ya-share2" id="shareBlock" style="display:none;" data-services="vkontakte,whatsapp,viber" data-title="Подборка для вас"></div>
				</div>
            </div>
            <input type="hidden" name="my_obj" id="my_obj" />
            <div style="display:none" id="hidden-fields">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 col-md-pull-4 col-lg-pull-3" id="search-result">
            </div>
    	</form>
    </div>
    <div id="showMore" class="row" style="visibility:hidden;">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div id="showMoreBtn" class="btn btn-primary btn-md" style="margin-top:20px;">Показать еще</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> </div>
    </div>
</div>
<script type="text/javascript">
	var isCanSlide = false;
	var selltype =0;
	$(document).ready(function () {
		$("#objTypeSelect").on("change", function (e) {
			AddFilterCheck();
		});
		$("#isSelectAll").on("click", function() {
			isSelectAll = !isSelectAll;
			$("#search-result input:checkbox").prop('checked', isSelectAll);
			var checkedCount = $("#search-result input:checkbox:checked").length;
			if (checkedCount < 1) {
				$('.create-btn').addClass('disabled');
			} else {
				$('.create-btn').removeClass('disabled');
			}
		});
		$(document).on('click', '.spoiler-trigger', function (e) {
			e.preventDefault();
			$(this).toggleClass('active');
			$('#extended-search').slideToggle(300);			
		});

		var i = 1;
		$("#showMoreBtn").click(function (e) {
			i++;
			document.getElementById("lastIndex").value = i;
			getAdv();
		});
		$("#frm").submit(function (e) {
			i = 0;
			document.getElementById("lastIndex").value = i;
			e.preventDefault(); // avoid to execute the actual submit of the form.
			getAdv();
		});
		$("#frmReport").submit(function (e) {			
			var $preloader = $('#page-preloader'),
				$spinner = $preloader.find('#spinner');
			$spinner.text("Формируем отчет...");
			$spinner.fadeIn();
			$preloader.delay(350).fadeIn('slow');

			var form = $("#frmReport");
			var url = form.attr('action');

			// сериализуем поисковую форму
			$("#lastIndex").val(0);
			var searchRequest = $("#frm").serialize();
			$("#searchRequest").val(searchRequest);
			$("#lastIndex").val(i);
			$.ajax({
				type: "POST",
				url: url,
				data: form.serialize(), // serializes the form's elements.
				success: function (result) {
					try {
						if (!result) {
							alert('нет ответа от сервера');
						}
						var json = JSON.parse(result);
						if (json["status-type"] != "OK") {
							$spinner.fadeOut();
							$preloader.delay(50).fadeOut('slow');
							alert(json["status"]);
							return;
						}
						console.log(json["dbg"]);
						var link = '<br><p>Ваш отчет: </p><a target="_blank" href="'+json["link"]+'">'+json["link"]+'</a><br>';
						$('#reportResult').html(link);

			            $('#shareBlock').css('display', 'block');
			            $('#shareBlock').attr('data-url', json["link"]);
                        var share = Ya.share2('#shareBlock');
                        share.updateContent({
                            url: json["link"]
                        });
					} catch (error) {
						console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
						console.log('result =  ' + result);
						alert('Ошибка ' + error.message);
					}
					$spinner.fadeOut();
					$preloader.delay(50).fadeOut('slow');
				},
				error: function (result) {
					$spinner.fadeOut();
					$preloader.delay(50).fadeOut('slow');
					console.log(result);
					var json = JSON.parse(result);
					alert('error: ' + JSON.stringify(json));
					document.body.appendChild(document.createTextNode(JSON.stringify(json)));
				},
				timeout: 15 * 60 * 1000
			});

			e.preventDefault(); // avoid to execute the actual submit of the form.
		});

		function getAdv() {
			var $preloader = $('#page-preloader'),
				$spinner = $preloader.find('#spinner');
			$spinner.text("Подбираем варианты...");
			$spinner.fadeIn();
			$preloader.delay(200).fadeIn('slow');
			$("#mkrs").val(selectedMicroDistrict);
			$("#selDistricts").val(selectedDistricts);
			var form = $("#frm");
			var url = form.attr('action');
			$.ajax({
				type: "POST",
				url: url,
				data: form.serialize(), // serializes the form's elements.
				success: function (result) {
					try {
						if (result == null) {
							$spinner.fadeOut(1);
							$preloader.fadeOut(1);
							$("#modalContent").html('нет ответа от сервера');
							openModal();
							return;
						}
						var json = JSON.parse(result);
						if (json == null || json["status-type"] == null) {
							$spinner.fadeOut(1);
							$preloader.fadeOut(1);
							$("#modalContent").html('нет ответа от сервера');
							openModal();
							return;
						}
						if (json["status-type"] != "OK") {
							$spinner.fadeOut(1);
							$preloader.fadeOut(1);
							$("#modalContent").html(json["status"]);
							openModal();
							return;
						}
						$('#result-row').css('display','block');
						var myNode = document.getElementById("search-result");
						if (i == 0) {
							while (myNode.firstChild) {
								myNode.removeChild(myNode.firstChild);
							}
						}
						var countDiv = document.getElementById("total-by-filter");
						countDiv.innerText = "Всего подходящих объявлений: " + json["total-count-by-filter"];
						if (json["html-pages"] != null) {
							json["html-pages"].forEach(function (page) {
								var newRow = document.createElement("div");
								newRow.setAttribute("class", "row list-item");
								newRow.innerHTML = page;
								myNode.appendChild(newRow);
							});
						}
						$('.fotorama').fotorama();

						var showMoreDiv = document.getElementById("showMore");
						showMoreDiv.style.visibility = 'visible';

					} catch (error) {
						console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
						console.log('result =  ' + result);
						$("#modalContent").html('Ошибка ' + error.message);
						openModal();
					}
					isCanSlide = true;
					$spinner.fadeOut();
					$preloader.delay(50).fadeOut('slow');
				},
				error: function (result) {
					$spinner.fadeOut();
					$preloader.delay(50).fadeOut('slow');  
					console.log(result);
					
					try {
						if(result != null && result.statusText == "timeout"){
							$("#modalContent").html("Время ожидания операции истекло. Повторите еще раз");
							openModal();
							return;
						}
						var json = JSON.parse(result);
						alert('error: '+JSON.stringify(json));
						document.body.appendChild(document.createTextNode(JSON.stringify(json)));
					} catch (error) {
						console.log('Ошибка ' + error.name + ":" + error.message + "\n" + error.stack);
						console.log('result =  ' + result);
						$("#modalContent").html('Ошибка: '+result);
						openModal();
					}
				},
				timeout: 15 * 60 * 1000
			});
		};

		$(".cost").on("change", function () {
			var val = XFormatPrice($(this).val());
			$(this).val(val);
		});

		function XFormatPrice(_number){
			var decimal=0;
			var separator=' ';
			var decpoint = '.';
			var format_string = '#';
		
			var r=parseFloat(_number)
		
			var exp10=Math.pow(10,decimal);// приводим к правильному множителю
			r=Math.round(r*exp10)/exp10;// округляем до необходимого числа знаков после запятой
		
			rr=Number(r).toFixed(decimal).toString().split('.');
		
			b=rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);
		
			r=(rr[1]?b+ decpoint +rr[1]:b);
			return format_string.replace('#', r);
		}
	});

	//добавляем таймер чтобы блок не дергался при быстрой прокрутке
	var side_block_timer = 0;
	//функция для анимации блока
	function side_block_animate() {
		var wnd = $(document).scrollTop();
		var frm = $('#frmReport').offset().top - 75;//$('div#side_block').height() / 4;
		var t = $(document).scrollTop();

		var targetOffset = 0;//$(window).scrollTop() - $(window).height() / 4;
		if (Math.abs(t) > Math.abs(frm))
			targetOffset = t;
		else
			targetOffset = frm;
		$('div#side_block').stop(true) //если анимация была - останавливаем
			.animate({ top: targetOffset - frm }); //смещаем блок к верху скрола

	}
	//при скроле
	$(window).scroll(function () {
		if (!isCanSlide) return;
		//если таймер есть, сбрасываем
		if (side_block_timer) clearTimeout(side_block_timer);
		//устанавливаем таймер для анимации
		side_block_timer = setTimeout(side_block_animate, 100);
	});

	function AddFilterCheck(){
		var objTypeId = $("#objTypeSelect").val();
		// если продажа
		if(selltype == 1){
			$("#sellTypeOption").prop('hidden', false);
			
			$('#IsWithFurniture').css('display', 'none');
			
			$('#isMls').removeClass('active');
			$("#isMls").children().attr("checked", false);
			$('#isMls').children().attr("disabled", "disabled");
			$('#isMls').css('display', 'none');

			$('#IsWithFurniture_on').removeClass('active');
			$("#IsWithFurniture_on").children().attr("checked", false);
			$('#IsWithFurniture_on').children().attr("disabled", "disabled");
			$('#IsWithFurniture_off').removeClass('active');
			$("#IsWithFurniture_off").children().attr("checked", false);
			$('#IsWithFurniture_off').children().attr("disabled", "disabled");
			// квартира
			if(objTypeId == 0){
				$('#selltypes_on').children().removeAttr("disabled");
				$('#selltypes_on').children().removeClass('active');
				$('#selltypes_off').children().removeAttr("disabled");
				$('#selltypes_off').children().removeClass('active');

				$('#selltypes').css('display', 'inherit');	
			}else{
				$('#selltypes_on').children().removeClass('active');
				$('#selltypes_on').children().attr("disabled", "disabled");
				$('#selltypes_off').children().removeClass('active');
				$('#selltypes_off').children().attr("disabled", "disabled");				

				$('#selltypes').css('display', 'none');
			}
			if(objTypeId != 4){
				$('#housetypes').css('display', 'inherit');	
				$('#IsNotLastFloorCb').css('display', 'inline-flex');	
			}else{
				$('#IsNotLastFloorCb').css('display', 'none');	
				$('#housetypes').css('display', 'none');	
			}
		}
		// аренда
		else{
			// сбросить землю
			if(objTypeId == 4){
				$("#objTypeSelect").val(0);
				objTypeId = 0;
			}
			$('#isMls').children().removeAttr("disabled");
			$('#isMls').children().removeClass('active');
			$('#isMls').css('display', 'inherit');

			$("#sellTypeOption").prop('hidden', true);

			$('#selltypes_on').children().removeClass('active');
			$('#selltypes_on').children().attr("disabled", "disabled");
			$('#selltypes_off').children().removeClass('active');
			$('#selltypes_off').children().attr("disabled", "disabled");

			$('#selltypes').css('display', 'none');

			$('#housetypes').css('display', 'none');

			// квартира
			if(objTypeId == 0 || objTypeId == 1){
				$('#IsWithFurniture').css('display', 'inherit');
				$('#IsWithFurniture-label').css('display', 'inherit');

				$('#IsWithFurniture_on').children().removeAttr("disabled");
				$('#IsWithFurniture_on').children().removeClass('active');
				$('#IsWithFurniture_off').children().removeAttr("disabled");
				$('#IsWithFurniture_off').children().removeClass('active');

			}else{
				$('#IsWithFurniture').css('display', 'none');
				$('#IsWithFurniture-label').css('display', 'none');
				
				$('#IsWithFurniture_on').removeClass('active');
				$("#IsWithFurniture_on").children().attr("checked", false);
				$('#IsWithFurniture_on').children().attr("disabled", "disabled");
				$('#IsWithFurniture_off').removeClass('active');
				$("#IsWithFurniture_off").children().attr("checked", false);
				$('#IsWithFurniture_off').children().attr("disabled", "disabled");
				if(objTypeId == 3 || objTypeId == 4){					
					$('#isMls').removeClass('active');
					$("#isMls").children().attr("checked", false);
					$('#isMls').children().attr("disabled", "disabled");
					$('#isMls').css('display', 'none');
				}
			}
		}
		if(objTypeId == 0){
			$('#countRoomsDiv').css('display', 'inherit');
		}else{
			$('#countRoomsDiv').css('display', 'none');
		}
		if(objTypeId == 4){
			$('#floor_label').css('visibility', 'hidden');
			$('#floor_min').css('visibility', 'hidden');
			$('#floor_min').attr("disabled", "disabled");
			$('#floor_max').css('visibility', 'hidden');
			$('#floor_max').attr("disabled", "disabled");
			
			$('#size_label').css('visibility', 'hidden');
			$('#size_min').css('visibility', 'hidden');
			$('#size_min').attr("disabled", "disabled");
			$('#size_max').css('visibility', 'hidden');
			$('#size_max').attr("disabled", "disabled");

		}else{
			$('#floor_label').css('visibility', 'inherit');
			$('#floor_min').css('visibility', 'inherit');
			$('#floor_min').removeAttr("disabled");
			$('#floor_max').css('visibility', 'inherit');
			$('#floor_max').removeAttr("disabled");
			
			$('#size_label').css('visibility', 'inherit');
			$('#size_min').css('visibility', 'inherit');
			$('#size_min').removeAttr("disabled");
			$('#size_max').css('visibility', 'inherit');
			$('#size_max').removeAttr("disabled");
		}
	}
	function RentTypeCheck(e) {
		selltype = e.value;
		AddFilterCheck();
	}
	
    $(document).on("click", "#search-result input:checkbox", function(e) {
		var checkedCount = $("#search-result input:checkbox:checked").length;
		if (checkedCount < 1) {
			$('.create-btn').addClass('disabled');
		} else {
			$('.create-btn').removeClass('disabled');
		}
    });
</script>
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async defer></script>
<script src="//yastatic.net/share2/share.js" async defer></script>