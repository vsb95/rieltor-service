<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
          *{
            font-size: 12px !important;
          }
          body { margin: 1cm; }
      .accept-preview {
        display: inline-block;
        color: white;
        background: #71bc37;
        border: solid #71bc37;
        border-width: 10px 20px 8px;
        font-weight: bold;
        border-radius: 4px; }

       h5 {
        margin-bottom: 20px;
        line-height: 1.25;
        font-size: 16px; }

      p {
        font-size: 16px;
        font-weight: normal;
        margin-bottom: 20px; }
      .preview-container {
        display: block !important;
        clear: both !important;
        margin: 0 auto !important;
        max-width: 700px !important;

        font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        line-height: 1.65; ;
        }
        .preview-container table {
          width: 100% !important;
          border-collapse: collapse; }
        .preview-container .masthead {
          padding: 50px 0;
          background: #71bc37;
          color: white; }
        .preview-container .preview-content {
          background: white;
          padding: 30px 35px; }
          .preview-container .preview-content.footer {
            background: none; }
            .preview-container .preview-content.footer p {
              margin-bottom: 0;
              color: #888;
              text-align: center;
              font-size: 14px; }
            .preview-container .preview-content.footer a {
              color: #888;
              text-decoration: none;
              font-weight: bold; }

        .td-head{
            text-align: center;
            font-weight: bold;
            padding: 15px;
        }
        .data-table{
          text-align: left;
          margin-bottom: 50px;
          display: inline-block;
          max-width: 45%;
        }
        .data-table, .data-table tr td {
          border-bottom: 1px solid black;
          padding:5px;
        }
        .data-table tr:hover {background-color: #f5f5f5;}
    </style>
</head>
<body>  

  <table class="preview-container">
      <tr>
          <td style="background-color: white;text-align:left;"> 
            <?php            
            if(!isset($isDescription) || !$isDescription){
              if(!isset($current_user) || empty($current_user))
                die('Вы не авторизованы');
              if(isset($current_user['org']))
                echo 'Организация: '.$current_user['org']['name'].'<br>';
              echo 'Риэлтор: '.$current_user['full_name'];
              if(isset($current_user['phone']))
                echo '<br>т.'.$current_user['phone'];
              if(isset($current_user['email']))
                echo '<br>email: <a href="mailto:'.$current_user['email'].'">'.$current_user['email'].'</a>';
            }
            ?>  
          </td>
      </tr> 
  </table>     
  <div class="preview-content">                        
    <table class="data-table">
      <tr><td>Cумма кредита</td><td><?php echo $params['summ']; ?></td></tr>  
      <tr><td>Cумма первоначального взноса</td><td><?php echo $params['first_summ']; ?></td></tr>  

      <tr><td colspan="2" class="td-head">Клиент</td></tr>  
      <?php
        foreach($params['client'] as $key => $val){
          if(is_array($val) || $key=='another-doc-descr') continue;
          echo '<tr>';
          switch($key){
              case 'name': echo '<td>ФИО</td>'; break;
              case 'birthday': echo '<td>Дата рождения</td>'; break;
              case 'phone': echo '<td>Телефон</td>'; break;
              case 'education': echo '<td>Образование</td>'; break;
              case 'little-child': echo '<td>Детей до 18</td>'; break;
              case 'married': echo '<td>Семейное положение</td>'; break;
              case 'married-doc': echo '<td>Есть брачный договор</td>'; $val = $val == 1?'Да':'Нет'; break;
              default: echo '<td>'.$key.'</td>'; break;
          }
          echo '<td>'.$val.'</td>';
          echo '</tr>';
        }
        if(isset($params['client']['old']) && !empty($params['client']['old'])
        && isset($params['client']['old']['name']) && !empty($params['client']['old']['name'])){
          echo '<tr><td>Смена ФИО</td><td>';
          echo 'Старое ФИО: '.$params['client']['old']['name'].'<br>';
          echo 'Дата смены: '.$params['client']['old']['date'].'<br>';
          echo 'Причина: '.$params['client']['old']['reason-descr'].'<br>';
          echo '</td></tr>';
        }
      ?>
      

      <tr><td colspan="2" class="td-head">Информация о доходах и расходах</td></tr>                          
      <?php
        foreach($params['client']['finances'] as $key => $val){
          if(is_array($val)) continue;
          echo '<tr>';
          switch($key){
              case 'proof': echo '<td>Способ подтверждения дохода</td>'; break;
              case 'bank': echo '<td>Банк, в котором получает зарплату\пенсию</td>'; break;
              case 'main': echo '<td>Основной доход</td>'; break;
              case 'alt': echo '<td>Дополнительный доход</td>'; break;
              case 'outlay': echo '<td>Расходы</td>'; break;
              default: echo '<td>'.$key.'</td>'; break;
          }
          echo '<td>'.$val.'</td>';
          echo '</tr>';
        }
      ?>

      <tr><td colspan="2" class="td-head">Место работы</td></tr>                     
      <?php
      if(isset($params['client']['lawyer']) && $params['client']['finances']['proof'] == 'ИП, нотариус, адвокат'){                            
        foreach($params['client']['lawyer'] as $key => $val){
          if($key == 'type-descr') continue;
          echo '<tr>';
          switch($key){
              case 'inn': echo '<td>ИНН</td>'; break;
              case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $params['client']['lawyer']['type-descr']; break;
              case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
              default: echo '<td>'.$key.'</td>'; break;
          }
          echo '<td>'.$val.'</td>';
          echo '</tr>';
        }
      }else if(isset($params['client']['org'])){
        foreach($params['client']['org'] as $key => $val){
          if($key == 'type-descr') continue;
          echo '<tr>';
          switch($key){
              case 'name': echo '<td>Название организации</td>'; break;
              case 'inn': echo '<td>ИНН</td>'; break;
              case 'phone': echo '<td>Телефон</td>'; break;
              case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $params['client']['org']['type-descr']; break;
              case 'staff-count': echo '<td>Кол-во сотрудников</td>'; break;
              case 'doc-type': echo '<td>Вид трудового договора</td>'; break;
              case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
              case 'exp-total': echo '<td>Общий трудовой стаж</td>'; break;
              case 'position-type': echo '<td>Категория занимаемой должности</td>'; break;
              case 'position-name': echo '<td>Должность</td>'; break;
              default: echo '<td>'.$key.'</td>'; break;
          }
          echo '<td>'.$val.'</td>';
          echo '</tr>';
        }
      }
      ?>

    <?php
    if(isset($params['client']['another-doc-descr']) && !empty($params['client']['another-doc-descr'])){
      echo '<tr><td colspan="2" class="td-head">Комментарий к дополнительным документам</td></tr> ';
      echo '<tr><td colspan="2" >'.$params['client']['another-doc-descr'].'</td></tr> ';
    }
    ?>
    </table>
                      
    <?php                        
    if(isset($params['co-borrowers'])){                          
        foreach($params['co-borrowers'] as $coBorrower){
          echo '<table class="data-table">';
          echo '<tr><td colspan="2" class="td-head">Созаемщик</td></tr>  ';
          foreach($coBorrower as $key => $val){
            if(is_array($val) || $key=='another-doc-descr') continue;
            echo '<tr>';
            switch($key){
              case 'name': echo '<td>ФИО</td>'; break;
              case 'birthday': echo '<td>Дата рождения</td>'; break;
              case 'phone': echo '<td>Телефон</td>'; break;
              case 'education': echo '<td>Образование</td>'; break;
              case 'little-child': echo '<td>Дети до 18</td>'; break;
              case 'married': echo '<td>Семейное положение</td>'; break;
              case 'married-doc': echo '<td>Есть брачный договор</td>'; $val = $val == 1?'Да':'Нет'; break;
              default: echo '<td>'.$key.'</td>'; break;
            }
            echo '<td>'.$val.'</td>';
            echo '</tr>';
          }
          if(isset($coBorrower['old']) && !empty($coBorrower['old'])
          && isset($coBorrower['old']['name']) && !empty($coBorrower['old']['name'])){
            echo '<tr><td>Смена ФИО</td><td>';
            echo 'Старое ФИО: '.$coBorrower['old']['name'].'<br>';
            echo 'Дата смены: '.$coBorrower['old']['date'].'<br>';
            echo 'Причина: '.$coBorrower['old']['reason-descr'].'<br>';
            echo '</td></tr>';
          }
          echo '<tr><td colspan="2" class="td-head">Информация о доходах и расходах</td></tr>';
          foreach($coBorrower['finances'] as $key => $val){
            if(is_array($val)) continue;
            echo '<tr>';
            switch($key){
              case 'proof': echo '<td>Способ подтверждения дохода</td>'; break;
              case 'bank': echo '<td>Банк, в котором получает зарплату\пенсию</td>'; break;
              case 'main': echo '<td>Основной доход</td>'; break;
              case 'alt': echo '<td>Дополнительный доход</td>'; break;
              case 'outlay': echo '<td>Расходы</td>'; break;
              default: echo '<td>'.$key.'</td>'; break;
            }
            echo '<td>'.$val.'</td>';
            echo '</tr>';
          }
          echo '<tr><td colspan="2" class="td-head">Место работы</td></tr> ';
          if(isset($coBorrower['lawyer']) && $coBorrower['finances']['proof'] == 'ИП, нотариус, адвокат'){                            
            foreach($coBorrower['lawyer'] as $key => $val){
              if($key == 'type-descr') continue;
              echo '<tr>';
              switch($key){
                  case 'inn': echo '<td>ИНН</td>'; break;
                  case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $coBorrower['lawyer']['type-descr']; break;
                  case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
                  default: echo '<td>'.$key.'</td>'; break;
              }
              echo '<td>'.$val.'</td>';
              echo '</tr>';
            }
          }else if(isset($coBorrower['org'])){
            foreach($coBorrower['org'] as $key => $val){
              if($key == 'type-descr') continue;
              echo '<tr>';
              switch($key){
                  case 'name': echo '<td>Название организации</td>'; break;
                  case 'inn': echo '<td>ИНН</td>'; break;
                  case 'phone': echo '<td>Телефон</td>'; break;
                  case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $coBorrower['org']['type-descr']; break;
                  case 'staff-count': echo '<td>Кол-во сотрудников</td>'; break;
                  case 'doc-type': echo '<td>Вид трудового договора</td>'; break;
                  case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
                  case 'exp-total': echo '<td>Общий трудовой стаж</td>'; break;
                  case 'position-type': echo '<td>Категория занимаемой должности</td>'; break;
                  case 'position-name': echo '<td>Должность</td>'; break;
                  default: echo '<td>'.$key.'</td>'; break;
              }
              echo '<td>'.$val.'</td>';
              echo '</tr>';
            }
          }
          
          if(isset($coBorrower['another-doc-descr']) && !empty($coBorrower['another-doc-descr'])){
            echo '<tr><td colspan="2" class="td-head">Комментарий к дополнительным документам</td></tr> ';
            echo '<tr><td colspan="2" >'.$coBorrower['another-doc-descr'].'</td></tr> ';
          }
          echo '</table>';
        }
    }
    if(isset($params['description']) && !empty($params['description'])){
      echo '<br><h5  class="noprint">Комментарий</h5> <p  class="noprint" style="text-align: left;">'.$params['description'].'</p>';
    }
    ?>
</div>   
</body>
</html>