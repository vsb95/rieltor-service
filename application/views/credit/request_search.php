<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .show-description{
        background-color: white;
        padding:10px 5px;
        box-shadow: 0 2px 1px -1px rgba(0,0,0,0.12), 0 1px 4px 0 rgba(0,0,0,0.10);
        border-radius: 7px;
        border: 1px solid gray;
        text-align: center;
    }
    .show-description:hover{
        background-color: #bdffc8da;
        cursor: pointer;
    }
</style>
<div class="row" style="background: white; padding:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">
        <div style="display: inline-block" >
            <input type="text" name="name" id="inputName" placeholder="Поиск по фио..." class="form-control" style="width: 277px" onkeydown="filtering()"/>
        </div>
        <div style="float: right;">
            <a class="btn btn-primary btn-md btn-main" role="button" href="/credit/NewRequest" style="margin:10px 0;">+ Создать заявку</a>
        </div>
        <div class="radio_" style="margin: 15px 0;">
            <div class="radio_button"><input type="radio" class="radio" id="radio_1_0" name="status-type" value="-1" checked><label for="radio_1_0" class="radio-inline">Все</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_1_1" name="status-type" value="0"><label for="radio_1_1" class="radio-inline">Отправленные</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_1_4" name="status-type" value="3"><label for="radio_1_4" class="radio-inline">В работе</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_1_5" name="status-type" value="4"><label for="radio_1_5" class="radio-inline">На доработке</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_1_2" name="status-type" value="1"><label for="radio_1_2" class="radio-inline">Одобренные</label></div>
            <div class="radio_button"><input type="radio" class="radio" id="radio_1_3" name="status-type" value="2"><label for="radio_1_3" class="radio-inline">Отмененные</label></div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-hover table-dark">
            <thead>
                <tr>
					<th>Дата</th>
					<th>Фио</th>
					<th>Статус</th>
					<th>Банк</th>
					<th>Описание </th>
                </tr>
            </thead>
            <tbody id="table_body">
                <?php 
                    foreach($requests as $request){
                        $status = 'Неизвестно ('.$request["status"].')';
                        switch($request["status"]){
                            case 0: $status = 'Отправлена'; break;
                            case 1: $status = 'Одобрена'; break;
                            case 2: $status = 'Отменена'; break;
                            case 3: $status = 'В работе'; break;
                            case 4: $status = 'На доработке'; break;
                        }
                        echo '<tr status="'.$request["status"].'" text="'.$request["name"].'">';
                        echo '<td>'.$request["date"].'</td>';
                        echo '<td>'.$request["name"].'</td>';
                        echo '<td>'.$status.'</td>';
                        echo '<td>'.$request['bank_name'].'</td>';
                        echo '<td><div class="show-description" data="'.$request["id"].'">Описание</div></td>';
                        echo '</tr>';
                    }                
                ?>
            </tbody>
        </table>
    </div>
 </div>
<div class="row" style="height: 150px;"> </div>
<script type="text/javascript">
    $('input').on('change', function() {
        filtering();
    });

    function filtering(){
        var statusType=  $('input[name=status-type]:checked').val();
        var text = $('#inputName').val().toLowerCase();
        $("#table_body tr").each(function(i){
            var row = $(this);
            var statusRow = row.attr("status"); 
            var textRow = row.attr("text").toLowerCase();
            if(statusType != -1 && statusRow != statusType){
                row.css('display','none');
            }else{
                if(!text || textRow.startsWith(text)){
                    row.css('display','table-row'); 
                }else{                    
                    row.css('display','none');
                }
            }
        });
    }
    $(document).on('click', '.show-description', function (e) {
        var id = $(this).attr("data");
        $.ajax({
            type: "GET",
            url: "/credit/requestdescription/"+id,
            success: function(result) {
                $("#modalContent").html(result);
                openModal();  
            },        
            timeout: 5*60*1000
        });
    });
    $(document).ready(function() {
        <?php 
        if(isset($_GET) && isset($_GET['client']) && !empty($_GET['client'])){
            echo "$('#inputName').val('".$_GET['client']."'); ";
            echo 'filtering();';
        } 
        ?>
    });
</script>