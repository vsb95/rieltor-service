<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .table caption{
        font-size: 16px;
    }
    td{
        text-align: left; 
    }
</style>
<div class="container" style="width:80%">
    <div class="row">
        <div class="col-lg-8">
            <hr>
            <h4><?php echo isset($bank_program['p_name']) ? $bank_program['p_name']: '"Программа"'; ?></h4>
            <hr>
            <table class="table">
                <thead>
                    <tr>
                        <th>"Новостройки / Вторичное жилье</th>
                        <th>"Комментарий к программе"</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row"><?php 
                        if (isset($bank_program['type'])) 
                        switch($bank_program['type']){
                            case "new": echo "Новостройки"; break;
                            case "old": echo "Вторичное жилье"; break;
                            case "both": echo 'Новостройки и вторичное жилье'; break;
                            default: echo $bank_program['type'];break;
                        } ?></td>
                        <td scope="row"><?php if (isset($bank_program['description'])) echo str_replace('\n','<br>', $bank_program['description']); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-8">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="row">Первоначальный взнос</th>
                        <th>Ставка</th>
                        <th>Срок кредита</th>
                        <th>Возраст</th>
                        <th>Сумма кредита</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row"><?php if (isset($bank_program['first_percent'])) echo number_format($bank_program['first_percent'], 2, '.',' '); ?> %</td>
                        <td scope="row"><?php if (isset($bank_program['percent'])) echo number_format($bank_program['percent'], 2,  '.',' '); ?> %</td>
                        <td scope="row"><?php if (isset($bank_program['month_min']) && isset($bank_program['month_max'])) echo 'от '.number_format(($bank_program['month_min']/12), 1,  '.',' ').' до '.number_format(($bank_program['month_max']/12), 1,  '.',' ').' лет'; ?></td>
                        <td scope="row"><?php if (isset($bank_program['age'])) echo $bank_program['age']; ?></td>
                        <td scope="row"><?php if (isset($bank_program['summ_min']) && isset($bank_program['summ_max'])) echo 'от '.(is_numeric($bank_program['summ_min']) ? number_format($bank_program['summ_min'], 0,  '.',' '):$bank_program['summ_min']).' '.(is_numeric($bank_program['summ_max']) ?'до '.number_format($bank_program['summ_max'], 0,  '.',' '):$bank_program['summ_max']); ?></td>
                    </tr>
                    <tr>
                        <td scope="row"><?php if (isset($bank_program['first_percent_descr'])) echo $bank_program['first_percent_descr']; ?></td>
                        <td scope="row"><?php if (isset($bank_program['percent_descr'])) echo $bank_program['percent_descr']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-8">
            <hr>
            <h4>Общая информация о банке</h4>
            <hr>
            <table class="table">
                <caption>Менеджер: <?php echo $bank['manager_name'].' т.'.$bank['manager_phone']; ?></caption>
                <thead>
                    <tr>
                        <th scope="row" colspan="2">Стаж работы</th>
                        <th>Оценочные компании</th>
                        <th>Страховые компании</th>
                        <th>Подтверждение дохода</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row"><i>Общий</i></td>
                        <td scope="row"><?php if (isset($bank['experience_total_requred'])) echo $bank['experience_total_requred']; ?></td>
                        <td scope="row" rowspan="2"><?php
                            if(isset($evaluation_companies))
                                foreach($evaluation_companies as $val)
                                    echo $val['name'].'<br>';  ?>
                        </td>
                        <td scope="row" rowspan="2"><?php
                            if(isset($insurance_companies))
                                foreach($insurance_companies as $val)
                                    echo $val['name'].'<br>';  ?>
                        </td>
                        <td scope="row" rowspan="2"><?php
                            if(isset($income_proofs))
                                foreach($income_proofs as $val)
                                    echo $val['name'].'<br>';;  ?>
                        </td>
                    </tr>
                    <tr>
                        <td><i>На последнем месте</i></td>
                        <td scope="row"><?php if (isset($bank['experience_last_requred'])) echo $bank['experience_last_requred']; ?></td>
                    </tr>
                </tbody>
            </table>
            <hr>
        </div>
        <div class="col-lg-4 col-md-4">
            <table class="table">
                <caption>Безопасные сделки</caption>
                <thead>
                    <tr>
                        <td scope="row"><i>Тип</i></td>
                        <td><i>Стоимость</i></td>
                        <td><i>Условия</i></td>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($bank['is_has_cell']) && $bank['is_has_cell'] == '1') {?>
                    <tr>
                        <td scope="row">Ячейка</td>
                        <td scope="row"><?php if(isset($bank['cell_cost'])) echo number_format($bank['cell_cost'], 0, '.',' '); ?></td>    
                        <td scope="row"><?php if(isset($bank['cell_descr'])) echo $bank['cell_descr']; ?></td>       
                    </tr>   
                    <?php } ?>
                    <?php if(isset($bank['is_has_accreddive']) && $bank['is_has_accreddive'] == '1') {?>
                    <tr>
                        <td scope="row">Аккредитив</td>
                        <td scope="row"><?php if(isset($bank['accreddive_cost'])) echo number_format($bank['accreddive_cost'], 0, '.',' '); ?></td>    
                        <td scope="row"><?php if(isset($bank['accreddive_descr'])) echo $bank['accreddive_descr']; ?></td>             
                    </tr>   
                    <?php } ?>
                    <?php if(isset($bank['another_name']) && !empty($bank['another_name'])) {?>
                    <tr>
                        <td scope="row"><?php echo $bank['another_name']; ?></td>
                        <td scope="row"><?php if(isset($bank['another_cost'])) echo number_format($bank['another_cost'], 0, '.',' '); ?></td>    
                        <td scope="row"><?php if(isset($bank['another_descr'])) echo $bank['another_descr']; ?></td>             
                    </tr>   
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4 col-md-4">
            <table class="table">
                <caption>Дополнительно</caption>
                <tbody>
                    <tr>
                        <td scope="row">Узнать причину отказа</td>
                        <td scope="row"><?php if (isset($bank['feedback'])) echo $bank['feedback']; ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Принимают анкеты других банков</td>
                        <td scope="row"><?php if (isset($bank['is_can_another_bank'])) echo $bank['is_can_another_bank']== 1 ?'Да':'Нет'; ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Рассматривают объекты с перепланировкой</td>
                        <td scope="row"><?php if (isset($bank['is_can_redevelopment'])) echo $bank['is_can_redevelopment'] == 1 ?'Да':'Нет'; ?></td>
                        <?php if (isset($bank['redevelop_descr']) && !empty($bank['redevelop_descr'])) 
                            echo '<td scope="row">'.$bank['redevelop_descr'].'</td>'; ?>
                    </tr>
                    <tr>
                        <td scope="row">Работают с плохой кредитной историей</td>
                        <td scope="row"><?php if (isset($bank['bad_story']))  echo $bank['bad_story']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-8">
            <hr>
            <h4>Партнерская программа</h4>
            <hr>
            <table class="table">
            <?php 
                    if (isset($bank['discount_partner']) && isset($bank['discount_client']))
                        echo '<caption>Возможно выбрать или скидку для клиента или вознаграждение агентству</caption>' 
            ?>
                <tbody>
                    <tr>
                        <td>Вознаграждение агентству</td>
                        <td scope="row"><?php if (isset($bank['discount_partner'])) echo number_format($bank['discount_partner'], 2, '.',' ').($bank['discount_partner']<100 ? ' %':' р'); ?></td>
                        <td scope="row"><?php if (isset($bank['discount_partner_descr'])) echo $bank['discount_partner_descr']; ?></td>
                    </tr>
                    <tr>
                        <td>Скидка клиенту</td>
                        <td scope="row"><?php if (isset($bank['discount_client'])) echo number_format($bank['discount_client'], 2, '.',' ').($bank['discount_client']<100 ? ' %':' р'); ?></td>
                        <td scope="row"><?php if (isset($bank['discount_client_descr'])) echo $bank['discount_client_descr']; ?></td>     
                    </tr>                   
                </tbody>
            </table>
        </div>
    </div>
</div>