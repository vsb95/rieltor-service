<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<content class="client-fields">
<div class="spoiler-row">
    <a href="#" class="spoiler-trigger"><span>Личные данные</span></a>
    <div class="spoiler-block">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <p>ФИО</p>
                <input type="text" name="<?php echo $name; ?>[name]" placeholder="ФИО полностью" required class="form-control"/>
                <label class="checkbox-inline"><input type="checkbox" class="checkbox changed-name" name="<?php echo $name; ?>[is_change]" value="1"><span class="pseudocheckbox">Меняли</span></label>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <p>Дата рождения</p>
                <input type="date" name="<?php echo $name; ?>[birthday]" min="01.01.1940" max="01.01.2010" required class="form-control"/>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <p>Телефон</p>
                <input type="tel" name="<?php echo $name; ?>[phone]" placeholder="Телефон" required class="form-control"/>
            </div>
        </div>
        <div class="row changed-name-row" style="display:none">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <p>Предыдущее ФИО</p>
                <input type="text" name="<?php echo $name; ?>[old][name]" placeholder="Предыдущее ФИО полностью" class="form-control"/>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <p>Когда меняли</p>
                <input type="date" name="<?php echo $name; ?>[old][date]" min="01.01.1940" max="01.01.2020" class="form-control"/>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <p>Причина</p>
                <input type="text" name="<?php echo $name; ?>[old][reason-descr]" placeholder="Иная Причина" class="form-control"/>
                <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="<?php echo $name; ?>[old][reason-married]" value="1"><span class="pseudocheckbox">Брак</span></label>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <p>Образование</p>                  
                <div class="btn-group">
                    <select name="<?php echo $name; ?>[education]" required>  
                        <option selected disabled></option>
                        <option>Ученая степень</option>
                        <option>Высшее</option>     
                        <option>Несколько высших</option>     
                        <option>Неоконченное высшее</option>     
                        <option>Среднее специальное</option>     
                        <option>Среднее</option>     
                        <option>Неоконченное среднее</option> 
                    </select>
                </div>   
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <p>Семейное положение</p>                  
                <div class="btn-group">
                    <select name="<?php echo $name; ?>[married]" required class="cbMarried">  
                        <option selected disabled></option>
                        <option>В браке</option>
                        <option>Не в браке</option>      
                        <option>Вдова\вдовец</option>
                        <option>В разводе</option>
                    </select>
                </div>
                <label class="checkbox-inline marriedDoc" style="display:none"><input type="checkbox" class="checkbox" name="<?php echo $name; ?>[married-doc]" value="1"><span class="pseudocheckbox">Есть брачный договор</span></label>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">                
                <p>Дети до 18</p>                  
                <div class="btn-group">
                    <select name="<?php echo $name; ?>[little-child]" required class="littleChild">  
                        <option selected>Нет</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="spoiler-row">
    <a href="#" class="spoiler-trigger"><span>Паспортные данные</span></a>
    <div class="spoiler-block">
        <h4>Паспортные данные</h4>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
            <p>Загрузите все развороты вашего паспорта (даже если они пустые)</p>
                <ul>
                    <li>Паспорт без обложки</li>
                    <li>Разворот виден полностью</li>
                    <li>Информация четко видна</li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][passport]"></imageuploadbox>
            </div>   
        </div>                      
    </div>
</div>
<div class="spoiler-row">
    <a href="#" class="spoiler-trigger"><span>Информация о доходах и расходах</span></a>
    <div class="spoiler-block">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">                
                <p>Способ подтверждения дохода</p>   
                <div class="btn-group">
                    <select name="<?php echo $name; ?>[finances][proof]" required class="cbProof">  
                        <option selected disabled></option>
                        <option>ИП, нотариус, адвокат</option>    
                        <option>Подтверждение справкой</option>    
                        <option>Без подтверждения дохода</option>    
                        <option>Пенсионер</option>
                    </select>
                </div>   
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">                
                <p>Банк, в котором вы получаете зарплату\пенсию</p>   
                <div class="btn-group">
                    <select name="<?php echo $name; ?>[finances][bank]">  
                        <option selected>Отсутствует</option>
                            <?php 
                        if(isset($banks) && !empty($banks)){
                            foreach($banks as $bank){
                                if($bank['id'] == 15) continue;
                                echo '<option>'.$bank['name'].'</option>';
                            }
                        } ?>    
                    </select>
                </div>   
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">                
                <p>Основной доход</p>   
                <input type="text" name="<?php echo $name; ?>[finances][main]" placeholder="Рублей в месяц" required class="form-control"/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">               
                <p>Дополнительный доход</p>   
                <input type="text" name="<?php echo $name; ?>[finances][alt]" placeholder="Рублей в месяц" class="form-control"/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">              
                <p>Расходы</p>
                <input type="text" name="<?php echo $name; ?>[finances][outlay]" placeholder="Рублей в месяц" required class="form-control"/>
            </div>
        </div>                
    </div>
</div>
<div class="spoiler-row work-row">
    <a href="#" class="spoiler-trigger"><span>Место работы</span></a>
    <div class="spoiler-block">
        <div class="row lawyer-work-row" style="display:none">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <p>ИНН организации</p>
                <input type="text" name="<?php echo $name; ?>[lawyer][inn]" placeholder="10/12 цифр" class="form-control" data-inputmask="'mask': '(9999999999)|(999999999999)'"/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <p>Вид деятельности организации</p>     
                <div class="btn-group">
                    <select name="<?php echo $name; ?>[lawyer][type]" class="cbOrgType">  
                        <option selected disabled></option>
                        <option>Финансы, банки, страхование</option>
                        <option>Консалтинговые услуги</option>
                        <option>Армия</option>
                        <option>Просышленность и машиностроение</option>
                        <option>Металлургия</option>
                        <option>Транспорт</option>
                        <option>Охранная деятельность</option>
                        <option>Туризм</option>
                        <option>Образование</option>
                        <option>Медицина</option>
                        <option>Культура и искусство</option>
                        <option>Органы власти и управления</option>
                        <option>Социальная сфера</option>
                        <option>Информационные технологии / телекоммуникации</option>
                        <option>Строительство</option>
                        <option>Наука</option>
                        <option>Розничная / Оптовая торговля</option>
                        <option>Услуги</option>
                        <option>Другое</option>     
                    </select>
                </div>   
                <input type="text" name="<?php echo $name; ?>[lawyer][type-descr]" placeholder="Укажите свой вариант" class="form-control tbOrgType" style="display:none"/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <p>Стаж на текущем месте</p>
                <input type="text" name="<?php echo $name; ?>[lawyer][exp-local]" placeholder="3 года" class="form-control"/>
            </div>
        </div>
        <div class="any-work-row">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <p>Организация</p>
                    <input type="text" name="<?php echo $name; ?>[org][name]" placeholder="Полное наименование" class="form-control"/>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>ИНН организации</p>
                    <input type="text" name="<?php echo $name; ?>[org][inn]" placeholder="10/12 цифр" class="form-control" data-inputmask="'mask': '(9999999999)|(999999999999)'"/>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Телефон работодателя</p>
                    <input type="tel" name="<?php echo $name; ?>[org][phone]" placeholder="Телефон" class="form-control"/>
                </div>
            </div>
            <div class="row" >
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <p>Вид деятельности организации</p>     
                    <div class="btn-group">
                        <select name="<?php echo $name; ?>[org][type]" class="cbOrgType">  
                            <option selected disabled></option>
                            <option>Финансы, банки, страхование</option>
                            <option>Консалтинговые услуги</option>
                            <option>Армия</option>
                            <option>Просышленность и машиностроение</option>
                            <option>Металлургия</option>
                            <option>Транспорт</option>
                            <option>Охранная деятельность</option>
                            <option>Туризм</option>
                            <option>Образование</option>
                            <option>Медицина</option>
                            <option>Культура и искусство</option>
                            <option>Органы власти и управления</option>
                            <option>Социальная сфера</option>
                            <option>Информационные технологии / телекоммуникации</option>
                            <option>Строительство</option>
                            <option>Наука</option>
                            <option>Розничная / Оптовая торговля</option>
                            <option>Услуги</option>
                            <option>Другое</option>     
                        </select>
                    </div>   
                    <input type="text" name="<?php echo $name; ?>[org][type-descr]" placeholder="Укажите свой вариант" class="form-control tbOrgType" style="display:none"/>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Кол-во сотрудников</p>     
                    <div class="btn-group">
                        <select name="<?php echo $name; ?>[org][staff-count]">  
                            <option selected disabled></option>
                            <option>До 5</option>   
                            <option>5-10</option>   
                            <option>10-50</option>   
                            <option>50-100</option>   
                            <option>Больше 100</option>   
                        </select>
                    </div>   
                </div>
            </div>                   
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <p>Вид трудового договора</p>
                    <div class="btn-group">
                        <select name="<?php echo $name; ?>[org][doc-type]">  
                            <option selected disabled></option>
                            <option>Срочный</option>
                            <option>Без срока</option>
                            <option>Частная практика</option>
                            <option>Агент на комиссионном договоре</option>
                            <option>Исполнитель по гражданско-правовому договору</option>       
                        </select>
                    </div>   
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Стаж на текущем месте</p>
                    <input type="text" name="<?php echo $name; ?>[org][exp-local]" placeholder="3 года" class="form-control"/>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <p>Общий стаж за 5 лет</p>
                    <input type="text" name="<?php echo $name; ?>[org][exp-total]" placeholder="12 лет и 6 мес" class="form-control"/>
                </div>
            </div>    
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <p>Категория занимаемой должности</p>
                    <div class="btn-group">
                        <select name="<?php echo $name; ?>[org][position-type]">  
                            <option selected disabled></option>
                            <option>Руководитель высшего звена</option>
                            <option>Руководитель среднего звена</option>
                            <option>Руководитель начального звена</option>
                            <option>Специалист, служащий</option>
                            <option>Рабочий</option>      
                        </select>
                    </div>   
                </div>
                <div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
                    <p>Должность</p>
                    <input type="text" name="<?php echo $name; ?>[org][position-name]" placeholder="Секретарь" class="form-control"/>
                </div>
            </div>       
        </div>       
    </div>
</div>
<div class="spoiler-row">
    <a href="#" class="spoiler-trigger"><span>Документы</span></a>
    <div class="spoiler-block">
        <p>Прикрепите следующие документы:</p>
        <div class="row imageuploadbox-any">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Любой из документов</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <ul>
                    <li>Загран паспорт</li>
                    <li>Водительское удостоверение</li>
                    <li>Военный билет</li>
                    <li>Удостоверение военослужащего</li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][any]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <div class="row imageuploadbox-lawyer" style="display:none">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Налоговая декларация</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][declaration]" ></imageuploadbox>  
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Cвидетельство о гос.рег. ИП / удостоверение адвоката</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][org-reg]" ></imageuploadbox>  
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Лицензия при наличии</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][license]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <div class="row imageuploadbox-oldman" style="display:none">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Справка о размере пенсионных начислений за последний месяц</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][inquiry-oldman]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <div class="row imageuploadbox-inquiry" style="display:none">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Справка</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <ul>
                    <li>Справка 2-НДФЛ (<a href="" target="_blank" download>образец</a>)</li>
                    <li>Справка о доходах и суммах налога физ.лица (<a href="" target="_blank" download>образец</a>)</li>
                    <li>Справка по форме банка (<a href="" target="_blank" download>образец</a>)</li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][inquiry]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <div class="row imageuploadbox-contract" style="display:none">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Трудовая книжка</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <ul>
                    <li>Копия заверенной трудовой книжки</li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][contract]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->        
        <div class="row imageuploadbox-children" style="display:none;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Свидетельство о рождении ребенка</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <ul>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][children]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <div class="row imageuploadbox-married-doc" style="display:none;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Брачный договор</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <ul>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][married-doc]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <div class="row imageuploadbox-death" style="display:none;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Cвидетельство о смерти супруга(и)</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <ul>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][death]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <div class="row imageuploadbox-un-married-doc" style="display:none;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Cвидетельство о разводе</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <ul>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">                        
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][un-married-doc]" ></imageuploadbox>  
            </div>
        </div>
        <!-- -->
        <h4></h4>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Прочие документы</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-push-6  col-md-push-6">
                <p>Комментарий</p>
                <textarea name="<?php echo $name; ?>[another-doc-descr]" class="form-control" rows="6" placeholder="Укажите, какие вы документы загружаете"></textarea>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  col-lg-pull-6  col-md-pull-6">            
                <imageuploadbox action="/upload/doc" name="<?php echo $name; ?>[doc][another]" ></imageuploadbox>  
            </div>
        </div>              
    </div>
</div>
</content>
     