<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    input, .btn-group{
        width:100%;
        margin-top:5px;
        margin-bottom:5px;
    }
    div p{
        margin: 0;
        margin-top:10px;
    }
    .open>.dropdown-menu{
        top: unset;
        left: auto;
        width: unset;
    }
    textarea{
        height: auto;
        resize: none;
    }
    content{
        display:block;
    }
    #container-row{
        padding: 10px 10px 30px 10px;
        background: white;
    }
    .spoiler-row{
        margin-left:0px;
    }
</style>
<form id="frm">
<div class="row" id="container-row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Создание заявки</h3>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <p>Сумма кредита</p>
                <input type="text" name="summ" placeholder="Сумма кредита" required class="form-control"/>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <p>Сумма первоначального взноса</p>
                <input type="text" name="first_summ" placeholder="Сумма первоначального взноса" required class="form-control"/>
            </div>          
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <p>Банки</p>                      
                <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown" style="margin-top:5px;">Выбрать банки для подачи</button>
                <ul class="dropdown-menu" role="menu">
                <?php if(isset($banks) && !empty($banks)){
                    foreach($banks as $bank){
                        if($bank['id'] == 15 || empty($bank['manager_email'])) continue;
                        echo '<li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="banks[]" value="'.$bank['id'].'"><span class="pseudocheckbox">'.$bank['name'].'</span></label></li>';
                    }
                } ?>    
                </ul>                        
            </div>       
        </div>
        
        <?php $this->load->view('credit/request_create_fields',array('name'=> 'client')); ?>
        <content id="co-borrowers">
            
        </content>
        <div class="spoiler-row">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <a class="btn btn-primary btn-md btn-send" role="button" href="#" id="addNewForm"> + Добавить созаемщика</a>
                </div>  
            </div>
        </div>
        <div class="spoiler-row">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                    <p>Дополнительная информация</p>
                    <textarea name="description" class="form-control" rows="4" placeholder="Здесь вы можете написать любую полезную, по вашему мнению информацию"></textarea>
                </div>  
            </div>
        </div>
        <div class="row" id="preview-row" style="margin-top:10px;">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <input type="submit" value="Предпросмотр" class="btn btn-primary btn-md" role="button" id="preview" >
            </div>
        </div>
        <div class="row" id="accept-row" style="display:none;margin-top:15px;">
            <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                <a href="/assets/files/согласие_на_обработку_персональных_данных.pdf" class="" download>Скачать согласие на обработку персональных данных</a>
                <label class="checkbox-inline"><input type="checkbox" class="checkbox" id="boxAccept"><span class="pseudocheckbox">Согласие на обработку персональных данных получено</span></label>
                <p>* Согласие требуется от каждого заемщика и созаемщика </p>
                <div id="imageuploadbox-accept" style="display:none; margin:10px;">         
                <p>Скан полученного согласия</p>   
                <imageuploadbox name="client[doc][accept]">Подписанное согласие на обработку персональных данных от клиента и от всех созаемщиков</imageuploadbox>
                </div>
            </div>
        </div>
        <div class="row" id="submit-row" style="display:none;margin-top:15px;">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <input type="submit" value="Отправить" class="btn btn-primary btn-md" role="button" id="send" >
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">    
var countCoBorrowers = 0;
var isSend = false;
    $(document).ready(function() {
        // иниицализируем блоки загрузки картинок
        InitImageUploadBoxes();

        // инициализируем маски
        $(":input").inputmask();

        $("#addNewForm").click(function(){            
            $.ajax({
                type: "GET",    
                url: "/credit/RequestForm?name=co-borrowers["+countCoBorrowers+"]",
                success: function(data) {
                    $("#co-borrowers").append('<hr><h4>Созаемщик</h4>'+data);
                    countCoBorrowers++;
                    InitImageUploadBoxes();
                    $(":input").inputmask();
                },
                error: function(result) {
                    console.log(result);
                    $("#modalContent").html('Ошибка: '+result);
                    openModal();
                },
                timeout: 15 * 60 * 1000
            });
        });
        $("#preview").click(function(){
            isSend = false;
            $('.spoiler-block').css('display', 'block');
        });
        $("#send").click(function(){
            isSend = true;
            $('.spoiler-block').css('display', 'block');
        });
        $('#frm').submit(function(e){
            e.preventDefault();
            e.stopPropagation();
            if(countUnLoadedImages > 0){
                alert("Пожалуйста подождите, еще не все фото загружены.\nОсталось: "+countUnLoadedImages);
                return;
            }
            var $preloader = $('#page-preloader'),
                $spinner   = $preloader.find('#spinner');   
                $spinner.html("Отправляем заявку...");
            $spinner.fadeIn();
            $preloader.delay(350).fadeIn('slow');
            var frm = $("#frm");
            $("#modalContent").html('');
            var url = $('#boxAccept')[0].checked && isSend ?'/credit/CreateRequest/':'/credit/PreviewRequest/';            
            $('#accept-row').css('display', 'block');
            $.ajax({
                type: "POST",    
                url: url,
                data: frm.serialize(),
                success: function(data) {
                    console.log(data);
                    $("#modalContent").html(data);
                    openModal();
                    $spinner.fadeOut();
                    $preloader.fadeOut();
                },
                error: function(result) {
                    console.log(result);
                    $("#modalContent").html('Ошибка: '+result);
                    openModal();
                    $spinner.fadeOut();
                    $preloader.fadeOut();
                },
                timeout: 15 * 60 * 1000
            });
        });  
        $('#boxAccept').on('click', function(e) {            
            if($(this)[0].checked){
                $('#imageuploadbox-accept').css('display', 'block');
                //$('#preview-row').css('display', 'none');
                $('#submit-row').css('display', 'block');
            }else{                
                $('#imageuploadbox-accept').css('display', 'none');
                //$('#preview-row').css('display', 'block');
                $('#submit-row').css('display', 'none');
            }
        });
    }); 
    
    $(document).on("click", ".accept-preview", function() {
        closeModal();
    });
    $(document).on("click", ".littleChild", function() {
        var imageuploadbox =  $(this).closest('.client-fields').find('.imageuploadbox-children');
        var cbMarried =  $(this).closest('.client-fields').find('.cbMarried');
        if($(this).val() != "Нет" && cbMarried.val() != "Не в браке"){
            imageuploadbox.css('display', 'block');
        }else{                
            imageuploadbox.css('display', 'none');
        }
    });
    $(document).on("click", ".changed-name", function() {
        var parent = $(this).parent().parent().parent().parent();
        var child = parent.children(".changed-name-row");
        if($(this)[0].checked){
            child.slideToggle(300);
        }else{                
            child.css('display', 'none');
        }
    });
    $(document).on("change", ".cbMarried", function() {
        var parent = $(this).parent().parent();
        var child = parent.children(".marriedDoc");     
        
        var imageuploadboxMarriedDoc =  $(this).closest('.client-fields').find('.imageuploadbox-married-doc');
        var imageuploadboxDeathDoc =  $(this).closest('.client-fields').find('.imageuploadbox-death');
        var imageuploadboxUnMarriedDoc =  $(this).closest('.client-fields').find('.imageuploadbox-un-married-doc');

        
        imageuploadboxMarriedDoc.css('display', 'none');
        imageuploadboxDeathDoc.css('display', 'none');
        imageuploadboxUnMarriedDoc.css('display', 'none');
        if($(this).val() == 'В браке'){
            child.css('display', 'block');
        }else{
            child.css('display', 'none');
            if($(this).val() == 'Вдова\\вдовец'){
                imageuploadboxDeathDoc.css('display', 'block');
            }
            if($(this).val() == 'В разводе'){
                imageuploadboxUnMarriedDoc.css('display', 'block');
            }
        }

        var cbChildren =  $(this).closest('.client-fields').find('.littleChild');
        var imageuploadboxChildren =  $(this).closest('.client-fields').find('.imageuploadbox-children');
        if(cbChildren.val() != "Нет" && $(this).val() != "Не в браке"){
            imageuploadboxChildren.css('display', 'block');
        }else{                
            imageuploadboxChildren.css('display', 'none');
        }
    });
    $(document).on("click", ".marriedDoc", function() {
        var imageuploadbox =  $(this).closest('.client-fields').find('.imageuploadbox-married-doc');
        if($(this).children('input')[0].checked){
            imageuploadbox.css('display', 'block');
        }else{                
            imageuploadbox.css('display', 'none');
        }
    });
    $(document).on("change", ".cbOrgType", function() {
        var parent = $(this).parent().parent();
        var child = parent.children(".tbOrgType");        
        if($(this).val() == 'Другое'){
            child.css('display', 'block');
        }else{
            child.css('display', 'none');
        }
    });
    $(document).on("change", ".cbProof", function() {
        var imageuploadboxAny =  $(this).closest('.client-fields').find('.imageuploadbox-any');
        var imageuploadboxInquiry =  $(this).closest('.client-fields').find('.imageuploadbox-inquiry');
        var imageuploadboxContract =  $(this).closest('.client-fields').find('.imageuploadbox-contract');
        var imageuploadboxLawyer =  $(this).closest('.client-fields').find('.imageuploadbox-lawyer');
        var imageuploadboxOldMan =  $(this).closest('.client-fields').find('.imageuploadbox-oldman');
        var workRow = $(this).closest('.client-fields').find('.work-row');
        var anyWorkRow = $(this).closest('.client-fields').find('.any-work-row');
        var lawyerWorkRow = $(this).closest('.client-fields').find('.lawyer-work-row');


        imageuploadboxAny.css('display', 'none');
        imageuploadboxInquiry.css('display', 'none');
        imageuploadboxContract.css('display', 'none');
        imageuploadboxLawyer.css('display', 'none');
        imageuploadboxOldMan.css('display', 'none');

        workRow.css('display', 'block');
        lawyerWorkRow.css('display', 'none');
        anyWorkRow.css('display', 'block');
        switch($(this).val()){
            case 'ИП, нотариус, адвокат': 
            imageuploadboxLawyer.css('display', 'block');
            lawyerWorkRow.css('display', 'block');
            anyWorkRow.css('display', 'none');
            break;
            case 'Подтверждение справкой':             
            imageuploadboxInquiry.css('display', 'block');
            imageuploadboxContract.css('display', 'block');
            break;
            case 'Без подтверждения дохода': break;
            case 'Пенсионер':
            imageuploadboxOldMan.css('display', 'block');
            workRow.css('display', 'none');
            break;
            default : console.log('неизвестный документ: '+$(this).val());
        }

    });
</script>