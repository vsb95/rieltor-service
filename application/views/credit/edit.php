<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    input.radio{
        display:none;
    }
    table{
        table-layout: fixed;
        width: 100% !important;
        padding:15px;
        margin: auto;
    }
    .tr-head{
        padding:10px !important;
        text-align: center;
        font-weight: bold;
    }
    .open>.dropdown-menu{
        top: unset;
        left: auto;
        width: unset;
    }
    .table>tbody>tr>td{
        vertical-align: middle;
    }
    .inline-select{
        display:inline-block;
        max-width:49%;
    }
    textarea{
        height: auto;
        resize: none;
    }
    .cell_on, .accredditive_on, .another_on{
        display:none;
    }
</style>
<div class="row" style="background-color: white; padding:15px;max-width:1200px;margin: auto;">    
    <form action="/CreditPublic/update" method="POST" class="frm_set">	
        <input type="hidden" name="bank" value="<?php echo $bank["hash"]; ?>" />
        <input type="hidden" name="type" value="bank" />    
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div style="display:inline-block;height:50px;">
                <img src="<?php echo $bank["logo"]; ?>" id="bank-logo" style=height:50px;"/>
            </div>
            <?php echo '<h2 class="page-title" style="display:inline-block;">'.$bank['name'].'</h2>'; ?>
        </div>  
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">      
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-head">
                        <td colspan="2">Менеджер</td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td><input type="text" placeholder="Менеджер" name="manager_name" value="<?php echo $bank['manager_name']; ?>" required class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td><input type="tel" placeholder="Менеджер тел." name="manager_phone" value="<?php echo $bank['manager_phone']; ?>" required class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input type="email" placeholder="Email" name="manager_email" value="<?php echo $bank['manager_email']; ?>" required class="form-control"/></td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Требования</td>
                    </tr>
                    <tr>
                        <td>Общий стаж</td>
                        <td><input type="text" placeholder="Общий стаж" name="exp_total" value="<?php echo $bank['experience_total_requred']; ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Стаж на последнем месте</td>
                        <td><input type="text" placeholder="Стаж на последнем месте" name="exp_last" value="<?php echo $bank['experience_last_requred']; ?>" class="form-control"/></td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Компании</td>
                    </tr>
                    <tr>
                        <td>Оценочная компания</td>
                        <td>
                            <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Выбрать</button>
                            <ul class="dropdown-menu" role="menu">
                                <?php if(isset($evaluationСompanies) && !empty($evaluationСompanies)){
                                foreach($evaluationСompanies as $company){
                                    echo '<li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="evaluation_companies[]" value="'.$company['id'].'" '.(in_array($company['id'],$enabledEvaluationСompanies)?' checked':'').'><span class="pseudocheckbox">'.$company['name'].'</span></label></li>';
                                }
                            } ?>    
                            </ul>    
                        </td>
                    </tr>
                    <tr>
                        <td>Страховая компания</td>
                        <td>
                            <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Выбрать</button>
                            <ul class="dropdown-menu" role="menu">
                                <?php if(isset($insuranceСompanies) && !empty($insuranceСompanies)){
                                foreach($insuranceСompanies as $company){
                                    echo '<li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="insurance_companies[]" value="'.$company['id'].'" '.(in_array($company['id'],$enabledInsuranceСompanies)?' checked':'').'><span class="pseudocheckbox">'.$company['name'].'</span></label></li>';
                                }
                            } ?>    
                            </ul>    
                        </td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Документы</td>
                    </tr>
                    <tr>
                        <td>Подтверждение дохода</td>
                        <td>
                            <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Выбрать</button>
                            <ul class="dropdown-menu" role="menu">
                                <?php if(isset($incomeProofs) && !empty($incomeProofs)){
                                foreach($incomeProofs as $doc){
                                    echo '<li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="income_proofs[]" value="'.$doc['id'].'" '.(in_array($doc['id'],$enabledIncomeProofs)?' checked':'').'><span class="pseudocheckbox">'.$doc['name'].'</span></label></li>';
                                }
                            } ?>    
                            </ul>    
                        </td>
                    </tr>   
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Скидки</td>
                    </tr>
                    <tr>
                        <td>Скидки партнерам (%)</td>
                        <td><input type="text" placeholder="Скидки партнерам (%)" name="discount_partner" class="form-control" <?php echo !empty($bank['discount_partner']) ?' value="'.$bank['discount_partner'].'"':''; ?>/></td>
                    </tr>
                    <tr>
                        <td>Комментарий</td>
                        <td><textarea name="discount_partner_descr" rows="2" style="width:100%" placeholder="Комментарий" class="form-control" ><?php echo !empty($bank['discount_partner_descr']) ?$bank['discount_partner_descr']:''; ?></textarea></td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td>Скидки клиенту (%)</td>
                        <td><input type="text" placeholder="Скидки клиенту (%)" name="discount_client" class="form-control" <?php echo !empty($bank['discount_client']) ?' value="'.$bank['discount_client'].'"':''; ?>/></td>
                    </tr>
                    <tr>
                        <td>Комментарий</td>
                        <td><textarea name="discount_client_descr" rows="2" style="width:100%" placeholder="Комментарий" class="form-control" ><?php echo !empty($bank['discount_client_descr']) ?$bank['discount_client_descr']:''; ?></textarea></td>
                    </tr>
                </tbody>
            </table>
        </div>                        
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">     
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Сообщаете причину отказа?</td>
                        <td>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_1_1" name="Feedback" value="Да" <?php echo $bank['feedback'] == 'Да' ?' checked':''; ?>><label for="radio_1_1" class="radio-inline">Да</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_1_2" name="Feedback" value="По возможности" <?php echo $bank['feedback'] == 'По возможности' ?' checked':''; ?>><label for="radio_1_2" class="radio-inline">По возможности</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_1_3" name="Feedback" value="Нет" <?php echo $bank['feedback'] == 'Нет' || empty($bank['feedback']) ?' checked':''; ?>><label for="radio_1_3" class="radio-inline">Нет</label></div>
                        </td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Работаете с плохой кредитной историей?</td>
                        <td>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_2_1" name="BadStory" value="Да" <?php echo $bank['bad_story'] == 'Да' ?' checked':''; ?>><label for="radio_2_1" class="radio-inline">Да</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_2_2" name="BadStory" value="Индивидуально" <?php echo $bank['bad_story'] == 'Индивидуально' ?' checked':''; ?>><label for="radio_2_2" class="radio-inline">Индивидуально</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_2_3" name="BadStory" value="Нет" <?php echo $bank['bad_story'] == 'Нет' || empty($bank['feedback']) ?' checked':''; ?>><label for="radio_2_3" class="radio-inline">Нет</label></div>
                        </td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Можно перепланировку?</td>
                        <td>    
                            <div class="radio_button"><input type="radio" class="radio" id="radio_3_1" name="Redevelop" value="1" <?php echo $bank['is_can_redevelopment'] == '1' ?' checked':''; ?>><label for="radio_3_1" class="radio-inline">Да</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_3_2" name="Redevelop" value="0" <?php echo $bank['is_can_redevelopment'] == '0' ?' checked':''; ?>><label for="radio_3_2" class="radio-inline">Нет</label></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Комментарий</td>
                        <td colspan="2"><textarea name="Redevelop_descr" rows="2" style="width:100%" placeholder="Укажите, какую перепланировку пропускаете, а какую нет" class="form-control"><?php echo !empty($bank['redevelop_descr']) ? $bank['redevelop_descr']:''; ?></textarea></td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Принимаете анкеты других банков?</td>
                        <td>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_4_1" name="AnotherBank" value="1" <?php echo $bank['is_can_another_bank'] == '1' ?' checked':''; ?>><label for="radio_4_1" class="radio-inline">Да</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_4_2" name="AnotherBank" value="0" <?php echo $bank['is_can_another_bank'] == '0' ?' checked':''; ?>><label for="radio_4_2" class="radio-inline">Нет</label></div>
                        </td>
                    </tr>
                    <!-- -->   
                    <tr class="tr-head">
                        <td colspan="2">Системы безопасных расчетов (ячейка)</td>                        
                        <td>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_5_1" name="Cell" value="1" <?php echo $bank['is_has_cell'] == '1' ?' checked':''; ?>><label for="radio_5_1" class="radio-inline">Есть</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_5_2" name="Cell" value="0" <?php echo $bank['is_has_cell'] == '0' ?' checked':''; ?>><label for="radio_5_2" class="radio-inline">Нет</label></div>
                        </td>
                    </tr>
                    <tr class="cell_on">
                        <td>Стоимость</td>
                        <td colspan="2"><input type="text" placeholder="Стоимость" name="Cell_cost"  style="margin:0;" class="form-control" <?php echo !empty($bank['cell_cost']) ?' value="'.$bank['cell_cost'].'"':''; ?>/></td>
                    </tr>
                    <tr class="cell_on">
                        <td>Комментарий</td>
                        <td colspan="2"><textarea name="Cell_descr" rows="2" style="width:100%" id="Cell_descr" placeholder="Комментарий" class="form-control" ><?php echo !empty($bank['cell_descr']) ? $bank['cell_descr']:''; ?></textarea></td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Аккредитив</td>                        
                        <td>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_6_1" name="Accredditive" value="1" <?php echo $bank['is_has_accreddive'] == '1' ?' checked':''; ?>><label for="radio_6_1" class="radio-inline">Есть</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_6_2" name="Accredditive" value="0" <?php echo $bank['is_has_accreddive'] == '0' ?' checked':''; ?>><label for="radio_6_2" class="radio-inline">Нет</label></div>
                        </td>
                    </tr>
                    <tr class="accredditive_on">
                        <td>Стоимость</td>
                        <td colspan="2"><input type="text" placeholder="Стоимость" name="Accredditive_cost"  style="margin:0;" class="form-control" <?php echo !empty($bank['accreddive_cost']) ?' value="'.$bank['accreddive_cost'].'"':''; ?>/></td>
                    </tr>
                    <tr class="accredditive_on">
                        <td>Комментарий</td>
                        <td colspan="2"><textarea name="Accredditive_descr" rows="2" style="width:100%" id="Accredditive_descr" placeholder="Комментарий" class="form-control"><?php echo !empty($bank['accreddive_descr']) ? $bank['accreddive_descr']:''; ?></textarea></td>
                    </tr>
                    <!-- -->  
                    <tr class="tr-head">
                        <td colspan="2">Другое</td><td>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_7_1" name="Another" value="1" <?php echo !empty($bank['another_name']) ?' checked':''; ?>><label for="radio_7_1" class="radio-inline">Есть</label></div>
                            <div class="radio_button"><input type="radio" class="radio" id="radio_7_2" name="Another" value="0" <?php echo  empty($bank['another_name']) ?' checked':''; ?>><label for="radio_7_2" class="radio-inline">Нет</label></div>
                        </td>
                    </tr>
                    <tr class="another_on">
                        <td>Название</td>
                        <td colspan="2"><input type="text" placeholder="Другое" name="Another_name" style="margin:0;" class="form-control" <?php echo !empty($bank['another_name']) ?' value="'.$bank['another_name'].'"':''; ?>/></td>
                    </tr>
                    <tr class="another_on">
                        <td>Стоимость</td>
                        <td colspan="2"><input type="text" placeholder="Стоимость" name="Another_cost"  style="margin:0;" class="form-control" <?php echo !empty($bank['another_cost']) ?' value="'.$bank['another_cost'].'"':''; ?>/></td>
                    </tr>
                    <tr class="another_on">
                        <td>Комментарий</td>
                        <td colspan="2"><textarea name="Another_descr" rows="2" style="width:100%" id="Another_descr" placeholder="Комментарий" class="form-control"><?php echo !empty($bank['another_descr']) ? $bank['another_descr']:''; ?></textarea></td>
                    </tr>
                    <!-- -->
                </tbody>
            </table>
        </div> 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:100px;">
            <div style="max-width: 300px;margin:auto;text-align:center">
                <input type="submit"  class="btn btn-primary btn-md btn-main" value="Сохранить">
            </div>  
        </div>     
    </form>
    <form action="/CreditPublic/update" method="POST" class="frm_set">	
        <input type="hidden" name="bank" value="<?php echo $bank["hash"]; ?>" />
        <input type="hidden" name="type" value="program" />    
        <div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">   
            <h3>Программа</h3>
            <div class="btn-group inline-select">
                <select id="programSelect" name="program" required>  
                    <option selected disabled>Выбрать</option>
                    <?php 
                    foreach($programs as $val){
                        echo ' <option value="'.$val['id'].'">'.$val['name'].'</option>';
                    }
                    ?> 
                </select>
            </div>
            <div class="btn-group inline-select">
                <select name="program_type" id="program_type" required>  
                    <option selected disabled>Тип программы</option>
                    <option value="new">Новостройки</option>
                    <option value="old">Вторичное жилье</option>
                    <option value="both">Новостройки и вторичное жилье</option>         
                </select>
            </div>     
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-head">
                        <td colspan="2">Ставка</td>
                    </tr>
                    <tr>
                        <td>Ставка (%)</td>
                        <td><input type="text" placeholder="Ставка (%)" name="percent" id="tbPercent" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Условия для снижения / увеличения</td>
                        <td><textarea name="percent_descr" rows="2" style="width:100%" id="tbPercentDescr" placeholder="Условия для снижения / увеличения" class="form-control"></textarea></td>
                    </tr>
                    
                    <tr class="tr-head">
                        <td colspan="2">Первоначальный взнос</td>
                    </tr>
                    <tr>
                        <td>Перв. взнос (%)</td>
                        <td><input type="text" placeholder="Перв. взнос (%)" name="first_percent" id="first_percent" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Условия для снижения / увеличения</td>
                        <td><textarea name="first_percent_descr" rows="2" style="width:100%" id="first_percent_descr" placeholder="Условия для снижения / увеличения" class="form-control"></textarea></td>
                    </tr>
                    
                    <tr class="tr-head">
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td>Возраст заемщика</td>
                        <td><input type="text" placeholder="Возраст заемщика" name="age_descr" id ="age_descr" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Сумма кредита</td>
                        <td>
                            <input type="text" placeholder="От" name="SummMin" style="margin:0;max-width:49%;display:inline;" id="SummMin" class="form-control">
                            <input type="text" placeholder="До" name="SummMax" style="margin:0;max-width:49%;display:inline;" id="SummMax" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>Срок кредита (мес)</td>
                        <td>
                            <input type="text" placeholder="От" name="MonthMin" style="margin:0;max-width:49%;display:inline;" id="MonthMin" class="form-control">
                            <input type="text" placeholder="До" name="MonthMax" style="margin:0;max-width:49%;display:inline;" id="MonthMax" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="description" rows="4" style="width:100%"  id="tbDescription" placeholder="Дополнительный комментарий к программе" class="form-control"></textarea>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
            <p>Если у вас возникли какие либо сложности, заметили некорректную работу или появились вопросы - пишите нам в техподержку (в правом нижнем углу или на почту <a href="mailto:rieltorservice55@gmail.com">rieltorservice@ya.ru</a>). </p>
        </div> 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div style="max-width: 300px;margin:auto;text-align:center">
                <input type="submit"  class="btn btn-primary btn-md btn-main" value="Сохранить">
            </div>  
        </div>                        
    </form>
</div>    
<script type="text/javascript">
    var idBank = '<?php echo $bank["hash"]; ?>';
    var idProgram = -1;

    $(document).ready(function() {

        $('input:radio[name=Cell]').click(function() {
            initSpoilers();
        });
        $('input:radio[name=Accredditive]').click(function() {
            initSpoilers();
        });
        $('input:radio[name=Another]').click(function() {
            initSpoilers();
        });
        function initSpoilers(){
            var val = $('input:radio[name=Cell]:checked').val();  

            if(val == 1){
                $(".cell_on").each(function(item) {
                    $(this).css('display', 'table-row');
                });
            }else{
                $(".cell_on").each(function(item) {
                    $(this).css('display', 'none');
                });
            }
            //
            val = $('input:radio[name=Accredditive]:checked').val();    
            if(val == 1){
                $(".accredditive_on").each(function(item) {
                    $(this).css('display', 'table-row');
                });
            }else{
                $(".accredditive_on").each(function(item) {
                    $(this).css('display', 'none');
                });
            }
            //
            val = $('input:radio[name=Another]:checked').val();            
            if(val == 1){
                $(".another_on").each(function(item) {
                    $(this).css('display', 'table-row');
                });
            }else{
                $(".another_on").each(function(item) {
                    $(this).css('display', 'none');
                });
            }
        }

        $(".frm_set").submit(function(e) {
            e.preventDefault();
            e.stopImmediatePropagation; 
            console.log($(this)); 
            var form = $(this);
            var url = form.attr('action');
            var data = form.serialize();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(result) {    
                    console.log(result);
                    if(result == "done"){           
                        alert('Успешно сохранено');
                        return;
                    }
                    console.log(result);
                    $("#modalContent").html(result);
                    openModal();  
                    var json= JSON.parse(result);
                    console.log(json);
                },        
                timeout: 5*60*1000
            });
        });  

        $('#programSelect').change(function() {            
            $("#program_type").val('');
            $("#tbPercent").val('');
            $("#tbPercentDescr").val('');
            $("#first_percent").val('');
            $("#first_percent_descr").val('');
            $("#age_descr").val('');
            $("#SummMin").val('');
            $("#SummMax").val('');
            $("#MonthMin").val('');
            $("#MonthMax").val('');
            $("#tbDescription").val('');
            getProgram();
        });

        initSpoilers();
    });
    function getProgram(){ 
        $.ajax({
            url: "/creditpublic/getprogram?bank="+idBank+"&program="+$("#programSelect").val(),
            context: document.body,
            success: function(result){
                console.log(result);
                if(result == "null"){                
                    $("#tbPercent").val("");
                    $("#tbDescription").val("");
                    return;
                }
                var json= JSON.parse(result);
                console.log(json);
                $("#age_descr").val(json["age"]);
                $("#tbDescription").val(json["description"]);
                $("#first_percent").val(json["first_percent"]);
                $("#first_percent_descr").val(json["first_percent_descr"]);
                $("#MonthMax").val(json["month_max"]);
                $("#MonthMin").val(json["month_min"]);
                $("#tbPercent").val(json["percent"]);
                $("#tbPercentDescr").val(json["percent_descr"]);
                $("#SummMax").val(json["summ_max"]);
                $("#SummMin").val(json["summ_min"]);
                $("#program_type").val(json["type"]);                
            }
        });  
    }
</script>