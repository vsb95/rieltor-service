<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
      * {
        margin: 0;
        padding: 0;
        font-size: 100%;
        font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        line-height: 1.65; }

      img {
        max-width: 100%;
        margin: 0 auto;
        display: block; }

      body,
      .body-wrap {
        width: 100% !important;
        height: 100%;
        background: #efefef;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none; }

      a {
        color: #71bc37;
        text-decoration: none; }

      .text-center {
        text-align: center; }

      .text-right {
        text-align: right; }

      .text-left {
        text-align: left; }

      .button {
        display: inline-block;
        color: white;
        background: #71bc37;
        border: solid #71bc37;
        border-width: 10px 20px 8px;
        font-weight: bold;
        border-radius: 4px; }

      h1, h2, h3, h4, h5, h6 {
        margin-bottom: 20px;
        line-height: 1.25; }

      h1 {
        font-size: 32px; }

      h2 {
        font-size: 28px; }

      h3 {
        font-size: 24px; }

      h4 {
        font-size: 20px; }

      h5 {
        font-size: 16px; }

      p, ul, ol {
        font-size: 16px;
        font-weight: normal;
        margin-bottom: 20px; }
      .container {
        display: block !important;
        clear: both !important;
        margin: 0 auto !important;
        max-width: 700px !important; }
        .container table {
          width: 100% !important;
          border-collapse: collapse; }
        .container .masthead {
          padding: 50px 0;
          background: #71bc37;
          color: white; }
          .container .masthead h1 {
            margin: 0 auto !important;
            max-width: 90%;
            text-transform: uppercase; }
        .container .content {
          background: white;
          padding: 30px 35px; }
          .container .content.footer {
            background: none; }
            .container .content.footer p {
              margin-bottom: 0;
              color: #888;
              text-align: center;
              font-size: 14px; }
            .container .content.footer a {
              color: #888;
              text-decoration: none;
              font-weight: bold; }

        .td-head{
            text-align: center;
            font-weight: bold;
            padding: 15px;
        }
        .data-table{
          text-align: left;
          margin-bottom: 50px;
        }
        .data-table, .data-table tr td {
          border-bottom: 1px solid black;
          padding:5px;
        }
        .data-table tr:hover {background-color: #f5f5f5;}
    </style>
</head>
<body>
<table class="body-wrap">
    <tr>
        <td class="container">
            <table>
                <tr>
                    <td align="center" class="masthead">
                        <h1><?php echo $title; ?></h1>
                        <h2>Здравствуйте<?php echo isset($manager_name) && !empty($manager_name) ? " ".$manager_name : ""; ?></h2>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: white;">   
                    <?php     
                        if(!isset($current_user) || empty($current_user))
                          die('Вы не авторизованы');
                        if(isset($current_user['org']))
                          echo 'Организация: '.$current_user['org']['name'].'<br>';
                        echo 'Риэлтор: '.$current_user['full_name'];
                        if(isset($current_user['phone']))
                          echo '<br>т.'.$current_user['phone'];
                        if(isset($current_user['email']))
                          echo '<br>email: <a href="mailto:'.$current_user['email'].'">'.$current_user['email'].'</a>';
                     ?>
                     </td>
                </tr> 
                <tr>
                    <td class="content">        
                        <p style="font-size:16px;">* Просьба не отвечать на это письмо, так как оно отправлено автоматически. Для связи с агентом используйте контактные данные указанные наверху страницы</p>
                                        
                        <table class="data-table">
                          <tr><td>Cумма кредита</td><td><?php echo $params['summ']; ?></td></tr>  
                          <tr><td>Cумма первоначального взноса</td><td><?php echo $params['first_summ']; ?></td></tr>  

                          <tr><td colspan="2" class="td-head">Клиент</td></tr>  
                          <?php
                            foreach($params['client'] as $key => $val){
                              if(is_array($val) || $key=='another-doc-descr') continue;
                              echo '<tr>';
                              switch($key){
                                 case 'name': echo '<td>ФИО</td>'; break;
                                 case 'birthday': echo '<td>Дата рождения</td>'; break;
                                 case 'phone': echo '<td>Телефон</td>'; break;
                                 case 'education': echo '<td>Образование</td>'; break;
                                 case 'little-child': echo '<td>Детей до 18</td>'; break;
                                 case 'married': echo '<td>Семейное положение</td>'; break;
                                 case 'married-doc': echo '<td>Есть брачный договор</td>'; $val = $val == 1?'Да':'Нет'; break;
                                 default: echo '<td>'.$key.'</td>'; break;
                              }
                              echo '<td>'.$val.'</td>';
                              echo '</tr>';
                            }
                            if(isset($params['client']['old']) && !empty($params['client']['old'])
                            && isset($params['client']['old']['name']) && !empty($params['client']['old']['name'])){
                              echo '<tr><td>Смена ФИО</td><td>';
                              echo 'Старое ФИО: '.$params['client']['old']['name'].'<br>';
                              echo 'Дата смены: '.$params['client']['old']['date'].'<br>';
                              echo 'Причина: '.$params['client']['old']['reason-descr'].'<br>';
                              echo '</td></tr>';
                            }
                          ?>
                          

                          <tr><td colspan="2" class="td-head">Информация о доходах и расходах</td></tr>                          
                          <?php
                            foreach($params['client']['finances'] as $key => $val){
                              if(is_array($val)) continue;
                              echo '<tr>';
                              switch($key){
                                 case 'proof': echo '<td>Способ подтверждения дохода</td>'; break;
                                 case 'bank': echo '<td>Банк, в котором получает зарплату\пенсию</td>'; break;
                                 case 'main': echo '<td>Основной доход</td>'; break;
                                 case 'alt': echo '<td>Дополнительный доход</td>'; break;
                                 case 'outlay': echo '<td>Расходы</td>'; break;
                                 default: echo '<td>'.$key.'</td>'; break;
                              }
                              echo '<td>'.$val.'</td>';
                              echo '</tr>';
                            }
                          ?>

                          <tr><td colspan="2" class="td-head">Место работы</td></tr>                     
                          <?php
                            if(isset($params['client']['lawyer']) && $params['client']['finances']['proof'] == 'ИП, нотариус, адвокат'){                            
                              foreach($params['client']['lawyer'] as $key => $val){
                                if($key == 'type-descr') continue;
                                echo '<tr>';
                                switch($key){
                                  case 'inn': echo '<td>ИНН</td>'; break;
                                  case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $params['client']['lawyer']['type-descr']; break;
                                  case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
                                  default: echo '<td>'.$key.'</td>'; break;
                                }
                                echo '<td>'.$val.'</td>';
                                echo '</tr>';
                              }
                            }else if(isset($params['client']['org'])){
                              foreach($params['client']['org'] as $key => $val){
                                if($key == 'type-descr') continue;
                                echo '<tr>';
                                switch($key){
                                  case 'name': echo '<td>Название организации</td>'; break;
                                  case 'inn': echo '<td>ИНН</td>'; break;
                                  case 'phone': echo '<td>Телефон</td>'; break;
                                  case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $params['client']['org']['type-descr']; break;
                                  case 'staff-count': echo '<td>Кол-во сотрудников</td>'; break;
                                  case 'doc-type': echo '<td>Вид трудового договора</td>'; break;
                                  case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
                                  case 'exp-total': echo '<td>Общий трудовой стаж</td>'; break;
                                  case 'position-type': echo '<td>Категория занимаемой должности</td>'; break;
                                  case 'position-name': echo '<td>Должность</td>'; break;
                                  default: echo '<td>'.$key.'</td>'; break;
                                }
                                echo '<td>'.$val.'</td>';
                                echo '</tr>';
                              }
                            }
                          ?>

                        <?php
                          if(isset($params['client']['another-doc-descr']) && !empty($params['client']['another-doc-descr'])){
                            echo '<tr><td colspan="2" class="td-head">Комментарий к дополнительным документам</td></tr> ';
                            echo '<tr><td colspan="2" >'.$params['client']['another-doc-descr'].'</td></tr> ';
                          }
                        ?>
                        </table>
                                          
                        <?php                        
                          if(isset($params['co-borrowers'])){                          
                              foreach($params['co-borrowers'] as $coBorrower){
                                echo '<table class="data-table">';
                                echo '<tr><td colspan="2" class="td-head">Созаемщик</td></tr>  ';
                                foreach($coBorrower as $key => $val){
                                  if(is_array($val) || $key=='another-doc-descr') continue;
                                  echo '<tr>';
                                  switch($key){
                                    case 'name': echo '<td>ФИО</td>'; break;
                                    case 'birthday': echo '<td>Дата рождения</td>'; break;
                                    case 'phone': echo '<td>Телефон</td>'; break;
                                    case 'education': echo '<td>Образование</td>'; break;
                                    case 'little-child': echo '<td>Дети до 18</td>'; break;
                                    case 'married': echo '<td>Семейное положение</td>'; break;
                                    case 'married-doc': echo '<td>Есть брачный договор</td>'; $val = $val == 1?'Да':'Нет'; break;
                                    default: echo '<td>'.$key.'</td>'; break;
                                  }
                                  echo '<td>'.$val.'</td>';
                                  echo '</tr>';
                                }
                                if(isset($coBorrower['old']) && !empty($coBorrower['old'])
                                && isset($coBorrower['old']['name']) && !empty($coBorrower['old']['name'])){
                                  echo '<tr><td>Смена ФИО</td><td>';
                                  echo 'Старое ФИО: '.$coBorrower['old']['name'].'<br>';
                                  echo 'Дата смены: '.$coBorrower['old']['date'].'<br>';
                                  echo 'Причина: '.$coBorrower['old']['reason-descr'].'<br>';
                                  echo '</td></tr>';
                                }
                                echo '<tr><td colspan="2" class="td-head">Информация о доходах и расходах</td></tr>';
                                foreach($coBorrower['finances'] as $key => $val){
                                  if(is_array($val)) continue;
                                  echo '<tr>';
                                  switch($key){
                                    case 'proof': echo '<td>Способ подтверждения дохода</td>'; break;
                                    case 'bank': echo '<td>Банк, в котором получает зарплату\пенсию</td>'; break;
                                    case 'main': echo '<td>Основной доход</td>'; break;
                                    case 'alt': echo '<td>Дополнительный доход</td>'; break;
                                    case 'outlay': echo '<td>Расходы</td>'; break;
                                    default: echo '<td>'.$key.'</td>'; break;
                                  }
                                  echo '<td>'.$val.'</td>';
                                  echo '</tr>';
                                }
                                echo '<tr><td colspan="2" class="td-head">Место работы</td></tr> ';
                                if(isset($coBorrower['lawyer']) && $coBorrower['finances']['proof'] == 'ИП, нотариус, адвокат'){                            
                                  foreach($coBorrower['lawyer'] as $key => $val){
                                    if($key == 'type-descr') continue;
                                    echo '<tr>';
                                    switch($key){
                                      case 'inn': echo '<td>ИНН</td>'; break;
                                      case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $coBorrower['lawyer']['type-descr']; break;
                                      case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
                                      default: echo '<td>'.$key.'</td>'; break;
                                    }
                                    echo '<td>'.$val.'</td>';
                                    echo '</tr>';
                                  }
                                }else if(isset($coBorrower['org'])){
                                  foreach($coBorrower['org'] as $key => $val){
                                    if($key == 'type-descr') continue;
                                    echo '<tr>';
                                    switch($key){
                                      case 'name': echo '<td>Название организации</td>'; break;
                                      case 'inn': echo '<td>ИНН</td>'; break;
                                      case 'phone': echo '<td>Телефон</td>'; break;
                                      case 'type': echo '<td>Вид деятельности организации</td>'; if($val == "Другое") $val = $coBorrower['org']['type-descr']; break;
                                      case 'staff-count': echo '<td>Кол-во сотрудников</td>'; break;
                                      case 'doc-type': echo '<td>Вид трудового договора</td>'; break;
                                      case 'exp-local': echo '<td>Стаж на текущем месте</td>'; break;
                                      case 'exp-total': echo '<td>Общий трудовой стаж</td>'; break;
                                      case 'position-type': echo '<td>Категория занимаемой должности</td>'; break;
                                      case 'position-name': echo '<td>Должность</td>'; break;
                                      default: echo '<td>'.$key.'</td>'; break;
                                    }
                                    echo '<td>'.$val.'</td>';
                                    echo '</tr>';
                                  }
                                }
                                
                                if(isset($coBorrower['another-doc-descr']) && !empty($coBorrower['another-doc-descr'])){
                                  echo '<tr><td colspan="2" class="td-head">Комментарий к дополнительным документам</td></tr> ';
                                  echo '<tr><td colspan="2" >'.$coBorrower['another-doc-descr'].'</td></tr> ';
                                }
                                echo '</table>';
                              }
                          }
                          if(isset($params['description']) && !empty($params['description'])){
                            echo '<br><h5>Комментарий</h5> <p style="text-align: left;">'.$params['description'].'</p>';
                          }
                        ?>
                        <p style="font-size:16px;">Для указания статуса текущей заявки используйте следующие кнопки:</p>
                        <p><em>Вы можете использовать эти кнопки несколько раз. Например при переходе от "Принять в работу" до "Одобрить". Риэтор получит соответсвующее оповещение, после того как вы нажмете кнопку</em></p>
                        <div style="margin:50px 0;">
                          <a href="<?php echo SERVER_DOMAIN.'creditpublic/UpdateRequest?request='.$params['public_key'].'&status=processing'; ?>" class="button">Принять в работу</a>
                          <a href="<?php echo SERVER_DOMAIN.'creditpublic/UpdateRequest?request='.$params['public_key'].'&status=do-work'; ?>" class="button">На доработку</a>
                          <a href="<?php echo SERVER_DOMAIN.'creditpublic/UpdateRequest?request='.$params['public_key'].'&status=accept'; ?>" class="button">Одобрить</a>
                          <a href="<?php echo SERVER_DOMAIN.'creditpublic/UpdateRequest?request='.$params['public_key'].'&status=decline'; ?>" class="button">Отменить</a>
                        </div>
                        <p><em>– С уважением, команда Rieltor-service.ru</em></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="container">
            <table>
                <tr>
                    <td class="content footer" align="center">
                        <p>Отправлено от <a href="https://rieltor-service.ru">rieltor-service.ru</a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>