<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
 #calc-iframe{
    background: #FFFFFF;
    box-shadow: 0 2px 1px -1px rgba(0,0,0,0.12), 0 1px 4px 0 rgba(0,0,0,0.10);
    border-radius: 7px;
    margin-bottom: 20px;
    height:100%;
 } 
 .show-description{
     padding:10px 5px;
    box-shadow: 0 2px 1px -1px rgba(0,0,0,0.12), 0 1px 4px 0 rgba(0,0,0,0.10);
    border-radius: 7px;
    border: 1px solid gray;
    text-align: center;
 }
 .show-description:hover{
    background-color: #81f1946b;
    cursor: pointer;
 }
 .bank-logo{
     max-width:60px;
     max-height:60px;
     margin-right: 10px;
 }
 td{
    vertical-align: middle !important;
 }
 div{
     margin-bottom:10px;
 }
</style>
<div class="row round-block" style="background: white; padding:10px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <!--    
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="row">    
            <h2 class="page-title"><?php echo $title; ?></h2>
                <iframe id="calc-iframe" width="100%"  style="width:1px; min-width: 100%;" scrolling="no" src="https://calcus.ru/kalkulyator-ipoteki?embed=1" frameborder="0"></iframe>
                <div style="margin-top:5px; font-size: 0.9em; color:#999">Ипотечный калькулятор предоставлен сайтом <a href="https://calcus.ru/kalkulyator-ipoteki" target="_blank">calcus.ru</a></div>
            </div>
        </div>
                -->
        <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12">        
            <div class="row">
                <h2 class="page-title">Ставки по ипотеке</h2>
                <form action="/Credit/Search" method="POST" id="frm_get">	
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p>Банк</p>                      
                        <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Банки</button>
                        <ul class="dropdown-menu" role="menu">
                            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="banks[]" value="-1" checked><span class="pseudocheckbox">Все</span></label></li>
                        <?php if(isset($banks) && !empty($banks)){
                            foreach($banks as $bank){
                                if($bank['id'] == 15) continue;
                                echo '<li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="banks[]" value="'.$bank['id'].'"><span class="pseudocheckbox">'.$bank['name'].'</span></label></li>';
                            }
                        } ?>    
                        </ul>                        
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <p>Программа</p>
                                       
                        <button type="button" class="btn btn-default dropdown-toggle dropdown-select" data-toggle="dropdown">Программы</button>
                        <ul class="dropdown-menu" role="menu">
                            <li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="programs[]" value="-1" checked><span class="pseudocheckbox">Все</span></label></li>
                        <?php if(isset($programs) && !empty($programs)){
                            $skipped = array(6, 16,17,18,19,20,21,22,23,24);
                            foreach($programs as $program){
                                if(in_array($program['id'],$skipped)) continue;
                                echo '<li><label class="checkbox-inline"><input type="checkbox" class="checkbox" name="programs[]" value="'.$program['id'].'"><span class="pseudocheckbox">'.$program['name'].'</span></label></li>';
                            }
                        } ?>    
                        </ul>    
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="IsOldFlat" value="1"><span class="pseudocheckbox">Вторичное жилье</span></label>
                        <br>
                        <label class="checkbox-inline"><input type="checkbox" class="checkbox" name="IsNewFlat" value="1"><span class="pseudocheckbox">Новостройки</span></label>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Сумма первоначального взноса (в %)</p>
                        <input type="text" name="PercentMin" placeholder="От" style="display: inline-block; max-width:150px;" class="form-control">
                        <input type="text" name="PercentMax" placeholder="До" style="display: inline-block; max-width:150px;" class="form-control">
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Ставка</p>
                        <input type="text" name="PricePercent" placeholder="До" class="form-control" style="max-width:150px;">
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="Показать" class="btn btn-primary btn-md" style="width:150px">
                    </div>
                </form>
            </div>     
        </div>
    </div>
</div>
<div class="row round-block" id="row_table" style="display:none">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-hover table-dark">
            <thead>
                <tr>
					<th>Банки</th>
					<th>Программа</th>
					<th>% ставка&nbsp;</th>
					<th>Скидка клиенту / Вознаграждение агентству</th>
					<th>Первоначальный взнос</th>
					<th>Подробнее</th>
                </tr>
            </thead>
            <tbody id="table_body">

            </tbody>
        </table>
    </div>
 </div>
<div class="row" style="height: 150px;"> </div>
<script type="text/javascript">
    var idBank = -1;
    var idProgram = -1;
    window.addEventListener('message', function(event) {
        if(height = event.data['height']) {
            document.getElementById('calc-iframe').style.height =  height + 'px'
        }
    });
    $('input:checkbox').on('click', function(e) {
        var val = $(this).val();
        var isChecked = $(this)[0].checked;
        var parent = $(this).parents('ul');
        
        if(val == -1){         
            if(isChecked){   
                parent.find('input:checkbox').each(function(i, cb){
                    if(i == 0) return;
                    cb.checked = isChecked;
                });                    
            }
        }else{
            parent.find('input:checkbox')[0].checked = false;
            return;
        }
    });
$(document).ready(function() {
	$("#frm_get").submit(function(e) {
	    e.preventDefault();
		e.stopImmediatePropagation;  
        $("#table_body").html("");
        var form = $("#frm_get");
        var url = form.attr('action');
        var data = $("#frm_get").serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(result) {    
                if(result == "null"){           
                    $("#modalContent").html('Нет ставок для выбранных банков с такими программами');
                    openModal();  
                    return;
                }
                
                var json= JSON.parse(result);
                var innerHtml = "";
                $.each( json, function(key, value ) {
                    var discount = Number(value["discount_partner"]).toLocaleString('ru-RU', { maximumFractionDigits: 2 });
                    var discountClient = Number(value["discount_client"]).toLocaleString('ru-RU', { maximumFractionDigits: 2 });
                    var row = '<tr><td>'+ (value["logo"] ? '<img class="bank-logo" src="'+value["logo"]+'" alt="'+value["b_name"]+'" title="'+value["b_name"]+'"/>' : value["b_name"])+"</td><td>"+value["p_name"]+"</td><td>"
                        +Number(value["percent"]).toLocaleString('ru-RU', { maximumFractionDigits: 2 })+"%</td><td>";
                    if(value["discount_client"]>100){
                        row += discountClient+" р / ";
                    }else{
                        row += discountClient+"% / ";                        
                    }
                    if(value["discount_partner"]>100){
                        row += discount+" р</td><td>";
                    }else{
                        row += discount+"%</td><td>";                        
                    }
                    row += Number(value["first_percent"]).toLocaleString('ru-RU', { maximumFractionDigits: 2 })+"%</td><td>";
                    row += "<div class=\"show-description\" data=\""+value["id"]+"\">Описание</div>";                    
                    row +="</td></tr>";
                    innerHtml+= row;
                });
                $("#table_body").html(innerHtml);
                $('#row_table').css('display', 'block');
                $('html,body').animate({scrollTop: $('#row_table').offset().top},'slow');
            },        
            timeout: 5*60*1000
        });
	});  

    $(document).on('click', '.show-description', function (e) {
        var id = $(this).attr("data");
        $.ajax({
            type: "GET",
            url: "/credit/description/"+id,
            success: function(result) {
                $("#modalContent").html(result);
                openModal();  
            },        
            timeout: 5*60*1000
        });
    });
});
</script>