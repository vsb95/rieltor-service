<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row round-block" style="background: white; padding:10px;">
    <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12">            
        <h3>Действия</h3>
        <table  class="table table-striped table-hover table-dark">
            <thead>
                <th>Действие</th>
                <th>Дата</th>
            </thead>
            <tbody>
            <?php 
            foreach($log as $action){
                echo '<tr><td>'.$action['action'].'</td><td>'.$action['date'].'</td></tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">        
        <h3>Банки</h3>
        <table  class="table table-striped table-hover table-dark">
            <thead>
                <th>id</th>
                <th>name</th>
            </thead>
            <tbody>
            <?php 
            foreach($banks as $item){
                echo '<tr><td>'.$item['id'].'</td><td>'.$item['name'].'</td></tr>';
            }
            ?>
            </tbody>
        </table>   
        <h3>Программы</h3>
        <table  class="table table-striped table-hover table-dark">
            <thead>
                <th>id</th>
                <th>name</th>
            </thead>
            <tbody>
            <?php 
            foreach($programs as $item){
                echo '<tr><td>'.$item['id'].'</td><td>'.$item['name'].'</td></tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>