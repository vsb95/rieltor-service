<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Favorites_model extends CI_Model
{   
    function IsFavorite($id_adv){     
		$id_user = $this->session->userdata('id_user');
        $this->db->reset_query();				
        $this->db->select('id_user');
        $this->db->from('favorites');			
        $this->db->where('id_adv', $id_adv);
        $this->db->where('id_user', $id_user);
        $data= $this->db->get()->result_array();
        return count($data) != 0;
    }

    /*
    Удалит если уже было, и добавит в обратном случае
    */
    function Set($id_adv, $deal_type,$is_mls,$id_obj_type)
    {
		$id_user = $this->session->userdata('id_user');
		$this->db->select('id_user');
		$this->db->from('favorites');			
		$this->db->where('id_adv', $id_adv);
		$this->db->where('id_user', $id_user);
		$data= $this->db->get()->result_array();
		if(count($data) != 0){
			$this->db->where('id_adv',$id_adv);
			$this->db->where('id_user', $id_user);
			$this->db->delete('favorites');
			return 'deleted';
		}

		$data = array(
			'id_adv' => $id_adv,
			'id_user' =>  $id_user,
			'deal_type' => $deal_type,
			'is_mls' => $is_mls,
			'id_obj_type' => $id_obj_type			
		);
		$this->db->insert('favorites', $data);
		return 'added';
    }	
}