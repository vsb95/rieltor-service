<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client_model extends CI_Model
{   
    function get($id){
        $res = $this->db->query("SELECT * FROM client WHERE id = ?;", array($id));
        $data = $res->result_array();
        if (count($data) != 0) {
            return $data[0];
        }
        return null;
    }

    function create($client_name, $client_phone)
    {
        $data['name']	= $client_name;
        $data['phone']	= $client_phone;
		
        $this->db->insert('client', $data);
        return $this->db->insert_id();
    }	
    
    function update($id, $client_name, $client_phone)
    {		
		$this->db->set('name', $client_name);
		$this->db->set('phone', $client_phone);
		$this->db->where('id', $id);
		$this->db->update('client');
    }	
}