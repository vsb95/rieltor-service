<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Export_model extends CI_Model
{       
	// Экспорт ътмл в файл
	// body - хтмл
	// wanted_name - желаемое название (префикс) файла
	// type - во что конвертировать
	// destination - путь к папке, куда сохранить. Если не указывать сохранит рядом с апишкой и не будет доступно извне
	// Вернет абсолютный путь к файлу
	function exportTo($body, $wanted_name, $type='pdf', $destination = null){		
		
		$url = API_SERVER_URL.'api/document';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('Html'=>$body, 'Type'=>$type, 'Wanted' => $wanted_name, 'Destination'=>$destination))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode($response, true);
		if(isset($json['status-type']) && $json['status-type'] == "OK"){
			return $json['status'];
		}
		$this->user_log_model->log($json['status'], 4);
		return null;
	}
}