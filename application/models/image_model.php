<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Image_model extends CI_Model
{   
    public function Upload($input_name, $resize_height = 1200){
        if(!isset($_FILES) || empty($_FILES)
            || !isset($_FILES[$input_name]) || empty($_FILES[$input_name]))
            return array('msg' =>'Файл не передан', 'status'=>'ERROR');
        $config['allowed_types'] = array('jpg','jpeg','png');//'gif|jpg|jpeg|png';
		$config['max_size'] = 1024 * 10;
		$config['encrypt_name'] = TRUE;	
        $config['upload_path'] = 'assets/img/tmp/';        
        $res = array('origin_name'=>$_FILES[$input_name]['name'], 'status'=>'ERROR');

        $ext = $this->getExtByMime($_FILES[$input_name]['tmp_name']);// $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
        if ($ext) {
            if($ext == 'jpg')
                $ext = 'jpeg';
            if(!in_array($ext, $config['allowed_types'])){
                $res['msg'] = 'Расширение файла не поддерживается';
                return $res;
            }

        } else {
            $res['msg'] = 'Некорректное изображение '.basename($image); // Выводим ошибку, если формат изображения недопустимый
            return $res;
        }
        $this->load->model('randomizer');
        $newName = $this->randomizer->generateRandomString().'.'.$ext;
        $uploadfile = $config['upload_path'].$newName;

        if (move_uploaded_file($_FILES[$input_name]['tmp_name'], $uploadfile)) {
            $res['new_name']= $uploadfile;
            $res['msg'] = "Файл корректен и был успешно загружен.";
            $res['status'] = 'OK';
			if($ext == 'jpeg')
				$this->AutoRotate($uploadfile);
			$this->resize_image($uploadfile, false, $resize_height);
        } else {
            $res['msg'] = "Возможная атака с помощью файловой загрузки!";
        }
        return $res;
    }
    public function AutoRotate($file_path){
        try {            
            $orientation=0; 
            $f=fopen($file_path,'r'); 
            $tmp=fread($f, 2); 
            if ($tmp==chr(0xFF).chr(0xD8)) { 
                $section_id_stop=array(0xFFD8,0xFFDB,0xFFC4,0xFFDD,0xFFC0,0xFFDA,0xFFD9); 
                while (!feof($f)) { 
                    $tmp=unpack('n',fread($f,2)); 
                    $section_id=$tmp[1]; 
                    $tmp=unpack('n',fread($f,2)); 
                    $section_length=$tmp[1]; 
            
                    // Началась секция данных, заканчиваем поиск 
                    if (in_array($section_id, $section_id_stop)) { 
                        break; 
                    } 
            
                    // Найдена EXIF-секция 
                    if ($section_id==0xFFE1) { 
                        $exif=fread($f,($section_length-2)); 
                        // Это действительно секция EXIF? 
                        if (substr($exif,0,4)=='Exif') { 
                            // Определить порядок следования байт 
                            switch (substr($exif,6,2)) { 
                                case 'MM': { 
                                    $is_motorola=true; 
                                    break; 
                                } 
                                case 'II': { 
                                    $is_motorola=false; 
                                    break; 
                                } 
                            } 
                            // Количество тегов 
                            if ($is_motorola) { 
                                $tmp=unpack('N',substr($exif,10,4)); 
                                $offset_tags=$tmp[1]; 
                                $tmp=unpack('n',substr($exif,14,2)); 
                                $num_of_tags=$tmp[1]; 
                            } 
                            else { 
                                $tmp=unpack('V',substr($exif,10,4)); 
                                $offset_tags=$tmp[1]; 
                                $tmp=unpack('v',substr($exif,14,2)); 
                                $num_of_tags=$tmp[1]; 
                            } 
                            if ($num_of_tags==0) { return true; } 
            
                            $offset=$offset_tags+8; 
            
                            // Поискать тег Orientation 
                            for ($i=0; $i<$num_of_tags; $i++) { 
                                if ($is_motorola) { 
                                    $tmp=unpack('n',substr($exif,$offset,2)); 
                                    $tag_id=$tmp[1]; 
                                    $tmp=unpack('n',substr($exif,$offset+8,2)); 
                                    $value=$tmp[1]; 
                                } 
                                else { 
                                    $tmp=unpack('v',substr($exif,$offset,2)); 
                                    $tag_id=$tmp[1]; 
                                    $tmp=unpack('v',substr($exif,$offset+8,2)); 
                                    $value=$tmp[1]; 
                                } 
                                $offset+=12; 
            
                                // Orientation 
                                if ($tag_id==0x0112) { 
                                    $orientation=$value; 
                                    break; 
                                } 
                            } 
                        } 
                    } 
                    else { 
                        // Пропустить секцию 
                        fseek($f, ($section_length-2), SEEK_CUR); 
                    } 
                    // Тег Orientation найден 
                    if ($orientation) { break; } 
                } 
            } 
            fclose($f); 
    
            $image = @imagecreatefromjpeg($file_path); 
			if (!$image)
			{
				$image= imagecreatefromstring(file_get_contents($file_path));
			}
            if ($orientation) { 
                switch($orientation) { 
                    // Поворот на 180 градусов 
                    case 3: { 
                        $image=imagerotate($image,180,0); 
                        break; 
                    } 
                    // Поворот вправо на 90 градусов 
                    case 6: { 
                        $image=imagerotate($image,-90,0); 
                        break; 
                    } 
                    // Поворот влево на 90 градусов 
                    case 8: { 
                        $image=imagerotate($image,90,0); 
                        break; 
                    } 
                } 
                imagejpeg($image, $file_path);
            } 
        } catch (Exception $e) {
            $html = 'Выброшено исключение: '. $e->getMessage()."<br>Изображение: ".$file_path;
            $this->user_log_model->logAction($this->session->userdata('id_user'), "Ошибка в Image_model::AutoRotate: ".$html, 4);
        }
    }

    //Поворот изображения по часовой стрелке
    // Вернет новый путь к файлу, если удачно
    public function Rotate($image, $angle){
        try {
            if (($angle < 0) || ($angle > 360)) {
                echo "Некорректные входные параметры";
                return false;
            }
            //list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
            //$types = array("jpg", "jpeg", "png"); // Массив с типами изображений
            $ext = $this->getExtByMime($image);// $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
            if ($ext) {
                if($ext == 'jpg')
                    $ext = 'jpeg';
                $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
                $img_i = @$func($image); // Создаём дескриптор для работы с исходным изображением
				if (!$img_i)
				{
					$img_i= imagecreatefromstring(file_get_contents($image));
				}
            } else {
                echo 'Некорректное изображение '.basename($image); // Выводим ошибку, если формат изображения недопустимый
                return false;
            }
            // вертаем
            $img_i=imagerotate($img_i,-$angle, 0); 
            
            $func = 'image'.$ext; // Получаем функция для сохранения результата            
            $this->load->model('randomizer');
            $newName = $this->randomizer->generateRandomString().'.'.$ext;
            $newPath = $image;//Временно убираю генерацию нового названия. Тк если валидация не пройдет - фото уже изменятся, а на UI ничего не изменится 
            $newPath = str_replace(basename($image), $newName,$image);
            
            if(!$func($img_i, $newPath)){
                return null;
            } 
            //@unlink($image);
            return $newPath;
        } catch (Exception $e) {
            $html = 'Выброшено исключение: '. $e->getMessage()."<br>Изображение: ".$image;
            $this->user_log_model->logAction($this->session->userdata('id_user'), "Ошибка в Image_model::Rotate: ".$html, 4);
        }
        return null;
    }
	/*
  $w_o и h_o - ширина и высота выходного изображения
  */
    public function resize_image($image, $w_o = false, $h_o = 1200) {
        try {
            if (($w_o < 0) || ($h_o < 0)) {
                echo "Некорректные входные параметры";
                return false;
            }
            list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
            $ext = $this->getExtByMime($image);
            if ($ext) {
                if($ext == 'jpg')
                    $ext = 'jpeg';
                $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
                $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
            } else {
                echo 'Некорректное изображение '.basename($image); // Выводим ошибку, если формат изображения недопустимый
                return false;
            }
            /* Если указать только 1 параметр, то второй подстроится пропорционально */
            if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
            if (!$w_o) $w_o = $h_o / ($h_i / $w_i);
            if($h_o == $h_i && $w_o == $w_i) return; // Размер подходящий
            
            $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения
            if($ext == 'png'){
                imagealphablending($img_o, false);
                imagesavealpha($img_o, true);
                $transparent = imagecolorallocatealpha($img_o, 255, 255, 255, 127);
                imagefilledrectangle($img_o, 0, 0, $w_i, $h_i, $transparent);
                imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
            }else{
                imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i); // Переносим изображение из исходного в выходное, масштабируя его
            }
            $func = 'image'.$ext; // Получаем функция для сохранения результата
            return $func($img_o, $image); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
        } catch (Exception $e) {
            $html = 'Выброшено исключение: '. $e->getMessage()."<br>Изображение: ".$image;
            $this->user_log_model->logAction($this->session->userdata('id_user'), "Ошибка в Image_model::resize_image: ".$html, 4);
        }
    }
    public function resizePng($im, $dst_width, $dst_height) {
        $width = imagesx($im);
        $height = imagesy($im);
    
        $newImg = imagecreatetruecolor($dst_width, $dst_height);
    
        imagealphablending($newImg, false);
        imagesavealpha($newImg, true);
        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
        imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
        imagecopyresampled($newImg, $im, 0, 0, 0, 0, $dst_width, $dst_height, $width, $height);
    
        return $newImg;
    }

    public function getExtByMime($image){
       $mime = @mime_content_type($image);
       $_mimes =& get_mimes();
       foreach($_mimes as $key => $value){
            if(is_string($value)){
                if($value == $mime)
                return $key;
            }else if(in_array($mime, $value))
                return $key;
       }
       return null;
    }
}