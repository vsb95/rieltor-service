<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Demand_model extends CI_Model
{       
	function getMyDemand(){		
		$this->db->where('id_user', $_SESSION['id_user']);		
		$this->db->from('demand');
		$this->db->order_by('create_dt', 'DESC');
		$result = $this->db->get()->result_array();

		foreach($result as &$demand){ 			
			$this->db->select('district.name');
			$this->db->where('id_demand', $demand['id']);		
			$this->db->join('district', 'demand_district.id_district = district.id');
			$this->db->from('demand_district');
			$demand['districts'] = $this->db->get()->result_array();			
		}
		return $result;
	}
	function search($request, $id_user = null){
		if(isset($request['district']) && !empty($request['district'])){
			$this->db->select('user.full_name as user_name, user.phone as user_phone, 
				user.phone_alternative as user_phone_alt, user.photo as user_photo, user.email as user_email, 
				demand.*, demand_district.id_district');
	
			if(isset($id_user) && !empty($id_user))
				$this->db->where('id_user', $id_user);
			if(isset($request['obj_types']) && !empty($request['obj_types']))
				$this->db->where_in('obj_type', $request['obj_types']);
			if(isset($request['form_pay']) && !empty($request['form_pay']))
				$this->db->where('form_pay', $request['form_pay']);
			if(isset($request['floor_type']) && !empty($request['floor_type']))
				$this->db->where('floor_type', $request['floor_type']);
			if(isset($request['cost_min']) && !empty($request['cost_min']))
				$this->db->where('cost_min >=', $request['cost_min']);
			if(isset($request['cost_max']) && !empty($request['cost_max']))
				$this->db->where('cost_max <=', $request['cost_max']);
			if(isset($request['size_min']) && !empty($request['size_min']))
				$this->db->where('size_min >=', $request['size_min']);
			if(isset($request['size_max']) && !empty($request['size_max']))
				$this->db->where('size_max <=', $request['size_max']);
			
			$this->db->where('demand_district.id_district', $request['district']);
			$this->db->from('demand');
			$this->db->join('user', 'user.id = demand.id_user');
			$this->db->join('demand_district', 'demand_district.id_demand = demand.id');
			$this->db->order_by('create_dt', 'DESC');
			$this->db->limit(100);
			$result = $this->db->get()->result_array();
			
			foreach($result as &$demand){ 			
				$this->db->select('district.name');
				$this->db->where('id_demand', $demand['id']);		
				$this->db->join('district', 'demand_district.id_district = district.id');
				$this->db->from('demand_district');
				$demand['districts'] = $this->db->get()->result_array();			
			}
			return $result;

		}else{
			$this->db->select('user.full_name as user_name, user.phone as user_phone, user.phone_alternative as user_phone_alt, user.photo as user_photo, user.email as user_email, demand.*');
	
			if(isset($id_user) && !empty($id_user))
				$this->db->where('id_user', $id_user);
			if(isset($request['obj_types']) && !empty($request['obj_types']))
				$this->db->where_in('obj_type', $request['obj_types']);
			if(isset($request['form_pay']) && !empty($request['form_pay']))
				$this->db->where('form_pay', $request['form_pay']);
			if(isset($request['floor_type']) && !empty($request['floor_type']))
				$this->db->where('floor_type', $request['floor_type']);
			if(isset($request['cost_min']) && !empty($request['cost_min']))
				$this->db->where('cost_min >=', $request['cost_min']);
			if(isset($request['cost_max']) && !empty($request['cost_max']))
				$this->db->where('cost_max <=', $request['cost_max']);
			if(isset($request['size_min']) && !empty($request['size_min']))
				$this->db->where('size_min >=', $request['size_min']);
			if(isset($request['size_max']) && !empty($request['size_max']))
				$this->db->where('size_max <=', $request['size_max']);
			
			$this->db->from('demand');
			$this->db->join('user', 'user.id = demand.id_user');
			$this->db->order_by('create_dt', 'DESC');
			$this->db->limit(100);
			$result = $this->db->get()->result_array();
	
			foreach($result as &$demand){ 			
				$this->db->select('district.name');
				$this->db->where('id_demand', $demand['id']);		
				$this->db->join('district', 'demand_district.id_district = district.id');
				$this->db->from('demand_district');
				$demand['districts'] = $this->db->get()->result_array();			
			}
			return $result;
		}
	}

	function create($request){
		$data['id_user']= $_SESSION['id_user'];
		$data['obj_type']= $request['objtype'];
		$data['form_pay']= $request['payform'];
		$data['floor_type']= $request['floortype'];
		$data['cost_min']= $request['costMin'];
		$data['cost_max']= $request['costMax'];
		$data['size_min']= $request['sizeMin'];
		$data['size_max']= $request['sizeMax'];
		$data['comment']= str_replace('\n', '<br>',$request['comment']);

		$this->db->insert('demand', $data);
		$idDemand = $this->db->insert_id();

		if(isset($request['districts'])){
			foreach($request['districts'] as $district){
				$data = array('id_demand' => $idDemand,'id_district'=> $district);
				$this->db->insert('demand_district', $data);
			}			
		}
	}
	function delete($id){		
		
		$this->db->where('id',$id);
		$this->db->where('id_user', $_SESSION['id_user']);		
		$this->db->from('demand');
		$res = $this->db->get()->result_array();
		if(count($res)  != 1)
			die('у вас нет прав на удаление');
			
		$this->db->where('id',$id);
		$this->db->delete('demand');

		$this->db->where('id_demand',$id);
		$this->db->delete('demand_district');
		return 'deleted';
	}
}