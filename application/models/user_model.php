<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model
{   
    function CheckToken(){
        if(!isset($_COOKIE["token"]) || empty($_COOKIE["token"]))
            return null;
        $token = $_COOKIE["token"];

        $res       = $this->db->query("SELECT * FROM user WHERE is_deleted = 0 AND token = ?;", array($token));
        $users      = $res->result_array();
        if(count($users) != 1)
            return null; // такого токена у нас
            
        $this->load->helper('cookie');
        set_cookie("token", $token, 86400);  /*Обновим куку - срок действия 1 сутки */  
        if($users[0]['id'] == 1) 
            set_cookie("token", $token, 86400, 't.rieltor-service.ru');  /*Обновим куку - срок действия 1 сутки */      
        return $users[0];        
    }
    /*
    Проверяет по кукам пользователя, если не залогинен - выкинет на страницу авторизации
    */
    function IsLogined(){
        $user = $this->CheckToken();     
        if(empty($user)){            
            $this->load->helper('cookie');
            delete_cookie("token"); 

            $this->session->unset_userdata('id_user');
            return false;
        }   

        $id_user = $this->session->userdata('id_user');           
        if(isset($id_user) && !empty($id_user))
            return true;

        //     заполним сессию        
		$res       = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($user['id_org']));
        $data      = $res->result_array();
        if (count($data) == 0) 
			die("Не определена организация, которой принадлежит сотрудник");
		$org = $data[0];
		$this->session->set_userdata('id_user', $user['id']);
		$this->session->set_userdata('is_admin', $user['is_admin']);	
		if(isset($user['photo']) && !empty($user['photo']))	
			$this->session->set_userdata('user_photo', $user['photo']);	
		$this->session->set_userdata('id_grant', $user['id_grant']);	
		$this->session->set_userdata('id_org', $org['id']);
		$this->session->set_userdata('org_logo', $org['logo_name']);
		$this->session->set_userdata('org_url', $org['url']);		
        $this->session->set_userdata('id_city', $org['id_city']);	
        //
		$url = API_SERVER_URL.'api/cities?id='.$org['id_city'];
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));
		$response = curl_exec($myCurl);
		curl_close($myCurl);
        $json = json_decode($response, true);
        if($json['status-type'] == "OK"){
            $this->session->set_userdata('name_city', $json['status']);	
        }
        //
		$this->load->model('user_log_model');		
        $this->user_log_model->logAction($this->session->userdata('id_user'), "login by token");
        	
        return true;
    }
    function IsLoginedAsAdmin(){
        if(!$this->IsLogined())
            return false;
            
		$is_admin = $this->session->userdata('is_admin');
		return $is_admin == 1;            
    }
    function IsLoginedAsSuperAdmin(){
        if(!$this->IsLogined())
            return false;
        $id_user = $this->session->userdata('id_user');   
        return isset($id_user) && !empty($id_user) && $id_user == 1;
    }

    function IsGranted($idNeedle){
        $idGrant = $this->session->userdata('id_grant');
        return $idNeedle == $idGrant || $idGrant == 555; // Подключен пакет или это техподдержка
    }

    function IsEmptyLogin($login){
        $res       = $this->db->query("SELECT * FROM user WHERE login = ? OR email = ?;", array($login,$login));
        $data      = $res->result_array();
        return count($data) == 0;
    }

    function addNew($login, $password, $id_org, $is_admin = 0, $email = null)
    {
        if(!$this->IsEmptyLogin($login))
            die("Логин уже занят");
        // Если новая организация - автомтаически выдаем доступ        
        $res = $this->db->query("SELECT reg_date FROM organization WHERE id = ?;", array($id_org));
        $org = $res->result_array();
        if($org == null || empty($org))
            die("Организации не существует");
        $regDate = date_create($org[0]["reg_date"]);
        $interval = date_diff($regDate, date_create(date('H:i:s d.m.Y')));
        $daysGone = $interval->format('%d');
        if($daysGone < 14)
            $data['id_grant'] = 2;
        //
        $data['login']  	= $login;
        $data['password']	= $password;
        $data['id_org']		= $id_org;
        $data['is_admin']	= $is_admin;
        if($email != null)
            $data['email']  = $email;


        $this->db->insert('user', $data);
        $this->db->insert_id();
    }	

    function ChangePassword($login, $newPsw){        
        if(!isset($newPsw) || empty($newPsw))
            return false;
        $res       = $this->db->query("SELECT * FROM user WHERE login = ? OR email = ?;", array($login, $login));
        $users      = $res->result_array();

        if(empty($users) || count($users) > 1)
            return false;

        $hash = hash('md5', $newPsw);

		$this->db->set('password', $hash);
		$this->db->where('id', $users[0]["id"]);
		$this->db->update('user');
        return true;
    }

    function CreateToken(){
		$this->load->model('randomizer');		
        $token =  $this->randomizer->generateRandomString(250);
        while(Count($this->db->query("SELECT id FROM user WHERE token = ?;", array($token))->result_array())>0){
            $token =  $this->randomizer->generateRandomString(250);
        }
       return $token;
    }

    function GetUser($id_user){
        if(!isset($id_user) || empty($id_user))
            return null;

        $res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($id_user));
        $data      = $res->result_array();
        return count($data) == 0 ?null: $data[0];        
    }

    function GetUserByLogin($login){
        if(!isset($login) || empty($login))
            return null;

        $res       = $this->db->query("SELECT * FROM user WHERE login = ? or email = ?;", array($login, $login));
        $data      = $res->result_array();
        return count($data) == 0 ?null: $data[0];        
    }

    function GetUserOrg($id_org){
        if(!isset($id_org) || empty($id_org))
            return null;

        $res       = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($id_org));
        $data      = $res->result_array();
        return count($data) == 0 ?null: $data[0];        
    }
    function GetCurrentUser(){
        $id_user = $this->session->userdata('id_user');   
        return $this->GetUser($id_user);
    }

    function LinkOnesignalUser($is_user, $onesignal_id){        
        if(!isset($is_user) || empty($is_user))
            return false;    
        if(!isset($onesignal_id) || empty($onesignal_id))
            return false;

		$this->db->set('onesignal_user_id', $onesignal_id);
		$this->db->where('id', $is_user);
		$this->db->update('user');
        return true;
    }
}