<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page_model extends CI_Model
{       
	// по дефолту создается публичный отчет
	function create($json, $filterfields = null, $id_client = 0, $is_hidden_description = 0, $is_public = 1, $description = null) {		
		
		$this->load->model('randomizer');
		$user_id = $this->session->userdata('id_user');
		$randId =  $this->randomizer->generateRandomString();
		$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($randId));
		
        while(count( $query->result())>0){
			$randId =  $this->randomizer->generateRandomString();
			$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($randId));			
		}

		$description = 'Отчет за '.date("d.m.Y");		
		$data = array(
			'id' => $randId,
			'json' => $json,
			'id_user' => $user_id,
			'filter_fields' => $filterfields,
			'description' => $description,
			'id_client' => $id_client,
			'is_hidden_description' => $is_hidden_description,
			'is_public' => $is_public
			
		);
		$this->db->insert('page', $data);
		return $randId;
	}
}