<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification_model extends CI_Model
{   
	// Связать пользователя с onesignal
    function LinkOnesignalUser($id_user, $onesignal_id){        
        if(!isset($id_user) || empty($id_user))
            return false;    
        if(!isset($onesignal_id) || empty($onesignal_id))
            return false;

        $data['id_user'] = $id_user;
		$data['id_onesignal_user'] = $onesignal_id;
		$this->db->insert('user_onesignal', $data);
        return true;
    }

	// Отправить группе\ролям
	function SendMessageToSegment($title, $message, $segments, $callbackUrl = SERVER_DOMAIN){
		$headings = array(
			"en" => $title,
			'ru' => $title
		);
		$content = array(
			"en" => $message,
			'ru' => $message
		);
		
		$fields = array(
			'app_id' => "6c07ad52-55aa-493b-9083-03920ae81274",
			'included_segments' => $segments,
			//'data' => array("foo" => "bar"),
			'contents' => $content,
			'headings' => $headings,
            'chrome_web_icon' => 'https://rieltor-service.ru/assets/img/favicon/favicon-01.png',
            'url' => $callbackUrl
		);
		
		$fields = json_encode($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic N2RjY2NhYTctZWJjNC00NThmLWE4NmYtYmM3MTczNDU5MzAy'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}

	// Отправить конкретному пользователю
	function SendMessageToUser($title, $message, $userId, $callbackUrl = SERVER_DOMAIN){
		$headings = array(
			"en" => $title,
			'ru' => $title
		);
		$content = array(
			"en" => $message,
			'ru' => $message
		);
		
		$fields = array(
			'app_id' => "6c07ad52-55aa-493b-9083-03920ae81274",
			'filters' => array(array("field" => "tag", "key" => "userId", "relation" => "=", "value" => $userId)),
			'contents' => $content,
			'headings' => $headings,
            'chrome_web_icon' => 'https://rieltor-service.ru/assets/img/favicon/favicon-01.png',
            'url' => $callbackUrl
		);
		
		$fields = json_encode($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic N2RjY2NhYTctZWJjNC00NThmLWE4NmYtYmM3MTczNDU5MzAy'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
}