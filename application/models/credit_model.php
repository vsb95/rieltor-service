<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Credit_model extends CI_Model
{       
	function getCreditById($id) {
		$this->db->select('credit_program.name as p_name, credit_bank_program.*');
		$this->db->where('credit_bank_program.id', $id);
		$this->db->from('credit_bank_program');
		$this->db->join('credit_program', 'credit_program.id = credit_bank_program.id_program');
		$data = $this->db->get()->result_array();
        if (count($data) == 0) 
			return null;
		return $data[0]; 
	}
	function getBankById($id){
		$this->db->where('id', $id);
		$this->db->from('credit_bank');
		$data = $this->db->get()->result_array();
        if (count($data) == 0) 
			return null;
		return $data[0]; 
	}
	function getBankInfo($id){
		$this->db->select('credit_program.id as program_id,
		credit_bank.manager_name, credit_bank.manager_phone, credit_bank.experience_total_requred, credit_bank.experience_last_requred, credit_bank.feedback, credit_bank.bad_story, credit_bank.is_can_redevelopment, credit_bank.is_can_another_bank,
		credit_bank_program.*');
		//, credit_evaluation_company.id as ec_id, credit_insurance_company.id as ic_id');
		$this->db->from('credit_bank_program');
		$this->db->where_in('credit_bank.id', $id);		
		$this->db->join('credit_bank', 'credit_bank.id = credit_bank_program.id_bank');
		$this->db->join('credit_program', 'credit_program.id = credit_bank_program.id_program');

		$this->db->order_by('program_id', 'ASC');
		$data = $this->db->get()->result_array();

        if (count($data) == 0) 
			return null;
		return $data; 
	}
	function getBankInsuranceCompanies($id){
		$this->db->select('credit_insurance_company.id as ic_id');
		$this->db->from('credit_bank');
		$this->db->where_in('credit_bank.id', $id);		

		$this->db->join('credit_bank_insurance_company', 'credit_bank_insurance_company.id_bank = credit_bank.id');
		$this->db->join('credit_insurance_company', 'credit_insurance_company.id = credit_bank_insurance_company.id_insurance_company');
		
		$this->db->order_by('ic_id', 'ASC');
		$data = $this->db->get()->result_array();

        if (count($data) == 0) 
			return null;
		return $data; 
	}
	function getBankEvaluationCompanies($id){
		$this->db->select('credit_evaluation_company.id as ec_id');
		$this->db->from('credit_bank');
		$this->db->where_in('credit_bank.id', $id);		

		$this->db->join('credit_bank_evaluation_company', 'credit_bank_evaluation_company.id_bank = credit_bank.id');
		$this->db->join('credit_evaluation_company', 'credit_evaluation_company.id = credit_bank_evaluation_company.id_evaluation_company');
		
		$this->db->order_by('ec_id', 'ASC');
		$data = $this->db->get()->result_array();

        if (count($data) == 0) 
			return null;
		return $data; 
	}
	function getBankIncomeProffs($id){
		$this->db->select('credit_income_proofs.id as ip_id');
		$this->db->from('credit_bank');
		$this->db->where_in('credit_bank.id', $id);		

		$this->db->join('credit_bank_income_proofs', 'credit_bank_income_proofs.id_bank = credit_bank.id');
		$this->db->join('credit_income_proofs', 'credit_income_proofs.id = credit_bank_income_proofs.id_income_proofs');
		
		$this->db->order_by('ip_id', 'ASC');
		$data = $this->db->get()->result_array();

        if (count($data) == 0) 
			return null;
		return $data; 
	}
	function getCredit($id_bank, $id_program) {
		//$id_city = $_SESSION['id_city'];
		//$this->db->where('id_city', $_SESSION['id_city']);
		$this->db->where('id_bank', $id_bank);
		$this->db->where('id_program', $id_program);
		$this->db->from('credit_bank_program');
		$data = $this->db->get()->result_array();
        if (count($data) == 0) 
			return null;
		return $data[0]; 
	}
 
	function getCredits($banks, $programs, $maxPercent = null, $programType = null, $firstPercentMin = null, $firstPercentMax = null) {
		$id_city = $_SESSION['id_city'];
		$this->db->select('credit_bank.id as b_id, credit_bank.name as b_name, credit_bank.logo, credit_program.id as p_id, credit_program.name as p_name, 
		credit_bank.manager_name, credit_bank.manager_phone, credit_bank.experience_total_requred, credit_bank.experience_last_requred, credit_bank.feedback, credit_bank.bad_story, credit_bank.is_can_redevelopment, credit_bank.is_can_another_bank,
		credit_bank_program.*, credit_bank.discount_partner, credit_bank.discount_client');
		//credit_evaluation_company.name as evaluation_company, credit_insurance_company.name as insurance_company');
		$this->db->from('credit_bank_program');
		//$this->db->where('id_city', $_SESSION['id_city']);
		if(!in_array(-1, $banks)){
			$this->db->where_in('id_bank', $banks);
		}
		if(isset($firstPercentMin) && !empty($firstPercentMin)){
			$this->db->where('first_percent >=',str_replace(',', '.', $firstPercentMin));
		}
		if(isset($firstPercentMax) && !empty($firstPercentMax)){
			$this->db->where('first_percent <=', str_replace(',', '.',$firstPercentMax));
		}
		if(isset($maxPercent) && !empty($maxPercent)){
			$this->db->where('percent <', str_replace(',', '.',$maxPercent));
		}
		if(isset($programType) && !empty($programType)){
			$this->db->where('type', $programType);
		}
		if(!in_array(-1, $programs)){
			$this->db->where_in('id_program', $programs);
		}
		$this->db->join('credit_bank', 'credit_bank.id = credit_bank_program.id_bank');
		$this->db->join('credit_program', 'credit_program.id = credit_bank_program.id_program');
		/*
		$this->db->join('credit_bank_evaluation_company', 'credit_bank_evaluation_company.id_bank = credit_bank_program.id_bank');
		$this->db->join('credit_bank_insurance_company', 'credit_bank_insurance_company.id_bank = credit_bank_program.id_bank');
		$this->db->join('credit_evaluation_company', 'credit_evaluation_company.id = credit_bank_evaluation_company.id_evaluation_company');
		$this->db->join('credit_insurance_company', 'credit_insurance_company.id = credit_bank_insurance_company.id_insurance_company');
		*/
		$this->db->order_by('b_name', 'ASC');
		$data = $this->db->get()->result_array();

        if (count($data) == 0) 
			return null;
		return $data; 
	}

	function getBanks() {
		return  $this->db->query("SELECT * FROM `credit_bank` ORDER BY name", array())->result_array();
	} 
	function getBankByHash($hash){
		return  $this->db->query("SELECT * FROM `credit_bank` WHERE hash = ?", array($hash))->result_array();
	}
	function getPrograms() {
		return  $this->db->query("SELECT * FROM `credit_program` ORDER BY name", array())->result_array();
	} 
	function getEvaluationCompany() {
		return  $this->db->query("SELECT * FROM `credit_evaluation_company`", array())->result_array();
	} 
	function getInsuranceCompany() {
		return  $this->db->query("SELECT * FROM `credit_insurance_company`", array())->result_array();
	} 
	function getIncomeProofs(){
		return  $this->db->query("SELECT * FROM `credit_income_proofs`", array())->result_array();
	}

	function getMyRequests(){
		return  $this->db->query("SELECT credit_bank.name as bank_name, credit_request.*  
		FROM `credit_request`
		INNER JOIN credit_bank on credit_bank.id = credit_request.id_bank
		WHERE id_user= ? 
		ORDER BY credit_request.date DESC
		LIMIT 100", array($_SESSION['id_user']))->result_array();
	}
	function getRequest($id){
		return  $this->db->query("SELECT credit_bank.name as bank_name, credit_request.*  
		FROM `credit_request`
		INNER JOIN credit_bank on credit_bank.id = credit_request.id_bank
		WHERE credit_request.id = ? ", array($id))->result_array();
	}

	function createRequest(&$request, $idBank){
		$data['name'] = $request['client']['name'];
		$data['id_user']= $_SESSION['id_user'];
		$data['id_bank']= $idBank;		
		$data['json'] = json_encode($request);		

		$this->db->insert('credit_request', $data);
		$request['id']=$this->db->insert_id();		
		$request['public_key'] = hash('md5', $request['id'].'пельмешка');

		$this->db->set('public_key', $request['public_key']);
		$this->db->where('id', $request['id']);
		$this->db->update('credit_request');
	}
	function updateRequestStatus($requestKey, $idStatus){
		$request = $this->db->query("SELECT * FROM `credit_request` WHERE public_key = ?", array($requestKey))->result_array();
		if(count($request) != 1)
			die('Заявка не найдена');
		
		$this->db->set('status', $idStatus);
		$this->db->where('id', $request[0]['id']);
		$this->db->update('credit_request');
		return $request[0];
	}
}