<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mailer extends CI_Model
{   
    
	public function sendEmail($emailTo, $caption, $email_html, $attach = null, $images = null) {
		$url = API_SERVER_URL.'api/email';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('EmailTo'=>$emailTo,
			 'Caption'=>$caption,
			 'Content'=>$email_html,
			 'Attachements' =>$attach,
			 'Images' =>$images))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		return $response;
	}
}