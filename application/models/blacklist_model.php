<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blacklist_model extends CI_Model
{       	
	function create($request){		
		if(!isset($request['description']) || empty($request['description']))
			die('не указано описание причины');
			
		if(!isset($request['section']) || empty($request['section']))
			die('не указан раздел');
		
		if(!isset($request['bad_ass']) || empty($request['bad_ass']))
			die('не указано фио плохого человека');
		
		$id_section = -1;
		switch($request['section']){
			case 'owners': $id_section = 1; break;
			case 'clients': $id_section = 2; break;
			case 'agents': $id_section = 3; break;
			case 'staff': $id_section = 4; break;
			default: $id_section = 0; break;
		}
        $data['bad_ass'] = $request['bad_ass'];
        $data['section'] = $id_section;
		$data['id_user'] = $_SESSION['id_user'];		
        $data['description'] = $request['description'];
		
		$this->db->insert('black_list', $data);
		$id = $this->db->insert_id();
		
		if(isset($request['images']) && !empty($request['images'])){			
			foreach($request['images'] as $image){
				$data = array('id_black_list' => $id, 'image_url' =>$image);
				$this->db->insert('black_list_images', $data);
			}
		}
	}
	
	function search($section){
		$id_section = -1;
		switch($section){
			case 'owners': $id_section = 1; break;
			case 'clients': $id_section = 2; break;
			case 'agents': $id_section = 3; break;
			case 'staff': $id_section = 4; break;
			default: $id_section = null; break;
		}
		$this->db->select('user.full_name as user_name, user.id as id_user, user.phone as user_phone, organization.name as org_name, black_list.*');
		$this->db->from('black_list');
		if(isset($id_section) && !empty($id_section) && $id_section > 0){
			$this->db->where('black_list.section', $id_section);
		}else if($section == 'my'){
			$this->db->where('black_list.id_user', $_SESSION['id_user']);
		}
		$this->db->join('user', 'user.id = black_list.id_user');
		$this->db->join('organization', 'organization.id = user.id_org');
		$this->db->order_by('black_list.date', 'DESC');
		$this->db->limit(200);
		$data = $this->db->get()->result_array();
		foreach($data as &$val){
			switch($val['section']){
				case 1: $val['section'] = 'Собственники'; break;
				case 2: $val['section'] = 'Клиенты'; break;
				case 3: $val['section'] = 'Риэлторы'; break;
				case 4: $val['section'] = 'Сотрудники'; break;
				default: $val['section'] = 'Не указано'; break;
			}
			$images = $this->db->query("SELECT * FROM black_list_images WHERE id_black_list = ?;", array($val['id']))->result_array();
			$val['images'] = count($images);
			$val['date'] = date('m.d.y', strtotime($val['date']));
		}
		return $data;
	}
	
	function delete($id){
		$this->db->select('black_list.*');
		$this->db->from('black_list');
		$this->db->where('id', $id);
		$this->db->where('id_user', $_SESSION['id_user']);
		$data = $this->db->get()->result_array();
		if(count($data) == 0)
			die('Запись не найдена');

		$this->db->where('id',$id);
		$this->db->delete('black_list');

		$this->db->where('id_black_list',$id);
		$this->db->delete('black_list_images');		
	}

	function getImages($id){
		return $this->db->query("SELECT image_url FROM black_list_images WHERE id_black_list = ?;", array($id))->result_array();
	}
}