<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_log_model extends CI_Model
{   
    /*
    1 Debug
    2 Info
    3 Access
    4 Error
    5 Warning
    6 Alarm
    */
    function logAction($id_user, $action, $id_type = 2)
    {
        $data['id_user']	= $id_user;
        $data['action']		= $action;
        $data['id_type']	= $id_type;
		
        $this->db->insert('user_log', $data);
        $this->db->insert_id();
    }	

    function log($action, $id_type = 2)
    {
        $data['action']		= $action;
        $data['id_type']	= $id_type;
        if(isset($_SESSION['id_user']))
            $data['id_user']= $_SESSION['id_user'];
        else
            $data['id_user']= -1;
        $this->db->insert('user_log', $data);
        $this->db->insert_id();
    }	
}