<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CreditPublic extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model('credit_model');
	}

	public function edit($bankHash)
	{
		if(!isset($bankHash) || empty($bankHash))
			die('Неизвестный банк');
		$banks = $this->credit_model->getBankByHash($bankHash);
		if(count($banks) != 1)
			die('Неизвестный банк');
		$bank = $banks[0];
		$data = array(
			'title' => 'Редактор ставок для '.$bank['name'],
			"insuranceСompanies" =>$this->credit_model->getInsuranceCompany(),
			"evaluationСompanies" =>$this->credit_model->getEvaluationCompany(),
			"incomeProofs" =>$this->credit_model->getIncomeProofs(),
			"bank" =>$bank,
			"programs" => $this->credit_model->getPrograms(),
			'enabledEvaluationСompanies' => array_column($this->db->query("SELECT * FROM `credit_bank_evaluation_company` WHERE id_bank = ?", array($bank['id']))->result_array(),'id_evaluation_company'),
			'enabledInsuranceСompanies' => array_column($this->db->query("SELECT id_insurance_company FROM `credit_bank_insurance_company` WHERE id_bank = ?", array($bank['id']))->result_array(),'id_insurance_company'),
			'enabledIncomeProofs' => array_column($this->db->query("SELECT * FROM `credit_bank_income_proofs` WHERE id_bank = ?", array($bank['id']))->result_array(),'id_income_proofs'), 
		);
		
		$id_user = $this->session->userdata('id_user');			
		$this->load->view('1_header_new', $data);
		$this->load->view('credit/edit', $data);
		$this->load->view('1_footer_new');	
	}

	public function Update(){
		if(!isset($_POST) || empty($_POST)){
			die('Ничего не указано');
		}
		if(!isset($_POST['type']) || empty($_POST['type'])){
			die('Критическая ошибка. Напишите в техподдержку');
		}
		if(!isset($_POST['bank']) || empty($_POST['bank'])){
			die('Банк не указан');
		}
		$banks = $this->credit_model->getBankByHash($_POST['bank']);
		if(count($banks) != 1)
			die('Неизвестный банк');
		$args = $_POST;
		$args['bank'] = $banks[0]['id'];
		if($_POST['type'] == 'bank'){			
			if(!isset($_POST['evaluation_companies']) || empty($_POST['evaluation_companies'])
			|| !isset($_POST['insurance_companies']) || empty($_POST['insurance_companies'])){
				die('Оценщики\страховщики не указаны');
			}
			if(!isset($_POST['income_proofs']) || empty($_POST['income_proofs'])){
				die('Подтверждение дохода не указано');
			}
			if(isset($args['discount_partner']))
				$args['discount_partner']= str_replace(',','.',$args['discount_partner']);	
			if(isset($args['discount_client']))
				$args['discount_client']= str_replace(',','.',$args['discount_client']);
			if(isset($args['Cell_cost']))
				$args['Cell_cost']= str_replace(',','.',$args['Cell_cost']);
			if(isset($args['Accredditive_cost']))
				$args['Accredditive_cost']= str_replace(',','.',$args['Accredditive_cost']);			
			if(isset($args['Another_cost']))
				$args['Another_cost']= str_replace(',','.',$args['Another_cost']);
			
			if(!isset($args['Cell']))
				$args['Cell'] = 0;
			if(!isset($args['Redevelop']))
				$args['Redevelop'] = 0;
			if(!isset($args['AnotherBank']))
				$args['AnotherBank'] = 0;
			if(!isset($args['Accredditive']))
				$args['Accredditive'] = 0;

			if($args['Cell'] != 1){
				$args['Cell_cost'] = null;
				$args['Cell_descr'] = null;
			}
			if($args['Accredditive'] != 1){
				$args['Accredditive_cost'] = null;
				$args['Accredditive_descr'] = null;
			}
			if($args['Another'] != 1){
				$args['Another_name'] = null;
				$args['Another_cost'] = null;
				$args['Another_descr'] = null;
			}
			$args['Cell'] = $args['Cell'] == 1;
			$args['Redevelop'] = $args['Redevelop'] == 1;
			$args['AnotherBank'] = $args['AnotherBank'] == 1;
			$args['Accredditive'] = $args['Accredditive'] == 1;
			$this->db->set('manager_name', $args['manager_name']);
			$this->db->set('manager_phone', $args['manager_phone']);
			$this->db->set('manager_email', $args['manager_email']);
			$this->db->set('experience_total_requred', $args['exp_total']);
			$this->db->set('experience_last_requred', $args['exp_last']);
			$this->db->set('discount_partner', $args['discount_partner']);
			$this->db->set('discount_partner_descr', $args['discount_partner_descr']);
			$this->db->set('discount_client', $args['discount_client']);
			$this->db->set('discount_client_descr', $args['discount_client_descr']);
			$this->db->set('feedback', $args['Feedback']);
			$this->db->set('bad_story', $args['BadStory']);
			$this->db->set('is_can_redevelopment', $args['Redevelop']);
			$this->db->set('redevelop_descr', $args['Redevelop_descr']);			
			$this->db->set('is_can_another_bank', $args['AnotherBank']);
			$this->db->set('is_has_cell', $args['Cell']);
			$this->db->set('cell_cost', $args['Cell_cost']);
			$this->db->set('cell_descr', $args['Cell_descr']);
			$this->db->set('is_has_accreddive', $args['Accredditive']);
			$this->db->set('accreddive_cost', $args['Accredditive_cost']);
			$this->db->set('accreddive_descr', $args['Accredditive_descr']);
			$this->db->set('another_name', $args['Another_name']);
			$this->db->set('another_cost', $args['Another_cost']);
			$this->db->set('another_descr', $args['Another_descr']);
			$this->db->where('id', $args['bank']);
			$this->db->update('credit_bank');
			//Сбросим все связанные компании
			$this->db->where('id_bank',$args['bank']);
			$this->db->delete('credit_bank_insurance_company'); 
			$this->db->where('id_bank',$args['bank']);
			$this->db->delete('credit_bank_evaluation_company'); 
			$this->db->where('id_bank',$args['bank']);
			$this->db->delete('credit_bank_income_proofs'); 
			if(count($args['evaluation_companies']) > 0){
				foreach($args['evaluation_companies'] as $val){
					$data = array(
						'id_bank' => $args['bank'],
						'id_evaluation_company' => $val,
					);
					$this->db->insert('credit_bank_evaluation_company', $data);
				}
			}		
			if(count($args['insurance_companies']) > 0){
				foreach($args['insurance_companies'] as $val){
					$data = array(
						'id_bank' => $args['bank'],
						'id_insurance_company' => $val,
					);
					$this->db->insert('credit_bank_insurance_company', $data);
				}
			}	
			if(count($args['income_proofs']) > 0){
				foreach($args['income_proofs'] as $val){
					$data = array(
						'id_bank' => $args['bank'],
						'id_income_proofs' => $val,
					);
					$this->db->insert('credit_bank_income_proofs', $data);
				}
			}
			
			$this->user_log_model->log( "edit credit bank: ".$banks[0]['name'], 2);	
			echo 'done';
			return;
			
		}else if($_POST['type'] == 'program'){
			if(!isset($_POST['program']) || empty($_POST['program'])){
				die('Программа не указана');
			}
			if(!isset($_POST['program_type']) || empty($_POST['program_type'])){
				die('Тип программа не указан');
			}
			if(!isset($_POST['percent']) || empty($_POST['percent'])){
				die('Ставка не указана');
			}
			if(!isset($_POST['MonthMin']) || empty($_POST['MonthMin'])
			|| !isset($_POST['MonthMax']) || empty($_POST['MonthMax'])){
				die('Срок кредита не указан');
			}
			if(!isset($_POST['SummMin']) || empty($_POST['SummMin'])
			|| !isset($_POST['SummMax']) || empty($_POST['SummMax'])){
				die('Сумма кредита не указана');
			}

			// отформатируем дроби
			if(isset($args['percent']))
				$args['percent']= str_replace(',','.',$args['percent']);
			if(isset($args['first_percent']))
				$args['first_percent']= str_replace(',','.',$args['first_percent']);	
			if(!isset($args['percent_descr']))
				$args['percent_descr']='';
			if(!isset($args['first_percent']))
				$args['first_percent']='';			
			if(!isset($args['first_percent_descr']))
				$args['first_percent_descr']='';		
			if(!isset($args['percent_descr']))
				$args['percent_descr']='';			
			if(!isset($args['age_descr']))
				$args['age_descr']='';	
			if(!isset($args['description']))
				$args['description']='';	

			$data = $this->credit_model->getCredit($args['bank'], $args['program']);
				
			if($data == null){	
				$data = array(
					'id_bank' => $args['bank'],
					'id_program' =>  $args['program'],	
					'type' => $args['program_type'],
					'percent' => $args['percent'],
					'percent_descr' => $args['percent_descr'],
					'first_percent' => $args['first_percent'],
					'first_percent_descr' => $args['first_percent_descr'],
					'age' => $args['age_descr'],
					'month_min' => $args['MonthMin'],
					'month_max' => $args['MonthMax'],
					'summ_min' => $args['SummMin'],
					'summ_max' => $args['SummMax'],
					'description'=> $args['description']
				);
				$this->db->insert('credit_bank_program', $data);
			}else{
				$this->db->set('type', $args['program_type']);
				$this->db->set('percent', $args['percent']);
				$this->db->set('percent_descr', $args['percent_descr']);
				$this->db->set('first_percent', $args['first_percent']);
				$this->db->set('first_percent_descr', $args['first_percent_descr']);
				$this->db->set('age', $args['age_descr']);
				$this->db->set('month_min', $args['MonthMin']);
				$this->db->set('month_max', $args['MonthMax']);
				$this->db->set('summ_min', $args['SummMin']);
				$this->db->set('summ_max', $args['SummMax']);
				$this->db->set('description', $args['description']);
				//
				$this->db->where('id_bank', $args['bank']);
				$this->db->where('id_program', $args['program']);
				$this->db->update('credit_bank_program');
			}
			$this->user_log_model->log( "edit credit program: ".$banks[0]['name'].' programId = '.$args['program'], 2);	
			echo 'done';
			return;
		}
		die('Критическая ошибка. Напишите в техподдержку');
	}

	public function GetProgram(){
		if(!isset($_GET['bank']) || empty($_GET['bank'])){
			die('Банк не указан');
		}
		if(!isset($_GET['program']) || empty($_GET['program'])){
			die('Программа не указана');
		}
		$banks = $this->credit_model->getBankByHash($_GET['bank']);
		if(count($banks) != 1)
			die('Неизвестный банк');
		$bank = $banks[0];
		$data = $this->credit_model->getCredit($bank['id'], $_GET['program']);
		
		if($data == null)
			die('null');
		echo json_encode($data);		
	}	

	public function Log(){
		if(!$this->user_model->IsLoginedAsSuperAdmin(2) && !$this->user_model->IsGranted(555))
		{
			header( 'Location: /', true, 301 );
			die('Вы не подключили соответствующий пакет');
		} 
		$banks = $this->db->query("SELECT id, name FROM `credit_bank`", array())->result_array();
		$programs = $this->db->query("SELECT id, name FROM `credit_program`", array())->result_array();
		$log = $this->db->query("SELECT action, `date` FROM `user_log` WHERE `action` like 'edit credit %' AND `id_user` = -1 ORDER BY date DESC", array())->result_array();
			
		$data = array('title' => 'Лог по банкам', 'banks'=>$banks, 'programs' =>$programs, 'log' =>$log);
		$this->load->view('1_header_new', $data);
		$this->load->view('credit/log', $data);	
		$this->load->view('1_footer_new');	
	}

	public function UpdateRequest(){
		if(!isset($_GET['request']) || empty($_GET['request']))
			die('Ошибка: Не указана заявка');
		if(!isset($_GET['status']) || empty($_GET['status']))
			die('Ошибка: Не указан статус заявки');

		$idStatus = -1;
		$status = '';
		switch($_GET['status']){
			case 'accept': $idStatus=1; $status='Одобрена'; break;
			case 'decline':$idStatus=2; $status='Отклонена'; break;
			case 'processing':$idStatus=3; $status='Принята в работу'; break;
			case 'do-work':$idStatus=4; $status='На доработку'; break;
			default: die('Указан неизвестный статус заявки');
		}
		$request = $this->credit_model->updateRequestStatus($_GET['request'], $idStatus);
		$this->load->view('1_header_new', array('title'=>'Смена статуса заявки'));
		$description = null;
		if($idStatus == 3 || $idStatus == 4){
			$description = 'Не забудьте обновить статус заявки после того как закончите.';
		}
		if($idStatus > 2){
			$description = 'Для обновления статуса достаточно открыть это же письмо и нажать на соответствующую кнопку';
		}
		$this->load->view('message_block', array('head'=>'Заявка обновлена успешно!', 'description' => $description));
		$this->load->view('1_footer_new', array());
		
		try{
			$user = $this->user_model->GetUser($request['id_user']);
			$bank = $this->credit_model->getBankById($request['id_bank']);
			if(!isset($user))
				return;
			$backUrl ='credit/ListRequest?client='.$request['name'];
			if(!empty($user['email'])){
				$body = 'Изменен статус по вашей заявки от '.$request['date'].' для клиента '.$request['name'].' в банке '.$bank['name'];
				$body .= '<br>Текущий статус заявки: '.$status;
				$body .= '<br><br>Почта менеджера: <a href="mailto:'.$bank['manager_email'].'">'.$bank['manager_email'].'</a>';;
				
				$email_html = $this->load->view('email',
					array('title'=>'Изменен статус по вашей заявки на ипотеку',
						'user_name'=>$user["full_name"],
						'content'=>$body,
						'back_url'=>$backUrl,
						'buttonText' => 'Посмотреть заявки'
				), TRUE);
				
				$this->load->model('mailer');
				$this->mailer->sendEmail($user['email'], "Изменен статус по вашей заявки на ипотеку", $body, null, null);
			}
			
			$this->load->model('notification_model');
			$title = 'Изменен статус по вашей заявки на ипотеку';
			$content = 'Заявка для клиента '.$request['name'].' в банке '.$bank['name'].'. Текущий статус: '.$status;
			$response = $this->notification_model->SendMessageToUser($title, $content, $user['id'], SERVER_DOMAIN.$backUrl);
			$this->user_log_model->log('Оповещение о смене статуса по ипотеке для пользователя '.$user['id'].' '.json_encode($response), 2);
		}catch(Exception $ex){
			$this->user_log_model->log($ex, 4);
		}
	}
}
