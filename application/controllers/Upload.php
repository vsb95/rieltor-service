<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		$this->load->model('image_model');
	}

	public function Photo(){		
        ini_set('memory_limit','580M');
		try {
			$res = $this->image_model->Upload('file');
			echo json_encode($res);
			$this->user_log_model->log( "upload photo", 1);	
			return;
		} catch (Exception $e) {
			$res['msg'] = 'Выброшено исключение: '. $e->getMessage()."\n";
			$this->user_log_model->log($res['msg'], 6);
		}
		die(json_encode($res));
	}

	public function Doc(){		
        ini_set('memory_limit','580M');
		try {
			$res = $this->image_model->Upload('file', 2400);
			echo json_encode($res);
			$this->user_log_model->log( "upload photo", 1);	
			return;
		} catch (Exception $e) {
			$res['msg'] = 'Выброшено исключение: '. $e->getMessage()."\n";
			$this->user_log_model->log($res['msg'], 6);
		}
		die(json_encode($res));
	}
}
