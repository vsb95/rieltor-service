<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		/*if(!$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}*/
	}

	public function index()
	{		
		$data = array('title'=>'МЛС: Поиск');
		$data['scripts'] =  array("/assets/js/fotorama.js","/assets/js/district_select.js",'https://api-maps.yandex.ru/2.1/?lang=ru_RU');
		$data['styles'] =  array("/assets/css/fotorama.css");
		$this->load->view('1_header_new', $data);
		$this->load->view('mls/search/search', $data);
		$this->load->view('1_footer_new');
	}

	public function GetSearchFields(){		
		if(!isset($_GET['offerType']) || empty($_GET['offerType']))
			die("не указан тип сделки");
		if(!isset($_GET['offerCategory']) || empty($_GET['offerCategory']))
			die("не указана категория");
		if(!isset($_GET['objType']) || empty($_GET['objType']))
			die("не указан объект");
		$offerType = $_GET['offerType'];
		$offerCategory = $_GET['offerCategory'];
		$objType = $_GET['objType'];

		switch($offerCategory){
			case "living":
				switch($objType){
					case "apartment":
						if($offerType == "rent")
							$this->load->view('mls/search/search_rent_apartment');
						else
							$this->load->view('mls/search/search_sell_apartment');
					break;
					case "apartment_new":
						$this->load->view('mls/search/search_sell_apartment_new');
					break;
					case "room":
						if($offerType == "rent")
							$this->load->view('mls/search/search_rent_room');
						else
							$this->load->view('mls/search/search_sell_room');
					break;
					case "house":
						if($offerType == "rent")
							$this->load->view('mls/search/search_rent_house');
						else
							$this->load->view('mls/search/search_sell_house');
					break;
					case "apartment_part":
						$this->load->view('mls/search/search_sell_apartment_part');
					break;
					case "house_part":
						$this->load->view('mls/search/search_sell_house_part');
					break;
					case "lot":
						$this->load->view('mls/search/search_sell_lot');
					break;
				}
			break;
			case "commercial":
				die("Коммерцию пока не поддерживаем");
			break;
		}
	}

	public function Searching()
	{		
		if(!isset($_GET) || empty($_GET)){
			die("ничего не указано");
		}
		//var_dump($_GET);
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 
		
		$request = $_GET;
		// Дернем API
		$url = API_SERVER_URL.'api/SearchMls';
		$request['RequestedUserId'] = $this->session->userdata('id_user');
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($request)
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);

		$json = json_decode($response, true);

		if(!isset($json['status-type']) || empty($json['status-type'])){
			$user = $this->user_log_model->log( "MLS::Searching ".$request['OfferType'].' -> '.$request['ObjectTypeSelect'].'<br> response: '.$response, 5);
			die('Ошибка на сервере. Обратитесь в техподдержку');
		}
		if($json['status-type'] != "OK")
			die($json["status"]);
		$result = array('total-count-by-filter' => $json["total-count-by-filter"]);
		//$this->load->model('favorites_model');
		if(isset($json["advertisments"]) && !empty($json["advertisments"])){
			$htmlResult = '';
			foreach($json["advertisments"] as $adv){
				// Проверим, есть ли в избранном	
				//if($this->favorites_model->IsFavorite($page['id'])) 
				//	$advertisement['is_liked'] = 1;			
				//
				$htmlResult.= $this->load->view('mls/search/list_item', array('adv'=>$adv, 'isCabinet' => isset($_GET['cabinet']) && $_GET['cabinet']), true);
			}		
			$result["html"] = $htmlResult;
		}
		echo json_encode($result);
		$user = $this->user_log_model->log( "MLS::Searching ".$request['OfferType'].' -> '.$request['ObjectTypeSelect'], 2);
	}
	
	public function GetDistricts(){
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			return null;
			//die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен. Повторите еще раз')));
		} 
		// Дернем API
		$idCity = $this->session->userdata('id_city');
		$url = API_SERVER_URL.'api/search?idCity='.$idCity;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		if(is_array($response))
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Не найден путь api/search')));
		$json = json_decode(json_decode($response, true), true);
		$initData["districts"] = $json["districts"];
		$initData["micro-districts"] = $json["micro-districts"];
		echo json_encode($initData);
	}
}
