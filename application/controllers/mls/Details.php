<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Details extends CI_Controller {
	function __construct() {
        parent::__construct();
		/*
		Отключено тк нам нужн опоказывать детализацию клиентам
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
		*/
	}
	
	public function Show($id){
		$IsLogined = $this->user_model->IsLogined();
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			$data['IsLogined'] = $IsLogined ;
			$data['IsCanEdit'] = false ;
			$data['title'] =  "МЛС: Объявление";
			$this->load->view('1_header_new',$data);
			$this->load->view('/mls/details', array('result'=>array('status'=>'Сервер не доступен. Попробуйте позже')));
			$this->load->view('1_footer_new');
			return;
		} 
		
		// Дернем API
		$id_user = $this->session->userdata('id_user');
		$url = API_SERVER_URL.'api/SearchMls?id='.$id.'&userId='.$id_user;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode($response, true);

		$data['IsLogined'] = $IsLogined ;
		$data['IsCanEdit'] = false ;
		$data['title'] =  "МЛС: Объявление";
		$data['scripts'] =  array("/assets/js/fotorama.js");
		$data['styles'] =  array("/assets/css/fotorama.css");
		if($json["status-type"] == "OK"){
			$advertisement = $json["advertisment"];			
			$data['id_page'] = $id;
			//
			if($IsLogined){
				$this->db->select('adv_comments.comment, adv_comments.date, user.full_name as user_name, user.email as user_email');
				$this->db->from('adv_comments');			
				$this->db->where('id_page', $id);		
				$this->db->where('is_mls', 1);				
				if(!$this->user_model->IsLoginedAsSuperAdmin())
					$this->db->where('adv_comments.id_org', $_SESSION['id_org']);
				$this->db->join('user', 'user.id = adv_comments.id_user');
				$this->db->order_by('date', 'ASC');
				$data['comments']= $this->db->get()->result_array();
				
				// Проверим, есть ли в избранном	
				/*$this->load->model('favorites_model');
				if($this->favorites_model->IsFavorite($id))
					$advertisement['is_liked'] = 1;	*/		
				if($advertisement['sales-agent']['id'] != $id_user){	 
					if($this->user_model->IsLoginedAsAdmin()){
						$currentUser = $this->user_model->GetUser($id_user);
						$ownerIdOrg = $advertisement['sales-agent']['org']['id'];
						$data['IsCanEdit'] = $currentUser['id_org'] == $ownerIdOrg || $this->user_model->IsLoginedAsSuperAdmin();
					}
				}else{
					$data['IsCanEdit'] = true;
				}
			}
			$this->load->view('1_header_new',$data);
			$this->load->view('/mls/details', array("advertisement"=>$advertisement));
			$this->load->view('1_footer_new');
		}else{
			$this->load->view('1_header_new',$data);
			$this->load->view('/mls/details', array("result"=>$json));
			$this->load->view('1_footer_new');
		}
	}
}
