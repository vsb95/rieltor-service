<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends CI_Controller {
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLoginedAsAdmin())
		{
			header( 'Location: /', true, 301 );
			die('У вас недостаточно прав');
		}		
		if(!$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}

	public function index(){	
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 2); 
		$data = array('title'=>'МЛС: Фид', 'status'=>$status);
		if ($status) { 
			// Дернем API
			$url = API_SERVER_URL.'api/Feed?idOrg='.$_SESSION['id_org'];
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
			));

			$response = curl_exec($myCurl);
			curl_close($myCurl);

			$json = json_decode($response, true);
			if($json['status-type'] == "OK"){				
				$data['yrl'] = SERVER_DOMAIN."assets/files/feed/".$json['file'];
				$data['yrl_download'] = "/download?file=feed/".$json['file'];
				$data['yrl_date'] = $json['create-date'];
				$data['yrl_association'] = $json['agent-associations'];
				if(isset($json['auto-import-feeds']) && !empty($json['auto-import-feeds']))
					$data['auto_import_feeds'] = $json['auto-import-feeds'];
				//
				$query = $this->db->query("SELECT user.id, user.full_name as name, user.phone, user.email, user.photo,  user.is_activate FROM user WHERE id_org = ? AND user.is_deleted = 0 ORDER BY user.is_admin;",					 array($_SESSION["id_org"]));
				$data["users"] = $query->result_array();

			}			
		}
		
		$this->load->view('1_header_new', $data);
		$this->load->view('mls/feed', $data);
		$this->load->view('1_footer_new');
		$user = $this->user_log_model->log( "MLS::Open feed",2);
	}

	public function upload(){
		if(!isset($_POST['file']) || empty($_POST['file']))
			die('Не указан файл');
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		$isAuto = isset($_POST['is_auto']) ? $_POST['is_auto'] : 0;
		$email = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : $this->user_model->GetCurrentUser()['email'];
		$idOrg = isset($_POST['org']) && !empty($_POST['org'])  ? $_POST['org'] :$_SESSION['id_org'];
		if ($status) { 
			// Дернем API
			$url = API_SERVER_URL.'api/Feed';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => http_build_query(array("IdOrg"=>$idOrg, "Src"=>$_POST['file'], "FeedbackEmailTo" => $email, "IsAuto" => $isAuto))
			));
			
			$response = curl_exec($myCurl);
			curl_close($myCurl);
			echo $response;			
			$this->user_log_model->log( "MLS::Upload feed: ".$_POST['file'],3);
		}else{
			die("Сервер не доступен. Попробуйте позднее");
		}
	}

	public function association(){
		if(!isset($_POST) || empty($_POST))
			die("Нечего сохранять");

		//var_dump($_POST);
		$usersId = array_unique(array_values($_POST['association']));
		$users = array();
		foreach($usersId as $userId){
			if($userId == -1) continue;
			$user = $this->user_model->GetUser($userId);
			$users[] = array(
				'Id'=>$user['id'],
				'IdOrg'=>$user['id_org'],
				'Phone'=>$user['phone'],
				'PhoneAlternative'=>$user['phone_alternative'],
				'Name'=>$user['full_name'],
				'Email'=>$user['email'],
			);
		}
		
		$associations = array();
		foreach($_POST['association'] as $virtualId => $realId){
			$associations[] = array('Key'=>$virtualId, 'Value' =>$realId);
			//$associations[] = $virtualId.'::'.$realId;
		}
		$url = API_SERVER_URL.'api/feed';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => http_build_query(array("IdOrg"=>$_SESSION['id_org'], "Associations"=>$associations, 'Users'=>$users))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);

		$json = json_decode($response, true);
		//var_dump($response);
		if($json['status-type'] != "OK"){				
			die($json['status']);
		}
		echo $json['status'];
	}
}
