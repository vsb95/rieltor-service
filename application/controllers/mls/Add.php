<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add extends CI_Controller {
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}

	public function index(){		
		$data = array('title'=>'МЛС: Подача объявления');
		$data['scripts'] = array(
			'https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=init',
			'/assets/js/image_upload.js',
			'/assets/js/jquery-ui.js'
		);
		$data['currentUser'] = $this->user_model->GetCurrentUser();
		$this->load->view('1_header_new', $data);
		if(!isset($data['currentUser']['full_name']) || empty($data['currentUser']['full_name'])
			|| !isset($data['currentUser']['phone']) || empty($data['currentUser']['phone']))
			$this->load->view('mls/must_fill_profile', $data);
		else
			$this->load->view('mls/add', $data);
		$this->load->view('1_footer_new');
	}
	
	public function AddObject(){				
		if(!isset($_POST) || empty($_POST)){
			die("Ничего не указано");
		}
		$request = $_POST;
		// Вертаем изображения
		$newImagesUrl = array();
		if(isset($request['ImagesList']) && !empty($request['ImagesList'])){
			$this->load->model('image_model');
			$newImagesList = array();
			foreach($request['ImagesList'] as &$image){
				$path = $image['src'];
				if($image['angle'] > 0){
					// если у изображения есть уже ссылка на домен - значит она не в tmp папке
					$pos = strpos($image['src'], SERVER_DOMAIN);
					if($pos !== false){
						$path = str_replace(SERVER_DOMAIN, '',$image['src']);
						$path = ltrim($path, '/');
					}
					$newPath = ($pos !== false? SERVER_DOMAIN :'').ltrim($this->image_model->Rotate($path, $image['angle']), '/');
					if(isset($newPath) && !empty($newPath)){
						$newImagesUrl[$image['src']] = $newPath;
						$image['src'] =$newPath;
					}
				}
				
				$newImagesList[] = $image;
			}
			$request['ImagesList'] = $newImagesList;
		}

		//var_dump($request['ImagesList']);
		$currentUser =isset($_POST['OwnerId']) && !empty($_POST['OwnerId']) ? $this->user_model->GetUser($_POST['OwnerId']):$this->user_model->GetCurrentUser();
		$request["user"] = array(
			'Id'=>$currentUser['id'],
			'IdOrg'=>$currentUser['id_org'],
			'Phone'=>$currentUser['phone'],
			'PhoneAlternative'=>$currentUser['phone_alternative'],
			'Name'=>$currentUser['full_name'],
			'Email'=>$currentUser['email'],
		);
		if(isset($currentUser['photo']) && !empty($currentUser['photo']))
			$request["user"]["Photo"]= SERVER_DOMAIN.'/assets/img/user/'.$currentUser['photo'];
			
		$org = $this->user_model->GetUserOrg($currentUser['id_org']);
		$request["user"]["Organization"] =  $org["name"];
		if(isset($org["logo_stamp"]) && !empty($org["logo_stamp"]))
			$request["user"]["OrganizationWatermark"] =  SERVER_DOMAIN.'/assets/img/logo/'.$org["logo_stamp"];
		
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 
		// Дернем API
		$url = API_SERVER_URL.(isset($request['Id']) && !empty($request['Id']) ?'api/UpdateMlsLivingOffer':'api/AddMlsLivingOffer');
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($request)
		));

		$response = curl_exec($myCurl);
		$json = json_decode($response,true);
		$json['new-images'] = $newImagesUrl;
		echo json_encode($json);
		curl_close($myCurl);
		if(isset($json['status-type']) && $json['status-type'] == "OK"){
			if((!isset($currentUser['phone']) || empty($currentUser['phone']) ) 
				&& isset($request['Phone']) && !empty($request['Phone'])){					
					$this->db->set('phone', $request['Phone']);
					$this->db->where('id', $currentUser["id"]);
					$this->db->update('user');
				}
			if((!isset($currentUser['phone_alternative']) || empty($currentUser['phone_alternative']) ) 
				&& isset($request['PhoneAlternative']) && !empty($request['PhoneAlternative'])){									
					$this->db->set('phone_alternative', $request['PhoneAlternative']);
					$this->db->where('id', $currentUser["id"]);
					$this->db->update('user');
				}
			$action = "MLS::".(isset($_POST['id']) && !empty($_POST['id']) ? 'edit advertisment: '.$_POST['id']:'add advertisment');
			$this->user_log_model->log( $action, 2);
		}
		//var_dump($request);
	}

	public function Edit($id){
		if(!isset($id) || empty($id))
		{
			header( 'Location: /', true, 301 );			
			die();
		}
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 
		$data = array('title'=>'МЛС: Редактирование объявления');
		$data['scripts'] = array(
			'https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=init', 
			'/assets/js/adv_edit.js?'.time(), 
			'/assets/js/image_upload.js',
			'/assets/js/jquery-ui.js');
		$data['currentUser'] = $this->user_model->GetCurrentUser();
		
		// Дернем API
		$id_user = $this->session->userdata('id_user');
		$url = API_SERVER_URL.'api/SearchMls?id='.$id.'&userId='.$id_user;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode($response, true);

		if($json["status-type"] == "OK"){
			if($json["advertisment"]['sales-agent']['id'] != $id_user){	
				if($this->user_model->IsLoginedAsAdmin() || $this->user_model->IsLoginedAsSuperAdmin()){
					$currentUser = $this->user_model->GetUser($id_user);
					if($currentUser['id_org'] == $json["advertisment"]['sales-agent']['org']['id'] || $this->user_model->IsLoginedAsSuperAdmin()){
						$query = $this->db->query("SELECT user.id, user.full_name as name, user.phone, user.email, user.photo,  user.is_activate FROM user WHERE id_org = ? AND user.is_deleted = 0 ORDER BY user.is_admin;",					 array($_SESSION["id_org"]));
						$data["users"] = $query->result_array();
						$data["isEditAdmin"] = true;
						$this->user_log_model->log( "MLS::Try edit slave advertisment: ".$id, 2);
					}else{
						$this->user_log_model->log( "MLS::Try edit alien advertisment: ".$id, 6);
						header( 'Location: /', true, 301 );
						die();
					}
				}else{
					$this->user_log_model->log( "MLS::Try edit alien advertisment: ".$id, 6);
					header( 'Location: /', true, 301 );
					die();
				}
			}
			$data["advertisment"] = $json["advertisment"];	
			$data["isEdit"] = true;
		}else{
			$data["result"]=$json;
		}
		//
		$this->load->view('1_header_new', $data);
		$this->load->view('mls/add', $data);
		$this->load->view('1_footer_new');
	}
	
	public function GetObjectFields(){		
		if(!isset($_GET['offerType']) || empty($_GET['offerType']))
			die("не указан тип сделки");
		if(!isset($_GET['objType']) || empty($_GET['objType']))
			die("не указан объект");
		$offerType = $_GET['offerType'];
		$objType = $_GET['objType'];
		$data = array('title'=>'МЛС: Добавление', 'type'=>$objType, 'offerType'=>$offerType);
		switch($objType){
			///////////// LIVING //////////////////
			case "apartment": 
				if($offerType == 'rent'){
					$this->load->view('mls/add/add_kvartira_rent', $data);	
				}else{
					$this->load->view('mls/add/add_kvartira_sell', $data);		
				}		
				$this->load->view('mls/add/add_sub_building', $data);
				break;
			case "apartment_new":
				$this->load->view('mls/add/add_kvartira_new', $data);
				$this->load->view('mls/add/add_kvartira_sell', $data);
				break;
			case "room": 
				if($offerType == 'rent')
					$this->load->view('mls/add/add_room_rent', $data);	
				else{
					$this->load->view('mls/add/add_room_sell', $data); 
					$this->load->view('mls/add/add_sub_building', $data);
				}
				break;
			case "house": 
			if($offerType == 'rent')
				$this->load->view('mls/add/add_house_rent', $data); 
			else
				$this->load->view('mls/add/add_house_sell', $data); 
				break;		
			case "lot": 
				$this->load->view('mls/add/add_house_sell', $data); 
				break;		
			case "house_part": 
				if($offerType == 'rent')
					die('Мы не арендуем части домов');
				$this->load->view('mls/add/add_house_sell', $data); 
				break;			
			case "apartment_part": 
				if($offerType == 'rent'){
					die('Мы не арендуем части квартир')	;
				}else{
					$this->load->view('mls/add/add_kvartira_part_sell', $data);		
				}		
				$this->load->view('mls/add/add_sub_building', $data);
				break;
			///////////// COMMERCIAL //////////////////
			case "office": 
				$this->load->view('mls/add/add_office', $data);
				break;
			case "building": 
				$this->load->view('mls/add/add_building', $data); 
				break;
			case "warehouse": 
				$this->load->view('mls/add/add_warehouse', $data); 
				break;
			case "market-place": 
				$this->load->view('mls/add/add_market_place', $data); 
				break;		
			case "free-space":
			 	$this->load->view('mls/add/add_free_space', $data); 
			 	break;

			default: echo '<p>Неизвестный тип объекта недвижимости</p>';			
		}
		//$this->load->view('mls/add/images_upload', $data);	
		$this->load->view('mls/add/description_block', $data);	
		if($offerType == 'rent')
			$this->load->view('mls/add/rent_price', $data);	
		else
			$this->load->view('mls/add/sell_price', $data);	
	}

	public function Upload(){
        ini_set('memory_limit','580M');

		try {
			$this->load->model('image_model');
			$res = $this->image_model->Upload('file');
			echo json_encode($res);
			return;
			/*
				$config['allowed_types'] = 'jpg|jpeg|png';//'gif|jpg|jpeg|png';
				$config['max_size'] = 1024 * 10;
				$config['encrypt_name'] = TRUE;	
				$config['upload_path'] = 'assets/img/tmp/';
				$config['mod_mime_fix'] = FALSE;	
				$config['detect_mime'] = FALSE;	
				$res = array('origin_name'=>$_FILES['file']['name']);
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("file"))
				{
					$status = 'error';
					$msg = $this->upload->display_errors('', '');
					$res['origin_msg'] = $msg;
					switch($msg){
						case "The filetype you are attempting to upload is not allowed.": $msg='Данное расширение изображения не поддерживается'; break;
						case "": $msg=''; break;
					}
				}
				else
				{
					$uploaddata = $this->upload->data();	
					$file_path = $config['upload_path'].$uploaddata['file_name'];
					$res['new_name']= $file_path;
					$res['status'] = 'OK';
					//Повернем
					$this->load->model('image_model');
					if($uploaddata['image_type'] == 'jpeg')
						$this->image_model->AutoRotate($file_path);
					$this->image_model->resize_image($file_path, false, 1200);
					echo json_encode($res);
					return;			
				}
				$res['status'] = 'ERROR';
				$res['msg'] = $msg;
			*/
		} catch (Exception $e) {
			$res['msg'] = 'Выброшено исключение: '. $e->getMessage()."\n";
			$this->user_log_model->log($res['msg'], 6);
		}
		die(json_encode($res));
	}
}
