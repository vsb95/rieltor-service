<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabinet extends CI_Controller {
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}

	public function index()
	{		
		$data = array('title'=>'МЛС: Кабинет');
		$data['scripts'] =  array("/assets/js/fotorama.js");
		$data['styles'] =  array("/assets/css/fotorama.css");		
		if(isset($_GET['offerType']) && !empty($_GET['offerType']))
			$data['offerType'] =  $_GET['offerType'];	
		if(isset($_GET['objType']) && !empty($_GET['objType']))
			$data['objType'] =  $_GET['objType'];	
		if(isset($_GET['advType']) && !empty($_GET['advType'])){
			$data['advType'] =  $_GET['advType'];	
			$data['title'] =  'МЛС: Архив';
		}
		$this->load->view('1_header_new', $data);
		$this->load->view('mls/cabinet', $data);
		$this->load->view('1_footer_new');
	}	

	public function Action(){
		if(!isset($_GET['action']) && !empty($_GET['action']))
			die("Не указано действие");
		if(!isset($_GET['id']) && !empty($_GET['id']))
			die("Не указано объявление");

		$action = $_GET['action'];
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status)
			die("Сервер временно недоступен. Повторите позже");
		// Дернем API
		$url = API_SERVER_URL.'api/UpdateMlsLivingOffer?userId='.$_SESSION['id_user'].'&advId='.$_GET['id'].'&action='.$action;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);

		$json = json_decode($response, true);
		if($json['status-type'] != "OK")
			die($json['status']);
		$this->user_log_model->log( "MLS::Advertisment '".$_GET['action']."': ".$_GET['id'], 2);
		echo "OK";
	}
}
