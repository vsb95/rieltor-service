<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluation extends CI_Controller {	
	function __construct() {
		parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(1) && !$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}

	public function index()
	{					
		$data['title'] =  "Оценка";		
		$data['scripts'] =  array("/assets/js/fotorama.js","/assets/js/district_select.js",'https://api-maps.yandex.ru/2.1/?lang=ru_RU');
		$data['styles'] =  array("/assets/css/fotorama.css");
		$data["help_video"] = "https://www.youtube.com/embed/-4egA2HH34g";
		$data["help_description"] = "Посмотрите видео, как формировать отчет из поиска";
		
		$initData = $this->LoadInitData();
		if(isset($initData) && !empty($initData)){
			$initData["districts"] = json_encode($initData["districts"]);
			$initData["micro-districts"] = json_encode($initData["micro-districts"]);
			$initData["id_city"] = $this->session->userdata('id_city');
		}
		$this->load->view('1_header_new',$data);
		$this->load->view('evaluation/search', array("initData"=>$initData));
		$this->load->view('1_footer_new'); 
	}
	
	public function Search(){
		if(!$this->user_model->IsLogined()){
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Вы не авторизованы')));
		}
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен')));
		} 
		$id_user = $this->session->userdata('id_user');
		$post = $_POST;
		$post["UserId"] =$id_user;
		// Дернем API
		$url = API_SERVER_URL.'api/search';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($post)
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);

		if(isset($json["pages"]) && !empty($json["pages"])){
			foreach($json["pages"] as $page){
				$json["html-pages"][]= $this->load->view('evaluation/list_item', array("page"=>$page), true);
			}			
		}
		echo json_encode($json);
		$this->user_log_model->log( "search evaluation", 1);
	}

	private function LoadInitData(){
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			return null;
			//die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен')));
		} 
		// Дернем API
		$idCity = $this->session->userdata('id_city');
		$url = API_SERVER_URL.'api/search?idCity='.$idCity;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		if(is_array($response))
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Не найден путь api/search')));
		$json = json_decode(json_decode($response, true), true);
		return $json;
	}

	public function Report(){
		set_time_limit(900);
		if(!isset($_POST["selected_adv"]) || empty($_POST["selected_adv"]))
			die(json_encode(array('status-type'=>"EMPTY", 'status'=>'Объявления не выбраны')));
		if(!isset($_POST["my_obj"]) || empty($_POST["my_obj"]))
			die(json_encode(array('status-type'=>"EMPTY", 'status'=>'Не указана информация по оцениваемому объекту')));
		$my_obj = $_POST["my_obj"];
		$id_user = $this->session->userdata('id_user');
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен')));
		}
        if(!isset($my_obj) || empty($my_obj))            
            die('Не указаны данные по оцениваемому объекту');
         
        if(!isset($my_obj['renovation']) || empty($my_obj['renovation']))            
            die('Не указан ремонт оцениваемого объекта');
        if(!isset($my_obj['floor']) || empty($my_obj['floor']))            
            die('Не указан этаж оцениваемого объекта');
        if(!isset($my_obj['furniture']) || empty($my_obj['furniture']))            
            die('Не указана мебель оцениваемого объекта');
        if(!isset($my_obj['floor_max']) || empty($my_obj['floor_max']))            
            die('Не указана этажность дома оцениваемого объекта');
        if(!isset($my_obj['size']) || empty($my_obj['size']))            
            die('Не указана площадь оцениваемого объекта');
		// Дернем API
		$url = API_SERVER_URL.'api/pages';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => http_build_query(array('IdList'=>$_POST["selected_adv"], 
			'userid'=>$id_user, 'isCutLogo' => 0))
		));
		
		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		if($json["status-type"] != "OK"){
			die(json_encode($json));
		}
		$pages = $json["pages"];
		$user = $this->user_model->GetCurrentUser();
		$user['org'] = $this->user_model->GetUserOrg($user['id_org']);
		$view = $this->load->view('evaluation/report', array('pages'=>$json["pages"], 'my_obj'=> $_POST["my_obj"], 'user' => $user, 'isForPrint' => true),true);

		$this->load->model('export_model');
		$pdfFile = $this->export_model->exportTo($view, 'Отчет по оценке ', 'pdf', realpath('assets/img/tmp'));
		if(isset($pdfFile) && !empty($pdfFile)){
			$pdfFile = str_replace(realpath(''), '', $pdfFile);
		}
		$this->load->view('evaluation/report', array('pages'=>$json["pages"], 'my_obj'=> $_POST["my_obj"], 'user' => $user, 'pdfFile'=> $pdfFile, 'isForPrint' => false));
		$user = $this->user_log_model->log("create evaluation", 2);
	}	

	public function Promo(){
		$data['title'] =  "Эталон";		
		$this->load->view('1_header_new',$data);
		$this->load->view('evaluation/promo');
		$this->load->view('1_footer_new'); 
	}
	public function EtalonRequest(){
		$user = $this->user_model->getCurrentUser();
		$user['org'] = $this->user_model->getUserOrg($user['id_org']);
		if ( (!isset($user['phone']) || empty($user['phone'])) && isset($_GET['phone']) && !empty($_GET['phone']))  {
			$user['phone'] = $_GET['phone'];
		}
		$this->load->view('1_header_new',array('title'=>'Заявка на оценку'));
		
		if(!isset($user['full_name']) || empty($user['full_name'])
			|| !isset($user['phone']) || empty($user['phone'])){ 
			$content = 'Прежде чем продолжить <a href="/settings" >заполните профиль</a> или укажите номер телефона <div><form method="GET" action="/evaluation/EtalonRequest"><input required type="tel" name="phone" placeholder="+7 (999) 999 9999" class="form-control" style="margin: 20px 15px 15px 0; display: inline-block; max-width:150px"/> <input type="submit" class="btn btn-primary btn-md" value="Отправить" style="max-width:150px"/></form> ';
			$this->load->view('message_block', array('head' => 'Укажите телефон', 'description' => $content));
		}
		else{
			$content = 'Заявка на оценку поступила от:<br>';
			$content .= $user['full_name'].'<br>';
			if(isset($user['email']))
				$content .= $user['email'].'<br>';
			if(isset($user['phone']))
				$content .= $user['phone'].'<br>';
			if(isset($user['phone_alternative']))
				$content .= $user['phone_alternative'].'<br>';
			$content .= '<br>Из организации:<br>';
			$content .= $user['org']['name'].'<br>';
			$content .= '<br>';
			$body = $this->load->view('email', array(
				'title'=>'Заявка на оценку',				
				'content'=>$content
			), true);
			$this->load->model('mailer');
			$this->mailer->sendEmail('etalon7.ocenka@mail.ru', "Заявка на кредит", $body, null, null);
	
			$this->load->view('message_block', array('head'=>'Заявка на оценку отправлена', 'description'=>'Для ускорения оценки вы можете сразу перейти в раздел <a href="http://omsk-etalon.ru/uslugi-ocenki?payment=sb" > "оплата" </a>'));
		}
		$this->load->view('1_footer_new'); 
		$this->user_log_model->log( "send etalon evaluation request", 1);
	}
}
