<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
	}

	private function load_view($result, $id_user){
		$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($id_user));
		$user      = $res->result_array()[0];
		$res       = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($user["id_org"]));
		$org      = $res->result_array()[0];
		$data = array(
			"title"=>"Настройки",
			//"scripts"=>array("/assets/js/settings.js","/assets/js/jquery.ajaxfileupload.js"),
			"styles"=>array("/assets/css/settings.css"),
			"result"=>$result,
			"user"=>$user, 
			"org"=>$org,
			"is_admin"=>$this->session->userdata('is_admin') == 1
		);
		$this->load->view('1_header_new', $data);
		if($this->session->userdata('is_admin') == 1){
			$res = $this->db->query("SELECT user.id, user.login, user.full_name, user.is_admin, user.id_grant, user.is_activate FROM user WHERE id_org = ? AND user.is_deleted = 0 ORDER BY user.is_admin;",
					 array($user["id_org"]));
			$data["users"] = $res->result_array();
			$res = $this->db->query("SELECT count(*) as c FROM user WHERE id_org = ? AND user.id_grant = 1",array($user["id_org"]));
			$data["count_grant_search_enable"] = $res->result_array()[0]['c'];
			$res = $this->db->query("SELECT count(*) as c FROM user WHERE id_org = ? AND user.id_grant = 2",array($user["id_org"]));
			$data["count_grant_full_search_enable"] = $res->result_array()[0]['c'];
		}
		$this->load->view('settings', $data);
		$this->load->view('1_footer_new');
	}


	public function index()
	{
		$id_user = $this->session->userdata('id_user');		
		$this->load_view(array(), $id_user);
	}

	public function saveOrg(){	
		$id_user = $this->session->userdata('id_user');
		$is_admin = $this->session->userdata('is_admin');
		
		if(!$this->user_model->IsLoginedAsAdmin())
		{
			header( 'Location: /', true, 301 );
			die();
		}
		
		$this->user_log_model->log("save org settings", 3);
		$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($id_user));
		$user      = $res->result_array()[0];

		$org_name =$this->input->post('org_name');
		$org_adress =$this->input->post('org_adress');
		$org_url =$this->input->post('org_url');
		$org_email =$this->input->post('org_email');
		$org_ya_geo =$this->input->post('org_ya_geo');
		$idCity =$this->input->post('id_city');
		$org_stamp = "";
		$org_logo = "";
		if(isset($_FILES['org_logo']) && !empty($_FILES['org_logo'])){
			$org_logo = $_FILES['org_logo']["name"];
		}
		if(isset($_FILES['org_stamp']) && !empty($_FILES['org_stamp'])){
			$org_stamp = $_FILES['org_stamp']["name"];
		}
		if($user["is_admin"] == "1"){
			$this->db->set('name', htmlspecialchars($org_name));
			$this->db->set('adress', htmlspecialchars($org_adress));
			$this->db->set('url', $org_url);
			$this->db->set('email', $org_email);
			$this->db->set('ya_geo', $org_ya_geo);
			if(!empty($idCity))
				$this->db->set('id_city', $idCity);
			$this->db->where('id', $user["id_org"]);
			$this->db->update('organization');
			$this->session->set_userdata('org_url',$org_url);		
		}
		$result = array("status" => "success");
		if(!empty($org_logo)){
			$result = $this->upload_file("org_logo", false);
		}
		if(!empty($org_stamp)){
			$fileName = $this->uploadPng("org_stamp");
			if(isset($fileName) && !empty($fileName)){
				$this->db->set('logo_stamp', $fileName);
				$this->db->where('id', $this->session->userdata('id_org'));
				$this->db->update('organization');
			}
		}
		header( 'Location: /settings?result=1', true, 301 );
		$this->saveCurrentUserToApi();
	}

	public function save(){		
		$id_user = $this->session->userdata('id_user');
		
		$this->user_log_model->log("save user settings", 3);
		$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($id_user));
		$user      = $res->result_array()[0];

		$firstname = $this->input->post('firstname');
		$phone = $this->input->post('phone');
		$phone2 = $this->input->post('phone2');
		$email= $this->input->post('email');
		$tagline= $this->input->post('tagline');
		
		$user_photo = "";
		if(isset($_FILES['user_photo']) && !empty($_FILES['user_photo'])){
			$user_photo = $_FILES['user_photo']["name"];
		} 
		if($email != $user["email"]){
			$this->load->model('user_model');
			if(!$this->user_model->IsEmptyLogin($email)){
				die("Email уже занят");
			}
		}
		
		//var_dump($_POST);
		$this->db->set('email', $email);
		$this->db->set('phone', $phone);
		$this->db->set('phone_alternative', $phone2);
		$this->db->set('tagline', $tagline);
		$this->db->set('full_name', $firstname);
		$this->db->where('id', $id_user);
		$this->db->update('user');


		$result = array("status" => "success");
		if(!empty($user_photo) && $result["status"] == "success"){			
			$result = $this->upload_file("user_photo", true);
		}
		header( 'Location: /settings?result=1', true, 301 );
		$this->saveCurrentUserToApi();
	}

	public function Changepassword(){
		$id_user = $this->session->userdata('id_user');

		$oldPsw = $this->input->post('password_old');
		$newPsw1 = $this->input->post('password1');
		$newPsw2 = $this->input->post('password2');

		if($newPsw1 != $newPsw2)
			die("Пароли не совпадают");
		$res = $this->db->query("SELECT user.password FROM user WHERE id = ?", array($id_user));
		$user = $res->result_array()[0];

		$hash = hash('md5', $oldPsw);
		if($user["password"] != $hash)
			die("Вы ввели неправильный пароль");

		
		$this->user_log_model->log( "change password", 3);

		$hash = hash('md5', $newPsw1);
		$this->db->set('password', $hash);
		$this->db->where('id', $id_user);
		$this->db->update('user');

		echo "Пароль успешно сменен!";
	}

	public function TryDeleteUser(){		
		if(!$this->user_model->IsLoginedAsAdmin())
		{
			header( 'Location: /', true, 301 );
			die();
		}
		if(!isset($_POST["user_login"]) || empty($_POST["user_login"]))
			die("email не указан");
		$result = array();
		$email = $_POST["user_login"];
		$id_org = $this->session->userdata('id_org');
		$res       = $this->db->query("SELECT id FROM user WHERE (id = ? OR email = ?) AND id_org = ?;", array($email, $email, $id_org));
		$users      = $res->result_array();
		if(count($users)  == 0)
			die("Такой пользователь не найден");

		$deletedUserId = $users[0]["id"];
		try{
			// Дернем API
			$url = API_SERVER_URL.'api/user?idUser='.$deletedUserId;
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
			));

			$response = curl_exec($myCurl);
			$json = json_decode($response,true);
			$result['offers'] = $json;
		}catch(Exception $ex){
			$this->user_log_model->log($ex, 4);
		}
		$res = $this->db->query("SELECT id, full_name as name, email, phone FROM user WHERE is_activate = 1 AND id_org = ? AND is_deleted = 0 ORDER BY name;",
			array($id_org));
		$result["users"] = $res->result_array();
		echo json_encode($result);
	}

	public function DeleteUser(){
		$id_user = $this->session->userdata('id_user');
		$is_admin = $this->session->userdata('is_admin');
		$id_org = $this->session->userdata('id_org');
		
		if(!$this->user_model->IsLoginedAsAdmin())
		{
			header( 'Location: /', true, 301 );
			die();
		}
		if(!isset($_POST["user_login"]) || empty($_POST["user_login"]))
			die("email не указан");
		$email = $_POST["user_login"];
		$res       = $this->db->query("SELECT id FROM user WHERE (id = ? OR email = ?) AND id_org = ?;", array($email, $email, $id_org));
		$users      = $res->result_array();
		if(count($users)  == 0)
			die("Такой пользователь не найден");

		$deletedUserId = $users[0]["id"];

		$this->db->set('is_deleted', 1);
		$this->db->set('id_grant', 0);
		$this->db->where('id', $deletedUserId);
		$this->db->update('user');

		
		$this->user_log_model->log("delete user: ".$email, 3);		
		echo "Сотрудник уволен";

		try{
			$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
			if (!$status) { 
				return;
			} 
			$user = array();
			if(isset($_POST["new_owner_id"]) && !empty($_POST["new_owner_id"])){
				$newOwnerId = $_POST["new_owner_id"];
				$dbUser = $this->user_model->GetUser($newOwnerId);
				$user = array(
					'Id'=>$dbUser['id'],
					'IdOrg'=>$dbUser['id_org'],
					'Phone'=>$dbUser['phone'],
					'PhoneAlternative'=>$dbUser['phone_alternative'],
					'Name'=>$dbUser['full_name'],
					'Email'=>$dbUser['email'],
				);
				if(isset($dbUser['photo']) && !empty($dbUser['photo']))
					$user["Photo"]= SERVER_DOMAIN.'/assets/img/user/'.$dbUser['photo'];
				$user["Organization"] =  $this->user_model->GetUserOrg($dbUser['id_org'])["name"];
			}

			// Дернем API
			$url = API_SERVER_URL.'api/user';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_CUSTOMREQUEST =>'DELETE',
				CURLOPT_POSTFIELDS => http_build_query(array('IdUser'=>$deletedUserId, 'IdOrg'=>$id_org, 'NewOwner' => $user))
			));

			$response = curl_exec($myCurl);
			$json = json_decode($response,true);
		}catch(Exception $ex){
			$this->user_log_model->log($ex, 4);
		}
		
	}

	public function Adduser(){
		$id_user = $this->session->userdata('id_user');
		$is_admin = $this->session->userdata('is_admin');
		$id_org = $this->session->userdata('id_org');
		
		if(!$this->user_model->IsLoginedAsAdmin())
		{
			header( 'Location: /', true, 301 );
			die();
		}

		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 

		$customLogin = $this->input->post('new_user_login');
		$email = $this->input->post('new_user_email');
		if(!isset($email) || empty($email))
			die("email не указан");

		if(!isset($customLogin) || empty($customLogin)){
			$customLogin = $email;
		}

		$this->load->model('user_model');
		if(!$this->user_model->IsEmptyLogin($customLogin)){
			die("Логин или email уже заняты");
		}

		$this->load->model('randomizer');
		$password =$this->randomizer->generateRandomString(5);
		$hash= hash('md5', $password);
		$body ="Вас пригласили на Rieltor-service.ru<br>Ваши логин и пароль: <br>".$customLogin.'<br>'.$password.'<br><br>';		
		
		$email_html = $this->load->view('email',
			array('title'=>'Приглашение в Rieltor-service.ru',
				'content'=>$body,
				'back_url'=> "",
				'buttonText' => 'Начнем!'
		), TRUE);
		$url = API_SERVER_URL.'api/email';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('EmailTo'=>$email,
			'Caption'=>'Приглашение в Rieltor-service.ru',
			'Content'=>$email_html))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		if($response == "\"Письмо успешно отправлено!\""){			
			$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($id_user));
			$user      = $res->result_array()[0];
			$this->user_model->addNew($customLogin, $hash, $user["id_org"], 0, $email);

			
			$this->user_log_model->log("invite user: ".$customLogin, 2);
		}
		echo $response;
	}

	private function upload_file($filename, $isUserPhoto){
		$status = "";
		$msg = "";
		$file_element_name = $filename;
		//echo "upload_file:: filename = ".$filename."<br/>";
		if ($status != "error")
		{
			$config['upload_path'] = 'assets/img/logo/';
			$config['allowed_types'] = 'gif|jpg|png|ico';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = TRUE;
			if($isUserPhoto == true){				
				$config['upload_path'] = 'assets/img/user/';
			}

			$this->load->library('upload', $config);
	
			if (!$this->upload->do_upload($file_element_name))
			{
				//echo "3.1 -><br/>";
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
				//echo $msg."<br/>";
			}
			else
			{
				//echo "3.2<br/>";
				$uploaddata = $this->upload->data();	
				
				$id_user = $this->session->userdata('id_user');
				if($isUserPhoto == true){	
					//echo "3.2.1<br/>";					
					$this->db->set('photo', $uploaddata['file_name']);
					$this->db->where('id', $id_user);
					$this->db->update('user');
					$result = true;
				}
				else{		
					//echo "3.2.2<br/>";			
					$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($id_user));
					$data      = $res->result_array();
					$result = false;
					if (count($data) != 0) {
						$user = $data[0];
						$this->db->set('logo_name', $uploaddata['file_name']);
						$this->db->where('id', $this->session->userdata('id_org'));
						$this->db->update('organization');
						$result = true;						
						$this->session->set_userdata('org_logo', $uploaddata['file_name']);
					}
				}		
				//$file_id = $this->files_model->insert_file($uploaddata['file_name'], $title);
				if($result == true)
				{
					$status = "success";
					$msg = "File successfully uploaded";
					
					$this->load->model('image_model');
					$this->image_model->AutoRotate($uploaddata['full_path']);
					$this->image_model->resize_image($uploaddata['full_path'], false, 450);
				}
				else
				{
					unlink($uploaddata['full_path']);
					$status = "error";
					$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			//echo "4<br/>";
			@unlink($_FILES[$file_element_name]);
		}
		return array('status' => $status, 'msg' => $msg);
	}

	public function changeGrant(){
		$id_user = $this->session->userdata('id_user');
		$is_admin = $this->session->userdata('is_admin');
		$id_org = $this->session->userdata('id_org');
		
		if(!$this->user_model->IsLoginedAsAdmin())
		{
			header( 'Location: /', true, 301 );
			die('У вас нет сотрудников');
		}
		if(!isset($_POST['grants']) || empty($_POST['grants'])){
			die('У вас нет сотрудников');
		}

		$res       = $this->db->query("SELECT count_grant_search, count_grant_full_search FROM organization WHERE id = ?;", array($id_org));
		$org      = $res->result_array()[0];

		$maxSearchCount = $org['count_grant_search'];
		$maxSearchPlusCount = $org['count_grant_full_search'];

		$neededSearchCount = 0;
		$neededSearchPlusCount = 0;
		foreach($_POST['grants'] as $id => $grant){
			if($grant == 1)
				$neededSearchCount++;
			if($grant == 2)
				$neededSearchPlusCount++;
		}
		if($neededSearchCount > $maxSearchCount){
			echo 'Вы пытаетесь подключить больше пакетов "Поиск" чем вам доступно ('.$neededSearchCount.'/'.$maxSearchCount.'). Обратитесь к вашему менеджеру';
			die();
		}
		if($neededSearchPlusCount > $maxSearchPlusCount){
			echo 'Вы пытаетесь подключить больше пакетов "Поиск+" чем вам доступно ('.$neededSearchPlusCount.'/'.$maxSearchPlusCount.'). Обратитесь к вашему менеджеру';
			die();
		}
		
		$this->load->model('randomizer');
		
		$this->user_log_model->log("settings: update grants", 3);

		$this->load->model('user_model');
		foreach($_POST['grants'] as $id => $grant){				
			$user = $this->user_model->GetUser($id);
			if($user['id_org'] != $id_org){
				$this->user_log_model->log("settings: ALERT! try update grant alien user", 6);
				echo 'Вы пытаетесь изменить доступ сотруднику не из вашей организации. Не надо так';
				die();				
			}
			if($user['id_grant'] == $grant)
				continue;
			if($grant != 0){
				$lastChange = date_create($user['last_date_grant_change']);
				$interval = date_diff($lastChange, date_create(date('H:i:s d.m.Y')));
				$daysGone = $interval->format('%d');
				if($daysGone < 7){
					echo 'Прошло '.$daysGone.'/7 дней после последней смены доступа у сотрудника "'.(empty($user['full_name'])?$user['email']:$user['full_name']).'". <br/>';
					continue;
				}					
			}
			$timestamp = date('Y-m-d H:i:s');
			$this->db->set('last_date_grant_change', $timestamp);
			$this->db->set('id_grant', $grant);
			$this->db->set('token', $this->randomizer->generateRandomString());
			$this->db->where('id', $id);
			$this->db->update('user');
		}
		echo 'Список доступов успешно изменен!';
	}

	private function uploadPng($filesName){
		$target_dir = "assets/img/logo/";
		
		$this->load->model('randomizer');
		$target_fileName = $this->randomizer->generateRandomString(15).'.png';

		$target_file = $target_dir . $target_fileName ;
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($_FILES[$filesName]["name"],PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES[$filesName]["tmp_name"]);
			if($check !== false) {
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}
		}
		// Check if file already exists
		if (file_exists($target_file)) {
			$this->user_log_model->log('Sorry, file already exists: '.$target_file, 6);
			die("Sorry, file already exists.");
			$uploadOk = 0;
		}
		// Check file size 500Kb
		if ($_FILES[$filesName]["size"] > 500*1024) {
			die("Слишком большой файл для водяного знакаю Максимальный размер 500 Кб");
			$uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "png") {
			$this->user_log_model->log('Недопустимый разширение для водяного знака: '.$imageFileType, 6);
			die("Недопустимый разширение для водяного знака. Должен быть PNG");
			$uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			return null;
		// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES[$filesName]["tmp_name"], $target_file)) {				
				$this->load->model('image_model');
				$this->image_model->resize_image($target_file, false, 700);
				echo "The file ". basename( $_FILES[$filesName]["name"]). " has been uploaded.";
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
		}
		return $target_fileName;
	}

	private function saveCurrentUserToApi(){
		// Попытаемся сохранить на API		
		try{
			$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
			if (!$status) { 
				return;
			} 
			$currentUser = $this->user_model->GetCurrentUser();
			$request = array(
				'Id'=>$currentUser['id'],
				'IdOrg'=>$currentUser['id_org'],
				'Phone'=>$currentUser['phone'],
				'PhoneAlternative'=>$currentUser['phone_alternative'],
				'Name'=>$currentUser['full_name'],
				'Email'=>$currentUser['email'],
			);
			if(isset($currentUser['photo']) && !empty($currentUser['photo']))
				$request["Photo"]= SERVER_DOMAIN.'/assets/img/user/'.$currentUser['photo'];
			$org = $this->user_model->GetUserOrg($currentUser['id_org']);
			$request["Organization"] =  $org["name"];
			if(isset($org["logo_stamp"]) && !empty($org["logo_stamp"]))
				$request["OrganizationWatermark"] =  SERVER_DOMAIN.'/assets/img/logo/'.$org["logo_stamp"];
			
			// Дернем API
			$url = API_SERVER_URL.'api/user';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => http_build_query($request)
			));

			$response = curl_exec($myCurl);
			$json = json_decode($response,true);
		}catch(Exception $ex){
			$this->user_log_model->log($ex, 4);
		}
	}
}
