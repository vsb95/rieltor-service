<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Work extends CI_Controller {
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}

	public function index()
	{		
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		$data = array(
			'title' => 'Сформировать отчет',
			'spinner_title' => 'Формируем отчет',
			'action' => '/work/createPage',
			'styles' => array("/assets/css/work.css"),
			"isServerOnline" => $status,
			"help_video" => "https://www.youtube.com/embed/PT2DHYwnJYE",
			"help_description" =>"Посмотрите видео, как формировать отчет из ссылок, с сайта-источника"
			//'scripts' => array("/assets/js/work.js")
		);
		$this->load->view('1_header_new', $data);
		if($status)
			$this->load->view('work', $data);
		else
			$this->load->view('shutdown_server', $data);
		$this->load->view('1_footer_new');
	}
	
	public function Actualize()
	{		
		if(!$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /', true, 301 );
		}
		else{
			$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
			$data = array(
				'title' => 'Актуализация',
				'spinner_title' => 'Актуализация...',
				'action' => '/work/createPage/1',
				'styles' => array("/assets/css/work.css"),
				"isServerOnline" => $status
				//'scripts' => array("/assets/js/work.js")
			);
			$this->load->view('1_header_new', $data);
			if($status)
				$this->load->view('work', $data);
			else
				$this->load->view('shutdown_server', $data);
			$this->load->view('1_footer_new');
		}
	}
	public function Cutter()
	{		
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		$data = array(
			'title' => 'Скачать фото',
			'spinner_title' => 'Скачиваем фото',
			'action' => '/work/cutImages',
			'styles' => array("/assets/css/work.css"),
			"isServerOnline" => $status
			//'scripts' => array("/assets/js/work.js")
		);
		$this->load->view('1_header_new', $data);
		if($status)
			$this->load->view('work', $data);
		else
			$this->load->view('shutdown_server', $data);
		$this->load->view('1_footer_new');		
	}

	public function createPage($isActualize = 0)
	{ 
		set_time_limit(300);
		$result = $this->get_response_api(0, $isActualize);
		if(count($result["pages"])>0){
			$this->load->model('page_model');
			$idView = $this->page_model->create(json_encode($result["pages"]));
			$result["link"] = SERVER_DOMAIN."page/show/".$idView;	
			if( $isActualize == 1){
				$result["debug"] = print_r($result["pages"], true);
			}
			
			$user = $this->user_log_model->log( ($isActualize ==0 ?'create':'actualize')." page: ".$idView, 2);
		}
		echo json_encode($result);
	}

	public function cutImages()
	{ 
		set_time_limit(300);
		$result = $this->get_response_api(1, 0);
		$result["link"]="";
		$result["link-download"] = "/assets/page_images/zip/".$result["zip-file"];//"/page/download/?file=".$result["zip-file"];
		echo json_encode($result);
		$this->user_log_model->log( "cut images", 2);
		
	}

	private function get_response_api($isCreateZip = 0, $isActualize = 0) {
		$post = $this->input->post('links');
		$isCutLogo = $this->input->post('isCutLogo');
		//$links = explode("\n",$post);
		if(!isset($post) || empty($post)){
			die(json_encode(array('status-type'=>"EMPTY")));
		}
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен. Повторите еще раз')));
		} 
		// Дернем API
		$url = API_SERVER_URL.'api/pages';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('data'=>$post, 'userid'=>$this->session->userdata('id_user'),
			'isCreateZip'=>$isCreateZip, 'IsActualize'=>$isActualize, 'isCutLogo'=>$isCutLogo))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		//var_dump($json);
		if($json["status-type"] != "OK"){
			die(json_encode($json));
		}
		$result = array(
			"total-sec"=>$json["total-sec"],
			"pages"=>$json["pages"],
			"unparsed-urls"=>$json["unparsed-urls"],
			"status-type"=>$json["status-type"],
			"zip-file"=>$json["zip-file"],
		);
		foreach($result["pages"] as &$page){			
			$description = strip_tags($page["description"]); // удаляем HTML теги
			$order   = array("\r\n", "\n", "\r");
			// Обрабатывает сначала \r\n для избежания их повторной замены.
			$page["description"] = str_replace($order, '<br />', $description);			
		}
		return $result;
	}
}
