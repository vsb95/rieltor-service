<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Credit extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2) && !$this->user_model->IsGranted(555))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		} 
		$this->load->model('credit_model');
	}

	public function index(){
		$data = array(
			'title' => 'Ипотека',
			'styles' => array("/assets/css/modal.css"),
			"banks" =>$this->credit_model->getBanks(),
			"programs" => $this->credit_model->getPrograms()
		);
		
		$this->load->view('1_header_new', $data);
		$this->load->view('credit/search', $data);
		$this->load->view('1_footer_new');		
	}

	public function ListRequest(){
		$data = array(
			'title' => 'Ипотека: Мои заявки',
			'styles' => array("/assets/css/modal.css"),
			"requests" =>$this->credit_model->getMyRequests()
		);
	
		$this->load->view('1_header_new', $data);
		$this->load->view('credit/request_search', $data);
		$this->load->view('1_footer_new');		
	}

	public function NewRequest(){
		$data = array(
			'title' => 'Ипотека: Создание заявки',
			'styles' => array("/assets/css/modal.css", '/assets/css/image_upload_box.css'),
			'scripts' =>array('/assets/js/jquery-ui.js','/assets/js/image_upload_box.js'),
			"banks" =>$this->credit_model->getBanks(),
			"programs" => $this->credit_model->getPrograms()
		);
	
		$this->load->view('1_header_new', $data);
		$this->load->view('credit/request_create', $data);
		$this->load->view('1_footer_new');		
	}

	public function PreviewRequest(){
		$request = $_POST;
		if(!isset($request['banks']) || empty($request['banks'])){
			die('не выбран банк');
		}
		if(!isset($request['summ']) || empty($request['summ'])){
			die('не указана сумма кредита');
		}
		
		$currentUser = $this->user_model->GetCurrentUser();
		$currentUser['org'] = $this->user_model->GetUserOrg($currentUser['id_org']);

		$this->load->view('credit/request_preview', array(
			'title'=>'Заявка на кредит',				
			'params'=>$request,
			'current_user' => $currentUser
		));
	}
	public function CreateRequest(){
		$request = $_POST;
		if(!isset($request['banks']) || empty($request['banks'])){
			die('не выбран банк');
		}
		if(!isset($request['summ']) || empty($request['summ'])){
			die('не указана сумма кредита');
		}
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if(!$status){
			die('Сервер временно недоступен. <br>Не закрывайте страницу и попробуйте повторить через пару минут');
		}
		// Вертаем изображения
		$images = array();
			
		$this->load->model('image_model');
		if(isset($request['client']['doc']) && !empty($request['client']['doc'])){
			foreach($request['client']['doc'] as $key => $collection){
				foreach($collection as &$image){
					$path = $image['src'];
					if($image['angle'] > 0){
						// если у изображения есть уже ссылка на домен - значит она не в tmp папке
						$pos = strpos($image['src'], SERVER_DOMAIN);
						if($pos !== false){
							$path = str_replace(SERVER_DOMAIN, '',$image['src']);
							$path = ltrim($path, '/');
						}
						$newPath = ($pos !== false? SERVER_DOMAIN :'').ltrim($this->image_model->Rotate($path, $image['angle']), '/');
						if(isset($newPath) && !empty($newPath))
							$image['src'] = $newPath;						
					}
					$image['name']='Клиент_';
					switch($key){
						case 'passport': $image['name'].='Паспорт'; break;
						case 'any': $image['name'].='Доп'; break;
						case 'another': $image['name'].='Другое'; break;
						case 'declaration': $image['name'].='Налоговая_декларация'; break;
						case 'org-reg': $image['name'].='Cвидетельство регистрации ИП'; break;
						case 'inquiry-oldman': $image['name'].='Пенсия'; break;
						case 'inquiry': $image['name'].='Справка'; break;
						case 'contract': $image['name'].='Трудовая'; break;
						case 'children': $image['name'].='Ребенок'; break;
						case 'married-doc': $image['name'].='Брачный договор'; break;
						case 'death': $image['name'].='Свидетельство о смерти супруга'; break;
						case 'un-married-doc': $image['name'].='Свидетельство о разводе'; break;
						case 'accept': $image['name']='Согласие на обработку данных'; break;
						default: echo '<br>'.$key; break;
					}
					$images[] = $image;
				}
			}
		}
		if(isset($request['co-borrowers']) && !empty($request['co-borrowers'])){
			$i=0;
			foreach($request['co-borrowers'] as $coBorrower){
				$i++;
				if(isset($coBorrower['doc']) && !empty($coBorrower['doc'])){
					foreach($coBorrower['doc'] as $key => $collection){
						foreach($collection as &$image){
							$path = $image['src'];
							if($image['angle'] > 0){
								// если у изображения есть уже ссылка на домен - значит она не в tmp папке
								$pos = strpos($image['src'], SERVER_DOMAIN);
								if($pos !== false){
									$path = str_replace(SERVER_DOMAIN, '',$image['src']);
									$path = ltrim($path, '/');
								}
								$newPath = ($pos !== false? SERVER_DOMAIN :'').ltrim($this->image_model->Rotate($path, $image['angle']), '/');
								if(isset($newPath) && !empty($newPath))
									$image['src'] = $newPath;							
							}
							
							$image['name']='Созаемщики_'.$i.'_';
							switch($key){
								case 'passport': $image['name'].='Паспорт'; break;
								case 'any': $image['name'].='Доп'; break;
								case 'another': $image['name'].='Другое'; break;
								case 'declaration': $image['name'].='Налоговая_декларация'; break;
								case 'org-reg': $image['name'].='Cвидетельство регистрации ИП'; break;
								case 'inquiry-oldman': $image['name'].='Пенсия'; break;
								case 'inquiry': $image['name'].='Справка'; break;
								case 'contract': $image['name'].='Трудовая'; break;
								case 'children': $image['name'].='Ребенок'; break;
								case 'married-doc': $image['name'].='Брачный договор'; break;
								case 'death': $image['name'].='Свидетельство о смерти супруга'; break;
								case 'un-married-doc': $image['name'].='Свидетельство о разводе'; break;
								case 'accept': $image['name']='Согласие на обработку данных'; break;
								default: echo '<br>'.$key; break;
							}
							$images[] = $image;
						}
					}
				}
			}
		}
		$currentUser = $this->user_model->GetCurrentUser();
		$currentUser['org'] = $this->user_model->GetUserOrg($currentUser['id_org']);
		$attach = array();
		//////////////////////
		$this->load->model('export_model');
		$preview = $this->load->view('credit/request_print', array(
			'title'=>'Заявка на кредит',				
			'params'=>$request,
			'current_user' => $currentUser
		), true);
		$pdfFile = $this->export_model->exportTo($preview, 'Анкета_');
		if(isset($pdfFile) && !empty($pdfFile)){
			$attach[]=$pdfFile;
		}
		/////////////////////
		$this->load->model('mailer');
		foreach($request['banks'] as $idBank){
			$bank = $this->credit_model->getBankById($idBank);
			if(!isset($bank['manager_email']) || empty($bank['manager_email'])){
				echo 'У банка '.$bank['name'].' не указан email';
				continue;
			}
			$email = $bank['manager_email'];
			$this->credit_model->CreateRequest($request, $idBank);
			$body = $this->load->view('credit/request_email', array(
				'title'=>'Заявка на кредит',				
				'params'=>$request,
				'manager_name'=> $bank['manager_name'],
				'current_user' => $currentUser
			), true);
			//die($body);
			$this->mailer->sendEmail($email, "Заявка на кредит", $body, $attach, $images);
			//die($body);
		}
		echo 'Заявка успешно отправлена!<br>Вы можете отслеживать заявки на странице <a href="/credit/ListRequest">"Мои заявки"</a>';
		
		$this->user_log_model->log( "create credit request", 1);
	}

	public function GetProgram(){
		if(!isset($_GET['bank']) || empty($_GET['bank'])){
			die('Банк не указан');
		}
		$data = null;
		if(isset($_GET['program']) && !empty($_GET['program'])){
			$data = $this->credit_model->getCredit($_GET['bank'], $_GET['program']);
		}else{
			$data = $this->credit_model->getCredit($_GET['bank']);
		}
		if($data == null)
			die('null');
		echo json_encode($data);		
	}
	
	public function GetBank(){
		if(!isset($_GET['bank']) || empty($_GET['bank'])){
			die('Банк не указан');
		}
		$data['bank'] = $this->credit_model->getBankById($_GET['bank']);
		$data['bank-insurance-companies'] = $this->credit_model->getBankInsuranceCompanies($_GET['bank']);
		$data['bank-evaluation-companies'] = $this->credit_model->getBankEvaluationCompanies($_GET['bank']);
		$data['bank-income-prooffs'] = $this->credit_model->getBankIncomeProffs($_GET['bank']);
		
		if($data == null)
			die('null');
		echo json_encode($data);		
	}

	public function Search(){
		if(!isset($_POST['banks']) || empty($_POST['banks'])
			|| !isset($_POST['programs']) || empty($_POST['programs'])){
				var_dump($_POST);
			die('Банки\программы не указаны');
		}
		$data = null;
		if(isset($_POST['programs']) && !empty($_POST['programs'])){
			$banks = $_POST['banks'];
			$programs = $_POST['programs'];

			// Все спец
			if(in_array(7,$programs)){
				$programs[] = 16;
				$programs[] = 17;
				$programs[] = 18;
			}
			// Все индивидуальные
			if(in_array(8,$programs)){
				$programs[] = 19;
				$programs[] = 20;
				$programs[] = 21;
			}
			// Все спец партн
			if(in_array(6,$programs)){
				$programs[] = 22;
				$programs[] = 23;
				$programs[] = 24;
			}
			$firstPercentMin = isset($_POST['PercentMin'])? $_POST['PercentMin']:null;
			$firstPercentMax = isset($_POST['PercentMax'])? $_POST['PercentMax']:null;
			$max_percent = isset($_POST['PricePercent'])? $_POST['PricePercent']: null; 
			$programType = null;
			if(!isset($_POST['IsOldFlat']) && !isset($_POST['IsNewFlat']) 
				|| isset($_POST['IsNewFlat']) && $_POST['IsNewFlat'] == "1" && isset($_POST['IsOldFlat']) && $_POST['IsOldFlat'] == "1" ){
				$programType = null;
			}else if(isset($_POST['IsOldFlat']) && $_POST['IsOldFlat'] == "1"){
				$programType = 'old';
			}else if(isset($_POST['IsNewFlat']) && $_POST['IsNewFlat'] == "1" ){
				$programType = 'new';
			}						
			$data = $this->credit_model->getCredits($banks, $programs,$max_percent, $programType, $firstPercentMin, $firstPercentMax);
		}

		if($data == null)
			die('null');
		echo json_encode($data);	
		$this->user_log_model->log( "search credit", 1);	
	}

	public function Description($id){
		if(!isset($id) || empty($id)){
			echo 'не указана программа';
			return;
		}
		$bank_program = $this->credit_model->getCreditById($id);
		if(!isset($bank_program) || empty($bank_program)){
			echo 'Не найдена программа';
			return;
		}
		$bank = $this->credit_model->getBankById($bank_program['id_bank']);
		
		$this->db->select('credit_bank_evaluation_company.*, credit_evaluation_company.name');
		$this->db->from('credit_bank_evaluation_company');
		$this->db->where('id_bank', $bank_program['id_bank']);
		$this->db->join('credit_evaluation_company', 'credit_evaluation_company.id = credit_bank_evaluation_company.id_evaluation_company');
		$evaluation_companies = $this->db->get()->result_array();
		
		$this->db->select('credit_bank_insurance_company.*, credit_insurance_company.name');
		$this->db->from('credit_bank_insurance_company');
		$this->db->where('id_bank', $bank_program['id_bank']);
		$this->db->join('credit_insurance_company', 'credit_insurance_company.id = credit_bank_insurance_company.id_insurance_company');
		$insurance_companies = $this->db->get()->result_array();

		$this->db->select('credit_bank_income_proofs.*, credit_income_proofs.name');
		$this->db->from('credit_bank_income_proofs');
		$this->db->where('id_bank', $bank_program['id_bank']);
		$this->db->join('credit_income_proofs', 'credit_income_proofs.id = credit_bank_income_proofs.id_income_proofs');
		$income_proofs = $this->db->get()->result_array();

		$this->load->view('credit/description', array(
			'bank' => $bank,
			'bank_program' => $bank_program,
			'evaluation_companies' => $evaluation_companies,
			'insurance_companies' => $insurance_companies,
			'income_proofs' => $income_proofs,
		));
	}
	
	public function RequestDescription($id){
		$requests = $this->credit_model->getRequest($id);
		if(!isset($requests) || empty($requests) || count($requests) < 1)
			die('Заявка не найдена');
		$requestData = json_decode($requests[0]['json'], true);
		
		$currentUser = $this->user_model->GetCurrentUser();
		$currentUser['org'] = $this->user_model->GetUserOrg($currentUser['id_org']);

		$this->load->view('credit/request_preview', array(
			'title'=>'Заявка на кредит',				
			'params'=>$requestData,
			'current_user' => $currentUser,
			'isDescription' => true
		));
	}
	public function RequestForm(){
		if(!isset($_GET['name']) || empty($_GET['name']))
			die('не указано название формы');
		$this->load->view('credit/request_create_fields',array('name'=> $_GET['name'], "banks" =>$this->credit_model->getBanks()));
	}
}
