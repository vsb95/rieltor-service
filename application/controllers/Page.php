<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function edit($id){
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /page/show/'.$id, true, 301 );
			die("Вы не авторизованы");
		}
		$user_id = $this->session->userdata('id_user');		
		if(!isset($user_id) || empty($user_id) || $user_id == null){
			header( 'Location: /page/show/'.$id, true, 301 );
			die("Нет прав на редактирование: ".$user_id);
		}


		$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($id));		
		$arr = $query->result();
		if(count($arr) != 1){
			echo "<h1>404</h1>";
			die();
		}
		
		// Если мы НЕ создатель этого отчета или НЕ суперадмин
		if($user_id != $arr[0]->id_user && $user_id != 1){
			$this->user_log_model->log("Page::try edit alien report: ".$id, 5);
			header( 'Location: /page/show/'.$id, true, 301 );
			die("Вы не авторизованы");
		}


		$json=$arr[0]->json;
		$reportDescription = $arr[0]->description;		
		$reportDescription =str_replace('<br/>',"\r\n",$reportDescription);
			
		$result =array("page_id"=>$id,
			"pages"=> json_decode($json, true),
			
			"downloadLink"=> '', 
			"reportDescription"=> $reportDescription, 
			"is_hidden_description"=>$arr[0]->is_hidden_description==1, 
			"isPublic"=>$arr[0]->is_public==1, 
			'createDate'=>$arr[0]->create_date
		);
		$result["is_creator"] = true;
		$result["is_edit"] = true;

		if(isset($arr[0]->id_client) && $arr[0]->id_client != 0){
			$this->load->model('client_model');			
			$client = $this->client_model->get($arr[0]->id_client);
			$result["client"]= $client;				
		}

		$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($arr[0]->id_user));
		$data      = $res->result_array();
		if (count($data) != 0) {
			$user = $data[0];
			$result["user_name"] = $user["full_name"];	
			$result["phone"] = $user["phone"];	
			$result["user_image"] = $user["photo"];	
			if(isset($user["email"]))
				$result["user_email"] = $user["email"];	
			if(isset($user["tagline"]))
				$result["user_tagline"] = $user["tagline"];							

			$res = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($user['id_org']));
			$data = $res->result_array();
			if (count($data) != 0) {
				$org = $data[0];
				$result["logo_image"] = $org["logo_name"];
				$result["org_url"] = $org["url"];
				$result["org_name"] = $org["name"];
				$result["org_email"] = $org["email"];
				$result["org_adress"] = $org["adress"];
				if(isset($org["ya_geo"]))
					$result["org_ya_geo"] = $org["ya_geo"];
			}
		}
		$this->load->view('report/report_edit', $result);		
		$user = $this->user_log_model->log("show edit page: ".$id,2);
	}
	

	public function show($id, $isEdit = null)
	{
		$downloadLink='';
		$user_id = $this->session->userdata('id_user');
		
		if($this->user_model->IsLogined())
		{
			
			$user = $this->user_log_model->log("show page: ".$id,2);
			$downloadLink="/page/download/".$id;
		}else{
			$user_id = null;
		}

		$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($id));		
		$arr = $query->result();
		if(count($arr) != 1){
			echo "<h1>404</h1>";
			die();
		}
		$json=$arr[0]->json;
		$reportDescription = $arr[0]->description;
		
		if($isEdit=="edit")
			$reportDescription =str_replace('<br/>',"\r\n",$reportDescription);
			
		$result =array("page_id"=>$id,
			"pages"=> json_decode($json, true),
			"downloadLink"=> $downloadLink, 
			"reportDescription"=> $reportDescription, 
			"is_hidden_description"=>$arr[0]->is_hidden_description==1, 
			"isPublic"=>$arr[0]->is_public==1, 
			'createDate'=>$arr[0]->create_date
		);

		if(isset($_GET["selected"]) && !empty($_GET["selected"])){
			//var_dump($_GET["selected"]);
			$result["selected"] = $_GET["selected"];
		}
		if(isset($arr[0]->id_client) && $arr[0]->id_client!= 0){
			$this->load->model('client_model');			
			$client = $this->client_model->get($arr[0]->id_client);
			$result["client"]= $client;				
		}
		// Если мы создатель этого отчета или админ
		if($user_id == $arr[0]->id_user || $user_id == 1){
			$result["is_creator"] = true;
			if($isEdit=="edit")
				$result["is_edit"] = true;
		}

		$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($arr[0]->id_user));
		$data      = $res->result_array();
		if (count($data) != 0) {
			$user = $data[0];
			$result["user_name"] = $user["full_name"];	
			$result["phone"] = $user["phone"];	
			$result["user_image"] = $user["photo"];	
			if(isset($user["email"]))
				$result["user_email"] = $user["email"];	
			if(isset($user["tagline"]))
				$result["user_tagline"] = $user["tagline"];							

			$res = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($user['id_org']));
			$data = $res->result_array();
			if (count($data) != 0) {
				$org = $data[0];
				$result["logo_image"] = $org["logo_name"];
				$result["org_url"] = $org["url"];
				$result["org_name"] = $org["name"];
				$result["org_email"] = $org["email"];
				$result["org_adress"] = $org["adress"];
				if(isset($org["ya_geo"]))
					$result["org_ya_geo"] = $org["ya_geo"];
			}
		}

		$this->load->view('report/report',$result);
	}

	public function printing(){

		if(!isset($_GET['type']) || empty($_GET['type'])
			|| !isset($_GET['id']) || empty($_GET['id']))
			die('Страница не найдена');
		$id = $_GET['id'];
		$type = $_GET['type'];
		$user_id = $this->session->userdata('id_user');

		if($this->user_model->IsLogined())
		{
			
			$user = $this->user_log_model->log("print page: ".$id.' type:'.$type,2);
		}else{
			header( 'Location: /page/show/'.$id, true, 301 );
			die("Вы не авторизованы");
		}

		$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($id));		
		$arr = $query->result();
		if(count($arr) != 1){
			echo "<h1>404 Страница не найдена</h1>";
			die();
		}
		
		// Если мы НЕ создатель этого отчета или НЕ суперадмин
		if($user_id != $arr[0]->id_user && $user_id != 1){
			$this->user_log_model->log( "Page::try PRINT alien report: ".$id, 5);
			header( 'Location: /page/show/'.$id, true, 301 );
			die("Вы не авторизованы");
		}

		$json=$arr[0]->json;
		$reportDescription = $arr[0]->description;
		
		$result =array(
			"advertisments"=> json_decode($json, true),
			"reportDescription"=> $reportDescription, 
			"is_hidden_description"=>$arr[0]->is_hidden_description==1,
			'createDate'=>$arr[0]->create_date
		);
		if(!empty($arr[0]->id_client)){
			$this->load->model('client_model');
			$result['client'] = $this->client_model->get($arr[0]->id_client);
		}

		$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($arr[0]->id_user));
		$data      = $res->result_array();
		if (count($data) != 0) {
			$user = $data[0];
			$result["user"] = $user;						

			$res = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($user['id_org']));
			$data = $res->result_array();
			if (count($data) != 0) {
				$org = $data[0];
				$result["org"] = $org;
			}
		}
		switch($type){
			case 'self': $this->load->view('report/report_print_self',$result); return;
			case 'client': $this->load->view('report/report_print_client',$result); return;
			default: 
			echo "<h1>Не понятно кому вы печатаете отчет</h1>";
			die();
		}
	}

	public function ReportOpen($pageid){
		// мб не отправлять письмо, если отчет создан в течении часа?

		if(!isset($pageid) || empty($pageid))
			die("Страница не найдена");
		$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($pageid));		
		$arr = $query->result_array();
		if(count($arr) < 1)
			die("Страница не найдена");
		$page = $arr[0];
		if($page["is_open"] == true || $page["is_open"] == 1 
			|| (!isset($page["is_public"])  || $page["is_public"] == 1 || $page["is_public"] == true))
			die("Отчет уже открыт или он публичный");
		$json=json_decode($page["json"], true);
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 
		$body ="";
		$client_id = $page["id_client"];
		if(isset($client_id) && !empty($client_id) && $client_id != 0){
			$this->load->model('client_model');			
			$client = $this->client_model->get($client_id);
			$body = "Клиент: ".$client["name"]." Тел: ".$client["phone"]."<hr/>";				
		}
		
		$res       = $this->db->query("SELECT * FROM user WHERE id = ?;", array($page["id_user"]));
		$data      = $res->result_array();
		$user = $data[0];
		if(!isset($user) || empty($user) || !isset($user["email"]) || empty($user["email"]))
			die("Некому отправлять");
		$backUrl ='page/show/'.$pageid.'?itsmy=1';
		$email_html = $this->load->view('email',
			array('title'=>'Ваш клиент открыл отчет',
				'user_name'=>$user["full_name"],
				'content'=>$body,
				'back_url'=>$backUrl,
				'buttonText' => 'Посмотреть отчет'
		), TRUE);
		$url = API_SERVER_URL.'api/email';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('EmailTo'=>$user["email"],
			 'Caption'=>'Ваш клиент открыл отчет',
			 'Content'=>$email_html))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		echo $response;
		if($response == '"Письмо успешно отправлено!"'){
			$this->db->set('is_open', true);
			$this->db->where('id', $pageid);
			$this->db->update('page');
		}
	}

	public function SendEmail(){
		$user_email= $this->input->post('user_email');
		$pageid = $this->input->post('pageId');
		$selected_pages = $this->input->post('selected_pages');
		$user_name = $this->input->post('user_name');
		$client_id = $this->input->post('client_id');
		$client_name = $this->input->post('client_name');
		$client_phone = $this->input->post('client_phone');


		
		$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($pageid));		
		$arr = $query->result();
		if(count($arr) < 1)
			die("Страница не найдена");
		
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 
		$body ="";
		if($arr[0]->is_public == 1){
			if(!isset($client_name) || empty($client_name) || !isset($client_phone) || empty($client_phone))
				die("Вы не указали имя и телефон");
				
			$this->load->model('client_model');	
			$client_id = $this->client_model->create($client_name, $client_phone);
			$body = "Клиент: ".$client_name." Тел: ".$client_phone."<hr/>";	
		}
		else if(isset($client_id) && !empty($client_id) && $client_id != 0){
			$this->load->model('client_model');			
			$client = $this->client_model->get($client_id);
			$body = "Клиент: ".$client["name"]." Тел: ".$client["phone"]."<hr/>";				
		}
		
		

		$json=json_decode($arr[0]->json, true);
		//var_dump($json);
		$body .="Выбранные объявления: <br/>";
		$selectedUrls= array();
		foreach($selected_pages as $key => $value){
			$selectedUrls[] = $json[$value]["url"];
			$body.= '<a href="'.$json[$value]["url"].'">'.$json[$value]["adress"]["street"].'</a><br/>';
		}
		$backUrl ='page/show/'.$pageid.'?selected[]='.$selected_pages[0];
		for($i=1; $i< count($selected_pages); $i++){
			$backUrl.='&selected[]='.$selected_pages[$i];
		}
		$email_html = $this->load->view('email',
			array('selected_urls'=>$selectedUrls,
				'title'=>'Ваш клиент выбрал квартиры на просмотр',
				'user_name'=>$user_name,
				'content'=>$body,
				'back_url'=>$backUrl,
				'buttonText' => 'Посмотреть выбранные варианты'
		), TRUE);
		$url = API_SERVER_URL.'api/email';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('EmailTo'=>$user_email,
			 'Caption'=>'Выбраны квартиры на просмотр',
			 'Content'=>$email_html))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		echo $response;

		try{
			$this->load->model('notification_model');
			$this->load->model('user_model');
			$user = $this->user_model->GetUserByLogin($user_email);
			$title = 'Ваш клиент выбрал квартиры на просмотр';
			$content = 'Посмотреть выбранные варианты';
			$response = $this->notification_model->SendMessageToUser($title, $content, $user['id'], SERVER_DOMAIN.$backUrl);
			$this->user_log_model->log('Оповещение о выбранных кв на просморт для юзера: '.$user['id'].' '.json_encode($response), 2);
		}catch(Exception $ex){
			$this->user_log_model->log($ex, 4);
		}
	}

	public function Save(){		
		if(!isset($_POST["pageId"]) || empty($_POST["pageId"])
			|| !isset($_POST["descriptions"]) || empty($_POST["descriptions"]))
			die("Нечего сохранять");
		//var_dump($_POST);
		$pageid = $_POST['pageId'];
		$descriptions = $_POST["descriptions"];
		$streets = $_POST["streets"];
		$costs = $_POST["costs"];
		$costTypes = isset($_POST["cost-types"]) ? $_POST["cost-types"] : array();

		$client_name = $_POST["client_name"];
		$client_phone = $_POST["client_phone"];
		$client_id = $_POST["client_id"];
		$isPublic = $_POST["isPublic"];
		$deletedAdv = isset($_POST["deleted_pages"]) ? $_POST["deleted_pages"]: array();
		$is_hidden_description = isset($_POST["hidden_description"]) && $_POST["hidden_description"] == 1;

		$user_id = $this->session->userdata('id_user');
		if(isset($user_id) && !empty($user_id)){
			
			$user = $this->user_log_model->log("change page: ".$pageid,2);
		}else{
			die("Вы не авторизованы");
		}


		$query = $this->db->query("SELECT * FROM `page` WHERE id = ?", array($pageid));		
		$arr = $query->result();
		if(count($arr) < 1)
			die("Страница не найдена");
		if($user_id != $arr[0]->id_user)
			die("Вы не имеете доступа к редактированию данной страницы");

		$json=json_decode($arr[0]->json, true);

		foreach($descriptions as $key => $value){
			$order   = array("\r\n", "\n", "\r");
			$replace = '<br />';
			// Обрабатывает сначала \r\n для избежания их повторной замены.
			$json[$key]["description"] =  str_replace($order, $replace, $value);
			$json[$key]['is_deleted'] =  0;	// сбрасываем
		}
		foreach($streets as $key => $value){
			$json[$key]['adress']['street'] = $value;	 
			$json[$key]['adress']['house-number'] = null;
		}
		foreach($costs as $key => $value){
			$json[$key]['cost'] = $value;	 
		}
		foreach($costTypes as $key => $value){
			$json[$key]['cost-type'] = $value;	 
		}
		foreach($deletedAdv as $key => $value){
			$json[$value]['is_deleted'] = 1;	 
		}

		$this->load->model('client_model');	

		if(!isset($isPublic) || empty($isPublic) || $isPublic == 0){
			if(!isset($client_id) || empty($client_id)){	
				if(isset($client_name) && !empty($client_name) || isset($client_phone) && !empty($client_phone)){		
					$client_id = $this->client_model->create($client_name, $client_phone);
				}	
			}else{
				$this->client_model->update($client_name, $client_phone);
			}
		}
		
		$reportDescription = $_POST["report_description"];
		$order   = array("\r\n", "\n", "\r");
		$replace = '<br />';
		$reportDescription =  str_replace($order, $replace, $reportDescription);
		
		$this->db->set('is_hidden_description', $is_hidden_description);
		if(isset($isPublic) && !empty($isPublic) && $isPublic == 1){
			$this->db->set('is_public', 1);
			$this->db->set('id_client', NULL);
		}else{
			$this->db->set('is_public', 0);
			$this->db->set('id_client', $client_id);
		}
		$this->db->set('description', $reportDescription);
		$this->db->set('json', json_encode($json));
		$this->db->where('id', $pageid);
		$this->db->update('page');
		echo "Изменения успешно сохранены!";
	}
}
