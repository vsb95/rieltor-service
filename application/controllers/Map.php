<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(555))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}

	public function index($idCity = -1){
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 2); 
		$this->load->view('1_header',array('title'=>'Определение координат микрорайонов', 
			'scripts'=>array('https://api-maps.yandex.ru/2.1/?lang=ru_RU',
			//'/assets/js/util.calculateArea.min.js',
			//'/assets/js/polylabel.min.js',// https://github.com/yandex/mapsapi-polylabeler
		))); 
		if(!$status){
			$this->load->view('shutdown_server', array());	
			$this->load->view('1_footer');
			return;
		}

		$url = API_SERVER_URL.'api/microdistrict?idCity='.$idCity;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		
		$url = API_SERVER_URL.'api/objtypes';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$advTypes = json_decode($response, true);
		//var_dump($json);
		$this->load->view('admin\map_selector', array("districts" => json_encode($json["districts"]),
			 "microdistricts"=>json_encode($json["microdistricts"]), "advTypes" => $advTypes));
		$this->load->view('1_footer');
	}

	public function sendNewZone(){
		if(!isset($_POST['zone']) || empty($_POST['zone']))
			die("Не указано ниодной зоны");
		$this->user_log_model->log( "Map::sendNewZone", 5);	
		$result ='<br/>'; //var_export($_POST['zone'], true).'<br/><br/><br/>';
		foreach($_POST['zone'] as $value){
			$result .= "insert into MicroDistrict (Name, Id_City, GeoCoordinates) values('";
			if(isset($value["name"]) && !empty($value["name"]))
				$result .= $value["name"];
			$result .= "',INSERT_IdCity,'";			
			if(isset($value["coords"]) && !empty($value["coords"]))
				$result .= $value["coords"];
			$result .= "');<br/>";
		}
		$email_html = $this->load->view('email', array('title'=>'Добавление зон','content'=>$result), TRUE);
		$url = API_SERVER_URL.'api/email';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('EmailTo'=>'vsb95@mail.ru',
			 'Caption'=>'Добавление зон',
			 'Content'=>$email_html))
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		echo $response;
	}

	public function getbadgeocoord(){
		$this->user_log_model->log( "Map::getbadgeocoord", 5);	
		$isOnlyWithoutRegion = false;
		if(isset($_POST['isOnlyWithoutRegion']) && !empty($_POST['isOnlyWithoutRegion']))
			$isOnlyWithoutRegion = $_POST['isOnlyWithoutRegion'] == 1;
		$isOnlyWithoutMicroRegion = false;
		if(isset($_POST['isOnlyWithoutMicroRegion']) && !empty($_POST['isOnlyWithoutMicroRegion']))
			$isOnlyWithoutMicroRegion = $_POST['isOnlyWithoutMicroRegion'] == 1;
		$objtypes = isset($_POST['objtypes'])?$_POST['objtypes']:null;
		$url = API_SERVER_URL.'api/Microdistrict';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array('isOnlyWithoutRegion'=>$isOnlyWithoutRegion,'isOnlyWithoutMicroRegion'=>$isOnlyWithoutMicroRegion, 'idCity' => 643, 'ObjTypes'=>$objtypes)) //634 - нск, 643 - омск
		));


		
		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode($response, true);
		//print_r($json);
		if(!isset($json["status-type"]) || $json["status-type"] != "OK"){
			die(json_encode($json));
		}
		echo $response;
	}
}
