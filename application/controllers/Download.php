<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {
	public function index()
	{		
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Войдите в систему, прежде чем скачать');
		}
		if(!isset($_GET["file"]) || empty($_GET["file"])){
			die("404. Страница не найдена");
		}
		$filename = $_GET["file"];
		$filepath='assets/files/'.$filename;
		$user = $this->user_log_model->log( "try download: ".$filename, 6);
		
		if (file_exists($filepath)) {
			//Get file type and set it as Content Type
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			header('Content-Type: ' . finfo_file($finfo, $filepath));
			finfo_close($finfo);
		
			//Use Content-Disposition: attachment to specify the filename
			header('Content-Disposition: attachment; filename='.$filename);
		
			//No cache
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
		
			//Define file size
			header('Content-Length: ' . filesize($filepath));
		
			ob_clean();
			flush();
			readfile($filepath);
			//@unllnk('/'.$filepath);
			exit;
		} else die("404. Страница не найдена");
	}
}
