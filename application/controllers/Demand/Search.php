<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2) && !$this->user_model->IsGranted(555))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		} 
		$this->load->model('demand_model');
	}

	public function index(){
		$data = array(
			'title' => 'Спрос: поиск клиентов'
		);
		
		$this->load->view('1_header_new', $data);
		$this->load->view('demand/search', $data);
		$this->load->view('1_footer_new');		
	}

	public function find(){
		$data = $this->demand_model->search($_POST); 
		echo json_encode($data);
		$this->user_log_model->log( "find demands", 1);	
	}
}
