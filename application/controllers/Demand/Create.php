<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2) && !$this->user_model->IsGranted(555))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		} 
		$this->load->model('demand_model');
	}

	public function index(){
		$data = array(
			'title' => 'Спрос: создать'
		);
		
		$this->load->view('1_header_new', $data);
		$this->load->view('demand/create', $data);
		$this->load->view('1_footer_new');		
	}

	public function new(){		
		if(!isset($_POST) || empty($_POST))
		{
			echo 'ничего не указано';
			return;
		}	
		if(!isset($_POST['objtype']) || empty($_POST['objtype']))
		{
			echo 'Не указан тип недвижимости';
			return;
		}
		if(!isset($_POST['districts']) || empty($_POST['districts']))
		{
			echo 'Не указаны районы';
			return;
		}
		if(!isset($_POST['floortype']) || empty($_POST['floortype']))
		{
			echo 'Не указан этаж';
			return;
		}
		if(!isset($_POST['costMin']) || empty($_POST['costMin'])
			|| !isset($_POST['costMax']) || empty($_POST['costMax'])
			|| $_POST['costMax'] < $_POST['costMin'])
		{
			echo 'Не верно указана цена';
			return;
		}
		if(!isset($_POST['comment']) || empty($_POST['comment']))
		{
			echo 'Вы не указали заметку. Это чревато тем, что вы можете забыть какой это клиент';
			return;
		}

		if(!isset($_POST['sizeMin']) || empty($_POST['sizeMin'])
			|| !isset($_POST['sizeMax']) || empty($_POST['sizeMax'])
			|| $_POST['sizeMax'] < $_POST['sizeMin'])
		{
			echo 'Не верно указана площадь';
			return;
		}
		$this->demand_model->create($_POST); 
		$this->user_log_model->log( "create demand", 1);	
		echo 'Успешно создано!';
	}
}
