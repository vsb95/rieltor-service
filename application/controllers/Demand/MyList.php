<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyList extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(2) && !$this->user_model->IsGranted(555))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		} 
		$this->load->model('demand_model');
	}

	public function index(){
		$data = array(
			'title' => 'Спрос: мои клиенты',
			'list'=> $this->demand_model->getMyDemand(null)
		);
		
		$this->load->view('1_header_new', $data);
		$this->load->view('demand/list', $data);
		$this->load->view('1_footer_new');		
	}
	public function delete($id){
		echo $this->demand_model->delete($id);
	}
	public function find(){
		$data = $this->demand_model->search($_POST, $_SESSION['id_user']); 
		echo json_encode($data);
	}
}
