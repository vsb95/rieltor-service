<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(1) && !$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}

	public function index()
	{
		$id_user = $this->session->userdata('id_user');			
		$this->load->view('1_header_new', array('title' => 'Мои отчеты',
		'help_video'=>'https://www.youtube.com/embed/lZChVCRztlg',
		'help_description'=>'Посмотрите видео, как обновлять отчеты по фильтру или по дате'));
		$this->load->view('reports', array('reports'=>$this->GetReports($id_user)));
		$this->load->view('1_footer_new');		
	}

	private function GetReports($id_user){		
		$res	 = $this->db->query("SELECT page.id, page.description, client.phone as client_phone, client.name as client_name, page.create_date, page.is_public, page.is_open, page.filter_fields 
		FROM page
		LEFT JOIN client
		ON client.id = page.id_client  
		WHERE page.id_user = ?		
		ORDER BY page.create_date DESC
		LIMIT 100", array($id_user));
		$data      = $res->result_array();		
		return $data;
	}

	public function Refresh($idReport){
		$id_user = $this->session->userdata('id_user');
		
		$res  = $this->db->query("SELECT * FROM page WHERE id = ? AND id_user = ?", array($idReport, $id_user));
		$data = $res->result_array();
		
		if(count($data) == 0)
			die('Не найден такой отчет');

		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 

		$advertisments = json_decode($data[0]["json"], true);

		$idAdv = array();
		foreach($advertisments as $key => $value){			
			$idAdv[] = $value['id'];
		}
		//var_dump($idAdv);
		//
		$myCurl = curl_init();		
		$url = API_SERVER_URL.'api/pages';
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => http_build_query(array('IdList'=>$idAdv, 'userid'=>$id_user))
		));		

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		if($json["status-type"] != "OK"){
			die(json_encode($json));
		}
		
		$result = array(
			"total-sec"=>$json["total-sec"],
			"pages"=>$json["pages"]
		);

		foreach($result["pages"] as &$page){			
			$description = strip_tags($page["description"]); // удаляем HTML теги
			$order   = array("\r\n", "\n", "\r");
			// Обрабатывает сначала \r\n для избежания их повторной замены.
			$page["description"] = str_replace($order, '<br />', $description);			
		}
		$data = array(
			'json' => json_encode($result["pages"])
		);
		$this->db->where('id', $idReport);
		$this->db->update('page', $data);
		
		$user = $this->user_log_model->log("refresh page: ".$idReport, 2);

		echo "Отчет успешно обновлен. Проверьте перед отправкой клиенту";
		$this->user_log_model->log( "refresh reports: ".$idReport, 1);	

	}

	public function Recreate($idReport){
		$id_user = $this->session->userdata('id_user');
		
		$this->load->view('1_header_new', array('title' => 'Мои отчеты'));

		$res  = $this->db->query("SELECT * FROM page WHERE id = ? AND id_user = ?", array($idReport, $id_user));
		$data = $res->result_array();
		
		if(count($data) == 0)
			die('Не найден такой отчет');

		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		}
		
		$advertisments = json_decode($data[0]["json"], true);
		$idAdv = array();
		foreach($advertisments as $key => $value){			
			$idAdv[] = $value['id'];
		}
		//echo  http_build_query(array('ExcludeIdList'=>$idAdv, 'ItIsForCreatedReport'=>1,'DateFrom'=>$data[0]["create_date"])).'&'.$data[0]["filter_fields"];
		$myCurl = curl_init();
		if(isset($data[0]["filter_fields"]) && !empty($data[0]["filter_fields"])){
			$url = API_SERVER_URL.'api/search';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => http_build_query(array('ExcludeIdList'=>$idAdv, 
				'ItIsForCreatedReport'=>1,
				'DateFrom'=>$data[0]["create_date"]))
				.'&'.$data[0]["filter_fields"]
			));
		}	
		else{
			die("Отчет не был сформирован через поиск");
		}

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		if($json["status-type"] != "OK"){
			die($json["status"]);
			var_dump($json);
		}
		
		$result = array(
			"total-sec"=>$json["total-sec"],
			"pages"=>$json["pages"]
		);
		if(count($result["pages"]) == 0){
			$msg =  "Нет новых объявлений по выбранному фильтру";			
			$this->load->view('reports', array('reports'=>$this->GetReports($id_user), 'msg'=>$msg));
			$this->load->view('1_footer_new');
			return;
		}

		foreach($result["pages"] as &$page){			
			$description = strip_tags($page["description"]); // удаляем HTML теги
			$order   = array("\r\n", "\n", "\r");
			// Обрабатывает сначала \r\n для избежания их повторной замены.
			$page["description"] = str_replace($order, '<br />', $description);			
		}
		
		$this->load->model('page_model');
		$idView = $this->page_model->create(json_encode($result["pages"]), $data[0]["filter_fields"], 
			$data[0]["id_client"], $data[0]["is_hidden_description"], $data[0]["is_public"]);

		
		$user = $this->user_log_model->log('recreate page: '.$idReport.' => '.$idView, 2);
		$link = SERVER_DOMAIN."page/show/".$idView;
				
		$msg = 'Отчет успешно создан <a href="'.$link.'" target="_blank">'.$link.'</a>. Проверьте перед отправкой клиенту';
		$this->load->view('reports', array('reports'=>$this->GetReports($id_user), 'msg'=>$msg));
		$this->load->view('1_footer_new');
		$this->user_log_model->log( "recreate report: ".$idReport, 1);	
	}
}
