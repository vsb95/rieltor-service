<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminPanel extends CI_Controller {

	public function index(){
		if(!$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		else{			
			$data['title'] =  "Панель администрирования";
			$data['styles'] =  array("/assets/css/admin_panel.css");
			$this->load->view('1_header',$data);			
			$this->load->view('admin\admin_panel');
			$this->load->view('1_footer');
		}
	}
	
	public function reports(){		
		$id_org = $this->session->userdata('id_org');
		if(!$this->user_model->IsLoginedAsAdmin() || !isset($id_org) || empty($id_org))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		$user = $this->user_log_model->log( "open admin reports",2);
		$id_user = $this->session->userdata('id_user');
		$data['title'] =  "Отчеты";
		$data['styles'] =  array("/assets/css/admin_panel.css");
		
		$selectedUserId = -1;
		if(isset($_GET["dtStart"]))
			$dtStart =$_GET["dtStart"];
		if(isset($_GET["dtEnd"]))
			$dtEnd = $_GET["dtEnd"];
		if(isset($_GET["user"]))
			$selectedUserId = $_GET["user"];
		// По дефолту за последнюю неделю
		if(!isset($dtStart)|| empty($dtStart))
			$dtStart =date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")-7,   date("Y")));
		if(!isset($dtEnd) || empty($dtEnd))
			$dtEnd = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")+1,   date("Y")));	

		$sqlQuery="";
		$users = $this->db->query("SELECT user.id, user.login, user.full_name as user_name"
		.($id_user == 1?', organization.name as org_name':'').
		" FROM user
		INNER JOIN organization
		WHERE user.id_org = organization.id".($id_user == 1 ? '' : ' AND  organization.id = ?'),array($id_org))->result_array();
		//superadmin
		if($id_user == 1){
			$sqlQuery ="SELECT page.id, page.create_date, organization.name as org_name, organization.id_city, user.id as user_id, user.login, user.full_name as user_name, page.filter_fields FROM page
			INNER JOIN user, organization
			WHERE page.id_user = user.id AND user.id_org = organization.id AND page.create_date >= ? AND page.create_date <= ? AND (user.id = ? or ? = -1)
			ORDER BY create_date DESC ";
			$res = $this->db->query($sqlQuery,array($dtStart, $dtEnd, $selectedUserId, $selectedUserId));			
		}else{
			$sqlQuery ="SELECT page.id, page.create_date, organization.name as org_name, organization.id_city, user.login, user.full_name as user_name, page.filter_fields FROM page
			INNER JOIN user, organization
			WHERE page.id_user = user.id AND user.id_org = organization.id AND page.create_date >= ? AND page.create_date <= ? AND user.id_org = ? AND (user.id = ? or ? = -1)
			ORDER BY create_date DESC ";				
			$res = $this->db->query($sqlQuery,array($dtStart, $dtEnd, $id_org, $selectedUserId, $selectedUserId));
		}
		$pages = $res->result_array();		

		$this->load->view('1_header',$data);			
		$this->load->view('admin\admin_panel_reports',array("pages"=>$pages, 'users'=>$users, 'selectedUserId' =>$selectedUserId, "cities" => $this->GetCities()));
		$this->load->view('1_footer');
	}
	
	public function Statistic(){		
		$id_org = $this->session->userdata('id_org');
		if(!$this->user_model->IsLoginedAsAdmin() || !isset($id_org) || empty($id_org))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		$user = $this->user_log_model->log( "open admin statistic",2);
		$id_user = $this->session->userdata('id_user');

		$data['title'] =  "Статистика";
		$data['styles'] =  array("/assets/css/admin_panel.css");
		
		if(isset($_GET["dtStart"]))
			$dtStart =$_GET["dtStart"];
		if(isset($_GET["dtEnd"]))
			$dtEnd = $_GET["dtEnd"];
		// По дефолту за последнюю неделю
		if(!isset($dtStart)|| empty($dtStart))
			$dtStart =date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")-7,   date("Y")));
		if(!isset($dtEnd) || empty($dtEnd))
			$dtEnd = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")+1,   date("Y")));	
			
		$sqlQuery="";
		//superadmin
		if($id_user == 1){
			$sqlQuery ="SELECT user.login, user.full_name as user_name, sub_table.*
			FROM user
			LEFT JOIN 
			(
				SELECT page.id_user, count(page.id) as created_count, SUM(page.is_open = 1) as opened_count 
				FROM page
				WHERE page.create_date >= ? AND page.create_date <= ? 
				GROUP BY page.id_user
			) as sub_table
			ON user.id  = sub_table.id_user 
			WHERE user.is_deleted = 0
			ORDER BY user_name DESC";
			$res = $this->db->query($sqlQuery,array($dtStart, $dtEnd));
		}else{
			$sqlQuery ="SELECT user.login, user.full_name as user_name, sub_table.*
			FROM user
			LEFT JOIN 
			(
				SELECT page.id_user, count(page.id) as created_count, SUM(page.is_open = 1) as opened_count 
				FROM page
				WHERE page.create_date >= ? AND page.create_date <= ? 
				GROUP BY page.id_user
			) as sub_table
			ON user.id  = sub_table.id_user 
			WHERE user.is_deleted = 0 AND user.id_org = ? 
			ORDER BY user_name DESC
			";
			$res = $this->db->query($sqlQuery,array($dtStart, $dtEnd, $id_org));
		}
		$rows = $res->result_array();		

		$this->load->view('1_header',$data);			
		$this->load->view('admin\admin_panel_statistic',array("rows"=>$rows));
		$this->load->view('1_footer');
	}

	
	private function GetCities(){
		$cities = array();
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if ($status) { 
			// Дернем API
			$url = API_SERVER_URL.'api/cities';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
			));

			$response = curl_exec($myCurl);
			curl_close($myCurl);
			$json = json_decode(json_decode($response, true), true);
			if($json["status-type"] != "OK"){
				die(json_encode($json));
			}
			$cities = $json["cities"];
		}
		return $cities;
	}
}
