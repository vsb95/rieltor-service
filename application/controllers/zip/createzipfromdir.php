<?php
class createDirZip extends createZip {

  function get_files_from_folder($directory, $put_into) {
    if ($handle = opendir($directory)) {
      while (false !== ($file = readdir($handle))) {
       // echo "<br/>1: directory.file: ".$directory.$file;
        //echo "<br/>2: put_into.file: ".$put_into.$file;
        if (is_file($directory.$file)) {
          $fileContents = file_get_contents($directory.$file);
          //echo "<br/>\t 2: fileContents: ".(isset($fileContents) && !empty($fileContents));
          $this->addFile($fileContents, ltrim($put_into.$file, '/'));//$put_into.$file);
        } elseif ($file != '.' and $file != '..' and is_dir($directory.$file)) {
          //echo "<br/>\t 1.1: it is directory ";
          $this->addDirectory($put_into.$file.'/');
          $this->get_files_from_folder($directory.$file.'/', $put_into.$file.'/');
        }
      }
    }
    else{
      echo "err dir: ".$directory;
    }
    closedir($handle);
  }
}
?>
