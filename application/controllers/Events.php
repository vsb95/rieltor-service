<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {	
	public function index()
	{					
		if(!$this->user_model->IsLogined()){			
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die;
		}else{
			$data['title'] =  "Мероприятия";
			$this->load->view('1_header_new',$data);
			$this->load->view('event_calend',$data);
			$this->load->view('1_footer_new',$data);
		}
	}
}
