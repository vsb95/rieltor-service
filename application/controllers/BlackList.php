<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BlackList extends CI_Controller {	
	function __construct() {
		parent::__construct();
        if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
	}

	public function index(){			
		$data = array(
			'title' => 'Черный список',
			'styles' => array("/assets/css/modal.css", '/assets/css/image_upload_box.css',"/assets/css/fotorama.css"),
			'scripts' =>array('/assets/js/jquery-ui.js','/assets/js/image_upload_box.js',"/assets/js/fotorama.js"),
		);
		
		$this->load->view('1_header_new', $data);
		$this->load->view('blackList', $data);
		$this->load->view('1_footer_new');	
	}
	
	public function Search(){
		if(!isset($_GET['type']) || empty($_GET['type']))
			$_GET['type'] = '';
		$this->load->model('blacklist_model');
		$data = $this->blacklist_model->search($_GET['type']);
		echo json_encode($data);		
		$this->user_log_model->log( "search blacklist", 1);
	}

	public function Create(){
		$request = $_POST;
		if(isset($request['images']) && !empty($request['images'])){
			// Вертаем изображения
			$images = array();
			$this->load->model('image_model');
			foreach($request['images'] as $image){
				$path = $image['src'];
				if($image['angle'] > 0){
					// если у изображения есть уже ссылка на домен - значит она не в tmp папке
					$pos = strpos($image['src'], SERVER_DOMAIN);
					if($pos !== false){
						$path = str_replace(SERVER_DOMAIN, '',$image['src']);
						$path = ltrim($path, '/');
					}
					$newPath = ($pos !== false? SERVER_DOMAIN :'').ltrim($this->image_model->Rotate($path, $image['angle']), '/');
					if(isset($newPath) && !empty($newPath)){
						$image['src'] = $newPath;
					}					
				}
				$newPath = str_replace('/tmp/','/black_list/',$image['src']);
				//echo $image['src'].' => '.$newPath.'<br>';
				if(rename($image['src'], $newPath))
					$images[] = '/'.$newPath; 
				else{
					echo 'Не удалось добавить фото: <img src="/'.$image['src'].'" style="max-width:150px;"/>';
				}
			}
			
			$request['images'] = $images;
		}
		$this->load->model('blacklist_model');
		$this->blacklist_model->create($request);
		$this->user_log_model->log( "create blacklist", 2);
		echo 'Ваше сообщение успешно добавлено!';			
	}

	// Показать картинки
	public function Show(){
		if(!isset($_GET['id']) || empty($_GET['id']))
			die('Не выбрана запись');
		
		$this->load->model('blacklist_model');
		$images = $this->blacklist_model->getImages($_GET['id']);
		echo json_encode($images);
	}
	public function Delete(){
		if(!isset($_GET['id']) || empty($_GET['id']))
			die('Не выбрана запись');
		
		$this->load->model('blacklist_model');
		$this->blacklist_model->delete($_GET['id']);
		$this->user_log_model->log( "delete blacklist", 2);
		echo 'OK';		
	}
}
