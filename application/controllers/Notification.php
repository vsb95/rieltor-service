<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model('notification_model');
	}

	public function LinkUser(){		
        if(!$this->user_model->IsLogined()){
			die('Вы не авторизованы');
		}		
		if(!isset($_GET) || empty($_GET)
			|| !isset($_GET['onesignal_id']) || empty($_GET['onesignal_id']))
			die('Ничего не указано');

		if($this->notification_model->LinkOnesignalUser($_SESSION['id_user'], $_GET['onesignal_id'])){
			$this->user_log_model->log('Привязан пользователь '.$_SESSION['id_user'].' -> '.$_GET['onesignal_id'], 2);
			echo 'linkuser done';
		}else{
			$this->user_log_model->log('Не привязан пользователь '.$_SESSION['id_user'].' -> '.$_GET['onesignal_id'], 6);
			die('не удалось привязать');
		}
	}
	public function Send(){		
		$title = 'Заголовок';
		$content= 'куча разнообразного текста для проверки данного пуша';
		$segments = array('Test');
		if(isset($_POST['title']))
			$title = $_POST['title'];
		if(isset($_POST['content']))
			$content = $_POST['content'];
		if(isset($_POST['segments']))
			$segments = $_POST['segments'];
		
		$response = $this->notification_model->SendMessageToSegment($title, $content, $segments);
		$return = json_decode($response, true);
		var_dump($return);
		echo json_encode($return);		
	}
	
	public function SendUser(){		
		$title = 'Заголовок 2';
		$content= 'куча 234 разнообразного текста для проверки данного пуша';
		$userId = 1;
		if(isset($_POST['title']))
			$title = $_POST['title'];
		if(isset($_POST['content']))
			$content = $_POST['content'];
		if(isset($_POST['id_user']))
			$userId = $_POST['id_user'];
		
		$response = $this->notification_model->SendMessageToUser($title, $content, $userId);
		$return = json_decode($response, true);
		var_dump($return);
		echo json_encode($return);		
	}

	
}
