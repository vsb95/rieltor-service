<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Board extends CI_Controller {

	function __construct() {
		parent::__construct();
        if($_SERVER['REQUEST_URI'] != '/board/search' && !$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}		
		if(!$this->user_model->IsGranted(1) && !$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
	}
	
	public function index(){
		$data['title'] =  "Доска объявлений";
		$data['scripts'] =  array("/assets/js/fotorama.js","/assets/js/district_select.js",'https://api-maps.yandex.ru/2.1/?lang=ru_RU');
		$data['styles'] =  array("/assets/css/fotorama.css","/assets/css/board.css");
		$data["help_video"] = "https://www.youtube.com/embed/-4egA2HH34g";
		$data["help_description"] = "Посмотрите видео, как формировать отчет из поиска";
		$this->load->view('1_header_new',$data);
		$initData = $this->LoadInitData();
		if(isset($initData) && !empty($initData)){
			$initData["districts"] = json_encode($initData["districts"]);
			$initData["micro-districts"] = json_encode($initData["micro-districts"]);
			$initData["id_city"] = $this->session->userdata('id_city');
		}
		$this->load->view('board', array("initData"=>$initData));
		$this->load->view('1_footer_new',$data);
		$this->user_log_model->log( "open board", 1);		
	}
	
	public function Search(){
		if(!$this->user_model->IsLogined()){
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Вы не авторизованы')));
		}
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен. Повторите еще раз')));
		} 
		$id_user = $this->session->userdata('id_user');
		$post = $_POST;
		$post["UserId"] =$id_user;
		//$post["DEBUG"] =var_export($_POST,true);
		//var_dump($post);
		// Дернем API
		$url = API_SERVER_URL.'api/search';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($post)
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		$this->load->model('favorites_model');
		if(isset($json["pages"]) && !empty($json["pages"])){
			foreach($json["pages"] as $page){
				// Проверим, есть ли в избранном	
				if($this->favorites_model->IsFavorite($page['id']))
					$advertisement['is_liked'] = 1;			
				//
				//
				$json["html-pages"][]= $this->load->view('board_list_item', array("page"=>$page), true);
			}			
		}
		echo json_encode($json);
		$this->user_log_model->log( "search on board", 1);
	}
	
	public function Details($id){
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен. Повторите еще раз')));
		} 
		
		// Дернем API
		$id_user = $this->session->userdata('id_user');
		$url = API_SERVER_URL.'api/search?id='.$id.'&userId='.$id_user;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);

		$data['title'] =  "Объявление";
		$data['scripts'] =  array("/assets/js/fotorama.js");
		$data['styles'] =  array("/assets/css/fotorama.css");
		$this->load->view('1_header_new', $data);
		if($json["status-type"] == "OK"){
			$advertisement = $json["page"];
			
			$data['title'] =  $advertisement["adress"]["street"];
			$data['id_page'] = $id;
			//
			$this->db->select('adv_comments.comment, adv_comments.date, user.full_name as user_name, user.email as user_email');
			$this->db->from('adv_comments');			
			$this->db->where('id_page', $id);		
			$this->db->where('(is_mls IS NULL OR is_mls = 0)', NULL);		
			if(!$this->user_model->IsLoginedAsSuperAdmin())
				$this->db->where('adv_comments.id_org', $_SESSION['id_org']);
			$this->db->join('user', 'user.id = adv_comments.id_user');
			$this->db->order_by('date', 'ASC');
			$data['comments']= $this->db->get()->result_array();
			
			// Проверим, есть ли в избранном	
			$this->load->model('favorites_model');
			if($this->favorites_model->IsFavorite($id))
				$advertisement['is_liked'] = 1;			
			//
			$data['advertisement'] = $advertisement;
			$data['mergedPages'] = $json["merged-pages"];
			$this->load->view('board_details', $data);
		}else{
			$this->load->view('board_details', array("result"=>$json));
		}
		$this->load->view('1_footer_new');
		$this->user_log_model->log( "open details board: ".$id, 1);
		//echo json_encode($json);
		//return $json;
	}
	
	private function LoadInitData(){
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			return null;
			//die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен. Повторите еще раз')));
		} 
		// Дернем API
		$idCity = $this->session->userdata('id_city');
		$url = API_SERVER_URL.'api/search?idCity='.$idCity;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		if(is_array($response))
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Не найден путь api/search')));
		$json = json_decode(json_decode($response, true), true);
		return $json;
	}

	public function Report(){
		set_time_limit(900);
		$id_user = $this->session->userdata('id_user');
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die(json_encode(array('status-type'=>"ERROR", 'status'=>'Сервер не доступен. Повторите еще раз')));
		}
		if(!isset($_POST["selected_adv"]) || empty($_POST["selected_adv"]))
			die(json_encode(array('status-type'=>"EMPTY", 'status'=>'Объявления не выбраны')));
		//$post["IdList"] = $_POST["selected_adv"];
		$isCutLogo = 0;
		if(isset($_POST["isCutLogo"]) && !empty($_POST["isCutLogo"]) && $_POST["isCutLogo"] == 1)
			$isCutLogo = 1;
		// Дернем API
		$url = API_SERVER_URL.'api/pages';
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => http_build_query(array('IdList'=>$_POST["selected_adv"], 
			'userid'=>$id_user, 'isCutLogo' => $isCutLogo))
		));
		
		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		if($json["status-type"] != "OK"){
			die(json_encode($json));
		}
		$result = array(
			"total-sec"=>$json["total-sec"],
			"pages"=>$json["pages"],
			"unparsed-urls"=>$json["unparsed-urls"],
			"dbg"=>$isCutLogo
		);

		foreach($result["pages"] as &$page){			
			$description = strip_tags($page["description"]); // удаляем HTML теги
			$order   = array("\r\n", "\n", "\r");
			// Обрабатывает сначала \r\n для избежания их повторной замены.
			$page["description"] = str_replace($order, '<br />', $description);			
		}
		
		$filterfields = isset($_POST["searchRequest"]) && !empty($_POST["searchRequest"]) ?$_POST["searchRequest"].'&UserId='.$id_user: null;
		if($isCutLogo == 1 && !empty($filterfields)){
			$filterfields .= '&isCutLogo='.$_POST["isCutLogo"];
		}
		
		$this->load->model('page_model');
		$Id = $this->page_model->create(json_encode($result["pages"]), $filterfields);
		echo json_encode(array('status-type'=>"OK", 'link'=>SERVER_DOMAIN.'page/show/'.$Id));
		
		$user = $this->user_log_model->log( "create report by search: ".$Id,2);
		//var_dump($json);
		//return $json;
	}	

	public function Sendalarm(){
		$this->load->model('mailer');
		$this->load->model('user_model');
		$user = $this->user_log_model->log("send alarm page",2);
		$user = $this->user_model->GetCurrentUser();
		
        $res       = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($user["id_org"]));
		$data      = $res->result_array();
		if(count($data) == 0)
			$org = 'Неизвестная организация!!!';
		else
			$org = $data[0]["name"];

		echo $this->mailer->sendEmail("rieltorservice@ya.ru", "Ошибка в объявлении!", 'Вот тут вот кривое объявление: <a href="'.SERVER_DOMAIN.'board/details/'.$_POST["id"].'">Страница у нас</a> <br/><a href="'.$_POST["url"].'">'.$_POST["url"].'</a> <br/> <br/>Отправлено: '
		.($user == null?"неизвестным пользователем = ".$this->session->userdata('id_user'):$user["full_name"].' ('.$user["id"].')<br>'.$user["email"].'<br>Из огранизации: '.$org.'<br>Описание:<br>'.$_POST["description"]));
	}

	public function Comment(){
		//var_dump($_POST);
		if(!isset($_POST['comment']) || empty($_POST['comment']))
		{
			echo 'Ошибка: не указан текст комментария';
			die;
		}
		if(!isset($_POST['id_page']) || empty($_POST['id_page']))
		{
			echo 'Ошибка: не указана страница';
			die;
		}
		$data = array(
			'id_page' => $_POST['id_page'],
			'id_org' =>  $_SESSION['id_org'],	
			'id_user' =>  $_SESSION['id_user'],
			'comment' =>  str_replace("\n","<br>",$_POST['comment'])
		);
		if(isset($_POST['is_mls']) && !empty($_POST['is_mls']) && $_POST['is_mls'] == "1")
			$data['is_mls'] = 1;
		$this->db->insert('adv_comments', $data);
		echo 'done';
	}
}
