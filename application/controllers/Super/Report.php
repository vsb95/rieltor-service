<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsGranted(555) && !$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /', true, 301 );
			die('Вы не авторизованы');
		}
	}
	
	public function countOfferPerOrg(){
		$data['title'] =  "Отчет: Количество активных объявлений по организациям";
		$data['styles'] =  array("/assets/css/admin_panel.css");
		$this->load->view('1_header_new',$data);			
		$this->load->view('admin\reports\super_count_offer_per_org');
		$this->load->view('1_footer_new');	
	}

	public function Get(){
		if(!isset($_POST['type']) || empty($_POST['type']))
			die('не указан тип отчета');
		$url = API_SERVER_URL.'api/report?type='.$_POST['type'];
		if(isset($_POST['dtStart']) && !empty($_POST['dtStart']))
			$url.='&beginDate='.$_POST['dtStart'];
		if(isset($_POST['dtEnd']) && !empty($_POST['dtEnd']))
			$url.='&endDate='.$_POST['dtEnd'];
		// Дернем API
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
		));

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode($response, true);
		if($json['status-type'] != "OK"){
			die($response);
		}
		if(isset($json['rows']) && count($json['rows']) > 0 && isset($json['rows'][0]['org-id'])){
			foreach($json['rows'] as &$item){
				$item['org'] = $this->user_model->GetUserOrg($item['org-id']);
			}
			echo json_encode($json);
		}		
	}
}
