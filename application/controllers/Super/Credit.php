<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Credit extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsLoginedAsSuperAdmin() && !$this->user_model->IsGranted(555))
		{
			header( 'Location: /', true, 301 );
			die('Вы не авторизованы');
		}
	}

	public function HashingBank(){
		$data = array('title'=>'Супер: ссылки для Банков');
		echo$this->load->view('1_header_new', $data, true);
		$this->load->model('credit_model');
		$banks = $this->credit_model->getBanks();

		echo '<table class="table table-striped table-hover table-dark" style="background-color:white"><thead><tr><th scope="col">Банк</th><th scope="col">Хэш</th><th scope="col">Публичная сылка на редактирование</th></tr></thead><tbody id="tbody">';
		foreach($banks as $bank){
			if(isset($bank['hash']) && !empty($bank['hash'])) {
				echo '<tr><td>'.$bank['name'].'</td><td>'.$bank['hash'].'</td><td><a target="_blank" href="'.SERVER_DOMAIN.'creditpublic/edit/'.$bank['hash'].'">'.SERVER_DOMAIN.'creditpublic/edit/'.$bank['hash'].'</a></td></tr>';
				continue;
			}

			$hash = hash('md5', $bank['name'].'хлорид натрия');
			$bank['hash'] = $hash;
			echo '<tr><td>'.$bank['name'].'</td><td>new '.$bank['hash'].'</td><td><a target="_blank" href="'.SERVER_DOMAIN.'creditpublic/edit/'.$bank['hash'].'">'.SERVER_DOMAIN.'creditpublic/edit/'.$bank['hash'].'</a></td></tr>';
			
			$this->db->set('hash', $hash);
			$this->db->where('id', $bank["id"]);
			$this->db->update('credit_bank');
		}

		$user = $this->user_log_model->log( "HashingBank", 3);
		echo '</tbody></table>';
		echo $this->load->view('1_footer_new',array(), true);		
	}
}
