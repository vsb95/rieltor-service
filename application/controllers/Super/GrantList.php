<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GrantList extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
	}

	public function index(){
		$user = $this->user_log_model->log( "open SUPER user grant list", 3);
		$data['title'] =  "Список прав пользователей";
		$data['styles'] =  array("/assets/css/admin_panel.css");

		$idOrg = isset($_GET["id_org"]) && !empty($_GET["id_org"]) ?$_GET["id_org"]:null;

		$res = $this->db->query("SELECT user.id, user.full_name, user.is_admin, user.email, user.is_activate, user.is_deleted, user.id_grant, organization.name as org_name FROM user
		 INNER JOIN organization ON user.id_org = organization.id "
		 .(isset($idOrg)?" WHERE organization.id = ?":"")
		 ."ORDER BY org_name, is_deleted, is_activate DESC, email"
		,array($idOrg));
		$users = $res->result_array();
		
        $res = $this->db->query("SELECT * FROM organization ORDER BY id",array());
		$organizations = $res->result_array();
		
        $res = $this->db->query("SELECT * FROM grants ORDER BY id",array());
        $grants = $res->result_array();

		$this->load->view('1_header_new',$data);			
		$this->load->view('admin\admin_panel_grantlist', array("organizations"=>$organizations, "users" => $users, "grants" => $grants));
		$this->load->view('1_footer_new');
	}
	
	public function updateGrant(){
		if(!isset($_POST["id_user"]) || empty($_POST["id_user"]))
			die("не указан пользователь");
		if(!isset($_POST["id_grant"]) || empty($_POST["id_grant"]) && $_POST["id_grant"] != 0)
			die("не указан новый грант");

		$id_user = $_POST["id_user"];
		$id_grant = $_POST["id_grant"];
		$this->load->model('randomizer');

		$this->db->set('id_grant', $id_grant);
		$this->db->set('token', $this->randomizer->generateRandomString());
		$this->db->where('id', $id_user);
		$this->db->update('user');

		$user = $this->user_log_model->log( "SUPER update grant for user: ".$id_user, 3);
		echo "done: user ($id_user) => grant ($id_grant)";
	}

}
