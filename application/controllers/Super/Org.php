<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Org extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
	}

	public function index(){
		$user = $this->user_log_model->log( "open SUPER org list", 3);
		$data['title'] =  "Список организаций";
		$data['styles'] =  array("/assets/css/admin_panel.css");

		
        $res = $this->db->query("SELECT organization.id, organization.*, users.user_count 
		FROM organization
		LEFT JOIN  (
            SELECT  count(user.id) as user_count, user.id_org 
			FROM user            
			Where user.is_activate = 1 AND user.is_deleted = 0
			GROUP BY user.id_org
        )  users 
		ON users.id_org = organization.id 
        order BY organization.reg_date ",array());
        $organizations = $res->result_array();

		$this->load->view('1_header_new',$data);			
		$this->load->view('admin\admin_panel_orglist', array("organizations"=>$organizations, "cities" =>  $this->GetCities(0)));
		$this->load->view('1_footer_new');
	}

	public function updateGrantCount(){		
		if(!isset($_POST["id_org"]) || empty($_POST["id_org"]))
			die("не указана организация");
		if(!isset($_POST["count_search"]) || empty($_POST["count_search"]) && $_POST["count_search"] != 0)
			die("не указано колво 'Поиск'");
		if(!isset($_POST["count_full_search"]) || empty($_POST["count_full_search"])&& $_POST["count_full_search"] != 0)
			die("не указано колво 'Поиск+'");

		$this->db->set('count_grant_search', $_POST["count_search"]);
		$this->db->set('count_grant_full_search', $_POST["count_full_search"]);
		$this->db->where('id', $_POST["id_org"]);
		$this->db->update('organization');

		$user = $this->user_log_model->log( "update SUPER org (".$_POST["id_org"].") grant count", 3);
		echo 'done: organization => count_grant_search: '.$_POST["count_search"].' count_grant_full_search: '.$_POST["count_full_search"];
	}

	public function GrantList(){
		$user = $this->user_log_model->log( "open SUPER user grant list", 3);
		$data['title'] =  "Список прав пользователей";
		$data['styles'] =  array("/assets/css/admin_panel.css");

		$idOrg = isset($_GET["id_org"]) && !empty($_GET["id_org"]) ?$_GET["id_org"]:null;

		$res = $this->db->query("SELECT user.id, user.full_name, user.is_admin, user.email, user.is_activate, user.is_deleted, user.id_grant, organization.name as org_name FROM user
		 INNER JOIN organization ON user.id_org = organization.id "
		 .(isset($idOrg)?" WHERE organization.id = ?":"")
		 ."ORDER BY org_name, is_deleted, is_activate DESC, email"
		,array($idOrg));
		$users = $res->result_array();
		
        $res = $this->db->query("SELECT * FROM organization ORDER BY id",array());
		$organizations = $res->result_array();
		
        $res = $this->db->query("SELECT * FROM grants ORDER BY id",array());
        $grants = $res->result_array();

		$this->load->view('1_header_new',$data);			
		$this->load->view('admin\admin_panel_grantlist', array("organizations"=>$organizations, "users" => $users, "grants" => $grants));
		$this->load->view('1_footer_new');
	}
	
	public function updateGrant(){
		if(!isset($_POST["id_user"]) || empty($_POST["id_user"]))
			die("не указан пользователь");
		if(!isset($_POST["id_grant"]) || empty($_POST["id_grant"]) && $_POST["id_grant"] != 0)
			die("не указан новый грант");

		$id_user = $_POST["id_user"];
		$id_grant = $_POST["id_grant"];
		$this->load->model('randomizer');

		$this->db->set('id_grant', $id_grant);
		$this->db->set('token', $this->randomizer->generateRandomString());
		$this->db->where('id', $id_user);
		$this->db->update('user');

		$user = $this->user_log_model->log( "SUPER update grant for user: ".$id_user, 3);
		echo "done: user ($id_user) => grant ($id_grant)";
	}

	public function add(){
		$user = $this->user_log_model->log( "open SUPER add org", 3);
		$data['title'] =  "Добавление организации";
		$data['styles'] =  array("/assets/css/admin_panel.css");
		$this->load->view('1_header_new',$data);			
		$this->load->view('admin\admin_panel_addorg');
		$this->load->view('1_footer_new');
	}
	
	public function add_org(){
		$this->load->model('user_model');
		if(!$this->user_model->IsEmptyLogin($this->input->post('admin_user_login'))){			
			$html = $this->load->view('1_header_new',array('title'=>"Добавление организации"),true) . "<h2>Логин занят!</h2>";			
			echo $html;
			$this->load->view('1_footer_new');	
			return;
		}

		$admin_user["login"] = $this->input->post('admin_user_login');
		$org["name"] = htmlspecialchars($this->input->post('org_name'));	
		if(!isset($org["name"]) || empty($org["name"]))
			die('Название организации не указано');
		if(!isset($admin_user["login"]) || empty($admin_user["login"]))
			die('Логин не указан');
		if(isset($_POST["isTest"]) && !empty($_POST["isTest"]))
			$org["is_test"] = $_POST["isTest"];
		$org["id_city"] = $this->input->post('id_city');
        $this->db->insert('organization', $org);
		$orgid= $this->db->insert_id();

		$admin_user["id_org"]  = $orgid;  
		$admin_user["is_admin"]  = 1;
		$admin_user["password"]  = hash('md5', $this->input->post('admin_user_password'));
		if(strpos($admin_user["login"], '@') !== false){
			$admin_user["email"] = $admin_user["login"];
		}

		$this->load->model('user_model');
		$this->user_model->addNew($admin_user["login"], $admin_user["password"], $admin_user["id_org"], $admin_user["is_admin"]);
		
		$this->user_log_model->log( "create org: '".$org["name"]." (id = '". $orgid."')", 3);

		$html = $this->load->view('1_header_new',array('title'=>"Добавление организации"),true)
		. '<div class="row" style="background: white; padding:10px;">'
		. "<h2>Done!</h2><br/>"
		. "Зарегистрирована организация '".$org["name"]." (id = '". $orgid."')'<br/>"
		. 'Login: '.$admin_user["login"].'<br/>'
		. 'Password: '.$this->input->post('admin_user_password').'<br/>'
		. 'City: '.$org["id_city"].'<br/>'
		.'</div>';
			
		echo $html;
		$this->load->view('1_footer_new');	
	}
	
	public function GetCities($isNeedPrint = 1){
		$cities = array();
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if ($status) { 
			// Дернем API
			$url = API_SERVER_URL.'api/cities';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
			));

			$response = curl_exec($myCurl);
			curl_close($myCurl);
			$json = json_decode(json_decode($response, true), true);
			if($json["status-type"] != "OK"){
				die(json_encode($json));
			}
			$cities = $json["cities"];
			if($isNeedPrint == 1)
				echo json_encode($cities);
		}
		else
			echo 'API не доступен';
		return $cities;
	}
}
