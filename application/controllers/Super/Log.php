<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
	}
	
	public function index(){
		$user = $this->user_log_model->log( "open SUPER log", 3);
		//
		$data['title'] =  "Лог";
		$data['styles'] =  array("/assets/css/admin_panel.css");	

        $res = $this->db->query("SELECT * FROM organization ORDER BY id",array());
		$organizations = $res->result_array();

		$this->load->view('1_header_new',$data);			
		$this->load->view('admin\admin_panel_log', array("organizations"=>$organizations, "logs"=>array()));
		$this->load->view('1_footer_new');		
	}

	public function Get(){
		if(isset($_GET["dtStart"]) && !empty($_GET["dtStart"]))
			$dtStart =$_GET["dtStart"];
		if(isset($_GET["dtEnd"]) && !empty($_GET["dtEnd"]))
			$dtEnd = $_GET["dtEnd"];
		if(isset($_GET["id_city"]) && !empty($_GET["id_city"]))
			$id_city = $_GET["id_city"];
		else
			$id_city = 643;
		// По дефолту за последнюю неделю
		if(!isset($dtStart)|| empty($dtStart))
			$dtStart =date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")-7,   date("Y")));
		if(!isset($dtEnd) || empty($dtEnd))
			$dtEnd = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")+2,   date("Y")));	
	
		$this->db->select('user_log.action, user_log.date, log_type.name as type_name, organization.name as org_name, organization.id_city, user.id as user_id, user.login, user.full_name as user_name');
		$this->db->from('user_log');
		if(isset($dtStart))
			$this->db->where('user_log.date >=', $dtStart);
		if(isset($dtEnd))
			$this->db->where('user_log.date <=', $dtEnd);
		if(isset($_GET["id_type"])&& !empty($_GET["id_type"]))
			$this->db->where('user_log.id_type', $_GET['id_type']);
		if(isset($id_city))
			$this->db->where('organization.id_city', $id_city);
		if(isset($_GET["id_org"]) && !empty($_GET["id_org"]))
			$this->db->where('organization.id', $_GET["id_org"]);
		$this->db->join('log_type', 'log_type.id = user_log.id_type');
		$this->db->join('user', 'user.id = user_log.id_user');
		$this->db->join('organization', 'organization.id = user.id_org');
		$this->db->order_by('date', 'DESC');
		$this->db->limit(100);
		$logs = $this->db->get()->result_array();

		echo json_encode($logs);
	}

	public function Statistic(){
		if(isset($_GET["dtStart"]) && !empty($_GET["dtStart"]))
			$dtStart =$_GET["dtStart"];
		if(isset($_GET["dtEnd"]) && !empty($_GET["dtEnd"]))
			$dtEnd = $_GET["dtEnd"];
		if(isset($_GET["id_city"]) && !empty($_GET["id_city"]))
			$id_city = $_GET["id_city"];
		else
			$id_city = 643;
		// По дефолту за последнюю неделю
		if(!isset($dtStart)|| empty($dtStart))
			$dtStart =date("Y-m-d H:i:s", mktime(0, 0, 0, date("m")-1, date("d")-7,   date("Y")));
		if(!isset($dtEnd) || empty($dtEnd))
			$dtEnd = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")+2,   date("Y")));	
	
		$this->db->select('organization.name as org_name, user.full_name as user_name, user.login, log_type.name as type_name, user_log.action, user_log.date');
		$this->db->from('user_log');
		if(isset($dtStart))
			$this->db->where('user_log.date >=', $dtStart);
		if(isset($dtEnd))
			$this->db->where('user_log.date <=', $dtEnd);
		if(isset($_GET["id_type"])&& !empty($_GET["id_type"]))
			$this->db->where('user_log.id_type', $_GET['id_type']);
		if(isset($id_city))
			$this->db->where('organization.id_city', $id_city);
		if(isset($_GET["id_org"]) && !empty($_GET["id_org"]))
			$this->db->where('organization.id', $_GET["id_org"]);
		$this->db->join('log_type', 'log_type.id = user_log.id_type');
		$this->db->join('user', 'user.id = user_log.id_user');
		$this->db->join('organization', 'organization.id = user.id_org');
		$this->db->order_by('org_name ASC, date DESC');
		$logs = $this->db->get()->result_array();
		
		$fp = fopen('file.csv', 'w');
		$statistic = array(); 
		fputcsv($fp, array('Отчет по действиям пользователей за период '.$dtStart.' по '.$dtEnd), ';');
		fputcsv($fp, array('Организация','Пользователь','email пользователя','Уровень','Действие', 'Дата'), ';');
		foreach ($logs as $fields) {
			if(empty($fields['user_name']))
				$fields['user_name'] = $fields['login'];

			if(!isset($statistic[$fields['org_name']]))
				$statistic[$fields['org_name']] = array();
			if(!isset($statistic[$fields['org_name']][$fields['user_name']]))
				$statistic[$fields['org_name']][$fields['user_name']] = 0;
			$statistic[$fields['org_name']][$fields['user_name']]++;
			fputcsv($fp, $fields, ';');
		}
		var_dump($statistic);

		fputcsv($fp, array(''), ';');
		fputcsv($fp, array(''), ';');

		fputcsv($fp, array('Организация','Пользователь','Количество действий за период'), ';');
		foreach($statistic as $org_name => $users){
			fputcsv($fp, array($org_name), ';');
			foreach($users as $user_name => $actionCount){
				fputcsv($fp, array('',$user_name, $actionCount), ';');
			}
		}
		fclose($fp);
		echo '<a href="/file.csv" download>Скачать</a>';
		/*echo 'Организация;Пользователь;Вид действия;Действие;Дата;';
		foreach($logs as $val){
			echo $val['org_name'].';'.$val['user_name'].(' ('.$val['login'].' - '.$val['user_id'].')').';'.$val['type_name'].';'.$val['action'].';'.$val[''].';'.$val[''].';'.;
		}*/
	}
}
