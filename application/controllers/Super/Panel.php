<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /', true, 301 );
			die('Вы не авторизованы');
		}
	}

	public function index(){
		$data['title'] =  "Панель администрирования";
		$data['styles'] =  array("/assets/css/admin_panel.css");
		$this->load->view('1_header_new',$data);			
		$this->load->view('admin\admin_panel');
		$this->load->view('1_footer_new');		
	}	
	
	public function GetCities($isNeedPrint = 1){
		$cities = array();
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if ($status) { 
			// Дернем API
			$url = API_SERVER_URL.'api/cities';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
			));

			$response = curl_exec($myCurl);
			curl_close($myCurl);
			$json = json_decode(json_decode($response, true), true);
			if($json["status-type"] != "OK"){
				die(json_encode($json));
			}
			$cities = $json["cities"];
			if($isNeedPrint == 1)
				echo json_encode($cities);
		}
		else
			echo 'API не доступен';
		return $cities;
	}
}
