<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsLoginedAsSuperAdmin())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
	}

	public function index(){
		$user = $this->user_log_model->log( "SUPER sendEmail", 3);
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		} 
		if(isset($_POST) && !empty($_POST)){
			$user_email = $_POST["email_to"];
			$caption = $_POST["caption"];
			$content = $_POST["content"];
			$url = API_SERVER_URL.'api/email';
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => http_build_query(array('EmailTo'=>$user_email,
				 'Caption'=>$caption,
				 'Content'=>$content))
			));
	
			$response = curl_exec($myCurl);
			curl_close($myCurl);
			echo $response;
		}else{
			$data['title'] =  "Панель администрирования";
			$data['scripts'] =  array("/assets/js/sortable_table.js");
			$data['styles'] =  array("/assets/css/admin_panel.css");
			$this->load->view('1_header_new',$data);			
			$this->load->view('admin\send_email');
			$this->load->view('1_footer_new');
		}
	}		
}
