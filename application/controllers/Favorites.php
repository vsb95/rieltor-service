<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favorites extends CI_Controller {	
	function __construct() {
		parent::__construct();
		if(!$this->user_model->IsGranted(1) && !$this->user_model->IsGranted(2))
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не подключили соответствующий пакет');
		}
		$this->load->model('favorites_model');
	}

	public function index()
	{					
		if(!$this->user_model->IsLogined()){			
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die;
		}else{
			
			$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
			$data = array(
				'title' => 'Избранное',
				"isServerOnline" => $status
			);
			$data['scripts'] =  array("/assets/js/fotorama.js",'https://api-maps.yandex.ru/2.1/?lang=ru_RU');
			$data['styles'] =  array("/assets/css/fotorama.css","/assets/css/board.css");
			$this->load->view('1_header_new',$data);
			$this->load->view('favorites',$data);
			$this->load->view('1_footer_new',$data);
			$this->user_log_model->log( "open favorites", 1);
		}
	}

	public function Get()
	{					
		if(!$this->user_model->IsLogined()){			
			echo 'Вы не авторизованы';
			die;
		}
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) { 
			die('Сервер не доступен. Повторите еще раз');
		}
		$id_user = $this->session->userdata('id_user');
		
		$this->db->select('id_adv');
		$this->db->from('favorites');
		if(isset($_POST['deal_type']) && !empty($_POST['deal_type']))
			$this->db->where('deal_type', $_POST['deal_type']);
		if(isset($_POST['is_mls']) && !empty($_POST['is_mls']))
			$this->db->where('is_mls', $_POST['is_mls']);
		$this->db->where('id_user', $id_user);
		$this->db->limit(100);
		$this->db->order_by('add_date', 'DESC');
		$id_adv_array= $this->db->get()->result_array();
		if(count($id_adv_array) == 0)
			die('У вас нет в избранном объявлений по текущему фильтру');

		$idAdvArray = array();
		foreach($id_adv_array as $key => $value){			
			$idAdvArray[] = $value['id_adv'];
		}
		//
		$myCurl = curl_init();		
		$url = API_SERVER_URL.'api/pages';
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => http_build_query(array('IdList'=>$idAdvArray, 'userid'=>$id_user))
		));		

		$response = curl_exec($myCurl);
		curl_close($myCurl);
		$json = json_decode(json_decode($response, true), true);
		if($json["status-type"] != "OK"){
			die(json_encode($json));
		}
		if(isset($json["pages"]) && !empty($json["pages"])){
			foreach($idAdvArray as $idAdv){
				foreach($json["pages"] as $page){
					if($page['id'] != $idAdv)
						continue; // сохраняем порядок 
					$json["html-pages"][]= $this->load->view('board_list_item', array("page"=>$page, 'isForFavorite'=>true), true);
				}			
			}
		}
		echo json_encode($json);		
		$this->user_log_model->log( "get favorites", 1);
	}

	public function Set()
	{					
		if(!$this->user_model->IsLogined()){			
			echo 'Вы не авторизованы';
			die;
		}
		if(!isset($_POST['id']) || empty($_POST['id'])){
			die('Не указан id объявления');
		}
		$id_user = $this->session->userdata('id_user');
		
		echo $this->favorites_model->Set($_POST['id'],$_POST['deal_type'],$_POST['is_mls'],$_POST['obj_type']);
		$this->user_log_model->log( "set favorites", 1);
	}

	public function Delete()
	{					
		if(!$this->user_model->IsLogined()){			
			echo 'Вы не авторизованы';
			die;
		}
		if(!isset($_POST['id']) || empty($_POST['id'])){
			die('Не указан id объявления');
		}
		$id_user = $this->session->userdata('id_user');
		
		$this->db->where('id_adv',$_POST['id']);
		$this->db->where('id_user', $id_user);
		$this->db->delete('favorites');
		$this->user_log_model->log( "delete favorites", 1);
		echo 'deleted';
	}

	public function DeleteAll()
	{					
		if(!$this->user_model->IsLogined()){			
			echo 'Вы не авторизованы';
			die;
		}

		$this->db->where('id_user', $this->session->userdata('id_user'));
		if(isset($_POST['deal_type']) && !empty($_POST['deal_type']))
			$this->db->where('deal_type', $_POST['deal_type']);					
		$this->db->delete('favorites');
		echo 'deleted';
	}
}
