<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {	
	public function index()
	{					
		if($this->user_model->IsLogined()){			
			header( 'Location: /home', true, 301 );
		}else{
			$data['title'] =  "Вход";
			if(isset($_GET['request']) && !empty($_GET['request'])){
				$data['request'] = $_GET['request'];
			}
			$this->load->view('auth',$data);
		}
	}

	public function login() {
		/*
		в сессию устанавливаются:
		 - id_user
		 - is_admin
		 - org_logo
		 - org_url
		 - id_city
		 - name_city
		 - id_org

		в куку ставится token
		*/
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if (!isset($username) || empty($username) || !isset($password) || empty($password))		
			die("Не указан логин или пароль");
		

		$hash = hash('md5', $password);
		$res       = $this->db->query("SELECT * FROM user WHERE login = ? OR email = ?;", array($username,$username));
        $data      = $res->result_array();
        if (count($data) == 0 || $data[0]['password'] != $hash) 
			die("Не верно указан логин или пароль");
					
		$user = $data[0];

		if($user["is_deleted"] == 1)
			die("К сожалению, вам заблокирован доступ в систему: вы более не состоите в вашей организации");

		//var_dump($user);
		$this->session->set_userdata('id_user', $user['id']);
		$this->session->set_userdata('is_admin', $user['is_admin']);	
		$this->session->set_userdata('id_grant', $user['id_grant']);	
		if(isset($user['photo']) && !empty($user['photo']))	
			$this->session->set_userdata('user_photo', $user['photo']);	
				
		$res       = $this->db->query("SELECT * FROM organization WHERE id = ?;", array($user['id_org']));
        $data      = $res->result_array();
        if (count($data) == 0) 
			die("Не определена организация, которой принадлежит сотрудник");
		$org = $data[0];
		$this->session->set_userdata('id_org', $org['id']);
		$this->session->set_userdata('org_logo', $org['logo_name']);
		$this->session->set_userdata('org_url', $org['url']);		
		$this->session->set_userdata('id_city', $org['id_city']);	
		//
		if(isset($org['id_city'])){
			$url = API_SERVER_URL.'api/cities?id='.$org['id_city'];
			$myCurl = curl_init();
			curl_setopt_array($myCurl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true
			));
			$response = curl_exec($myCurl);
			curl_close($myCurl);
			$json = json_decode($response, true);
			if($json['status-type'] == "OK"){
				$this->session->set_userdata('name_city', $json['status']);	
			}
		}
		//
				
		$this->user_log_model->log( "login", 3);	

		$token = $this->user_model->CreateToken();
		$this->load->helper('cookie');
		set_cookie("token", $token, 86400);  /*Обновим куку - срок действия 1 сутки */
		if($user["is_activate"] == 0){
			$this->user_log_model->log( "activate", 3);	
			$this->db->set('is_activate', 1);
			$this->db->set('token', $token);
			$this->db->where('id', $user['id']);
			$this->db->update('user');
		}else{
			$this->db->set('token', $token);
			$this->db->where('id', $user['id']);
			$this->db->update('user');			
		}
		echo "OK";	
		//header( 'Location: /home', true, 301 ); 	
		//echo "Сомнительная ошибка при аутентификации: ";
		//var_dump($_SESSION);
	}

	public function logout() {
		$this->load->helper('cookie');
		delete_cookie("token"); 
		
		$user = $this->user_log_model->log("logout",3);
		$this->session->unset_userdata('id_user');
		header( 'Location: /', true, 301 );
	}

	public function forgive(){
		if(!isset($_POST["email"]) || empty($_POST["email"]))
			die("Не указан email");
		$email = $_POST["email"];
		$res       = $this->db->query("SELECT * FROM user WHERE login = ? OR email = ?;", array($email, $email));
        $users      = $res->result_array();
        if (count($users) == 0) 
			die("Пользователь не найден");
		
		$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
		if (!$status) 
			die('Сервер не доступен. Повторите еще раз');
		
		$this->load->model('randomizer');
		$this->load->model('user_model');
		$this->load->model('mailer');
		$newPsw = $this->randomizer->generateRandomString(6);
		if(!$this->user_model->ChangePassword($email, $newPsw))
			die("Не удалось сменить пароль");
		
		$email_html = $this->load->view('email',
			array(
				'title'=>'Пароль успешно сменен!',
				'content'=>'Ваш новый пароль: '.$newPsw,
				'user_name'=> isset($users[0]["full_name"]) ?$users[0]["full_name"]:null
			), TRUE);
		echo $this->mailer->sendEmail($email, "Пароль успешно сменен!", $email_html);
	}

	public function register() {
			print_r($_GET);
		if (!isset($_GET['usernamesignup']) || empty($_GET['usernamesignup']) 
			|| !isset($_GET['emailsignup']) || empty($_GET['emailsignup'])
			|| !isset($_GET['passwordsignup']) || empty($_GET['passwordsignup']))
		{
			throw new Exception("Не указан логин или пароль");
		}
		if (!isset($_GET['orgname']) || empty($_GET['orgname']))
		{
			throw new Exception("Не указан логин или пароль");
		}
		if (!isset($_GET['orgname']) || empty($_GET['orgname']))
		{
			throw new Exception("Не указан логин или пароль");
		}
		if ($_GET['passwordsignup'] != $_GET['passwordsignup_confirm'])
		{
			throw new Exception("Пароли не совпадают");
		}
		$this->load->model('user_model'); // загрузка модели
		$hash = $_GET['passwordsignup'];

		$user = $this->user_model->authAndGetUser($_GET['usernamesignup'],$hash);
			
		
		echo "Сомнительная ошибка при аутентификации: ";
		print_r($user);
	}
}
