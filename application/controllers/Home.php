<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index()
	{		
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		else{
			$data['title'] =  "Блог";
			$data['styles'] =  array("/assets/css/home.css");
			$this->load->view('1_header_new',$data);
			$this->load->view('home');
			$this->load->view('1_footer_new');
		}
	}
	
	public function help()
	{		
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		else{
			$videos[] = array(
				'url'=>'https://www.youtube.com/embed/PT2DHYwnJYE',
				'title' => 'Отчет из ссылок',
				'description' => 'Посмотрите видео, как формировать отчет из ссылок, с сайта-источника',
				'anchor' => 'url_report'
			);
			$videos[] = array(
				'url'=>'https://www.youtube.com/embed/-4egA2HH34g',
				'title' => 'Отчет из поиска',
				'description' => 'Посмотрите видео, как формировать отчет из поиска',
				'anchor' => 'search_report'
			); 		
			$videos[] = array(
				'url'=>'https://www.youtube.com/embed/lZChVCRztlg',
				'title' => 'Обновление отчетов',
				'description' => 'Посмотрите видео, как обновлять отчеты по фильтру или по дате',
				'anchor' => 'report_refresh'
			); 	


			$this->load->view('help', array('videos'=>$videos));
		}
	}

	public function terms()
	{		
		$this->tariffs();
		return;
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		else{
			$this->load->view('terms');
		}
	}

	public function tariffs(){		
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		else{
			$data['title'] =  "Тарифы";
			$this->load->view('1_header_new',$data);
			$this->load->view('tariffs');
			$this->load->view('1_footer_new');
		}
	}
	public function about()
	{		
		if(!$this->user_model->IsLogined())
		{
			header( 'Location: /?request='.$_SERVER['REQUEST_URI'], true, 301 );
			die('Вы не авторизованы');
		}
		else{			
			$json = array();
			
			$status = @fsockopen(API_SERVER_DOMAIN, API_SERVER_PORT, $errno, $errstr, 5); 
			if ($status) { 
				// Дернем API
				$myCurl = curl_init();
				curl_setopt_array($myCurl, array(
					CURLOPT_URL => API_SERVER_URL.'api/report?type=count-offer-per-org',
					CURLOPT_RETURNTRANSFER => true,
				));

				$response = curl_exec($myCurl);
				curl_close($myCurl);
				$json = json_decode($response, true);
				if($json['status-type'] != "OK"){
					die($response);
				}
				if(isset($json['rows']) && count($json['rows']) > 0 && isset($json['rows'][0]['org-id'])){
					$res = $this->db->query("SELECT id, name, logo_name FROM organization WHERE is_test = 0 ORDER BY name",array());
					$orgs = $res->result_array();
					
					// не эффективно, зато сотрировать не придется
					if(isset($json['rows']) && count($json['rows']) > 0 && isset($json['rows'][0]['org-id'])){
						foreach($json['rows'] as &$item){
							$item['org'] = $this->user_model->GetUserOrg($item['org-id']);
						}
						foreach($orgs as $org){
							$isFound = false;
							foreach($json['rows'] as &$item){
								if($org['id'] == $item['org-id']){
									$isFound = true;
									break;
								}
							}
							if(!$isFound) $json['rows'][] = array('org-id' => $org['id'], 'offer-count'=> 0, 'org' => $org);
						}
					} 

				} 
			}
			$this->load->view('about',array('reportCountPerOrg'=> $json));
		}
	}
}
